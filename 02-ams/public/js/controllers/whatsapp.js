let gui = {
  mphone: (phone) => {
    phone = phone.replace(/\D/g,'');
    phone = phone.replace(/^(\d)/,'+$1');
    phone = phone.replace(/(.{3})(\d)/,'$1($2');
    phone = phone.replace(/(.{6})(\d)/,'$1)$2');
    //
    if (phone.length == 12) phone=phone.replace(/(.{1})$/,'-$1');
    else if (phone.length == 13) phone = phone.replace(/(.{2})$/,'-$1');
    else if (phone.length == 14) phone = phone.replace(/(.{3})$/,'-$1');
    else if (phone.length == 15) phone = phone.replace(/(.{4})$/,'-$1');
    else if (phone.length > 15) phone = phone.replace(/(.{4})$/,'-$1');
    //
    return phone.replace('(', ' (').replace(')', ') ');
  }
}

let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/whatsapp',
            map: (raw) => {
              raw.forEach(item => {
                item.last_timestamp = moment.unix(parseInt(item.last_timestamp)).format('DD/MM/YYYY HH:mm:ss');
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
          field: 'projeto_id',
          title: '#',
          sortable: false,
          width: 20,
          type: 'number',
          selector: {
            class: ''
          },
          textAlign: 'center'
        }, {
          field: 'projeto_nome',
          title: 'Projeto',
          width: 250,
          template: (row) => {
            return row.projeto_nome + '<br><small>' + row.cliente_nome + '</small>';
          }
        }, {
          field: 'last_timestamp',
          title: 'Detalhes',
          width: 400,
          template: (row) => {
            return gui.mphone(row.sender.numero) + '<br><small>Integração criada em ' + row.last_timestamp + '.</small>';
          }
        }, {
          field: 'status',
          title: 'Status',
          textAlign: 'center',
          width: 60,
          template: (row) => {
            if (row.status === 'true') return '<span class="label font-weight-bold label-lg label-light-success label-inline w-100">Ativa</span>';
            else return '<span class="label font-weight-bold label-lg label-light-danger label-inline w-100">Inativa</span>';
          }
        }, {
          field: 'Actions',
          title: '',
          sortable: false,
          width: 50,
          autoHide: false,
          overflow: 'visible',
          template: (row) => {
            return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover py-2">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">\
                            ESCOLHA UMA OPÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-debug" data-id="' + row.projeto_id + '">\
                                <span class="navi-icon"><i class="la la-bug"></i></span>\
                                <span class="navi-text">Testar</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-update" data-id="' + row.projeto_id + '" data-label="' + (row.status === 'true' ? 'desabilita' : 'habilita') + '">\
                                <span class="navi-icon"><i class="la la-power-off"></i></span>\
                                <span class="navi-text">' + (row.status === 'true' ? 'Desabilitar' : 'Habilitar') + '</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-delete" data-id="' + row.projeto_id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>\
                    </ul>\
                </div>\
            </div>';
          }
        }
      ],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let api = {
  create: async () => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR DADOS DE CLIENTES E PROJETOS
      let clientes = (await axios.get(API_URL + '/clientes')).data;
      // MONTAR LISTA DE PROJETOS PARA SELEÇÃO
      let projetos = {};
      clientes.forEach(cliente => {
        projetos[cliente.nome] = {};
        cliente.projetos.forEach(projeto => {
          let integracoes = JSON.parse(projeto.integracoes);
          if (!(integracoes && integracoes.whatsapp)) projetos[cliente.nome][projeto.id] = projeto.nome;
        });
        if ($.isEmptyObject(projetos[cliente.nome])) delete projetos[cliente.nome];
      });
      // MONTAR LISTA DE INTÂNCIAS DO WHATSAPP
      let senders = '<select id="sender-id-id" class="swal2-select"><option value="">Selecionar</option>' + (await axios.get(API_URL + '/whatsapp/senders')).data.map(sender => '<option value="' + sender.id +'">' + sender.nome + ' | ' + gui.mphone(sender.numero) + '</option>') + '</select>';
      // SOLICITAR AO USUÁRIO QUAL PROJETO UTILIZAR
      swal.close();
      let projeto = await swal.fire({
        icon: 'question',
        title: 'Vamos lá!',
        html: 'Qual número será utilizado?' + senders + 'Em qual projeto associar a integração?',
        input: 'select',
        inputOptions: projetos,
        inputPlaceholder: 'Selecionar',
        didOpen: () => $('#sender-id-id').focus(),
        preConfirm: (value) => {
          return [
            // URL DA LOJA
            $('#sender-id-id').val(),
            // ID DO PROJETO
            value
          ]
        },
        inputValidator: (value) => {
          return new Promise((resolve) => {
            // SENDER ID
            if (value && $('#sender-id-id').val()) resolve()
            else {
              if (!$('#sender-id-id').val()) {
                setTimeout(() => $('#sender-id-id').focus() , 10);
                resolve('Você precisa selecionar um número.');
              }
              if (!value) resolve('Você precisa selecionar um projeto.');
            }
          });
        },
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        // footer: 'Ao continuar será exibido um QR Code. Certifique-se de ter acesso ao telefone.',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0',
          footer: 'text-center'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = projeto.value[1];
        let sender_id = projeto.value[0];   
        //
        let whatsapp = (await axios.post(API_URL + '/whatsapp/login/' + projeto_id, {
          sender_id: sender_id
        })).data;
        // DEU BOM
        datatable.reload();
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi adicionada com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  update: async (id, label) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo ' + label + 'r esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = id;
        await axios.put(API_URL + '/whatsapp/' + projeto_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi ' + label + 'da com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = id;
        await axios.delete(API_URL + '/whatsapp/' + projeto_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi excluída com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

let custom = {
  debug: async(id) => {
    try {
      // PERGUNTAR O TELEFONE
      let temp = await swal.fire({
        icon: 'question',
        title: 'Vamos lá!',
        text: 'Para qual número enviar a mensagem?',
        input: 'text',
        inputPlaceholder: '(62) 99503-0938',
        didOpen: () => {
          let SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          };
          let spOptions = {
            onKeyPress: function(val, e, field, options) {
              field.mask(SPMaskBehavior.apply({}, arguments), options);
            },
            clearIfNotMatch: true
          };
          $('.phone').mask(SPMaskBehavior, spOptions);
        },
        inputValidator: (value) => {
          return new Promise((resolve) => {
            if (value) resolve();
            else resolve('Você precisa inserir um número.');
          });
        },
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Prosseguir',
        customClass: {
          input: 'phone',
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      if (temp.value) {
        let phone = temp.value.replace(/\D/g, '');
        if (phone.length == 11) phone = phone.slice(0, 2) + phone.slice(3, phone.length);
        phone = '55' + phone;
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = id;
        let debug = (await axios.post(API_URL + '/whatsapp/debug/' + projeto_id, {
          telefone: phone,
          mensagem: 'Esta é uma mensagem de teste do Kampana Hub. 😊'
        })).data;
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          html: '<code>' + JSON.stringify(debug) + '</code>',
          width: '90%'
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create', function() {
    api.create();
  });
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-id'), $(this).attr('data-label'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete($(this).attr('data-id'));
  });
  // CONFIGURAR CUSTOM
  $(document).on('click', '.btn-debug', function() {
    custom.debug($(this).attr('data-id'));
  });
});