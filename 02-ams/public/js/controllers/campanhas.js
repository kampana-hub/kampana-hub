let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/campanhas',
            map: (raw) => {
              raw.forEach(item => {
                item.id_formatted = item.id;
                item.agendamento_formatted = item.agendamento ? moment(item.agendamento).format('DD/MM/YYYY HH:mm') : '';
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [/*{
          field: 'id_formatted',
          title: '#',
          sortable: false,
          width: 20,
          type: 'number',
          selector: {
            class: ''
          },
          textAlign: 'center'
        }, */{
          field: 'id',
          title: '#',
          autoHide: false,
          width: 30
        }, {
          field: 'projeto_nome',
          title: 'Projeto',
          width: 200,
          template: (row) => {
            return row.projeto_nome + '<br><small>' + row.cliente_nome + '</small>';
          }
        }, {
          field: 'nome',
          title: 'Nome',
          autoHide: false,
          width: 150,
          template: (row) => {
            if (row.descricao) return '<span class="ellipsis">' + row.nome + '<br><small>' + row.descricao + '</small></span>';
            else return '<span class="ellipsis">' + row.nome + '</span>';
          }
        }, {
          field: 'agendamento',
          title: 'Agendamento',
          width: 125,
          template: (row) => {
            return row.agendamento_formatted;
          }
        }, {
          field: 'status_nome',
          title: 'Status',
          textAlign: 'center',
          width: 125,
          template: (row) => {
            let label = '';
            switch (row.status_id) {
              case 1:
                // RASCUNHO
                label = 'light';
                break;
              case 2:
                // AGENDADA
                label = 'light-info';
                break;
              case 3:
                // EM ANDAMENTO
                label = 'light-warning';
                break;
              case 4:
                // PAUSADA
                label = 'light-danger';
                break;
              case 5:
                // COM ERRO
                label = 'danger';
                break;
              case 6:
                // CONCLUÍDA
                label = 'light-success';
                break;
              case 7:
                // CANCELADA
                label = 'light';
                break;
            }
            return '<span class="label font-weight-bold label-lg label-' + label + ' label-inline w-100">' + row.status_nome + '</span>';
          }
        }, {
          field: 'Actions',
          title: '',
          autoHide: false,
          sortable: false,
          width: 50,
          overflow: 'visible',
          template: (row) => {
            return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">\
                            CONFIGURAÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-update" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-' + ([1].includes(row.status_id) ? 'pencil' : 'eye') + '"></i></span>\
                                <span class="navi-text">' + ([1].includes(row.status_id) ? 'Editar' : 'Visualizar') + '</span>\
                            </span>\
                        </li>' + ([1].includes(row.status_id) ?
                        '<li class="navi-item">\
                            <span class="navi-link btn-delete" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>' : '') + ([2, 3, 4, 5, 6, 7].includes(row.status_id) ?
                        '<li class="navi-separator my-2"></li>\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">\
                            OPERAÇÃO\
                        </li>' + ([3, 4, 5].includes(row.status_id) ?
                        '<li class="navi-item">\
                            <span class="navi-link btn-roll" data-id="' + row.id + '" data-status="' + (row.status_id == 3 ? 4 : 3) + '">\
                                <span class="navi-icon"><i class="la la-power-off"></i></span>\
                                <span class="navi-text">' + (row.status_id == 3 ? 'Suspender' : 'Resumir') + '</span>\
                            </span>\
                        </li>' : '') + ([2, 4, 5].includes(row.status_id) ?
                        '<li class="navi-item">\
                            <span class="navi-link btn-roll" data-id="' + row.id + '" data-status="7">\
                                <span class="navi-icon"><i class="la la-times"></i></span>\
                                <span class="navi-text">Cancelar</span>\
                            </span>\
                        </li>' : '') +
                        '<li class="navi-item">\
                            <span class="navi-link btn-queue" data-id="' + row.id + '" data-label="' + row.nome + '">\
                                <span class="navi-icon"><i class="la la-users"></i></span>\
                                <span class="navi-text">Fila de Envio</span>\
                            </span>\
                        </li>' : '') +
                        '<li class="navi-item">\
                            <span class="navi-link btn-log" data-id="' + row.id + '" data-label="' + row.nome + '">\
                                <span class="navi-icon"><i class="la la-binoculars"></i></span>\
                                <span class="navi-text">Histórico</span>\
                            </span>\
                        </li>\
                    </ul>\
                </div>\
            </div>';
          }
        }
      ],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let tagify;
let tagify_input = document.getElementById('campanha-tags');
let autoupdate;

let gui = {
  init: async () => {
    // CARREGAR DADOS DE CLIENTES E PROJETOS
    let clientes = (await axios.get(API_URL + '/clientes')).data;
    // MONTAR LISTA DE PROJETOS PARA SELEÇÃO
    let projetos = {};
    clientes.forEach(cliente => {
      projetos[cliente.nome] = {};
      cliente.projetos.forEach(projeto => {
        let integracoes = JSON.parse(projeto.integracoes);
        if (integracoes && integracoes.rdstation) projetos[cliente.nome][projeto.id] = projeto.nome;
      });
      if ($.isEmptyObject(projetos[cliente.nome])) delete projetos[cliente.nome];
    });
    // INICIALIZAR SELELCT2 DOS PROJETOS
    Object.keys(projetos).forEach((cliente) => $('#campanha-projeto').append('<optgroup label="' + cliente + '">' + Object.entries(projetos[cliente]).map((projeto) => '<option value="' + projeto.shift() + '">' + projeto.pop() + '</option>') + '</optgroup>'));
    $('#campanha-projeto').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    // INICIALIZAR DATE TIME PICKER
    $('#campanha-agendamento').datetimepicker({
      locale: 'pt-br',
      minDate: moment() //.add(1, 'hour')
    });
    // INICIALIZAR TAGS
    $('#campanha-projeto').on('change', async function() {
      // LIMPAR COMPONENTE
      if (tagify) {
        tagify.destroy();
        tagify = null;
      }
      $('#campanha-tags').val('').prop('disabled', true);
      // SE TIVER ID POPULAR NOVAMENTE
      if ($(this).val()) {
        let tags = (await axios.get(API_URL + '/leads/tags/' + $(this).val())).data;
        tagify = new Tagify(tagify_input, {
          enforceWhitelist: true,
          whitelist: tags
        });
        $('#campanha-tags').prop('disabled', false);
      }
    });
    // INICIALIZAR O CLICK & ADD DAS VARIÁVEIS
    $('.campanha-variavel').on('click', function() {
      let mensagem = $('#campanha-mensagem').val();
      let variavel = $(this).html();
      $('#campanha-mensagem').val(mensagem + variavel);
      $('#campanha-mensagem').focus();
    });
    // MONITORAR O AUTOUPDATE
    $('#autoupdate').on('change', function() {
      if ($(this).prop('checked')) autoupdate = setInterval(() => datatable.reload(), 5000);
      else clearInterval(autoupdate);
    });
    // MONITORAR O UPLOAD
    $('#campanha-anexo').on('change', function() {
      if ($(this).val()) {
        $(this).prop('disabled', true);
        $('#campanha-anexo-label').html($(this).val().split('\\').pop());
        $('.btn-attachment').prop('disabled', false);
      }
      else {
        $(this).prop('disabled', false);
        $('.btn-attachment').prop('disabled', true);
        $('#campanha-anexo-label').html('Selecionar')
        $('#campanha-anexo-extra').html('');
      }
    });
    $('.btn-attachment').on('click', function() {
      $('#campanha-anexo').val('');
      $('#campanha-anexo').trigger('change');
    });
  },
  mphone: (phone) => {
    phone = phone.replace(/\D/g,'');
    phone = phone.replace(/^(\d)/,'+$1');
    phone = phone.replace(/(.{3})(\d)/,'$1($2');
    phone = phone.replace(/(.{6})(\d)/,'$1)$2');
    //
    if (phone.length == 12) phone=phone.replace(/(.{1})$/,'-$1');
    else if (phone.length == 13) phone = phone.replace(/(.{2})$/,'-$1');
    else if (phone.length == 14) phone = phone.replace(/(.{3})$/,'-$1');
    else if (phone.length == 15) phone = phone.replace(/(.{4})$/,'-$1');
    else if (phone.length > 15) phone = phone.replace(/(.{4})$/,'-$1');
    //
    return phone.replace('(', ' (').replace(')', ') ');
  }
}

let api = {
  create: () => {
    try {
      // REDEFINIR FORMULÁRIO
      $('#form :input').prop('disabled', false);
      $('#campanha-tags').prop('readOnly', false);
      $('.btn-save, .btn-debug').prop('disabled', false);
      $('#form').trigger('reset');
      $('#campanha-projeto').trigger('change');
      $('#campanha-anexo').trigger('change');
      $('#campanha-anexo-extra').html('');
      $('#form').validate();
      // GERENCIAR CRIAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        // VALIDAR FORMULÁRIO
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo adicionar esta campanha?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let agendamento = $('#campanha-agendamento').find('input').val();
              //
              let dia  = agendamento.substring(0, 2);
              let mes  = agendamento.substring(3, 5);
              let ano  = agendamento.substring(6, 10);
              let hora = agendamento.substring(11, 16);
              //
              let campanha = {
                projeto_id: $('#campanha-projeto').val(),
                nome: $('#campanha-nome').val(),
                descricao: $('#campanha-descricao').val(),
                mensagem: $('#campanha-mensagem').val(),
                agendamento: ano + '-' + mes + '-' + dia + ' ' + hora,
                tags: $('#campanha-tags').val() ? JSON.stringify(JSON.parse($('#campanha-tags').val()).map(tag => tag.value)) : [],
                status_id: $('#campanha-status').prop('checked') ? 2 : 1
              }
              //
              let formData = new FormData();
              // DADOS
              Object.keys(campanha).forEach(function(key) {
                formData.append(key, campanha[key]);
              });
              // ANEXO
              if ($('#campanha-anexo').prop('files').length > 0) formData.append('file', $('#campanha-anexo').prop('files')[0]);
              // CHAMADA
              await axios.post(API_URL + '/campanhas/', formData, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
              });
              //
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'A campanha foi adicionada com sucesso.',
              });
            }
            //
            catch (e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      $('#modal-title').html('Adicionar Campanha');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  update: async (id) => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR DADOS DO CAMPANHA
      let campanha = (await axios.get(API_URL + '/campanhas/' + id)).data;
      let extra = JSON.parse(campanha.extra) || {};
      // REDEFINIR FORMULÁRIO
      $('#form :input').prop('disabled', false);
      $('#campanha-tags').prop('readOnly', false);
      $('.btn-save, .btn-debug').prop('disabled', false);
      $('#form').trigger('reset');
      $('#form').validate();
      // INSERIR DADOS DO CAMPANHA NO FORMULÁRIO
      $('#campanha-projeto').val(campanha.projeto_id).trigger('change');
      $('#campanha-nome').val(campanha.nome);
      $('#campanha-descricao').val(campanha.descricao);
      $('#campanha-mensagem').val(campanha.mensagem);
      $('#campanha-agendamento').find('input').val(moment(campanha.agendamento).format('DD/MM/YYYY HH:mm'));
      // AJUSTAR AS TAGS
      let tags = JSON.parse(campanha.tags);
      if (tags.length > 0) {
        setTimeout(() => {
          tagify.addTags(JSON.parse(campanha.tags));
        }, 500);
      }      
      // AJUSTAR STATUS
      $('#campanha-status').prop('checked', campanha.status_id >= 2 ? true : false);
      // AJUSTAR ANEXO
      if (extra.attachment) {
        $('#campanha-anexo').prop('disabled', true);
        $('.btn-attachment').prop('disabled', false);
        $('#campanha-anexo-label').html(extra.attachment.split('/').pop());
        $('#campanha-anexo-extra').html('Clique <a href="' + AWS_S3_URL + extra.attachment + '" target="_blank">aqui</a> para visualizar o anexo.');
      }
      else {
        $('#campanha-anexo').prop('disabled', false);
        $('.btn-attachment').prop('disabled', true);
        $('#campanha-anexo-extra').html('');
      }
      // GERENCIAR ATUALIZAÇÃO DO REGISTRO
      $('.btn-save').off();
      if (campanha.status_id < 2) {
        $('.btn-save').on('click', async function() {
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo atualizar esta campanha?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              let agendamento = $('#campanha-agendamento').find('input').val();
              //
              let dia  = agendamento.substring(0, 2);
              let mes  = agendamento.substring(3, 5);
              let ano  = agendamento.substring(6, 10);
              let hora = agendamento.substring(11, 16);
              //
              if ($('.btn-attachment').prop('disabled')) delete extra.attachment;
              //
              let campanha = {
                projeto_id: $('#campanha-projeto').val(),
                nome: $('#campanha-nome').val(),
                descricao: $('#campanha-descricao').val(),
                mensagem: $('#campanha-mensagem').val(),
                agendamento: ano + '-' + mes + '-' + dia + ' ' + hora,
                tags: $('#campanha-tags').val() ? JSON.stringify(JSON.parse($('#campanha-tags').val()).map(tag => tag.value)) : [],
                status_id: $('#campanha-status').prop('checked') ? 2 : 1,
                extra: JSON.stringify(extra)
              }
              //
              let formData = new FormData();
              // DADOS
              Object.keys(campanha).forEach(function(key) {
                formData.append(key, campanha[key]);
              });
              // ANEXO
              if ($('#campanha-anexo').prop('files').length > 0) formData.append('file', $('#campanha-anexo').prop('files')[0]);
              // CHAMADA
              await axios.patch(API_URL + '/campanhas/' + id, formData, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
              });
              //
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'A campanha foi atualizada com sucesso.',
              });
            }
            //
            catch(e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      }
      //
      else {
        $('#form :input').prop('disabled', true);
        $('#campanha-tags').prop('readOnly', true);
        $('.btn-save, .btn-debug').prop('disabled', true);
      }
      // EXIBIR MODAL
      swal.close();
      $('#modal-title').html('Editar Campanha');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let campanha = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir esta campanha?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (campanha.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let campanha_id = id;
        await axios.delete(API_URL + '/campanhas/' + campanha_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A campanha foi excluída com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

let custom = {
  debug: async() => {
    if ($('#form').valid()) {
      try {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // CARREGAR DADOS DE CLIENTES E PROJETOS
        let leads = (await axios.get(API_URL + '/leads/' + $('#campanha-projeto').val())).data;
        // MONTAR LISTA DE PROJETOS PARA SELEÇÃO
        let inputOptions = {};
        leads.forEach((lead, index) => {
          inputOptions[index] = lead.name;
        });
        // SOLICITAR AO USUÁRIO DADOS DO TESTE
        swal.close();
        let campanha = await Swal.fire({
          icon: 'question',
          title: 'Vamos lá!',
          html: 'Para qual número enviar a mensagem?<input id="swal2-telefone" class="swal2-input form-control" type="text" placeholder="(62) 99503-0938">Para qual lead preencher as variáveis?',
          input: 'select',
          inputOptions: inputOptions,
          inputPlaceholder: 'Selecionar',
          didOpen: () => {
            let SPMaskBehavior = function (val) {
              return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            };
            let spOptions = {
              onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
              },
              clearIfNotMatch: true
            };
            $('#swal2-telefone').mask(SPMaskBehavior, spOptions);
            $('#swal2-telefone').focus();
          },
          preConfirm: (value) => {
            return [
              // TELEFONE
              $('#swal2-telefone').val(),
              // LEAD
              value
            ]
          },
          inputValidator: (value) => {
            return new Promise((resolve) => {
              // TELEFONE
              if (!$('#swal2-telefone').val()) {
                setTimeout(() => $('#swal2-telefone').focus() , 10);
                resolve('Você precisa inserir um número.');
              }
              // LEAD
              else {
                if (value) resolve();
                else resolve('Você precisa selecionar um lead.');
              }
            });
          },
          showCancelButton: true,
          reverseButtons: true,
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Continuar',
          customClass: {
            confirmButton: 'btn btn-primary mt-0',
            cancelButton: 'btn btn-light mt-0'
          }
        });
        // VALIDAR CONTINUAÇÃO
        if (campanha.value) {
          // EXIBIR TELA DE CARREGAMENTO
          swal.fire({
            title: 'Aguarde...',
            text: 'Processando a solicitação.',
            onOpen: () => {
              swal.showLoading();
            },
            allowOutsideClick: false,
            allowEscapeKey: false
          });
          // PREPARAR ENVIO - TELEFONE
          let telefone = campanha.value.shift().replace(/\D/g, '');
          if (telefone.length == 11) telefone = telefone.slice(0, 2) + telefone.slice(3, telefone.length);
          telefone = '55' + telefone;
          // PREPARAR ENVIO - MENSAGEM
          let mensagem = $('#campanha-mensagem').val();
          if (mensagem.includes('{nome_completo}') || mensagem.includes('{primeiro_nome}') || mensagem.includes('{sobrenome}')) {
            let nome = leads[campanha.value.shift()].name.split(' ').map(nome => nome.charAt(0).toUpperCase() + nome.slice(1));
            //
            mensagem = mensagem.replace('{nome_completo}', nome.join(' '));
            mensagem = mensagem.replace('{primeiro_nome}', nome.slice().shift());
            mensagem = mensagem.replace('{sobrenome}', nome.slice(1).join(' '));
          }
          // ENVIAR REQUISIÇÃO PARA O BACK-END
          let debug = (await axios.get(API_URL + '/whatsapp/debug/' + $('#campanha-projeto').val() + '?phone=' + telefone + '&message=' + mensagem)).data;
          // DEU CERTO
          swal.close();
          swal.fire({
            icon: 'success',
            title: 'Excelente!',
            html: '<code>' + JSON.stringify(debug) + '</code>',
            width: '90%'
          });
        }
      }
      //
      catch (e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    }
  },
  log: async(id, label) => {
    $('#modal-log').off();
    // MATAR A TAELA ANTIGA AO FECHAR O MODAL
    $('#modal-log').on('hidden.bs.modal', function() {
      $('#kt_datatable_log').KTDatatable().destroy();
    });
    // CARREGAR A NOVA TABELA APENAS COM O MODAL ABERTO
    $('#modal-log').on('shown.bs.modal', function() {
      // RENDERIZAR A NOVA TABELA
      $('#kt_datatable_log').KTDatatable({
        // CARREGAR E MANIPULAR OS DADOS
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'GET',
              url: API_URL + '/campanhas/logs/' + id,
              map: (raw) => {
                raw.forEach(item => {
                  item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
                });
                return raw;
              }
            }
          }
        },
        // DEFINIR LAYOUT
        layout: {
          scroll: false,
          footer: false
        },
        sortable: true,
        pagination: true,
        // DEFINIR FILTROS
        search: {
          input: $('#kt_datatable_search_query_log'),
          key: 'generalSearch'
        },
        // DEFINIR COLUNAS
        columns: [{
            field: 'id',
            title: '#',
            width: 50
          }, {
            field: 'descricao',
            title: 'Descrição',
            width: 600,
            template: (row) => {
              if (row.user_display_name) return row.descricao +  '<span class="label ml-3" data-toggle="tooltip" data-html="true" title="' + row.user_display_name + '<br>' + row.user_email + '"><i class="icon-sm la la-cog"></i></span>';
              // else if (row.rdstation_event_type == 'ORDER_PLACED') return row.descricao + '<span class="label label-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-shopping-cart text-white"></i></span>';
              // else if (row.rdstation_event_type == 'SALE') return row.descricao + '<span class="label label-success ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-dollar text-white"></i></span>';
              // else if (row.rdstation_lead_id) return row.descricao +  '<span class="label label-light-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-user"></i></span>';
              else return row.descricao;
            }
          }, {
            field: 'origem',
            title: 'Origem',
            width: 75
          }, {
            field: 'tipo',
            title: 'Tipo',
            textAlign: 'center',
            width: 75,
            template: (row) => {
              return '<span class="label font-weight-bold label-lg label-light-' + row.tipo + ' label-inline w-100 text-uppercase">' + row.tipo + '</span>';
            }
          }, {
            field: 'created_at',
            title: 'Data',
            width: 200
          }
        ],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
      });
      // EXIBIR TOOLTIPS
      $('#kt_datatable_log').KTDatatable().on('datatable-on-layout-updated', function() {
        $('[data-toggle="tooltip"]').tooltip();
      });
    });
    // EXIBIR O MODAL
    $('#modal-log-title').html(label);
    $('#modal-log').modal('show');
  },
  queue: async (id, label) => {
    $('#modal-queue').off();
    // MATAR A TAELA ANTIGA AO FECHAR O MODAL
    $('#modal-queue').on('hidden.bs.modal', function() {
      $('#kt_datatable_queue').KTDatatable().destroy();
    });
    // CARREGAR A NOVA TABELA APENAS COM O MODAL ABERTO
    $('#modal-queue').on('shown.bs.modal', function() {
      // RENDERIZAR A NOVA TABELA
      $('#kt_datatable_queue').KTDatatable({
        // CARREGAR E MANIPULAR OS DADOS
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'GET',
              url: API_URL + '/campanhas/filas/' + id,
              map: (raw) => {
                /*raw.forEach(item => {
                  item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
                });*/
                return raw;
              }
            }
          }
        },
        // DEFINIR LAYOUT
        layout: {
          scroll: false,
          footer: false
        },
        sortable: true,
        pagination: true,
        // DEFINIR FILTROS
        search: {
          input: $('#kt_datatable_search_query_queue'),
          key: 'generalSearch'
        },
        // DEFINIR COLUNAS
        columns: [/*{
            field: 'rd_id',
            title: '#',
            width: 65,
            template: (row) => {
              return '<small>' + row.rd_id + '</small>';
            }
          }, */{
            field: 'nome',
            title: 'Nome',
            width: 175,
            template: (row) => {
              return row.nome + '<br><small>' + gui.mphone(row.telefone) + '</small>';
            }
          }, {
            field: 'mensagem',
            title: 'Mensagem',
            width: 400,
            template: (row) => {
              return '<small>' + row.mensagem + '</small>';
            }
          }, {
            field: 'status',
            title: 'Status',
            textAlign: 'center',
            width: 100,
            template: (row) => {
              let type = '';
              let text = '';
              switch (row.status) {
                case 0:
                  // AGUARDANDO
                  type = 'light';
                  text = 'Não Enviada';
                  break;
                case 1:
                  // ENVIADA
                  type = 'light-success';
                  text = 'Enviada';
                  break;
                case 2:
                  // ERRO
                  type = 'light-danger';
                  text = 'Com Erro';
                  break;
              }
              return '<span class="label font-weight-bold label-lg label-' + type + ' label-inline w-100 text-uppercase">' + text + '</span>';
            }
          }
        ],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
      });
      // EXIBIR TOOLTIPS
      $('#kt_datatable_queue').KTDatatable().on('datatable-on-layout-updated', function() {
        $('[data-toggle="tooltip"]').tooltip();
      });
      // MONITORAR O AUTOUPDATE
      let autoupdate_queue;
      $('#autoupdate-queue').off();
      $('#autoupdate-queue').on('change', function() {
        if ($(this).prop('checked')) autoupdate_queue = setInterval(() => $('#kt_datatable_queue').KTDatatable().reload(), 5000);
        else clearInterval(autoupdate_queue);
      });
    });
    $('#modal-queue').on('hidden.bs.modal', function() {
      $('#autoupdate-queue').prop('checked', false);
      $('#autoupdate-queue').trigger('change');
    });
    // EXIBIR O MODAL
    $('#modal-queue-title').html(label);
    $('#modal-queue').modal('show');
  },
  roll: async (id, status) => {
    let text;
    // DEFINIR A PERGUNTA COM BASE NA AÇÃO
    if (status == '4') text = 'Deseja mesmo suspender esta campanha?'
    else if (status == '3') text = 'Deseja mesmo resumir esta campanha?'
    else if (status == '7') text = 'Deseja mesmo cancelar esta campanha?'
    // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
    let campanha = await swal.fire({
      icon: 'warning',
      title: 'Atenção!',
      text: text,
      showCancelButton: true,
      reverseButtons: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Continuar',
      customClass: {
        confirmButton: 'btn btn-primary mt-0',
        cancelButton: 'btn btn-light mt-0'
      }
    });
    // VALIDAR CONTINUAÇÃO
    if (campanha.value) {
      try {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        await axios.patch(API_URL + '/campanhas/' + id, {
          status_id: status
        });
        datatable.reload();
        // DEU CERTO
        $('#modal').modal('hide');
        swal.close();
        let text;
        // DEFINIR A RESPOSTA COM BASE NA AÇÃO
        if (status == '4') text = 'A campanha foi suspensa com sucesso.'
        else if (status == '3') text = 'A campanha foi resumida com sucesso.'
        else if (status == '7') text = 'A campanha foi cancelada com sucesso.'
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: text,
        });
      }
      //
      catch(e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    }
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
  // GUI
  gui.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create', function() {
    api.create();
  });
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete($(this).attr('data-id'));
  });
  // CONFIGURAR CUSTOM
  $(document).on('click', '.btn-debug', function() {
    custom.debug();
  });
  $(document).on('click', '.btn-log', function() {
    custom.log($(this).attr('data-id'), $(this).attr('data-label'));
  });
  $(document).on('click', '.btn-queue', function() {
    custom.queue($(this).attr('data-id'), $(this).attr('data-label'));
  });
  $(document).on('click', '.btn-roll', function() {
    custom.roll($(this).attr('data-id'), $(this).attr('data-status'));
  });
});