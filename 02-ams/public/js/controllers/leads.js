let datatable = {
  init: (id) => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/leads/' + id,
            map: (raw) => {
              raw.forEach(item => {
                let telefone = (item.mobile_phone || item.personal_phone).replace(/\D/g, '');
                if (telefone.startsWith('55')) telefone = telefone.replace('55', '');
                if (telefone.length == 11) telefone = telefone.slice(0, 2) + telefone.slice(3, telefone.length);
                telefone = '55' + telefone;
                item.telefone = telefone;
                //
                item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
        field: 'id',
        title: '#',
        sortable: false,
        width: 20,
        type: 'number',
        selector: {
          class: ''
        },
        textAlign: 'center'
      }, {
        field: 'name',
        title: 'Nome',
        width: 200,
        template: (row) => {
          if (row.tags) return row.name + '<span class="label label-light-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.tags.join('<br>') + '"><i class="icon-sm la la-tag"></i></span>';
          else return row.name
        }
      }, {
        field: 'email',
        title: 'E-mail',
        width: 300
      }, {
        field: 'telefone',
        title: 'Telefone',
        width: 200
      }, {
        field: 'created_at',
        title: 'Data de Importação',
        width: 200
      }],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
    // EXIBIR TOOLTIPS
    $('#kt_datatable').KTDatatable().on('datatable-on-layout-updated', function() {
      $('[data-toggle="tooltip"]').tooltip();
    });
  },
  destroy: () => {
    try {
      $('#kt_datatable').KTDatatable().destroy();
    } catch (e) {}
  }
}

let gui = {
  init: async () => {
    // CARREGAR DADOS DE CLIENTES E PROJETOS
    let clientes = (await axios.get(API_URL + '/clientes')).data;
    // MONTAR LISTA DE PROJETOS PARA SELEÇÃO
    let projetos = {};
    clientes.forEach(cliente => {
      projetos[cliente.nome] = {};
      cliente.projetos.forEach(projeto => {
        let integracoes = JSON.parse(projeto.integracoes);
        if (integracoes && integracoes.rdstation) projetos[cliente.nome][projeto.id] = projeto.nome;
      });
      if ($.isEmptyObject(projetos[cliente.nome])) delete projetos[cliente.nome];
    });
    // INICIALIZAR SELELCT2 DOS PROJETOS
    Object.keys(projetos).forEach((cliente) => $('#projetos').append('<optgroup label="' + cliente + '">' + Object.entries(projetos[cliente]).map((projeto) => '<option value="' + projeto.shift() + '">' + projeto.pop() + '</option>') + '</optgroup>'));
    $('#projetos').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    // OBSERVAR MUDANÇAS NA SELEÇÃO
    $('#projetos').on('change', function() {
      datatable.destroy();
      if ($(this).val()) {
        $('#box-pesquisa, #box-tabela').removeClass('d-none');
        $('.btn-import').removeClass('disabled');
        datatable.init($(this).val());
      }
      //
      else {
        $('#box-pesquisa, #box-tabela').addClass('d-none');
        $('.btn-import').addClass('disabled');
      }
    });
  }
}

let custom = {
  import: async () => {
    const { value: file } = await Swal.fire({
      icon: 'question',
      title: 'Vamos lá!',
      text: 'Selecione um arquivo para importação.',
      input: 'file',
      inputAttributes: {
        'accept': '.csv',
        'aria-label': 'Selecionar planilha.'
      },
      inputValidator: (value) => {
        return new Promise((resolve) => {
          if (value) resolve();
          else resolve('Você precisa selecionar um arquivo.');
        });
      },
      showCancelButton: true,
      reverseButtons: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Continuar',
      footer: 'Certifique-se de ter exportado uma base de leads válida (no formato .csv) diretamente do RD Station.',
      customClass: {
        confirmButton: 'btn btn-primary mt-0',
        cancelButton: 'btn btn-light mt-0',
        footer: 'text-center'
      }
    });
    // VALIDAR CONTINUAÇÃO
    if (file) {
      try {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let formData = new FormData();
        formData.append('file', file);
        await axios.post(API_URL + '/leads/' + $('#projetos').val(), formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        });
        datatable.destroy();
        datatable.init($('#projetos').val());
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A importação foi realizada com sucesso.',
        });
      }
      //
      catch(e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    }
    
  }
}

jQuery(document).ready(() => {
  // GUI
  gui.init();
  // CONFIGURAR CUSTOM
  $(document).on('click', '.btn-import', function() {
    custom.import();
  });
});