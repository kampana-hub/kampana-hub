// METRONIC SETUP
let KTAppSettings = {
  'breakpoints': {
    'sm': 576,
    'md': 768,
    'lg': 992,
    'xl': 1200,
    'xxl': 1200
  },
  'colors': {
    'theme': {
      'base': {
        'white': '#ffffff',
        'primary': '#6993FF',
        'secondary': '#E5EAEE',
        'success': '#1BC5BD',
        'info': '#8950FC',
        'warning': '#FFA800',
        'danger': '#F64E60',
        'light': '#F3F6F9',
        'dark': '#212121'
      },
      'light': {
        'white': '#ffffff',
        'primary': '#E1E9FF',
        'secondary': '#ECF0F3',
        'success': '#C9F7F5',
        'info': '#EEE5FF',
        'warning': '#FFF4DE',
        'danger': '#FFE2E5',
        'light': '#F3F6F9',
        'dark': '#D6D6E0'
      },
      'inverse': {
        'white': '#ffffff',
        'primary': '#ffffff',
        'secondary': '#212121',
        'success': '#ffffff',
        'info': '#ffffff',
        'warning': '#ffffff',
        'danger': '#ffffff',
        'light': '#464E5F',
        'dark': '#ffffff'
      }
    },
    'gray': {
      'gray-100': '#F3F6F9',
      'gray-200': '#ECF0F3',
      'gray-300': '#E5EAEE',
      'gray-400': '#D6D6E0',
      'gray-500': '#B5B5C3',
      'gray-600': '#80808F',
      'gray-700': '#464E5F',
      'gray-800': '#1B283F',
      'gray-900': '#212121'
    }
  },
  'font-family': 'Poppins'
};

// TRADUÇÃO PARA AS TABELAS
let kt_datatable_pt_br = {
  records: {
    processing: 'Carregando...',
    noRecords: 'Não foram encontrados registros.'
  },
  toolbar: {
    pagination: {
      items: {
        default: {
          first: 'Primeiro',
          prev: 'Anterior',
          next: 'Seguinte',
          last: 'Último',
          more: 'Mais Páginas',
          input: 'Número da Página',
          select: 'Selecionar Tamanho da Página'
        },
        info: 'Vendo {{start}} - {{end}} de {{total}} registros'
      }
    }
  }
}

// USABILITY
jQuery(document).ready(function() {
  // SET ACTIVE MENU LINK
  let pathname = window.location.pathname.split('/').filter(Boolean).shift() || '';
  $('a.menu-link[href="/' + pathname + '"]').parents().filter('li').addClass('menu-item-active');
	$('a.menu-link[href="/' + pathname + '"]').closest('.tab-pane').addClass('show active');
  $('[data-target="#' + $('a.menu-link[href="/' + pathname + '"]').closest('.tab-pane').attr('id') + '"]').addClass('active');
  // LOGOUT
  $('.btn-logout').on('click', function(e) {
    e.preventDefault();
    swal.fire({
      icon: 'question',
      title: 'Deseja sair?',
      text: 'Sua sessão será encerrada.',
      showCancelButton: true,
      reverseButtons: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Continuar',
      customClass: {
        confirmButton: 'btn btn-primary mt-0',
        cancelButton: 'btn btn-light mt-0',
        footer: 'text-center'
      }
    }).then((click) => {
      if (click.value) window.location.href = '/logout';
    });
  });
  // WATCHDOG
  let watchdog = setInterval(async function() {
    try {
      await axios.get('/watchdog');
    } catch (e) {
      clearInterval(watchdog);
      swal.fire({
        type: 'error',
        title: 'Sessão Expirada',
        text: 'Por favor, clique no botão abaixo para recarregar a sessão.',
        confirmButtonText: 'Recarregar'
      }).then(function() {
        window.open('/refresh','targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=360,height=640')
      });
    }
  }, 5000);
});