function weightedMean(arrValues, arrWeights) {
  let result = arrValues.map(function(value, i) {
    let weight = arrWeights[i];
    let sum = value * weight;
    return [sum, weight];
  }).reduce(function(p, c) {
    return [p[0] + c[0], p[1] + c[1]];
  }, [0, 0]);
  return (result[0] / result[1]) || 0;
}

let gui = {
  init: async () => {
    let dashboard = (await axios.get(API_URL + '/dashboard')).data;
    // LINHA 1
    $('#dashboard-clientes').html(dashboard.clientes.ativos);
    $('#dashboard-projetos').html(dashboard.projetos.ativos);
    if (['Diretor', 'Gerente de Contas'].includes(auth0.funcao)) $('#dashboard-mrr').html(dashboard.mrr.ativo.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
    else $('#dashboard-mrr').html('N/A');
    $('#dashboard-nps').html(parseFloat(weightedMean(dashboard.nps.map(item => item.nps), dashboard.nps.map(item => item.promoters + item.passives + item.detractors)).toFixed(2)).toLocaleString('pt-BR') + '%');
    $('#dashboard-nps').append('<small> / ' + parseFloat(weightedMean(dashboard.nps.map(item => item.nps_noshow), dashboard.nps.map(item => item.promoters + item.passives + item.detractors)).toFixed(2)).toLocaleString('pt-BR') + '% </small>');
    
    // GRÁFICO DO NPS
    if ($('#nps').length) {
      let calendario = {
        1:  'Janeiro',
        2:  'Fevereiro',
        3:  'Março',
        4:  'Abril',
        5:  'Maio',
        6:  'Junho',
        7:  'Julho',
        8:  'Agosto',
        9:  'Setembro',
        10: 'Outubro',
        11: 'Novembro',
        12: 'Dezembro'
      };
      let chart = new ApexCharts(document.getElementById('nps'), {
        series: [{
          name: 'NPS',
          data: dashboard.nps.map(item => item.nps)
        }, {
          name: 'NPS Noshow',
          data: dashboard.nps.map(item => item.nps_noshow)
        }],
        chart: {
          type: 'bar',
          height: 250,
          toolbar: {
            show: false
          }
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: ['30%'],
            endingShape: 'rounded'
          },
        },
        legend: {
          show: false
        },
        dataLabels: {
          enabled: true,
          formatter: function(val) {
            return Math.round(val) + '%'
          }
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: dashboard.nps.map(item => calendario[item.mes] + '/' + item.ano),
          axisBorder: {
            show: false,
          },
          axisTicks: {
            show: false
          },
          labels: {
            style: {
              colors: KTApp.getSettings().colors.gray['gray-500'],
              fontSize: '12px',
              fontFamily: KTApp.getSettings()['font-family']
            }
          }
        },
        yaxis: {
          labels: {
            style: {
              colors: KTApp.getSettings().colors.gray['gray-500'],
              fontSize: '12px',
              fontFamily: KTApp.getSettings()['font-family']
            }
          },
          max: 100,
          min: -100,
          tickAmount: 4
        },
        fill: {
          opacity: 1
        },
        states: {
          normal: {
            filter: {
              type: 'none',
              value: 0
            }
          },
          hover: {
            filter: {
              type: 'none',
              value: 0
            }
          },
          active: {
            allowMultipleDataPointsSelection: false,
            filter: {
              type: 'none',
              value: 0
            }
          }
        },
        tooltip: {
          style: {
            fontSize: '12px',
            fontFamily: KTApp.getSettings()['font-family']
          },
          y: {
            formatter: function(val) {
              return val.toLocaleString('pt-BR') + '%'
            }
          }
        },
        colors: [
          ({value, seriesIndex, w}) => {
            if      (value >= 75) return KTApp.getSettings().colors.theme.base.success
            else if (value >= 50) return KTApp.getSettings().colors.theme.base.primary
            else if (value >= 0)  return KTApp.getSettings().colors.theme.base.warning
            else                  return KTApp.getSettings().colors.theme.base.danger
          },
          ({value, seriesIndex, w}) => {
            if      (value >= 75) return KTApp.getSettings().colors.theme.base.success
            else if (value >= 50) return KTApp.getSettings().colors.theme.base.primary
            else if (value >= 0)  return KTApp.getSettings().colors.theme.base.warning
            else                  return KTApp.getSettings().colors.theme.base.danger
          }
        ],
        grid: {
          borderColor: KTApp.getSettings().colors.gray['gray-200'],
          strokeDashArray: 4,
          yaxis: {
            lines: {
              show: true
            }
          }
        }
      });
      chart.render();
    }
    
    // ANIVERSARIANTES
    if ($('#aniversariantes').length) {
      let backoffice = (await axios.get(API_URL + '/back-office')).data;
      let date = new Date();
      date.setMonth(date.getMonth());
      let mes = date.getMonth();
      date.setMonth(date.getMonth() + 1);
      let mes_seguinte = date.getMonth();
      let aniversariantes = backoffice.filter(user => (user.user_metadata.nascimento.split('-')[1] - 1) == mes || (user.user_metadata.nascimento.split('-')[1] - 1) == mes_seguinte);
      if (aniversariantes.length > 0) {
        aniversariantes.forEach((user, index) => {
          if (index == aniversariantes.length - 1) $('#aniversariantes .card-body').append('<div class="d-flex align-items-center mb-2"><div class="symbol symbol-40 symbol-sm flex-shrink-0"><img src="' + user.picture + '" alt="' + user.name + '"></div><div class="d-flex flex-column flex-grow-1 font-weight-bold ml-4"><span class="text-dark mb-1 font-size-lg">' + user.name + '</span><span class="text-muted">' + user.user_metadata.nascimento.split('-').reverse().join('/') + '</span></div></div>');
          else $('#aniversariantes .card-body').append('<div class="d-flex align-items-center mb-10"><div class="symbol symbol-40 symbol-sm flex-shrink-0"><img src="' + user.picture + '" alt="' + user.name + '"></div><div class="d-flex flex-column flex-grow-1 font-weight-bold ml-4"><span class="text-dark mb-1 font-size-lg">' + user.name + '</span><span class="text-muted">' + user.user_metadata.nascimento.split('-').reverse().join('/') + '</span></div></div>');
        });
        $('#aniversariantes').removeClass('d-none');
      }
    }
    
    // MONITORAR O SWIITCH DE VISUALIZAÇÃO
    $('#hide-dashboard').on('change', function() {
      if ($(this).prop('checked')) $('.dashboard').removeClass('blurry');
      else $('.dashboard').addClass('blurry');
    });
  }
}

jQuery(document).ready(() => {
  // GUI
  gui.init();
});