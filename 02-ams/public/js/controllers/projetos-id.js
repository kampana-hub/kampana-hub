let datatable = {
  atividades: {
    init: () => {
      $('#atividades').KTDatatable({
        // CARREGAR E MANIPULAR OS DADOS
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'GET',
              url: API_URL + '/projetos/' + id + '/atividades',
              map: (raw) => {
                raw.forEach(item => {
                  item.leader = JSON.parse(item.leader);
                  item.squad = JSON.parse(item.squad) || [];
                  //
                  let datetime = item.data;
                  //
                  item.data = moment(datetime).format('DD/MM/YYYY');
                  item.hora = moment(datetime).format('HH:mm:ss');
                });
                return raw;
              }
            }
          }
        },
        // DEFINIR LAYOUT
        layout: {
          scroll: false,
          footer: false
        },
        sortable: true,
        pagination: false,
        // DEFINIR FILTROS
        search: {
          input: $('#atividades-search-query'),
          key: 'generalSearch'
        },
        // DEFINIR COLUNAS
        columns: [{
          field: 'id',
          title: 'Atividade',
          autoHide: false,
          template: (row) => {
            let template = '<small>' + row.id + '</small> - ' + row.nome;
            if (row.descricao) template += '<br><small>' + row.descricao + '</small>';
            template += '<br><span class="label label-sm label-light-success label-inline mr-2">' + row.leader.value + '</span>'
            if (row.squad.length > 0) template += row.squad.map(tag => '<span class="label label-sm label-light-info label-inline mr-2">' + tag.value + '</span>').join('');
            template += '<br><small class="d-inline-text d-sm-none">' + row.data + ' ' + row.hora + '</small>';
            return template;
          }
        }, {
          field: 'data',
          title: 'Data',
          width: 80,
          template: (row) => {
            return row.data + '<br><small>' + row.hora + '</small>';
          },
          responsive: {
            hidden: 'xs',
            visible: 'sm'
          }
        }],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
      });
      $('#atividades').KTDatatable().sort('data', 'DESC');
    },
    reload: () => {
      $('#atividades').KTDatatable().reload();
    }
  },
  relatorios: {
    init: () => {
      $('#relatorios').KTDatatable({
        // CARREGAR E MANIPULAR OS DADOS
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'GET',
              url: API_URL + '/projetos/' + id + '/relatorios',
              map: (raw) => {
                raw.forEach(item => {
                  item.funil = JSON.parse(item.funil);
                  item.competencia = item.mes + '/' + item.ano
                });
                return raw;
              }
            }
          }
        },
        // DEFINIR LAYOUT
        layout: {
          scroll: false,
          footer: false
        },
        sortable: true,
        pagination: false,
        // DEFINIR FILTROS
        search: {
          input: $('#relatorios-search-query'),
          key: 'generalSearch'
        },
        // DEFINIR COLUNAS
        columns: [{
          field: 'id',
          title: 'Relatório',
          autoHide: false,
          // width: 30
          template: (row) => {
            let template = '<span class="ellipsis"><span class="label label-dot label-' + (row.status ? 'success' : 'danger') + '"></span> ' + '<small>' + row.competencia + '</small> - Relatório Mensal de Resultados</span>';
            /*
              if (row.funil) {
                let visitantes    = '<div class="progress-bar bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">' + row.funil.visitantes + ' Visitantes</div>';
                let leads         = '<div class="progress-bar bg-warning" role="progressbar" style="width: ' + ((row.funil.leads / row.funil.visitantes) * 100).toFixed(2) + '%;" aria-valuenow="' + ((row.funil.leads / row.funil.visitantes) * 100).toFixed(2) + '" aria-valuemin="0" aria-valuemax="100">'+ row.funil.leads + ' Leads</div>';
                let oportunidades = '<div class="progress-bar bg-primary" role="progressbar" style="width: ' + ((row.funil.oportunidades / row.funil.leads) * 100).toFixed(2) + '%;" aria-valuenow="' + ((row.funil.oportunidades / row.funil.leads) * 100).toFixed(2) + '" aria-valuemin="0" aria-valuemax="100">' + row.funil.oportunidades + ' Oportunidades</div>';
                let vendas        = '<div class="progress-bar bg-success" role="progressbar" style="width: ' + ((row.funil.vendas / row.funil.oportunidades) * 100).toFixed(2) + '%;" aria-valuenow="' + ((row.funil.vendas / row.funil.oportunidades) * 100).toFixed(2) + '" aria-valuemin="0" aria-valuemax="100">'+ row.funil.vendas + ' Vendas</div>';
                template += '<br><div class="progress" style="height: 30px">' + visitantes + leads + oportunidades + vendas + '</div>';
              }
            */
            return template;
          }
        }, {
          field: 'Actions',
          title: '',
          sortable: false,
          width: 50,
          template: (row) => {
            return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover py-2">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">\
                            ESCOLHA UMA OPÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <a class="navi-link" href="/relatorios/' + row.id + '/visualizar" target="_blank">\
                                <span class="navi-icon"><i class="la la-eye"></i></span>\
                                <span class="navi-text">Visualizar</span>\
                            </a>\
                        </li>\
                        <li class="navi-item">\
                            <a class="navi-link" href="/relatorios/' + row.id + '">\
                                <span class="navi-icon"><i class="la la-pencil"></i></span>\
                                <span class="navi-text">Editar</span>\
                            </a>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-delete-relatorio" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>\
                    </ul>\
                </div>\
            </div>';
          }
        }],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
      });
      $('#relatorios').KTDatatable().sort('id', 'DESC');
    },
    reload: () => {
      $('#relatorios').KTDatatable().reload();
    }
  },
  pesquisas: {
    init: () => {
      $('#pesquisas').KTDatatable({
        // CARREGAR E MANIPULAR OS DADOS
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'GET',
              url: API_URL + '/projetos/' + id + '/pesquisas',
              map: (raw) => {
                raw.forEach(item => {
                  item.competencia = item.mes + '/' + item.ano;
                  //
                  switch (item.status) {
                    case 0:
                      // RASCUNHO
                      item.status_label = 'danger';
                      item.status_text = 'Aguardando';
                      break;
                    case 1:
                      // CONCLUÍDA
                      item.status_label = 'success';
                      item.status_text = 'Preenchida';
                      break;
                  }
                });
                return raw;
              }
            }
          }
        },
        // DEFINIR LAYOUT
        layout: {
          scroll: false,
          footer: false
        },
        sortable: true,
        pagination: false,
        // DEFINIR FILTROS
        search: {
          input: $('#pesquisas-search-query'),
          key: 'generalSearch'
        },
        // DEFINIR COLUNAS
        columns: [{
          field: 'competencia',
          title: 'Pesquisa',
          autoHide: false,
          template: (row) => {
            let template = '<span class="label label-dot label-' + row.status_label + '"></span> <small>' + row.competencia + '</small> - ' + row.formulario_nome + ' <small>(' + row.id + ')</small>';
            template += '\
            <div class="dropdown dropdown-inline d-inline-block d-sm-none float-right">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover py-2">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">\
                            ESCOLHA UMA OPÇÃO\
                        </li>' + (row.status == 0 ? '\
                        <li class="navi-item">\
                            <span class="navi-link" onclick="gui.copy(\'' + window.location.origin +'/pesquisas/' + row.id + '\')">\
                                <span class="navi-icon"><i class="la la-link"></i></span>\
                                <span class="navi-text">Copiar Link</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-update-pesquisa" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-edit"></i></span>\
                                <span class="navi-text">Editar</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-delete-pesquisa" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>' : '\
                        <li class="navi-item">\
                            <a class="navi-link" href="' + window.location.origin + '/pesquisas/' + row.id + '" target="_blank">\
                                <span class="navi-icon"><i class="la la-eye"></i></span>\
                                <span class="navi-text">Visualizar</span>\
                            </a>\
                        </li>') + '\
                    </ul>\
                </div>\
            </div>';
            return template;
          }
        }, {
          field: 'Actions',
          title: '',
          sortable: false,
          width: 40,
          template: (row) => {
            return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover py-2">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">\
                            ESCOLHA UMA OPÇÃO\
                        </li>' + (row.status == 0 ? '\
                        <li class="navi-item">\
                            <span class="navi-link" onclick="gui.copy(\'' + window.location.origin + '/pesquisas/' + row.id + '\')">\
                                <span class="navi-icon"><i class="la la-link"></i></span>\
                                <span class="navi-text">Copiar Link</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-update-pesquisa" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-edit"></i></span>\
                                <span class="navi-text">Editar</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-delete-pesquisa" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>' : '\
                        <li class="navi-item">\
                            <a class="navi-link" href="' + window.location.origin + '/pesquisas/' + row.id + '" target="_blank">\
                                <span class="navi-icon"><i class="la la-eye"></i></span>\
                                <span class="navi-text">Visualizar</span>\
                            </a>\
                        </li>') + '\
                    </ul>\
                </div>\
            </div>';
          },
          responsive: {
            hidden: 'xs',
            visible: 'sm'
          }
        }],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
      });
      $('#pesquisas').KTDatatable().sort('competencia', 'DESC');
    },
    reload: () => {
      $('#pesquisas').KTDatatable().reload();
    }
  },
  integracoes: {
    init: (integracoes) => {
      $('#integracoes').KTDatatable({
        // CARREGAR E MANIPULAR OS DADOS
        data: {
          type: 'local',
          source: integracoes
        },
        // DEFINIR LAYOUT
        layout: {
          scroll: false,
          footer: false
        },
        sortable: true,
        pagination: false,
        // DEFINIR FILTROS
        search: {
          input: $('#integracoes-search-query'),
          key: 'generalSearch'
        },
        // DEFINIR COLUNAS
        columns: [{
          field: 'nome',
          title: 'Integração',
          autoHide: false,
          template: (row) => {
            return '<img class="max-h-20px mb-4" alt="' + row.nome + '" src="' + row.logo + '"><br><span class="label label-dot label-' + (row.status ? 'success' : 'danger') + '"></span> ' + row.conta + '<br><small class="d-inline-text d-sm-none">' + row.data + ' ' + row.hora + '</small>';
          }
        }, {
          field: 'last_timestamp',
          title: 'Data',
          width: 80,
          template: (row) => {
            return row.data + '<br><small>' + row.hora + '</small>';
          },
          responsive: {
            hidden: 'xs',
            visible: 'sm'
          }
        }],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
      });
      $('#integracoes').KTDatatable().sort('nome', 'DESC');
    },
    reload: () => {
      $('#integracoes').KTDatatable().reload();
    }
  },
  escopo: {
    init: (escopo) => {
      $('#escopo').KTDatatable({
        // CARREGAR E MANIPULAR OS DADOS
        data: {
          type: 'local',
          source: escopo
        },
        // DEFINIR LAYOUT
        layout: {
          scroll: false,
          footer: false
        },
        sortable: true,
        pagination: false,
        // DEFINIR FILTROS
        search: {
          input: $('#escopo-search-query'),
          key: 'generalSearch'
        },
        // DEFINIR COLUNAS
        columns: [{
          field: 'nome',
          title: 'Nome',
          autoHide: false,
        }, {
          field: 'quantidade',
          title: 'Quantidade',
          width: 100,
          template: (row) => {
            return row.quantidade + ' - ' + row.frequencia
          },
          responsive: {
            hidden: 'xs',
            visible: 'sm'
          }
        }],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
      });
      $('#escopo').KTDatatable().sort('nome', 'DESC');
    },
    reload: () => {
      $('#escopo').KTDatatable().reload();
    }
  },
  credenciais: {
    init: () => {
      $('#credenciais').KTDatatable({
        // CARREGAR E MANIPULAR OS DADOS
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'GET',
              url: API_URL + '/projetos/' + id + '/credenciais',
              map: (raw) => {
                raw.forEach(item => {
                  item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
                });
                return raw;
              }
            }
          }
        },
        // DEFINIR LAYOUT
        layout: {
          scroll: false,
          footer: false
        },
        sortable: true,
        pagination: false,
        // DEFINIR FILTROS
        search: {
          input: $('#credenciais-search-query'),
          key: 'generalSearch'
        },
        // DEFINIR COLUNAS
        columns: [/*{
          field: 'id',
          title: '#',
          sortable: false,
          width: 20,
          type: 'number',
          selector: {
            class: ''
          },
          textAlign: 'center'
        }, */{
          field: 'nome',
          title: 'Nome',
          autoHide: false,
          width: 150,
          template: (row) => {
            return row.nome
          }
        },{
          field: 'url',
          title: 'Url',
          autoHide: false,
          width: 350,
          template: (row) => {
            return row.url
          }
        }/*,{
          field: 'usuario',
          title: 'Usuário',
          autoHide: false,
          width: 150,
          template: (row) => {
            return row.usuario
          }
        },{
          field: 'senha',
          title: 'Senha',
          autoHide: false,
          width: 150,
          template: (row) => {
            return row.senha
          }
        }*/,{
          field: 'projeto_nome',
          title: 'Projeto',
          autoHide: false,
          width: 120,
          template: (row) => {
            return row.projeto_nome
          }
        },{
          field: 'data',
          title: 'Data',
          autoHide: false,
          width: 80,
          template: (row) => {
            return row.created_at
          }
        }, {
          field: 'Actions',
          title: '',
          sortable: false,
          width: 50,
          autoHide: false,
          overflow: 'visible',
          template: (row) => {
            return '\
              <div class="dropdown dropdown-inline">\
                  <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                      <span class="svg-icon svg-icon-md">\
                          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                  <rect x="0" y="0" width="24" height="24"/>\
                                  <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                              </g>\
                          </svg>\
                      </span>\
                  </a>\
                  <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                      <ul class="navi flex-column navi-hover ">\
                          <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">\
                              CONFIGURAÇÃO\
                          </li>\
                          <li class="navi-item">\
                              <span class="navi-link btn-update-credencial" data-id="' + row.id + '">\
                                  <span class="navi-icon"><i class="la la-pencil"></i></span>\
                                  <span class="navi-text">Gerenciar</span>\
                              </span>\
                          </li>\
                          <li class="navi-item">\
                              <span class="navi-link btn-delete-credencial" data-id="' + row.id + '">\
                                  <span class="navi-icon"><i class="la la-trash"></i></span>\
                                  <span class="navi-text">Excluir</span>\
                              </span>\
                          </li>' +
                          /*<li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">\
                              OPERAÇÃO\
                          </li>\
                          <li class="navi-item">\
                              <span class="navi-link btn-password" data-id="' + row.user_id + '">\
                                  <span class="navi-icon"><i class="la la-key"></i></span>\
                                  <span class="navi-text">Alterar Senha</span>\
                              </span>\
                          </li>
                          <li class="navi-item">\
                              <span class="navi-link btn-log" data-id="' + row.user_id + '" data-label="' + row.name + '">\
                                  <span class="navi-icon"><i class="la la-binoculars"></i></span>\
                                  <span class="navi-text">Histórico</span>\
                              </span>\
                          </li>\*/
                      '</ul>\
                  </div>\
              </div>';
          },
          responsive: {
            hidden: 'xs',
            visible: 'sm'
          }
        }],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
      });
      $('#credenciais').KTDatatable().sort('id', 'DESC');
    },
    reload: () => {
      $('#credenciais').KTDatatable().reload();
    }
  }
}

let tagify;
let tagify_input = document.getElementById('atividade-squad');

let gui = {
  init: async () => {
    // INFORMAÇÕES NECESSÁRIAS
    let projeto = (await axios.get(API_URL + '/projetos/' + id)).data;
    let cliente = (await axios.get(API_URL + '/clientes/' + projeto.cliente_id)).data;
    let administrador =  projeto.gerente_id ? (await axios.get(API_URL + '/back-office/' + projeto.gerente_id)).data : null;
    // TÍTULO DA PÁGINA
    document.title = document.title.split(' ').slice(0, -1).join(' ') + ' ' + projeto.nome;
    // CABEÇALHO - THUMBNAIL
    projeto.imagens = JSON.parse(projeto.imagens);
    //
    if (projeto.imagens && projeto.imagens.thumbnail) $('#thumbnail').html('<div class="symbol symbol-80"><img alt="' + projeto.nome + '" src="' + projeto.imagens.thumbnail + '"></div>');
    else $('#thumbnail').html('<div class="symbol symbol-80 symbol-light"><span class="font-size-h3 symbol-label font-weight-boldest">' + projeto.nome.charAt(0) + '</span></div>');
    // CABEÇALHO - IDENTIFICAÇÃO
    if (cliente.tipo == 'PF') cliente.id_mf = cliente.id_mf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '\$1.\$2.\$3\-\$4');
    if (cliente.tipo == 'PJ') cliente.id_mf = cliente.id_mf.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, '\$1.\$2.\$3\/\$4\-\$5');
    //
    $('#projeto-nome').html(projeto.nome);
    $('#cliente-nome').html(cliente.nome + ' <small>' + cliente.id_mf + '</small>');
    //
    if (administrador) $('#administrador-nome').html(administrador.name);
    else $('#administrador-nome').html('N/A');
    //
    if (projeto.descricao) $('#projeto-descricao').html(projeto.descricao);
    else $('#projeto-descricao').html('N/A');
    //
    projeto.tags = JSON.parse(projeto.tags) || [];
    $('#projeto-tags').html(projeto.tags.map(tag => '<span class="label label-sm label-light-info label-inline mr-2">' + tag + '</span>').join(''));
    // ABA - INTEGRAÇÕES
    let integracoes = [];
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    Object.keys(projeto.integracoes).forEach(integracao => {
      switch (integracao) {
        case 'activecampaign':
          integracoes.push({
            nome: 'ActiveCampaign',
            logo: '/media/integrations/logo-activecampaign.png',
            conta: projeto.integracoes[integracao].url,
            status: projeto.integracoes[integracao].status,
            last_timestamp: projeto.integracoes[integracao].last_timestamp,
            data: moment.unix(parseInt(projeto.integracoes[integracao].last_timestamp)).format('DD/MM/YYYY'),
            hora: moment.unix(parseInt(projeto.integracoes[integracao].last_timestamp)).format('HH:mm:ss')
          });
          break;
        case 'rdstation':
          integracoes.push({
            nome: 'RD Station',
            logo: '/media/integrations/logo-rdstation.png',
            conta: projeto.integracoes[integracao].name,
            status: projeto.integracoes[integracao].status,
            last_timestamp: projeto.integracoes[integracao].last_timestamp,
            data: moment.unix(parseInt(projeto.integracoes[integracao].last_timestamp)).format('DD/MM/YYYY'),
            hora: moment.unix(parseInt(projeto.integracoes[integracao].last_timestamp)).format('HH:mm:ss')
          });
          break;
        case 'nuvemshop':
          integracoes.push({
            nome: 'Nuvemshop',
            logo: '/media/integrations/logo-nuvemshop.png',
            conta: projeto.integracoes[integracao].original_domain,
            status: projeto.integracoes[integracao].status,
            last_timestamp: projeto.integracoes[integracao].last_timestamp,
            data: moment.unix(parseInt(projeto.integracoes[integracao].last_timestamp)).format('DD/MM/YYYY'),
            hora: moment.unix(parseInt(projeto.integracoes[integracao].last_timestamp)).format('HH:mm:ss')
          });
          break;
        case 'whatsapp':
          console.log(projeto.integracoes[integracao]);
          integracoes.push({
            nome: 'WhatsApp',
            logo: '/media/integrations/logo-whatsapp.png',
            conta: 'Conectado #' + projeto.integracoes[integracao].sender_id,
            status: projeto.integracoes[integracao].status,
            last_timestamp: projeto.integracoes[integracao].last_timestamp,
            data: moment.unix(parseInt(projeto.integracoes[integracao].last_timestamp)).format('DD/MM/YYYY'),
            hora: moment.unix(parseInt(projeto.integracoes[integracao].last_timestamp)).format('HH:mm:ss')
          });
          break;
      }
    });
    datatable.integracoes.init(integracoes);
    // ABA - ESCOPO
    projeto.comercial = JSON.parse(projeto.comercial);
    if (projeto.comercial && projeto.comercial.kampana && projeto.comercial.kampana.escopo && projeto.comercial.kampana.escopo.length > 0) {
      let escopo = projeto.comercial.kampana.escopo.map(item => {
        return {
          nome: item[0], 
          quantidade: item[1],
          frequencia: item[2]
        }
      });
      datatable.escopo.init(escopo);
    }
    // ETC
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      if ($(this).attr('href') == '#kt_tab_pane_1') setTimeout(function() {
        datatable.atividades.reload();
      }, 500);
      if ($(this).attr('href') == '#kt_tab_pane_2') setTimeout(function() {
        datatable.relatorios.reload();
      }, 500);
      if ($(this).attr('href') == '#kt_tab_pane_3') setTimeout(function() {
        datatable.pesquisas.reload();
      }, 500);
      if ($(this).attr('href') == '#kt_tab_pane_4') setTimeout(function() {
        datatable.integracoes.reload();
      }, 500);
      if ($(this).attr('href') == '#kt_tab_pane_5') setTimeout(function() {
        datatable.escopo.reload();
      }, 500);
      if ($(this).attr('href') == '#kt_tab_pane_6') setTimeout(function() {
        datatable.credenciais.reload();
      }, 500);
    });
    $('a.nav-link.active[data-toggle="tab"]').trigger('shown.bs.tab');
  },
  atividade: {
    init: async () => {
      // INICIALIZAR DATE TIME PICKER
      $('#atividade-data').datetimepicker({
        locale: 'pt-br',
        maxDate: moment()
      });
      // INICIALIZAR SQUAD
      let squad = (await axios.get(API_URL + '/back-office')).data;
      tagify = new Tagify(tagify_input, {
        enforceWhitelist: true,
        whitelist: squad.filter(user => user.user_id != auth0.user_id).map(user => {
          return {
            value: user.name,
            user_id: user.user_id
          }
        })
      });
    }
  },
  relatorio: {
    init: () => {
      // INICIALIAZR SELECT2 DE DATAS
      $('#relatorio-mes').select2({
        placeholder: {
          id: '',
          text: 'Selecionar'
        },
        allowClear: true
      });
      $('#relatorio-ano').select2({
        placeholder: {
          id: '',
          text: 'Selecionar'
        },
        allowClear: true
      });
    }
  },
  pesquisa: {
    init: async () => {
      // CARREGAR DADOS DE FORMULÁRIOS
      let formularios = (await axios.get(API_URL + '/formularios')).data;
      // INICIALIAZR SELECT2 DE DATAS
      $('#pesquisa-mes').select2({
        placeholder: {
          id: '',
          text: 'Selecionar'
        },
        allowClear: true
      });
      $('#pesquisa-ano').select2({
        placeholder: {
          id: '',
          text: 'Selecionar'
        },
        allowClear: true
      });
      // INICIALIZAR SELELCT2 DOS FORMULÁRIOS
      formularios.filter(formulario => formulario.status).forEach((formulario) => $('#pesquisa-formulario').append('<option value="' + formulario.id + '">' + formulario.nome + '</option>'));
      $('#pesquisa-formulario').select2({
        placeholder: {
          id: '',
          text: 'Selecionar'
        },
        allowClear: true
      });
    }
  },
  credencial: {
    init: () => {
      // VISUALAR SENHAR
      $('#btnViewPass').on('click', function() {
        let senha = document.querySelector('#credencial-senha');
        let tipo_senha = senha.getAttribute('type') === 'password' ? 'text' : 'password';
        senha.setAttribute('type', tipo_senha);
        $('#btnViewPass').addClass('bi-eye-slash');
        this.classList.toggle('bi-eye');
      });
      // COPIAR SENHAR
      $('#btnCopyPass').on('click', function() {
        let senha = document.getElementById("credencial-senha");
        senha.select();
        senha.setSelectionRange(0, 99999);
        navigator.clipboard.writeText(senha.value);
        swal.fire({
          icon: 'success',
          text: JSON.stringify($('#credencial-senha').val()) + " copiado",
        });
      });
      // COPIAR USUÁRIO
      $('#btnCopyUser').on('click', function() {
        let usuario = document.getElementById("credencial-usuario");
        usuario.select();
        usuario.setSelectionRange(0, 99999);
        navigator.clipboard.writeText(usuario.value);
        swal.fire({
          icon: 'success',
          text: JSON.stringify($('#credencial-usuario').val()) + " copiado",
        });
      });
      // REDIRECIONAR URL
      $('#btnRdrtUrl').on('click', function() {
        window.open($('#credencial-url').val());
      });
    }
  },
  copy: (text) => {
    var input = document.body.appendChild(document.createElement('input'));
    input.value = text;
    input.focus();
    input.select();
    document.execCommand('copy');
    input.parentNode.removeChild(input);
    swal.fire({
      icon: 'success',
      title: 'Excelente!',
      text: 'Link copiado para a área de transferência.'
    });
  }
}

let api = {
  atividade: {
    create: () => {
      try {
        tagify.removeAllTags();
        // REDEFINIR FORMULÁRIO
        $('#form-atividade').trigger('reset');
        $('#form-atividade').validate();
        $('#atividade-data').find('input').val(moment().format('DD/MM/YYYY HH:mm'));
        // GERENCIAR CRIAÇÃO DO REGISTRO
        $('.btn-save-atividade').off();
        $('.btn-save-atividade').on('click', async function() {
          // VALIDAR FORMULÁRIO
          if ($('#form-atividade').valid()) {
            let confirmacao = await swal.fire({
              icon: 'warning',
              title: 'Atenção!',
              text: 'Deseja mesmo adicionar esta atividade?',
              showCancelButton: true,
              reverseButtons: true,
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Continuar',
              customClass: {
                confirmButton: 'btn btn-primary mt-0',
                cancelButton: 'btn btn-light mt-0'
              }
            });
            if (confirmacao.value) {
              try {
                // EXIBIR TELA DE CARREGAMENTO
                swal.fire({
                  title: 'Aguarde...',
                  text: 'Processando a solicitação.',
                  onOpen: () => {
                    swal.showLoading();
                  },
                  allowOutsideClick: false,
                  allowEscapeKey: false
                });
                // ENVIAR REQUISIÇÃO PARA O BACK-END
                let data = $('#atividade-data').find('input').val();
                //
                let dia = data.substring(0, 2);
                let mes = data.substring(3, 5);
                let ano = data.substring(6, 10);
                let hora = data.substring(11, 16);
                //
                let atividade = {
                  projeto_id: id,
                  nome: $('#atividade-nome').val(),
                  descricao: $('#atividade-descricao').val() || null,
                  data: ano + '-' + mes + '-' + dia + ' ' + hora,
                  leader: JSON.stringify({
                    user_id: auth0.user_id,
                    value: auth0.name
                  }),
                  squad: $('#atividade-squad').val() ? $('#atividade-squad').val() : '[]',
                }
                await axios.post(API_URL + '/atividades/', atividade);
                datatable.atividades.reload();
                // DEU CERTO
                $('#modal-atividade').modal('hide');
                swal.close();
                swal.fire({
                  icon: 'success',
                  title: 'Excelente!',
                  text: 'A atividade foi adicionada com sucesso.',
                });
              }
              //
              catch (e) {
                console.log(e);
                swal.close();
                swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Algo errado não está certo.',
                  footer: 'Confira o erro no log do navegador.'
                });
              }
            }
          }
        });
        // EXIBIR MODAL
        $('#modal-atividade-title').html('Adicionar Atividade');
        $('#modal-atividade').modal('show');
      }
      //
      catch (e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    }
  },
  relatorio: {
    create: () => {
      try {
        // REDEFINIR FORMULÁRIO
        $('#form-relatorio').trigger('reset');
        $('#form-relatorio').validate();
        // PREENCHER DATA AUTOMATICAMENTE
        $('#relatorio-mes').val(new Date().getMonth() + 1);
        $('#relatorio-ano').val(new Date().getFullYear());
        $('#relatorio-mes, #relatorio-ano').trigger('change');
        // GERENCIAR CRIAÇÃO DO REGISTRO
        $('.btn-save-relatorio').off();
        $('.btn-save-relatorio').on('click', async function() {
          // VALIDAR FORMULÁRIO
          if ($('#form-relatorio').valid()) {
            let confirmacao = await swal.fire({
              icon: 'warning',
              title: 'Atenção!',
              text: 'Deseja mesmo adicionar este relatório?',
              showCancelButton: true,
              reverseButtons: true,
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Continuar',
              customClass: {
                confirmButton: 'btn btn-primary mt-0',
                cancelButton: 'btn btn-light mt-0'
              }
            });
            if (confirmacao.value) {
              try {
                // EXIBIR TELA DE CARREGAMENTO
                swal.fire({
                  title: 'Aguarde...',
                  text: 'Processando a solicitação.',
                  onOpen: () => {
                    swal.showLoading();
                  },
                  allowOutsideClick: false,
                  allowEscapeKey: false
                });
                // ENVIAR REQUISIÇÃO PARA O BACK-END
                let relatorio = {
                  projeto_id: id,
                  mes: $('#relatorio-mes').val(),
                  ano: $('#relatorio-ano').val(),
                  status: 0
                }
                // CHAMADA
                await axios.post(API_URL + '/relatorios', relatorio);
                datatable.relatorios.reload();
                // DEU CERTO
                $('#modal').modal('hide');
                swal.close();
                swal.fire({
                  icon: 'success',
                  title: 'Excelente!',
                  text: 'O relatório foi adicionado com sucesso.',
                });
              }
              //
              catch (e) {
                console.log(e);
                swal.close();
                swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Algo errado não está certo.',
                  footer: 'Confira o erro no log do navegador.'
                });
              }
            }
          }
        });
        // EXIBIR MODAL
        $('#modal-relatorio-title').html('Adicionar Relatório');
        $('#modal-relatorio').modal('show');
      }
      //
      catch (e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    },
    delete: async (id) => {
      try {
        // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
        let relatorio = await swal.fire({
          icon: 'warning',
          title: 'Atenção!',
          text: 'Deseja mesmo excluir este relatório?',
          showCancelButton: true,
          reverseButtons: true,
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Continuar',
          customClass: {
            confirmButton: 'btn btn-primary mt-0',
            cancelButton: 'btn btn-light mt-0'
          }
        });
        // VALIDAR CONTINUAÇÃO
        if (relatorio.value) {
          // EXIBIR TELA DE CARREGAMENTO
          swal.fire({
            title: 'Aguarde...',
            text: 'Processando a solicitação.',
            onOpen: () => {
              swal.showLoading();
            },
            allowOutsideClick: false,
            allowEscapeKey: false
          });
          // ENVIAR REQUISIÇÃO PARA O BACK-END
          let relatorio_id = id;
          await axios.delete(API_URL + '/relatorios/' + relatorio_id);
          datatable.reload();
          // DEU CERTO
          swal.close();
          swal.fire({
            icon: 'success',
            title: 'Excelente!',
            text: 'O relatório foi excluído com sucesso.',
          });
        }
      }
      //
      catch (e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    }
  },
  pesquisa: {
    create: () => {
      try {
        // REDEFINIR FORMULÁRIO
        $('#form-pesquisa').trigger('reset');
        $('#form-pesquisa').validate();
        // PREENCHER DATA AUTOMATICAMENTE
        $('#pesquisa-mes').val(new Date().getMonth() + 1);
        $('#pesquisa-ano').val(new Date().getFullYear());
        $('#pesquisa-mes, #pesquisa-ano, #pesquisa-formulario').trigger('change');
        // GERENCIAR CRIAÇÃO DO REGISTRO
        $('.btn-save-pesquisa').off();
        $('.btn-save-pesquisa').on('click', async function() {
          // VALIDAR FORMULÁRIO
          if ($('#form-pesquisa').valid()) {
            let confirmacao = await swal.fire({
              icon: 'warning',
              title: 'Atenção!',
              text: 'Deseja mesmo adicionar esta pesquisa?',
              showCancelButton: true,
              reverseButtons: true,
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Continuar',
              customClass: {
                confirmButton: 'btn btn-primary mt-0',
                cancelButton: 'btn btn-light mt-0'
              }
            });
            if (confirmacao.value) {
              try {
                // EXIBIR TELA DE CARREGAMENTO
                swal.fire({
                  title: 'Aguarde...',
                  text: 'Processando a solicitação.',
                  onOpen: () => {
                    swal.showLoading();
                  },
                  allowOutsideClick: false,
                  allowEscapeKey: false
                });
                // ENVIAR REQUISIÇÃO PARA O BACK-END
                let pesquisa = {
                  projeto_id: id,
                  mes: $('#pesquisa-mes').val(),
                  ano: $('#pesquisa-ano').val(),
                  formulario_id: $('#pesquisa-formulario').val(),
                  status: 0
                }
                // CHAMADA
                await axios.post(API_URL + '/pesquisas', pesquisa);
                datatable.pesquisas.reload();
                // DEU CERTO
                $('#modal-pesquisa').modal('hide');
                swal.close();
                swal.fire({
                  icon: 'success',
                  title: 'Excelente!',
                  text: 'A pesquisa foi adicionada com sucesso.',
                });
              }
              //
              catch (e) {
                console.log(e);
                swal.close();
                swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Algo errado não está certo.',
                  footer: 'Confira o erro no log do navegador.'
                });
              }
            }
          }
        });
        // EXIBIR MODAL
        $('#modal-pesquisa-title').html('Adicionar Pesquisa');
        $('#modal-pesquisa').modal('show');
      }
      //
      catch (e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    },
    update: async (id) => {
      try {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // CARREGAR DADOS DO CAMPANHA
        let pesquisa = (await axios.get(API_URL + '/pesquisas/' + id)).data;
        // REDEFINIR FORMULÁRIO
        $('#form-pesquisa').trigger('reset');
        $('#form-pesquisa').validate();
        // INSERIR DADOS DO CAMPANHA NO FORMULÁRIO
        $('#pesquisa-mes').val(pesquisa.mes).trigger('change');
        $('#pesquisa-ano').val(pesquisa.ano).trigger('change');
        $('#pesquisa-formulario').val(pesquisa.formulario_id).trigger('change');
        // GERENCIAR ATUALIZAÇÃO DO REGISTRO
        $('.btn-save').off();
        $('.btn-save').on('click', async function() {
          if ($('#form').valid()) {
            let confirmacao = await swal.fire({
              icon: 'warning',
              title: 'Atenção!',
              text: 'Deseja mesmo atualizar esta pesquisa?',
              showCancelButton: true,
              reverseButtons: true,
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Continuar',
              customClass: {
                confirmButton: 'btn btn-primary mt-0',
                cancelButton: 'btn btn-light mt-0'
              }
            });
            if (confirmacao.value) {
              try {
                // EXIBIR TELA DE CARREGAMENTO
                swal.fire({
                  title: 'Aguarde...',
                  text: 'Processando a solicitação.',
                  onOpen: () => {
                    swal.showLoading();
                  },
                  allowOutsideClick: false,
                  allowEscapeKey: false
                });
                // ENVIAR REQUISIÇÃO PARA O BACK-END
                let pesquisa = {
                  mes: $('#pesquisa-mes').val(),
                  ano: $('#pesquisa-ano').val(),
                  projeto_id: id,
                  formulario_id: $('#pesquisa-formulario').val(),
                  status: 0
                }
                // CHAMADA
                await axios.patch(API_URL + '/pesquisas/' + id, pesquisa);
                datatable.reload();
                // DEU CERTO
                $('#modal-pesquisa').modal('hide');
                swal.close();
                swal.fire({
                  icon: 'success',
                  title: 'Excelente!',
                  text: 'A pesquisa foi atualizada com sucesso.',
                });
              }
              //
              catch(e) {
                console.log(e);
                swal.close();
                swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Algo errado não está certo.',
                  footer: 'Confira o erro no log do navegador.'
                });
              }
            }
          }
        });
        // EXIBIR MODAL
        swal.close();
        $('#modal-pesquisa-title').html('Editar Pesquisa');
        $('#modal-pesquisa').modal('show');
      }
      //
      catch (e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    },
    delete: async (id) => {
      try {
        // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
        let pesquisa = await swal.fire({
          icon: 'warning',
          title: 'Atenção!',
          text: 'Deseja mesmo excluir esta pesquisa?',
          showCancelButton: true,
          reverseButtons: true,
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Continuar',
          customClass: {
            confirmButton: 'btn btn-primary mt-0',
            cancelButton: 'btn btn-light mt-0'
          }
        });
        // VALIDAR CONTINUAÇÃO
        if (pesquisa.value) {
          // EXIBIR TELA DE CARREGAMENTO
          swal.fire({
            title: 'Aguarde...',
            text: 'Processando a solicitação.',
            onOpen: () => {
              swal.showLoading();
            },
            allowOutsideClick: false,
            allowEscapeKey: false
          });
          // ENVIAR REQUISIÇÃO PARA O BACK-END
          let pesquisa_id = id;
          await axios.delete(API_URL + '/pesquisas/' + pesquisa_id);
          datatable.reload();
          // DEU CERTO
          swal.close();
          swal.fire({
            icon: 'success',
            title: 'Excelente!',
            text: 'A pesquisa foi excluída com sucesso.',
          });
        }
      }
      //
      catch (e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    }
  },
  credencial: {
    create: () => {
      try {
        // REDEFINIR FORMULÁRIO
        $('#form-credencial').trigger('reset');
        $('#form-credencial').validate();
        // GERENCIAR CRIAÇÃO DO REGISTRO
        $('.btn-save-credencial').off();
        $('.btn-save-credencial').on('click', async function() {
          // VALIDAR FORMULÁRIO
          if ($('#form-credencial').valid()) {
            let confirmacao = await swal.fire({
              icon: 'warning',
              title: 'Atenção!',
              text: 'Deseja mesmo adicionar esta Credencial?',
              showCancelButton: true,
              reverseButtons: true,
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Continuar',
              customClass: {
                confirmButton: 'btn btn-primary mt-0',
                cancelButton: 'btn btn-light mt-0'
              }
            });
            if (confirmacao.value) {
              try {
                // EXIBIR TELA DE CARREGAMENTO
                swal.fire({
                  title: 'Aguarde...',
                  text: 'Processando a solicitação.',
                  onOpen: () => {
                    swal.showLoading();
                  },
                  allowOutsideClick: false,
                  allowEscapeKey: false
                });
                // ENVIAR REQUISIÇÃO PARA O BACK-END
                let credencial = {
                  projeto_id: id,
                  nome: $('#credencial-nome').val(),
                  usuario: $('#credencial-usuario').val(),
                  url: $('#credencial-url').val(),
                  senha: $('#credencial-senha').val(),
                }
                await axios.post(API_URL + '/credenciais/', credencial);
                datatable.credenciais.reload();
                // DEU CERTO
                $('#modal-credencial').modal('hide');
                swal.close();
                swal.fire({
                  icon: 'success',
                  title: 'Excelente!',
                  text: 'A credencial foi adicionada com sucesso.',
                });
              }
              //
              catch (e) {
                console.log(e);
                swal.close();
                swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Algo errado não está certo.',
                  footer: 'Confira o erro no log do navegador.'
                });
              }
            }
          }
        });
        // EXIBIR MODAL
        $('#modal-title-credencial').html('Adicionar Credencial');
        $('#modal-credencial').modal('show');
      }
      //
      catch (e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    },
    update: async (id) => {
      try {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // CARREGAR DADOS DA CREDENCIAL
        let credencial = (await axios.get(API_URL + '/credenciais/' + id)).data;
        let projeto_id = credencial.projeto_id;
        // REDEFINIR FORMULÁRIO
        $('#form-credencial').trigger('reset');
        $('#form-credencial').validate();
        // INSERIR DADOS DA CREDENCIAL NO FORMULÁRIO
        $('#credencial-nome').val(credencial.nome);
        $('#credencial-url').val(credencial.url);
        $('#credencial-usuario').val(credencial.usuario);
        $('#credencial-senha').val(credencial.senha);
        // GERENCIAR ATUALIZAÇÃO DO REGISTRO
        $('.btn-save-credencial').off();
        $('.btn-save-credencial').on('click', async function() {
          if ($('#form-credencial').valid()) {
            let confirmacao = await swal.fire({
              icon: 'warning',
              title: 'Atenção!',
              text: 'Deseja mesmo atualizar esta credencial?',
              showCancelButton: true,
              reverseButtons: true,
              cancelButtonText: 'Cancelar',
              confirmButtonText: 'Continuar',
              customClass: {
                confirmButton: 'btn btn-primary mt-0',
                cancelButton: 'btn btn-light mt-0'
              }
            });
            if (confirmacao.value) {
              try {
                // EXIBIR TELA DE CARREGAMENTO
                swal.fire({
                  title: 'Aguarde...',
                  text: 'Processando a solicitação.',
                  onOpen: () => {
                    swal.showLoading();
                  },
                  allowOutsideClick: false,
                  allowEscapeKey: false
                });
                // ENVIAR REQUISIÇÃO PARA O BACK-END
                let credencial = {
                  projeto_id: projeto_id,
                  nome: $('#credencial-nome').val(),
                  usuario: $('#credencial-usuario').val(),
                  url: $('#credencial-url').val(),
                  senha: $('#credencial-senha').val(),
                }
                await axios.patch(API_URL + '/credenciais/' + id, credencial);
                datatable.credenciais.reload();
                // DEU CERTO
                $('#modal-credencial').modal('hide');
                swal.close();
                swal.fire({
                  icon: 'success',
                  title: 'Excelente!',
                  text: 'A credencial foi atualizado com sucesso.',
                });
              }
              //
              catch(e) {
                console.log(e);
                swal.close();
                swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Algo errado não está certo.',
                  footer: 'Confira o erro no log do navegador.'
                });
              }
            }
          }
        });
        // EXIBIR MODAL
        swal.close();
        $('#modal-title-credencial').html('Editar Credencial');
        $('#modal-credencial').modal('show');
      }
      //
      catch (e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    },
    delete: async (id) => {
      try {
        // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
        let cliente = await swal.fire({
          icon: 'warning',
          title: 'Atenção!',
          text: 'Deseja mesmo excluir esta credencial?',
          showCancelButton: true,
          reverseButtons: true,
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Continuar',
          customClass: {
            confirmButton: 'btn btn-primary mt-0',
            cancelButton: 'btn btn-light mt-0'
          }
        });
        // VALIDAR CONTINUAÇÃO
        if (cliente.value) {
          // EXIBIR TELA DE CARREGAMENTO
          swal.fire({
            title: 'Aguarde...',
            text: 'Processando a solicitação.',
            onOpen: () => {
              swal.showLoading();
            },
            allowOutsideClick: false,
            allowEscapeKey: false
          });
          // ENVIAR REQUISIÇÃO PARA O BACK-END
          await axios.delete(API_URL + '/credenciais/' + id);
          datatable.credenciais.reload();
          // DEU CERTO
          swal.close();
          swal.fire({
            icon: 'success',
            title: 'Excelente!',
            text: 'A credencial foi excluído com sucesso.',
          });
        }
      }
      //
      catch (e) {
        console.log(e);
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Confira o erro no log do navegador.'
        });
      }
    }
  }
}

jQuery(document).ready(async () => {
  // INICIALIZAR AS TABELAS
  datatable.atividades.init();
  datatable.relatorios.init();
  datatable.pesquisas.init();
  datatable.credenciais.init();
  // GUI
  gui.init();
  gui.atividade.init();
  gui.relatorio.init();
  gui.pesquisa.init();
  gui.credencial.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create-atividade', function() {
    api.atividade.create();
  });
  $(document).on('click', '.btn-create-relatorio', function() {
    api.relatorio.create();
  });
  $(document).on('click', '.btn-create-pesquisa', function() {
    api.pesquisa.create();
  });
  $(document).on('click', '.btn-create-credencial', function() {
    $('#credencial-senha').attr('type', 'password');
    $('#btnCopyUser').addClass('d-none');
    $('#btnCopyPass').addClass('d-none');
    $('#btnViewPass').addClass('d-none');
    $('#btnRdrtUrl').addClass('d-none');
    api.credencial.create();
  });
  //
  $(document).on('click', '.btn-update-pesquisa', function() {
    api.pesquisa.update($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-update-credencial', function() {
    $('#btnCopyUser').removeClass('d-none');
    $('#btnCopyPass').removeClass('d-none');
    $('#btnViewPass').removeClass('d-none');
    $('#btnRdrtUrl').removeClass('d-none');
    api.credencial.update($(this).attr('data-id'));
  });
  //
  $(document).on('click', '.btn-delete-relatorio', function() {
    api.relatorio.delete($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-delete-pesquisa', function() {
    api.pesquisa.delete($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-delete-credencial', function() {
    api.credencial.delete($(this).attr('data-id'));
  });
});