// http://api.ipstack.com/138.255.213.133?access_key=43f2d32b45dc5aba239bf6674f352b75

let uppyDashboard;

let tagify;
let tagify_input = document.getElementById('usuario-projetos');
let autoupdate;

let gui = {
  init: () => {
    // MÁSCARA DO TELEFONE
    let SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };
    let spOptions = {
      onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      },
      clearIfNotMatch: true
    };
    $('#usuario-telefone').mask(SPMaskBehavior, spOptions);
    // DATA DE NASCIMENTO
    $('#usuario-nascimento').datetimepicker({
      locale: 'pt-br',
      format: 'L',
      maxDate: moment()
    });
    // AVATAR
    uppyDashboard = Uppy.Core({
      restrictions: {
        maxFileSize: 1000000, // 1mb
        maxNumberOfFiles: 1,
        minNumberOfFiles: 1
      },
      locale: Uppy.locales.pt_BR
    });
    uppyDashboard.use(Uppy.Dashboard, {
      proudlyDisplayPoweredByUppy: false,
      target: '#kt_uppy .uppy-dashboard',
      inline: false,
      replaceTargetContent: true,
      height: 470,
      browserBackButtonClose: true,
      trigger: ' .uppy-btn'
    });
    uppyDashboard.use(Uppy.Webcam, {
      target: Uppy.Dashboard,
      modes: [
        'picture'
      ],
      videoConstraints: {
        facingMode: 'user',
        width: { min: 480, ideal: 800, max: 1080 },
        height: { min: 480, ideal: 800, max: 1080 }
      }
    });
    uppyDashboard.on('file-added', (file) => {
      var reader  = new FileReader();
      reader.onloadend = function() {
        $('#kt_image .image-input-wrapper').attr('style', 'background-image: url("' + reader.result + '")');
      }
      reader.readAsDataURL(file.data);
    });
    uppyDashboard.on('file-removed', (file, reason) => {
      $('#kt_image .image-input-wrapper').attr('style', 'background-image: url("/media/users/blank.png")');
    });
    $('.uppy-btn-remove').on('click', function() {
      uppyDashboard.cancelAll();
    });
    // INICIALIZAR SELECT2
    $('#usuario-funcao').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
  },
  mphone: (phone) => {
    phone = phone.replace(/\D/g,'');
    phone = phone.replace(/^(\d)/,'+$1');
    phone = phone.replace(/(.{3})(\d)/,'$1($2');
    phone = phone.replace(/(.{6})(\d)/,'$1)$2');
    //
    if (phone.length == 12) phone=phone.replace(/(.{1})$/,'-$1');
    else if (phone.length == 13) phone = phone.replace(/(.{2})$/,'-$1');
    else if (phone.length == 14) phone = phone.replace(/(.{3})$/,'-$1');
    else if (phone.length == 15) phone = phone.replace(/(.{4})$/,'-$1');
    else if (phone.length > 15) phone = phone.replace(/(.{4})$/,'-$1');
    //
    return phone.replace('(', ' (').replace(')', ') ');
  }
}

let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/back-office',
            map: (raw) => {
              raw.forEach(item => {
                item.last_login = moment(item.last_login).format('DD/MM/YYYY HH:mm:ss');
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [/*{
        field: 'id',
        title: '#',
        sortable: false,
        width: 20,
        type: 'number',
        selector: {
          class: ''
        },
        textAlign: 'center'
      }, */{
        field: 'name',
        title: 'Nome',
        autoHide: false,
        width: 200,
        template: (row) => {
          return '\
            <div class="d-flex align-items-center">\
                <div class="symbol symbol-40 symbol-sm flex-shrink-0">' +
            '<img src="' + row.picture + '" alt="' + row.name + '"/>' +
            '</div>\
                <div class="ml-4 ellipsis">' +
            row.name + '<br><small>' + (row.app_metadata.admin ? '<i class="icon-sm flaticon2-correct text-success mr-1"></i>' : '') + row.email + '</small>' +
            '</div>\
            </div>'
        }
      }, {
        field: 'telefone',
        title: 'Contato',
        width: 200,
        template: (row) => {
          return gui.mphone(row.user_metadata.telefone);
        }
      }, {
        field: 'last_login',
        title: 'Último Login',
        width: 200,
        template: (row) => {
          return row.last_login + (row.last_ip ? '<br><small class="ellipsis">' + row.last_ip + '</small>' : '');
        }
      }, {
        field: 'status',
        title: 'Status',
        textAlign: 'center',
        width: 60,
        template: (row) => {
          if (row.blocked) return '<span class="label font-weight-bold label-lg label-light-danger label-inline w-100">Inativo</span>';
          else return '<span class="label font-weight-bold label-lg label-light-success label-inline w-100">Ativo</span>';
        }
      }, {
        field: 'Actions',
        title: '',
        sortable: false,
        width: 50,
        autoHide: false,
        overflow: 'visible',
        template: (row) => {
          return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover ">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">\
                            CONFIGURAÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-update" data-id="' + row.user_id + '">\
                                <span class="navi-icon"><i class="la la-pencil"></i></span>\
                                <span class="navi-text">Editar</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-delete" data-id="' + row.user_id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">\
                            OPERAÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-password" data-id="' + row.user_id + '">\
                                <span class="navi-icon"><i class="la la-key"></i></span>\
                                <span class="navi-text">Alterar Senha</span>\
                            </span>\
                        </li>' + 
                        /*<li class="navi-item">\
                            <span class="navi-link btn-log" data-id="' + row.user_id + '" data-label="' + row.name + '">\
                                <span class="navi-icon"><i class="la la-binoculars"></i></span>\
                                <span class="navi-text">Histórico</span>\
                            </span>\
                        </li>\*/
                    '</ul>\
                </div>\
            </div>';
        }
      }],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let custom = {
  password: async (id) => {
    try {
      let { value: password } = await swal.fire({
        icon: 'question',
        title: 'Vamos lá!',
        text: 'Qual senha definir para o usuário?',
        input: 'password',
        inputPlaceholder: 'Digite a senha.',
        inputAttributes: {
          // maxlength: 10,
          autocapitalize: 'off',
          autocorrect: 'off'
        },
        inputValidator: (value) => {
          return new Promise((resolve) => {
            if (value) resolve();
            else resolve('Você precisa inserir uma senha.');
          });
        },
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      if (password) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // CHAMADA
        await axios.put(API_URL + '/back-office/password/' + id, {
          password: password
        });
        datatable.reload();
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A senha foi definida com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

let api = {
  create: async () => {
    try {
      // CARREGAR DADOS DOS PROJETOS
      if (tagify) {
        tagify.destroy();
        tagify = null;
      }
      let projetos = (await axios.get(API_URL + '/projetos')).data.map(projeto => {
        return {
          id: projeto.id,
          value: projeto.nome
        }
      });
      setTimeout(() => {
          tagify = new Tagify(tagify_input, {
            enforceWhitelist: true,
            whitelist: projetos
          });
      }, 500);
      // REDEFINIR FORMULÁRIO
      $('#form').trigger('reset');
      $('#usuario-funcao').trigger('change');
      $('#form').validate();
      // GERENCIAR CRIAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        // VALIDAR FORMULÁRIO
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo adicionar este usuario?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let nascimento = $('#usuario-nascimento').find('input').val();
              //
              let dia  = nascimento.substring(0, 2);
              let mes  = nascimento.substring(3, 5);
              let ano  = nascimento.substring(6, 10);
              let hora = nascimento.substring(11, 16);
              //
              let usuario = {
                nome: $('#usuario-nome').val(),
                funcao: $('#usuario-funcao').val(),
                email: $('#usuario-email').val(),
                telefone: '55' + $('#usuario-telefone').cleanVal(),
                nascimento: ano + '-' + mes + '-' + dia,
                projetos: $('#usuario-projetos').val() ? JSON.parse($('#usuario-projetos').val()).map(projeto => projeto.id) : '',
                status: $('#usuario-status').prop('checked'),
                admin: $('#usuario-admin').prop('checked')
              }
              let formData = new FormData();
              // DADOS
              Object.keys(usuario).forEach(function(key) {
                formData.append(key, usuario[key]);
              });
              // ANEXO
              if (uppyDashboard.getFiles().length > 0) formData.append('file', uppyDashboard.getFiles()[0].data);
              // CHAMADA
              await axios.post(API_URL + '/back-office/', formData, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
              });
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'O usuario foi adicionado com sucesso.',
              });
            }
            //
            catch (e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      $('#modal-title').html('Adicionar Usuário');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  update: async (id) => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });      
      // CARREGAR DADOS DO USUÁRIO
      let usuario = (await axios.get(API_URL + '/back-office/' + id)).data;
      // CARREGAR DADOS DOS PROJETOS
      if (tagify) {
        tagify.destroy();
        tagify = null;
      }
      let projetos = (await axios.get(API_URL + '/projetos')).data.map(projeto => {
        return {
          id: projeto.id,
          value: projeto.nome
        }
      });
      setTimeout(() => {
        tagify = new Tagify(tagify_input, {
          enforceWhitelist: true,
          whitelist: projetos
        });
        if (usuario.app_metadata.projetos.length > 0) tagify.addTags((projetos.filter(projeto => (usuario.app_metadata.projetos.split(',')).includes(projeto.id.toString()))));
      }, 500);
      // REDEFINIR FORMULÁRIO
      $('#form').trigger('reset');
      $('#form').validate();
      // INSERIR DADOS DO USUÁRIO NO FORMULÁRIO
      $('#kt_image .image-input-wrapper').attr('style', 'background-image: url("' + usuario.picture + '")');
      $('#usuario-nome').val(usuario.name);
      $('#usuario-funcao').val(usuario.app_metadata.funcao).trigger('change');
      $('#usuario-status').prop('checked', usuario.blocked ? false : true);
      $('#usuario-admin').prop('checked', usuario.app_metadata.admin);
      $('#usuario-email').val(usuario.email);
      $('#usuario-telefone').val($('#usuario-telefone').masked(usuario.user_metadata.telefone.replace('55', '')));
      $('#usuario-nascimento').find('input').val(moment(usuario.user_metadata.nascimento).format('DD/MM/YYYY'));
      // GERENCIAR ATUALIZAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo atualizar este usuario?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let nascimento = $('#usuario-nascimento').find('input').val();
              //
              let dia  = nascimento.substring(0, 2);
              let mes  = nascimento.substring(3, 5);
              let ano  = nascimento.substring(6, 10);
              let hora = nascimento.substring(11, 16);
              //
              let usuario = {
                nome: $('#usuario-nome').val(),
                funcao: $('#usuario-funcao').val(),
                email: $('#usuario-email').val(),
                telefone: '55' + $('#usuario-telefone').cleanVal(),
                nascimento: ano + '-' + mes + '-' + dia,
                projetos: $('#usuario-projetos').val() ? JSON.parse($('#usuario-projetos').val()).map(projeto => projeto.id) : '',
                status: $('#usuario-status').prop('checked'),
                admin: $('#usuario-admin').prop('checked')
              }
              let formData = new FormData();
              // DADOS
              Object.keys(usuario).forEach(function(key) {
                formData.append(key, usuario[key]);
              });
              // ANEXO
              if (uppyDashboard.getFiles().length > 0) formData.append('file', uppyDashboard.getFiles()[0].data);
              // CHAMADA
              await axios.put(API_URL + '/back-office/' + id, formData, {
                headers: {
                  'Content-Type': 'multipart/form-data'
                }
              });
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'O usuario foi atualizado com sucesso.',
              });
            }
            //
            catch(e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      swal.close();
      $('#modal-title').html('Editar Usuário');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let usuario = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir este usuario?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (usuario.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let usuario_id = id;
        await axios.delete(API_URL + '/back-office/' + usuario_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'O usuario foi excluído com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  gui.init();
  // INICIALIZAR A TABELA
  datatable.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create', function() {
    api.create();
  });
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete($(this).attr('data-id'));
  });
  // CONFIGURAR CUSTOM
  $(document).on('click', '.btn-password', function() {
    custom.password($(this).attr('data-id'));
  });
});