function mphone (phone) {
  phone = phone.replace(/\D/g,'');
  phone = phone.replace(/^(\d)/,'+$1');
  phone = phone.replace(/(.{3})(\d)/,'$1($2');
  phone = phone.replace(/(.{6})(\d)/,'$1)$2');
  //
  if (phone.length == 12) phone=phone.replace(/(.{1})$/,'-$1');
  else if (phone.length == 13) phone = phone.replace(/(.{2})$/,'-$1');
  else if (phone.length == 14) phone = phone.replace(/(.{3})$/,'-$1');
  else if (phone.length == 15) phone = phone.replace(/(.{4})$/,'-$1');
  else if (phone.length > 15) phone = phone.replace(/(.{4})$/,'-$1');
  //
  return phone.replace('(', ' (').replace(')', ') ');
}

jQuery(document).ready(async () => {
  try {
    // EXIBIR TELA DE CARREGAMENTO
    swal.fire({
      title: 'Aguarde...',
      text: 'Processando a solicitação.',
      onOpen: () => {
        swal.showLoading();
      },
      allowOutsideClick: false,
      allowEscapeKey: false
    });
    let relatorio = (await axios.get(API_URL + '/relatorios/' + id)).data;
    swal.close();

    /*
     * TAB 1 - INÍCIO
     */
    
    // MANIPULAÇÃO
    relatorio.projeto.imagens = JSON.parse(relatorio.projeto.imagens);
    if (relatorio.cliente.tipo == 'PF') relatorio.cliente.id_mf = relatorio.cliente.id_mf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '\$1.\$2.\$3\-\$4');
    if (relatorio.cliente.tipo == 'PJ') relatorio.cliente.id_mf = relatorio.cliente.id_mf.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, '\$1.\$2.\$3\/\$4\-\$5');
    // THUMBNAIL DO PROJETO
    if (relatorio.projeto.imagens.thumbnail) $('#thumbnail').html('<div class="symbol symbol-80"><img alt="' + relatorio.projeto.nome + '" src="' + relatorio.projeto.imagens.thumbnail + '"></div>');
    // IDENTIFICAÇÃO
    $('.projeto-nome').html(relatorio.projeto.nome);
    $('.cliente-nome').html(relatorio.cliente.nome + ' <small>' + relatorio.cliente.id_mf + '</small>');
    //
    let gerente = '\
      <div class="d-flex align-items-center">\
          <div class="symbol symbol-circle symbol-80 symbol-sm flex-shrink-0">' +
            '<img src="' + relatorio.gerente.picture + '" alt="' + relatorio.gerente.name + '"/>' +
          '</div>\
          <div class="ml-4">' +
            '<span style="font-family: \'Just Me Again Down Here\', cursive; font-size: 26px;">' + relatorio.gerente.name.split(' ').slice(0, 3).join(' ') + '</span><br><a href="https://api.whatsapp.com/send?1=pt_br&phone=' + relatorio.gerente.user_metadata.telefone + '" target="_blank">' + mphone(relatorio.gerente.user_metadata.telefone) + '</a><br><small><a href="mailto:' + relatorio.gerente.email + '">' + (relatorio.gerente.email_verified ? '<i class="icon-sm flaticon2-correct text-success mr-1"></i>' : '') + relatorio.gerente.email + '</a></small>' +
          '</div>\
      </div>';
    $('#gerente').html(gerente);
    //
    let calendario = {
      1:  'Janeiro',
      2:  'Fevereiro',
      3:  'Março',
      4:  'Abril',
      5:  'Maio',
      6:  'Junho',
      7:  'Julho',
      8:  'Agosto',
      9:  'Setembro',
      10: 'Outubro',
      11: 'Novembro',
      12: 'Dezembro'
    }
    $('.mes-ano').html(calendario[relatorio.mes] + '/' + relatorio.ano);
    
    /*
     * TAB 2 - DESTAQUES DO MÊS
     */
    
    // 1 - VISÃO GERAL
    $('#visao-geral').html(relatorio.visao_geral);
    // 2 - FUNIL DE VENDAS
    relatorio.funil = JSON.parse(relatorio.funil);
    let funil = new FunnelGraph({
      container: '#funil',
      gradientDirection: 'horizontal',
      data: {
        labels: ['Visitantes', 'Leads', 'Oportunidades', 'Vendas'],
        colors: ['#2a004e', '#ab20fd'],
        values: [relatorio.funil.visitantes, relatorio.funil.leads, relatorio.funil.oportunidades, relatorio.funil.vendas]
      },
      displayPercent: true,
      direction: 'horizontal',
      width: 800,
      height: 300
    });
    funil.draw();
    let funil_responsivo = new FunnelGraph({
      container: '#funil-responsivo',
      gradientDirection: 'vertical',
      data: {
        labels: ['Visitantes', 'Leads', 'Oportunidades', 'Vendas'],
        colors: ['#2a004e', '#ab20fd'],
        values: [relatorio.funil.visitantes, relatorio.funil.leads, relatorio.funil.oportunidades, relatorio.funil.vendas]
      },
      displayPercent: true,
      direction: 'vertical',
      width: 150,
      height: 400
    });
    funil_responsivo.draw();
    $('#funil-comentarios').html(relatorio.funil_comentarios);
    // 3 - PROCESSO DE COMPRA
    relatorio.projeto.tags = JSON.parse(relatorio.projeto.tags);
    if (relatorio.projeto.tags.includes('Nuvemshop')) $('#processo-de-compra-titulo').append('<img class="max-h-30px float-right d-none d-sm-block" alt="Nuvemshop" src="/media/integrations/logo-nuvemshop.png">');
    if (relatorio.projeto.tags.includes('Shopify')) $('#processo-de-compra-titulo').append('<img class="max-h-30px float-right d-none d-sm-block" alt="Shopify" src="/media/integrations/logo-shopify.png">');
    relatorio.processo_de_compra = JSON.parse(relatorio.processo_de_compra);
    let processo_de_compra = new FunnelGraph({
      container: '#processo-de-compra',
      gradientDirection: 'horizontal',
      data: {
        labels: ['Carrinho Criado', 'Início Compra', 'Endereço Preenchido', 'Pedido Criado'],
        colors: ['#2a004e', '#ab20fd'],
        values: [relatorio.processo_de_compra.carrinho_criado, relatorio.processo_de_compra.inicio_compra, relatorio.processo_de_compra.endereco_preenchido, relatorio.processo_de_compra.pedido_criado]
      },
      displayPercent: true,
      direction: 'horizontal',
      width: 800,
      height: 300
    });
    processo_de_compra.draw();
    let processo_de_compra_responsivo = new FunnelGraph({
      container: '#processo-de-compra-responsivo',
      gradientDirection: 'vertical',
      data: {
        labels: ['Carrinho Criado', 'Início Compra', 'Endereço Preenchido', 'Pedido Criado'],
        colors: ['#2a004e', '#ab20fd'],
        values: [relatorio.processo_de_compra.carrinho_criado, relatorio.processo_de_compra.inicio_compra, relatorio.processo_de_compra.endereco_preenchido, relatorio.processo_de_compra.pedido_criado]
      },
      displayPercent: true,
      direction: 'vertical',
      width: 150,
      height: 400
    });
    processo_de_compra_responsivo.draw();
    // 4 - MÉTRICAS DE AQUISIÇÃO
    $('#aquisicao-comentarios').html(relatorio.aquisicao_comentarios);
    // 4.1 - VISÃO CONSOLIDADA
    relatorio.aquisicao_visao_consolidada = JSON.parse(relatorio.aquisicao_visao_consolidada);
    // PEDIDOS PAGOS
    $('#pedidos-pagos-quantidade').html(relatorio.aquisicao_visao_consolidada.pedidos_pagos.quantidade.toLocaleString('pt-BR'));
    $('#pedidos-pagos-faturamento').html(relatorio.aquisicao_visao_consolidada.pedidos_pagos.faturamento.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
    $('#pedidos-pagos-ticket-medio').html((relatorio.aquisicao_visao_consolidada.pedidos_pagos.faturamento / relatorio.aquisicao_visao_consolidada.pedidos_pagos.quantidade).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
    // PEDIDOS REALIZADOS
    $('#pedidos-realizados-quantidade').html(relatorio.aquisicao_visao_consolidada.pedidos_realizados.quantidade.toLocaleString('pt-BR'));
    $('#pedidos-realizados-faturamento').html(relatorio.aquisicao_visao_consolidada.pedidos_realizados.faturamento.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
    $('#pedidos-realizados-ticket-medio').html((relatorio.aquisicao_visao_consolidada.pedidos_realizados.faturamento / relatorio.aquisicao_visao_consolidada.pedidos_realizados.quantidade).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
    // PEDIDOS REALIZADOS VS. PEDIDOS PAGOS
    let aquisicao_visao_consolidada = new FunnelGraph({
      container: '#aquisicao-visao-consolidada',
      gradientDirection: 'horizontal',
      data: {
        labels: ['Pedidos Realizados', 'Pedidos Pagos'],
        colors: ['#2a004e', '#ab20fd'],
        values: [relatorio.aquisicao_visao_consolidada.pedidos_realizados.faturamento, relatorio.aquisicao_visao_consolidada.pedidos_pagos.faturamento]
      },
      displayPercent: true,
      direction: 'horizontal',
      width: 800,
      height: 300
    });
    aquisicao_visao_consolidada.draw();
    let aquisicao_visao_consolidada_responsivo = new FunnelGraph({
      container: '#aquisicao-visao-consolidada-responsivo',
      gradientDirection: 'vertical',
      data: {
        labels: ['Pedidos Realizados', 'Pedidos Pagos'],
        colors: ['#2a004e', '#ab20fd'],
        values: [relatorio.aquisicao_visao_consolidada.pedidos_realizados.faturamento, relatorio.aquisicao_visao_consolidada.pedidos_pagos.faturamento]
      },
      displayPercent: true,
      direction: 'vertical',
      width: 150,
      height: 400
    });
    aquisicao_visao_consolidada_responsivo.draw();
    // 4.2 - TRÁFEGO PAGO
    relatorio.aquisicao_trafego_pago = JSON.parse(relatorio.aquisicao_trafego_pago);
    let trafego_pago = (
      '<table class="table table-responsive-kampana table-bordered mt-10 text-center">' +
      '<thead><tr><td>Tipo</td><td>Pedidos</td><td>Investimento</td><td>Faturamento</td><td>Ticket Médio</td><td>ROAS</td></tr>' +
      '<tbody>' + relatorio.aquisicao_trafego_pago.map(trafego => {
        return '<tr><td>' + trafego.tipo + '</td><td>' + trafego.pedidos.toLocaleString('pt-BR') + '</td><td>' + trafego.investimento.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) + '</td><td>' + trafego.faturamento.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) + '</td><td>' + (trafego.faturamento / trafego.pedidos).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) + '</td><td>' + parseFloat((trafego.faturamento / trafego.investimento).toFixed(2)).toLocaleString('pt-BR');
      }).join('') + '</tbody>' +
      '</table>' +
      '<div class="d-block d-sm-none text-center"><small><span class="label pulse mr-5"><span class="position-relative">↔</span><span class="pulse-ring"></span></span>Deslize lateralmente para visualizar a tabela.</small></div>'
    );
    $('#trafego-pago').html(trafego_pago);
    
    /*
     * TAB 3 - MÍDIA PAGA
     */
    
    relatorio.midia_paga = JSON.parse(relatorio.midia_paga);
    let index = 5;
    relatorio.midia_paga.forEach((midia, i) => {
      index = 5 + i;
      // TÍTULO - FACEBOOK ADS
      if (midia.tipo == 'Facebook Ads') $('#midia-paga').append('<h3>' + (5 + i) + '. ' + midia.tipo + '<img class="max-h-30px float-right d-none d-sm-block" alt="Facebook" src="/media/integrations/logo-facebook.png"></h3>');
      // TÍTULO - GOOGLE ADFS
      else if (midia.tipo == 'Google Ads') $('#midia-paga').append('<h3>' + (5 + i) + '. ' + midia.tipo + '<img class="max-h-30px float-right d-none d-sm-block" alt="Facebook" src="/media/integrations/logo-google.png"></h3>');
      // TÍTULO - GENÉRICO
      else $('#midia-paga').append('<h3>' + index + '. ' + midia.tipo + '</h3>');
      // TÍTULO - SEPARADOR
      $('#midia-paga').append('<div class="separator separator-solid my-5"</div>');
      // COMENTÁRIOS
      $('#midia-paga').append(midia.comentarios);
      // TABELA
      let campanhas = (
        '<table class="table table-responsive-kampana table-bordered mt-10 text-center">' +
        '<thead><tr><td>Campanha</td><td>Alcance</td><td>Impressões</td><td>Resultado</td><td>Investimento</td><td>Custo por Resultado</td><td>Receita</td><td>ROAS</td></tr>' +
        '<tbody>' + midia.campanhas.map(campanha => {
          return '<tr><td>' + campanha.tipo + '</td><td>' + campanha.alcance.toLocaleString('pt-BR') + '</td><td>' + campanha.impressoes.toLocaleString('pt-BR') + '</td><td>' + campanha.resultado.toLocaleString('pt-BR') + '<br><small>' + campanha.resultado_indicador + '</small></td><td>' + campanha.investimento.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) + '</td><td>' + (campanha.investimento / campanha.resultado).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) + '</td><td>' + (campanha.receita ? campanha.receita.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) : 'N/A') + '</td><td>' + parseFloat((campanha.receita / campanha.investimento).toFixed(2)).toLocaleString('pt-BR') + '</td>'
        }).join('') + '</tbody>' +
        '</table>' +
        '<div class="d-block d-sm-none text-center"><small><span class="label pulse mr-5"><span class="position-relative">↔</span><span class="pulse-ring"></span></span>Deslize lateralmente para visualizar a tabela.</small></div>'
      );
      if (midia.tipo == 'Facebook Ads' && midia.ads_library) campanhas += '<div class="text-center pt-5"><a class="btn btn-primary btn-sm" href="' + midia.ads_library + '" target="_blank">Biblioteca de Anúncios</a></div>';
      $('#midia-paga').append(campanhas);
    });
    
    /*
     * TAB 4 - LANDING PAGES
     */
    
    index++;
    $('.landing-pages-index').html(index);
    $('#landing-pages-comentarios').html(relatorio.landing_pages_comentarios);
    relatorio.landing_pages = JSON.parse(relatorio.landing_pages);
    //relatorio.landing_pages_destaques = JSON.parse(relatorio.landing_pages_destaques);
    let landing_pages = (
      '<table class="table table-responsive-kampana table-bordered mt-10 text-center">' +
      '<thead><tr><td>Nome</td><td>Visitantes</td><td>Leads</td><td>Taxa de Conversão</td></tr>' +
      '<tbody>' + relatorio.landing_pages.colecoes.map(landing_page => {
        return '<tr><td>' + landing_page.nome + '</td><td>' + landing_page.visitantes.toLocaleString('pt-BR') + '</td><td>' + landing_page.leads.toLocaleString('pt-BR') + '</td><td>' + parseFloat(((landing_page.leads / landing_page.visitantes) * 100).toFixed(2)).toLocaleString('pt-BR') + '%</td>';
      }).join('') + '</tbody>' +
      '</table>' +
      '<div class="d-block d-sm-none text-center"><small><span class="label pulse mr-5"><span class="position-relative">↔</span><span class="pulse-ring"></span></span>Deslize lateralmente para visualizar a tabela.</small></div>'
    );
    $('#landing-pages').html(landing_pages);
    let urls = [];
    relatorio.landing_pages.destaques.forEach(landing_page => {
      urls = urls.concat(landing_page);
    });
    let size_lp = 0;
    if (urls.length == 1) size_lp = 12;
    if ((urls.length % 2) == 0) size_lp = 6;
    else size_lp = 4;
    urls.forEach(url => {
      $('#landing-pages-destaques').append(
        '<div class="col-md-' + size_lp + '">' +
        //
        '<div class="css-mb">\
          <div class="mb-display-position">\
            <div class="mb-display">\
              <div class="mb-screen-position">\
                <div class="mb-screen">\
                  <iframe src="' + url + '" frameborder="0"></iframe>\
                </div>\
              </div>\
            </div>\
          </div>\
          <div class="mb-body"></div>\
          <div class="mb-bottom-cover"></div>\
        </div>' +
        //
        '<div class="text-center pt-5"><a class="btn btn-primary btn-sm" href="' + url + '" target="_blank">Acessar</a></div>' +
        //
        '</div>'
      );
    })
    
    /*
     * TAB 5 - E-MAIL MARKETING
     */
    
    index++;
    $('.email-marketing-index').html(index);
    relatorio.email_marketing = JSON.parse(relatorio.email_marketing);
    $('#emails-enviados').html(relatorio.email_marketing.campanhas.length.toLocaleString('pt-BR'));
    $('#emails-entregues').html(relatorio.email_marketing.campanhas.reduce((a, b) => a + b.emails_entregues, 0).toLocaleString('pt-BR'));
    $('#emails-abertos').html(Math.round(relatorio.email_marketing.campanhas.reduce((a, b) => a + b.emails_entregues * b.taxa_de_abertura, 0)).toLocaleString('pt-BR'));
    $('#emails-taxa-de-abertura').html(parseFloat(((relatorio.email_marketing.campanhas.reduce((a, b) => a + b.taxa_de_abertura, 0) / relatorio.email_marketing.campanhas.length) * 100).toFixed(2)).toLocaleString('pt-BR') + '%');
    // X.1
    let size_email = 0;
    if (relatorio.email_marketing.destaques.length == 1) size_email = 12;
    if ((relatorio.email_marketing.destaques.length % 2) == 0) size_email = 6;
    else size_email = 4;
    relatorio.email_marketing.destaques.forEach(url => {
      $('#email-destaques').append('<div class="col-md-' + size_email + '"><img class="img-fluid shadow" alt="E-mail Marketing" src="' + url + '"></div>');
    });
    // X.2
    let campanhas = (
      '<table class="table table-responsive-kampana table-bordered my-10 text-center">' +
      '<thead><tr><td>Campanha</td><td>E-mails Entregues</td><td>Taxa de Entrega</td><td>Taxa de Abertura</td><td>Taxa de Cliques</td></tr>' +
      '<tbody>' + relatorio.email_marketing.campanhas.map(campanha => {
        return '<tr><td>' + campanha.nome + '</td><td>' + campanha.emails_entregues.toLocaleString('pt-BR') + '</td><td>' + parseFloat((campanha.taxa_de_entrega * 100).toFixed(2)).toLocaleString('pt-BR') + '%</td><td>' + parseFloat((campanha.taxa_de_abertura * 100).toFixed(2)).toLocaleString('pt-BR') + '%</td><td>' + parseFloat((campanha.taxa_de_cliques * 100).toFixed(2)).toLocaleString('pt-BR') + '%';
      }).join('') + '</tbody>' +
      '</table>' +
      '<div class="d-block d-sm-none text-center"><small><span class="label pulse mr-5"><span class="position-relative">↔</span><span class="pulse-ring"></span></span>Deslize lateralmente para visualizar a tabela.</small></div>'
    );
    $('#email-marketing-campanhas').append(campanhas);
    // X.3
    $('#email-marketing-comentarios').html(relatorio.email_marketing_comentarios);
    
    /*
     * TAB 6 - REDES SOCIAIS
     */
    
    relatorio.redes_sociais = JSON.parse(relatorio.redes_sociais);
    
    index++;
    $('.redes-sociais-index').html(index);
    
    $('#redes-sociais-campanhas').html(relatorio.redes_sociais.campanhas);
    $('#redes-sociais-pecas-de-feed').html(relatorio.redes_sociais.pecas_de_feed);
    $('#redes-sociais-pecas-de-stories').html(relatorio.redes_sociais.pecas_de_stories);
    $('#redes-sociais-videos-gifs').html(relatorio.redes_sociais.videos_gifs);
    $('#redes-sociais-criativos').html(relatorio.redes_sociais.pecas_de_feed + relatorio.redes_sociais.pecas_de_stories + relatorio.redes_sociais.videos_gifs);
    
    // X.1
    let size_social = 0;
    if (relatorio.redes_sociais.destaques.length == 1) size_social = 12;
    if ((relatorio.redes_sociais.destaques.length % 2) == 0) size_social = 6;
    else size_social = 4;
    relatorio.redes_sociais.destaques.forEach(url => {
      $('#redes-sociais-destaques').append('<div class="col-md-' + size_social + '"><img class="img-fluid shadow" alt="Redes Sociais" src="' + url + '"></div>');
    });
    
    /*
     * TAB 7 - BACKSTAGE
     */
    
    // $('#backstage').html(relatorio.backstage);
    
    relatorio.atividades.forEach(atividade => {
      atividade.leader = JSON.parse(atividade.leader);
      atividade.squad = JSON.parse(atividade.squad) || [];
      atividade.data  = moment(atividade.data).format('DD/MM/YYYY');
    });
    
    $('#backstage-table').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'local',
        source: relatorio.atividades
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: false,
      // DEFINIR FILTROS
      search: {
        input: $('#backstage-table-search-query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
        field: 'id',
        title: '#',
        autoHide: false,
        width: 30
      }, {
        field: 'nome',
        title: 'Atividade',
        autoHide: false,
        // width: 900,
        template: (row) => {
          let template = row.nome;
          if (row.descricao) template += '<br><small>' + row.descricao + '</small>';
          template += '<br><span class="label label-sm label-light-success label-inline mr-2">' + row.leader.value + '</span>'
          if (row.squad.length > 0) template += row.squad.map(tag => '<span class="label label-sm label-light-info label-inline mr-2">' + tag.value + '</span>').join('');
          return template;
        }
      }, {
        field: 'data',
        title: 'Data',
        autoHide: false,
        width: 100
      }],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
    
    /*
     * TAB 8 - PRÓXIMAS AÇÕES
     */
    
    $('#proximas-acoes').html(relatorio.proximas_acoes);
    
    /*
     * ETC
     */
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      // NAVEGAÇÃO INFERIOR - NEXT
      let next = $(e.target).parent().next().children().attr('href');
      $('.btn-next').attr('disabled', true).off();
      if (next) $('.btn-next').attr('disabled', false).on('click', function() {
        $('a.nav-link[data-toggle="tab"][href="' + next + '"]').click();
      });
      else $('.btn-next').attr('disabled', true);
      // NAVEGAÇÃO INFERIOR - PREV
      let prev = $(e.target).parent().prev().children().attr('href');
      $('.btn-prev').off();
      if (prev) $('.btn-prev').attr('disabled', false).on('click', function() {
        $('a.nav-link[data-toggle="tab"][href="' + prev + '"]').click();
      });
      else $('.btn-prev').attr('disabled', true);
      // RETORNAR AO TOPO DA PÁGINA
      $('#kt_scrolltop').click();
      // DATATABLE DE BACKSTAGE
      if ($(this).attr('href') == '#kt_tab_pane_7') $('#backstage-table').KTDatatable().reload();
    });
    $('a.nav-link.active[data-toggle="tab"]').trigger('shown.bs.tab');
  }
  // ERRO AO CARREGAR O RELATÓRIO
  catch(e) {
    console.log(e);
    swal.close();
    swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Algo errado não está certo.',
      footer: 'Confira o erro no log do navegador.'
    });
  }
});