jQuery(document).ready(async () => {
  try {
    // EXIBIR TELA DE CARREGAMENTO
    swal.fire({
      title: 'Aguarde...',
      text: 'Processando a solicitação.',
      onOpen: () => {
        swal.showLoading();
      },
      allowOutsideClick: false,
      allowEscapeKey: false
    });
    let pesquisa = (await axios.get(API_URL + '/pesquisas/' + id)).data;
    // TÍTULO DA PESQUISA
    $('#title').html(pesquisa.formulario.nome + ' <small>' + pesquisa.cliente.nome + '</small>');
    $('#subtitle').html('<i class="flaticon2-calendar-9"></i> <small>Referência</small> ' + pesquisa.mes + '/' + pesquisa.ano);
    // PESQUISA ABERTA
    if (pesquisa.status == 0) {
      $('#subtitle').addClass('btn-info');
      //
      $('#subtitle').attr('data-toggle', 'tooltip');
      $('#subtitle').attr('data-placement', 'bottom');
      $('#subtitle').attr('data-html', 'true');
      $('#subtitle').attr('title', '<small>DÚVIDAS?</small><br>Fale com seu Gerente!');
      $('[data-toggle="tooltip"]').tooltip();
      //
      Formio.createForm(document.getElementById('formio'), JSON.parse(pesquisa.formulario.conteudo), {
        language: 'pt',
        i18n: {
          'pt': {
            error: 'Corrija os seguintes erros antes de enviar.',
            required: '{{field}} * Obrigatório.',
            complete: 'Submissão Completa'
          }
        }
      }).then(function(form) {
        swal.close();
        // ENVIAR 
        $('.btn-submit').on('click', function() {
          form.submit();
        });
        form.on('submit', async (submission) => {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Confirma o envio das respostas?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Voltar',
            confirmButtonText: 'Enviar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              let pesquisa = {
                respostas: JSON.stringify(submission.data),
                status: 1
              }
              //
              await axios.patch(API_URL + '/pesquisas/' + id, pesquisa);
              // DEU CERTO
              swal.close();
              await swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'A pesquisa foi enviada com sucesso.',
              });
              location.reload();
            }
            // ERRO AO ENVIAR A PESQUISA
            catch (e) {
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        });
        // ERRO
        form.on('error', (errors) => {
          window.scrollTo(0, 0);
        });
      });
      // EXIBIR BOTÃO DE SUBMISSÃO
      $('.card-footer').removeClass('d-none');
    }
    // PESQUISA CONCLUÍDA
    else {
      $('#subtitle').addClass('btn-success');
      //
      $('#subtitle').attr('data-toggle', 'tooltip');
      $('#subtitle').attr('data-placement', 'bottom');
      $('#subtitle').attr('data-html', 'true');
      $('#subtitle').attr('title', '<small>RESPONDIDA EM</small><br>' + moment(pesquisa.updated_at).format('DD/MM/YYYY - HH:mm:ss'));
      $('[data-toggle="tooltip"]').tooltip();
      // NÃO EXIBIR AS PESQUISAS PRIVADAS
      if (pesquisa.formulario.privado) {
        $('.alert').removeClass('d-none');
        $('.card-footer').addClass('d-none');
        swal.close();
      }
      //
      else {
        Formio.createForm(document.getElementById('formio'), JSON.parse(pesquisa.formulario.conteudo), {
          readOnly: true
        }).then(function(form) {
          form.submission = {
            data: JSON.parse(pesquisa.respostas)
          };
          // EXIBIR MENSAGEM DE RESPONDIDA
          $('.alert').removeClass('d-none');
          // OCULTAR BOTÃO DE SUBMISSÃO
          $('.card-footer').addClass('d-none');
          swal.close();
        });
      }
    }
  }
  // ERRO AO CARREGAR A PESQUISA
  catch (e) {
    console.log(e);
    swal.close();
    swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Algo errado não está certo.',
      footer: 'Confira o erro no log do navegador.'
    });
  }
});