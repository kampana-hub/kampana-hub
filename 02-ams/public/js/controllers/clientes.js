let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/clientes',
            map: (raw) => {
              raw.forEach(item => {
                item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
                item.updated_at = moment(item.updated_at).format('DD/MM/YYYY HH:mm:ss');
                if (item.tipo == 'PF') item.id_mf = item.id_mf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '\$1.\$2.\$3\-\$4');
                if (item.tipo == 'PJ') item.id_mf = item.id_mf.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, '\$1.\$2.\$3\/\$4\-\$5');
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
          field: 'id',
          title: '#',
          sortable: false,
          width: 20,
          type: 'number',
          selector: {
            class: ''
          },
          textAlign: 'center'
        }, {
          field: 'nome',
          title: 'Nome',
          width: 250,
          template: (row) => {
            return row.nome + '<br><small>' + row.tipo + ' - ' + row.id_mf + '</small>';
          }
        }, {
          field: 'updated_at',
          title: 'Detalhes',
          width: 400,
          template: (row) => {
            if (row.projetos.length == 1) return 'Cliente criado em ' + row.created_at + '.<br><small>' + row.projetos.length + ' Projeto Associado</small>';
            else if (row.projetos.length > 1) return 'Cliente criado em ' + row.created_at + '.<br><small>' + row.projetos.length + ' Projetos Associados</small>';
            else return 'Cliente criado em ' + row.created_at + '.';
          }
        }, {
          field: 'status',
          title: 'Status',
          textAlign: 'center',
          width: 60,
          template: (row) => {
            if (row.status) return '<span class="label font-weight-bold label-lg label-light-success label-inline w-100">Ativo</span>';
            else return '<span class="label font-weight-bold label-lg label-light-danger label-inline w-100">Inativo</span>';
          }
        }, {
          field: 'Actions',
          title: '',
          sortable: false,
          width: 50,
          autoHide: false,
          overflow: 'visible',
          template: (row) => {
            return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover py-2">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">\
                            ESCOLHA UMA OPÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-update" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-pencil"></i></span>\
                                <span class="navi-text">Editar</span>\
                            </span>\
                        </li>' + (row.projetos.length == 0 ?
                        '<li class="navi-item">\
                            <span class="navi-link btn-delete" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>' : '') +
                    '</ul>\
                </div>\
            </div>';
          }
        }
      ],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let gui = {
  init: () => {
    $('#cliente-tipo').on('change', function() {
      // LIMPAR CAMPO
      $('#cliente-id-mf').val('');
      // OPÇÕES DA MÁSCARA
      let options = {
        clearIfNotMatch: true,
        everse: true
      }
      // ALTERAR MÁSCARA
      if ($(this).val() == 'PF') $('#cliente-id-mf').mask('000.000.000-00', options);
      else if ($(this).val() == 'PJ') $('#cliente-id-mf').mask('00.000.000/0000-00', options);
      else $('#cliente-id-mf').unmask();
    });
    // MÁSCARA DO TELEFONE
    let SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };
    let spOptions = {
      onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      },
      clearIfNotMatch: true
    };
    $('#cliente-telefone').mask(SPMaskBehavior, spOptions);
    $('#cliente-whatsapp').mask(SPMaskBehavior, spOptions);
    // MÁSCARA DO CEP
    let SPMaskBehaviorCep = function (val) {
      return '00000-000'
    };
    let spOptionsCep = {
      onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehaviorCep.apply({}, arguments), options);
      },
      clearIfNotMatch: true
    };
    $('#endereco-cep').mask(SPMaskBehaviorCep, spOptionsCep);
    
    function limpaFormularioCep() {
      // Limpa valores do formulário de cep.
      $('#endereco-logradouro').val('');
      $('#endereco-bairro').val('');
      $('#endereco-cidade').val('');
      $('#endereco-estado').val('');
    }
           
    $('#endereco-cep').blur(async function() {
      let options = {
        clearIfNotMatch: true,
        everse: true
      }
      // ALTERAR MÁSCARA
      $('#endereco-cep').mask('00000-000', options);
      //Nova variável "cep" somente com dígitos.
      let cep = $(this).val().replace(/\D/g, '');
      //Verifica se campo cep possui valor informado.
      if (cep != '') {
        //Expressão regular para validar o CEP.
        let validacep = /^[0-9]{8}$/;
        //Valida o formato do CEP.
        if(validacep.test(cep)) {
          limpaFormularioCep();
          //Preenche os campos com "..." enquanto consulta webservice.
          $('#endereco-logradouro').addClass('loading');
          $('#endereco-bairro').addClass('loading');
          $('#endereco-cidade').addClass('loading');
          $('#endereco-estado').addClass('loading');
          //Consulta o webservice viacep.com.br/
          $.getJSON('https://viacep.com.br/ws/'+ cep +'/json/?callback=?', async function(dados) {
            if (!('erro' in dados)) {
              $('#endereco-logradouro').removeClass('loading');
              $('#endereco-bairro').removeClass('loading');
              $('#endereco-cidade').removeClass('loading');
              $('#endereco-estado').removeClass('loading');
              //Atualiza os campos com os valores da consulta.
              $('#endereco-logradouro').val(dados.logradouro);
              $('#endereco-bairro').val(dados.bairro);
              $('#endereco-cidade').val(dados.localidade);
              $('#endereco-estado').val(dados.uf);
            } else {
              //CEP pesquisado não foi encontrado.
              limpaFormularioCep();
              await swal.fire({
                icon: 'warning',
                title: 'Oops...',
                text: 'CEP não encontrado!',
                confirmButtonText: 'Fechar',
                customClass: {
                  confirmButton: 'btn btn-primary mt-0',
                  cancelButton: 'btn btn-light mt-0'
                }
              });
            }
          });
        } else {
          //cep é inválido.
          limpaFormularioCep();
          await swal.fire({
            icon: 'warning',
            title: 'Oops...',
            text: 'O formato do CEP é inválido!',
            confirmButtonText: 'Fechar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
        }
      } else {
        //cep sem valor, limpa formulário.
        limpaFormularioCep();
      }
    });
  }
}

let api = {
  create: () => {
    try {
      // REDEFINIR FORMULÁRIO
      $('#form').trigger('reset');
      $('#form').validate();
      // GERENCIAR CRIAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        // VALIDAR FORMULÁRIO
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo adicionar este cliente?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let cliente = {
                tipo: $('#cliente-tipo').val(),
                id_mf: $('#cliente-id-mf').cleanVal(),
                nome: $('#cliente-nome').val(),
                descricao: $('#cliente-descricao').val(),
                status: $('#cliente-status').prop('checked'),
                contato: JSON.stringify({
                  email: $('#cliente-email').val(),
                  telefone: $('#cliente-telefone').val().replace(/\D/g, ''),
                  whatsapp: $('#cliente-whatsapp').val().replace(/\D/g, ''),
                }),
                endereco: JSON.stringify({
                  cep: $('#endereco-cep').val().replace(/\D/g, ''),
                  logradouro: $('#endereco-logradouro').val(),
                  numero: $('#endereco-numero').val(),
                  complemento: $('#endereco-complemento').val(),
                  bairro: $('#endereco-bairro').val(),
                  cidade: $('#endereco-cidade').val(),
                  estado: $('#endereco-estado').val()
                })
              }
              await axios.post(API_URL + '/clientes/', cliente);
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'O cliente foi adicionado com sucesso.',
              });
            }
            //
            catch (e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      $('#modal-title').html('Adicionar Cliente');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  update: async (id) => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR DADOS DO CLIENTE
      let cliente = (await axios.get(API_URL + '/clientes/' + id)).data;
      // REDEFINIR FORMULÁRIO
      $('#form').trigger('reset');
      $('#form').validate();
      // INSERIR DADOS DO CLIENTE NO FORMULÁRIO
      /*/
      /* ABA 1
      /*/
      $('#cliente-tipo').val(cliente.tipo);
      $('#cliente-tipo').trigger('change');
      $('#cliente-id-mf').val($('#cliente-id-mf').masked(cliente.id_mf));
      $('#cliente-nome').val(cliente.nome);
      $('#cliente-descricao').val(cliente.descricao);
      $('#cliente-status').prop('checked', cliente.status ? true : false);
      cliente.contato = JSON.parse(cliente.contato);
      if (cliente.contato) {
        $('#cliente-email').val(cliente.contato.email);
        $('#cliente-telefone').val( $('#cliente-telefone').masked(cliente.contato.telefone));
        $('#cliente-whatsapp').val($('#cliente-whatsapp').masked(cliente.contato.whatsapp));
      }
      /*/
      /* ABA 2
      /*/
      cliente.endereco = JSON.parse(cliente.endereco);
      if (cliente.endereco) {
        $('#endereco-cep').val($('#endereco-cep').masked(cliente.endereco.cep));
        $('#endereco-logradouro').val(cliente.endereco.logradouro);
        $('#endereco-numero').val(cliente.endereco.numero);
        $('#endereco-complemento').val(cliente.endereco.complemento);
        $('#endereco-bairro').val(cliente.endereco.bairro);
        $('#endereco-cidade').val(cliente.endereco.cidade);
        $('#endereco-estado').val(cliente.endereco.estado);
        $('#endereco-estado').trigger('change');
      }
      // GERENCIAR ATUALIZAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo atualizar este cliente?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let cliente = {
                tipo: $('#cliente-tipo').val(),
                id_mf: $('#cliente-id-mf').cleanVal(),
                nome: $('#cliente-nome').val(),
                descricao: $('#cliente-descricao').val(),
                status: $('#cliente-status').prop('checked'),
                contato: JSON.stringify({
                  email: $('#cliente-email').val(),
                  telefone: $('#cliente-telefone').val().replace(/\D/g, ''),
                  whatsapp: $('#cliente-whatsapp').val().replace(/\D/g, ''),
                }),
                endereco: JSON.stringify({
                  cep: $('#endereco-cep').val().replace(/\D/g, ''),
                  logradouro: $('#endereco-logradouro').val(),
                  numero: $('#endereco-numero').val(),
                  complemento: $('#endereco-complemento').val(),
                  bairro: $('#endereco-bairro').val(),
                  cidade: $('#endereco-cidade').val(),
                  estado: $('#endereco-estado').val()
                })
              }
              //CHAMADA
              await axios.patch(API_URL + '/clientes/' + id, cliente);
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'O cliente foi atualizado com sucesso.',
              });
            }
            //
            catch(e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      swal.close();
      $('#modal-title').html('Editar Cliente');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let cliente = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir este cliente?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (cliente.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let cliente_id = id;
        await axios.delete(API_URL + '/clientes/' + cliente_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'O cliente foi excluído com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
  // GUI
  gui.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create', function() {
    api.create();
  });
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete($(this).attr('data-id'));
  });
});