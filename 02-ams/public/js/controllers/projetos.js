let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/projetos',
            map: (raw) => {
              raw.forEach(item => {
                item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
                item.updated_at = moment(item.updated_at).format('DD/MM/YYYY HH:mm:ss');
                item.tags = JSON.parse(item.tags) || [];
                item.integracoes = JSON.parse(item.integracoes);
                item.imagens = JSON.parse(item.imagens);
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: false,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
        field: 'nome',
        title: 'Nome',
        autoHide: false,
        template: (row) => {
          return '\
            <div class="d-flex align-items-center">\
                <a href="/projetos/' + row.id + '"><div class="symbol symbol-40 symbol-sm flex-shrink-0">' +
            (row.imagens ? '<img src="' + row.imagens.thumbnail + '" alt="' + row.nome + '"/>' : '<span class="symbol-label font-size-h4">' + row.nome.charAt(0) + '</span>') +
            '</div></a>\
                <div class="ml-4 ellipsis">' +
            row.nome + '<br><small>' + row.cliente.nome + '</small></div></div>' +
            (row.tags.length > 0 ? '<div class="ml-16 w-100">' + row.tags.map(tag => '<span class="label label-sm label-light-info label-inline mr-2">' + tag + '</span>').join('') + '</div>' : '')
            
        }
      }, {
        field: 'gerente_nome',
        title: 'Gerente de Contas',
        width: 200,
        responsive: {
          hidden: 'xs',
          visible: 'sm'
        }
      }, {
        field: 'status',
        title: 'Status',
        textAlign: 'center',
        width: 80,
        template: (row) => {
          if (row.status === 1) return '<span class="label font-weight-bold label-lg label-light-success label-inline w-100">Ativo</span>';
          else return '<span class="label font-weight-bold label-lg label-light-danger label-inline w-100">Inativo</span>';
        },
        responsive: {
          hidden: 'xs',
          visible: 'sm'
        }
      }, {
        field: 'Actions',
        title: '',
        sortable: false,
        width: 50,
        overflow: 'visible',
        template: (row) => {
          if (auth0.admin) return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover py-2">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">\
                            ESCOLHA UMA OPÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <a class="navi-link" href="/projetos/' + row.id + '">\
                                <span class="navi-icon"><i class="la la-eye"></i></span>\
                                <span class="navi-text">Visualizar</span>\
                            </a>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-update" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-pencil"></i></span>\
                                <span class="navi-text">Editar</span>\
                            </span>\
                        </li>' + (row.integracoes ? '' : '\
                        <li class="navi-item">\
                            <span class="navi-link btn-delete" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>') + '\
                    </ul>\
                </div>\
            </div>';
          else return '\
            <div class="dropdown dropdown-inline">\
                <a href="/projetos/' + row.id + '" class="btn btn-sm btn-clean btn-icon mr-2">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
            </div>';
        },
        responsive: {
          hidden: 'xs',
          visible: 'sm'
        }
      }],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let tagify;
let tagify_input = document.getElementById('projeto-tags');

let gui = {
  init: () => {
    // MONITORAR O UPLOAD
    $('#projeto-avatar').on('change', function() {
      if ($(this).val()) {
        $(this).prop('disabled', true);
        $('#projeto-avatar-label').html($(this).val().split('\\').pop());
        $('.btn-attachment').prop('disabled', false);
      } else {
        $(this).prop('disabled', false);
        $('.btn-attachment').prop('disabled', true);
        $('#projeto-avatar-label').html('Selecionar')
        $('#projeto-avatar-extra').html('');
      }
    });
    $('.btn-attachment').on('click', function() {
      $('#projeto-avatar').val('');
      $('#projeto-avatar').trigger('change');
    });
    // INICIALIZAR TAGS
    tagify = new Tagify(tagify_input, {
      enforceWhitelist: true,
      whitelist: [
        'B2B', 'B2C',
        'Hospedagem',
        'Nuvemshop',
        'RD Station Light', 'RD Station Basic', 'RD Station Pro', 'RD Station Enterprise', 'RD Station CRM',
        'Shopify',
        'Tray',
        'Wix',
        'WordPress'
      ]
    });
    // INICIALIZAR MÁSCARAS
    $('#comercial-kampana-mrr, #comercial-rd-mrr').mask('#.##0,00', {reverse: true, clearIfNotMatch: true});
    // INICIALIZAR SELECT2
    $('#comercial-rd-plano, #comercial-rd-status').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    // INICIALIZAR DATAS
    $('#comercial-kampana-data-venda, #comercial-kampana-data-fim, #comercial-rd-data-venda').datetimepicker({
      locale: 'pt-br',
      format: 'L'
      // maxDate: moment()
    });
  }
}

let api = {
  create: async () => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR DADOS DE CLIENTES
      let clientes = (await axios.get(API_URL + '/clientes')).data;
      $('#projeto-cliente').html('<option value="">Selecionar</option>' + clientes.map(cliente => '<option value="' + cliente.id + '">' + cliente.nome + '</option>'));
      $('#projeto-cliente').select2({
        placeholder: {
          id: '',
          text: 'Selecionar'
        },
        allowClear: true
      });
      // CARREGAR DADOS DOS ADMINISTRADORES
      let administradores = (await axios.get(API_URL + '/back-office')).data;
      $('#projeto-gerente').html('<option value="">Selecionar</option>' + administradores.map(administrador => '<option value="' + administrador.user_id + '">' + administrador.name + '</option>'));
      $('#projeto-gerente').select2({
        placeholder: {
          id: '',
          text: 'Selecionar'
        },
        allowClear: true
      });
      // REDEFINIR TAGS
      tagify.removeAllTags();
      // REDEFINIR FORMULÁRIO
      $('#form').trigger('reset');
      $('#projeto-avatar').trigger('change');
      $('#form select').trigger('change');
      escopo.setData([['', '', '']]);
      dominios.setData([['']]);
      redes_sociais.setData([['', '']]);
      $('#form').validate();
      // GERENCIAR CRIAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        // VALIDAR FORMULÁRIO
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo adicionar este projeto?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let projeto = {
                cliente_id: $('#projeto-cliente').val(),
                nome: $('#projeto-nome').val(),
                descricao: $('#projeto-descricao').val(),
                gerente_id: $('#projeto-gerente').val() ? $('#projeto-gerente').val() : null,
                tags: $('#projeto-tags').val() ? JSON.stringify(JSON.parse($('#projeto-tags').val()).map(tag => tag.value).sort()) : '[]',
                status: $('#projeto-status').prop('checked')
              }
              await axios.post(API_URL + '/projetos/', projeto);
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'O projeto foi adicionado com sucesso.',
              });
            }
            //
            catch (e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      swal.close();
      $('a[href="#kt_tab_pane_1"]').click();
      $('#modal-title').html('Adicionar Projeto');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  update: async (id) => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR DADOS DO PROJETO
      let projeto = (await axios.get(API_URL + '/projetos/' + id)).data;
      let imagens = JSON.parse(projeto.imagens) || {};
      // CARREGAR DADOS DE CLIENTES
      let clientes = (await axios.get(API_URL + '/clientes')).data;
      $('#projeto-cliente').html('<option value="">Selecionar</option>' + clientes.map(cliente => '<option value="' + cliente.id + '">' + cliente.nome + '</option>'));
      $('#projeto-cliente').select2({
        placeholder: {
          id: '',
          text: 'Selecionar'
        },
        allowClear: true
      });
      setTimeout(() => $('#projeto-cliente').trigger('change'), 1000);
      // CARREGAR DADOS DOS ADMINISTRADORES
      let administradores = (await axios.get(API_URL + '/back-office')).data;
      $('#projeto-gerente').html('<option value="">Selecionar</option>' + administradores.map(administrador => '<option value="' + administrador.user_id + '">' + administrador.name + '</option>'));
      $('#projeto-gerente').select2({
        placeholder: {
          id: '',
          text: 'Selecionar'
        },
        allowClear: true
      });
      setTimeout(() => $('#projeto-gerente').trigger('change'), 1000);
      // AJUSTAR AS TAGS
      tagify.removeAllTags();
      let tags = JSON.parse(projeto.tags) || [];
      if (tags.length > 0) {
        setTimeout(() => {
          tagify.addTags(JSON.parse(projeto.tags));
        }, 500);
      }
      // REDEFINIR FORMULÁRIO
      $('#form').trigger('reset');
      $('#form select').trigger('change');
      escopo.setData([['', '', '']]);
      dominios.setData([['']]);
      redes_sociais.setData([['', '']]);
      $('#form').validate();
      /*/
      /* ABA 1
      /*/
      $('#projeto-cliente').val(projeto.cliente_id);
      $('#projeto-nome').val(projeto.nome);
      $('#projeto-descricao').val(projeto.descricao);
      $('#projeto-gerente').val(projeto.gerente_id);
      $('#projeto-status').prop('checked', projeto.status ? true : false);
      // AJUSTAR AVATAR
      if (imagens.thumbnail) {
        $('#projeto-avatar').prop('disabled', true);
        $('.btn-attachment').prop('disabled', false);
        $('#projeto-avatar-label').html(imagens.thumbnail.split('/').pop());
        $('#projeto-avatar-extra').html('Clique <a href="' + imagens.thumbnail + '" target="_blank">aqui</a> para visualizar o anexo.');
      } else {
        $('#projeto-avatar').prop('disabled', false);
        $('.btn-attachment').prop('disabled', true);
        $('#projeto-avatar-extra').html('');
      }
      /*/
      /* ABA 2
      /*/
      projeto.comercial = JSON.parse(projeto.comercial) || {};
      if (projeto.comercial) {
        if (projeto.comercial.kampana) {
          if (projeto.comercial.kampana.mrr) $('#comercial-kampana-mrr').val(projeto.comercial.kampana.mrr.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).substring(3));
          if (projeto.comercial.kampana.data_da_venda) $('#comercial-kampana-data-venda').find('input').val(moment(projeto.comercial.kampana.data_da_venda).format('DD/MM/YYYY'));
          if (projeto.comercial.kampana.data_fim) $('#comercial-kampana-data-fim').find('input').val(moment(projeto.comercial.kampana.data_fim).format('DD/MM/YYYY'));
          //
          if (projeto.comercial.kampana.escopo) escopo.setData(projeto.comercial.kampana.escopo);
        }
        //
        if (projeto.comercial.rdstation) {
          if (projeto.comercial.rdstation.plano) $('#comercial-rd-plano').val(projeto.comercial.rdstation.plano).trigger('change');
          if (projeto.comercial.rdstation.mrr) $('#comercial-rd-mrr').val(projeto.comercial.rdstation.mrr.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).substring(3));
          if (projeto.comercial.rdstation.data_da_venda) $('#comercial-rd-data-venda').find('input').val(moment(projeto.comercial.rdstation.data_da_venda).format('DD/MM/YYYY'));
          if (projeto.comercial.rdstation.status) $('#comercial-rd-status').val(projeto.comercial.rdstation.status).trigger('change');
        }
      }      
      /*/
      /* ABA 3
      /*/
      projeto.dominios = JSON.parse(projeto.dominios) || [['']];
      if (projeto.dominios) dominios.setData(projeto.dominios.map(item => { return [item] }));
      //
      projeto.redes_sociais = JSON.parse(projeto.redes_sociais) || [['', '']];
      if (projeto.redes_sociais) redes_sociais.setData(projeto.redes_sociais.map(item => { return [item.rede, item.usuario] }));
      // GERENCIAR ATUALIZAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo atualizar este projeto?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let projeto = {
                cliente_id: $('#projeto-cliente').val(),
                nome: $('#projeto-nome').val(),
                descricao: $('#projeto-descricao').val(),
                comercial: {},
                gerente_id: $('#projeto-gerente').val() ? $('#projeto-gerente').val() : null,
                tags: $('#projeto-tags').val() ? JSON.stringify(JSON.parse($('#projeto-tags').val()).map(tag => tag.value).sort()) : '[]',
                status: $('#projeto-status').prop('checked')
              }
              //
              if ($('#comercial-kampana-mrr').val() || $('#comercial-kampana-data-venda').val() || $('#comercial-kampana-data-fim').val() || escopo.getData().length > 0) {
                projeto.comercial.kampana = {};
                if ($('#comercial-kampana-mrr').val()) projeto.comercial.kampana.mrr = parseFloat($('#comercial-kampana-mrr').cleanVal()) / 100;
                if ($('#comercial-kampana-data-venda').find('input').val()) projeto.comercial.kampana.data_da_venda = $('#comercial-kampana-data-venda').find('input').val().split('/').reverse().join('-');
                if ($('#comercial-kampana-data-fim').find('input').val()) projeto.comercial.kampana.data_fim = $('#comercial-kampana-data-fim').find('input').val().split('/').reverse().join('-');
                if (escopo.getData().length > 0) projeto.comercial.kampana.escopo = escopo.getData();
              }
              //
              if ($('#comercial-rd-plano').val() || $('#comercial-rd-mrr').cleanVal() || $('#comercial-rd-data-venda').val() ||  $('#comercial-rd-status').val()) {
                projeto.comercial.rdstation = {};
                if ($('#comercial-rd-plano').val()) projeto.comercial.rdstation.plano = $('#comercial-rd-plano').val();
                if ($('#comercial-rd-mrr').val()) projeto.comercial.rdstation.mrr = parseFloat($('#comercial-rd-mrr').cleanVal()) / 100;
                if ($('#comercial-rd-data-venda').find('input').val()) projeto.comercial.rdstation.data_da_venda = $('#comercial-rd-data-venda').find('input').val().split('/').reverse().join('-');
                if ($('#comercial-rd-status').val()) projeto.comercial.rdstation.status = $('#comercial-rd-status').val();
              }
              //
              if (dominios.getData().length > 0) projeto.dominios = dominios.getData().map(item => { return item.shift() });
              else projeto.dominios = {};
              //
              if (redes_sociais.getData().length > 0) projeto.redes_sociais = redes_sociais.getData().map(item => { return { rede: item[0], usuario: item[1] } });
              else projeto.redes_sociais = {};
              //
              projeto.comercial = JSON.stringify(projeto.comercial);
              projeto.dominios = JSON.stringify(projeto.dominios);
              projeto.redes_sociais = JSON.stringify(projeto.redes_sociais);
              //
              await axios.patch(API_URL + '/projetos/' + id, projeto);
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'O projeto foi atualizado com sucesso.',
              });
            }
            //
            catch (e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      swal.close();
      $('a[href="#kt_tab_pane_1"]').click();
      $('#modal-title').html('Editar Projeto');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir este projeto?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = id;
        await axios.delete(API_URL + '/projetos/' + projeto_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'O projeto foi excluído com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
  // GUI
  gui.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create', function() {
    api.create();
  });
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete($(this).attr('data-id'));
  });
  //
  setTimeout(function() {
    $('#kt_datatable').KTDatatable().reload();
  }, 3000);
});