let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/relatorios',
            map: (raw) => {
              raw.forEach(item => item.competencia = item.mes + '/' + item.ano);
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
          field: 'id',
          title: '#',
          autoHide: false,
          width: 30
        }, {
          field: 'projeto_nome',
          title: 'Projeto',
          width: 530,
          template: (row) => {
            return row.projeto_nome + '<br><small>' + row.cliente_nome + '</small>';
          }
        }, {
          field: 'competencia',
          title: 'Mês/Ano',
          width: 125
        }, {
          field: 'status',
          title: 'Status',
          textAlign: 'center',
          width: 80,
          template: (row) => {
            if (row.status) return '<span class="label font-weight-bold label-lg label-light-success label-inline w-100">Publicado</span>';
            else return '<span class="label font-weight-bold label-lg label-light-danger label-inline w-100">Rascunho</span>';
          }
        }, {
          field: 'Actions',
          title: '',
          autoHide: false,
          sortable: false,
          width: 150,
          overflow: 'visible',
          template: (row) => {
            return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover py-2">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">\
                            ESCOLHA UMA OPÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <a class="navi-link" href="/relatorios/' + row.id + '/visualizar" target="_blank">\
                                <span class="navi-icon"><i class="la la-eye"></i></span>\
                                <span class="navi-text">Visualizar</span>\
                            </a>\
                        </li>\
                        <li class="navi-item">\
                            <a class="navi-link" href="/relatorios/' + row.id + '">\
                                <span class="navi-icon"><i class="la la-pencil"></i></span>\
                                <span class="navi-text">Editar</span>\
                            </a>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-delete" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>\
                    </ul>\
                </div>\
            </div>';
          }
        }
      ],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let gui = {
  init: async () => {
    // CARREGAR DADOS DE CLIENTES E PROJETOS
    let clientes = (await axios.get(API_URL + '/clientes')).data;
    // MONTAR LISTA DE PROJETOS PARA SELEÇÃO
    let projetos = {};
    clientes.filter(cliente => cliente.status).forEach(cliente => {
      projetos[cliente.nome] = {};
      cliente.projetos.filter(cliente => cliente.status).forEach(projeto => {
        projetos[cliente.nome][projeto.id] = projeto.nome;
      });
    });
    // INICIALIAZR SELECT2 DE DATAS
    $('#mes').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    $('#ano').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    // INICIALIZAR SELELCT2 DOS PROJETOS
    Object.keys(projetos).forEach((cliente) => $('#projeto').append('<optgroup label="' + cliente + '">' + Object.entries(projetos[cliente]).map((projeto) => '<option value="' + projeto.shift() + '">' + projeto.pop() + '</option>') + '</optgroup>'));
    $('#projeto').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
  }
}

let api = {
  create: () => {
    try {
      // REDEFINIR FORMULÁRIO
      $('#form :input').prop('disabled', false);
      $('#form').trigger('reset');
      $('#form').validate();
      // PREENCHER DATA AUTOMATICAMENTE
      $('#mes').val(new Date().getMonth() + 1);
      $('#ano').val(new Date().getFullYear());
      $('#mes, #ano, #projeto').trigger('change');
      // GERENCIAR CRIAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        // VALIDAR FORMULÁRIO
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo adicionar este relatório?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let relatorio = {
                mes: $('#mes').val(),
                ano: $('#ano').val(),
                projeto_id: $('#projeto').val(),
                status: 0
              }
              // CHAMADA
              await axios.post(API_URL + '/relatorios', relatorio);
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'O relatório foi adicionado com sucesso.',
              });
            }
            //
            catch (e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      $('#modal-title').html('Adicionar Relatório');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let relatorio = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir este relatório?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (relatorio.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let relatorio_id = id;
        await axios.delete(API_URL + '/relatorios/' + relatorio_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'O relatório foi excluído com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
  // GUI
  gui.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create', function() {
    api.create();
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete();
  });
});