let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/nuvemshop',
            map: (raw) => {
              raw.forEach(item => {
                item.last_timestamp = moment.unix(parseInt(item.last_timestamp)).format('DD/MM/YYYY HH:mm:ss');
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
          field: 'projeto_id',
          title: '#',
          sortable: false,
          width: 20,
          type: 'number',
          selector: {
            class: ''
          },
          textAlign: 'center'
        }, {
          field: 'projeto_nome',
          title: 'Projeto',
          width: 250,
          template: (row) => {
            return row.projeto_nome + '<br><small class="ellipsis">' + row.cliente_nome + '</small>';
          }
        }, {
          field: 'access_token',
          title: 'Token',
          width: 400,
          template: (row) => {
            return '<small>' + row.token_type + '</small> ' + row.access_token + '<br><small>' + row.original_domain + '</small>';
          }
        }, {
          field: 'status',
          title: 'Status',
          textAlign: 'center',
          width: 60,
          template: (row) => {
            if (row.status === 'true') return '<span class="label font-weight-bold label-lg label-light-success label-inline w-100">Ativa</span>';
            else return '<span class="label font-weight-bold label-lg label-light-danger label-inline w-100">Inativa</span>';
          }
        }, {
          field: 'Actions',
          title: '',
          sortable: false,
          width: 50,
          autoHide: false,
          overflow: 'visible',
          template: (row) => {
            return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">\
                            CONFIGURAÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-debug" data-id="' + row.projeto_id + '">\
                                <span class="navi-icon"><i class="la la-bug"></i></span>\
                                <span class="navi-text">Testar</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-update" data-id="' + row.projeto_id + '" data-label="' + (row.status === 'true' ? 'desabilita' : 'habilita') + '">\
                                <span class="navi-icon"><i class="la la-power-off"></i></span>\
                                <span class="navi-text">' + (row.status === 'true' ? 'Desabilitar' : 'Habilitar') + '</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-delete" data-id="' + row.projeto_id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>\
                        <li class="navi-separator my-2"></li>\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">\
                            OPERAÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-webhook" data-id="' + row.projeto_id + '">\
                                <span class="navi-icon"><i class="la la-plug"></i></span>\
                                <span class="navi-text">Eventos</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-script" data-id="' + row.projeto_id + '">\
                                <span class="navi-icon"><i class="la la-code"></i></span>\
                                <span class="navi-text">Scripts</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-log" data-id="' + row.projeto_id + '" data-label="' + row.original_domain + '">\
                                <span class="navi-icon"><i class="la la-binoculars"></i></span>\
                                <span class="navi-text">Histórico</span>\
                            </span>\
                        </li>\
                    </ul>\
                </div>\
            </div>';
          }
        }
      ],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let api = {
  create: async () => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR DADOS DE CLIENTES E PROJETOS
      let clientes = (await axios.get(API_URL + '/clientes')).data;
      // MONTAR LISTA DE PROJETOS PARA SELEÇÃO
      let projetos = {};
      clientes.forEach(cliente => {
        projetos[cliente.nome] = {};
        cliente.projetos.forEach(projeto => {
          let integracoes = JSON.parse(projeto.integracoes);
          if (!(integracoes && integracoes.nuvemshop)) projetos[cliente.nome][projeto.id] = projeto.nome;
        });
        if ($.isEmptyObject(projetos[cliente.nome])) delete projetos[cliente.nome];
      });
      // SOLICITAR AO USUÁRIO QUAL PROJETO UTILIZAR
      swal.close();
      let projeto = await swal.fire({
        icon: 'question',
        title: 'Vamos lá!',
        text: 'Em qual projeto associar a integração?',
        input: 'select',
        inputOptions: projetos,
        inputPlaceholder: 'Selecionar',
        inputValidator: (value) => {
          return new Promise((resolve) => {
            if (value) resolve();
            else resolve('Você precisa selecionar um conta.');
          });
        },
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        footer: 'Ao continuar você será redirecionado para o Nuvemshop. Certifique-se de ter acesso à conta.',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0',
          footer: 'text-center'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = projeto.value;
        window.location.href = API_URL + '/nuvemshop/login/' + projeto_id;
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  update: async (id, label) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo ' + label + 'r esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = id;
        await axios.put(API_URL + '/nuvemshop/' + projeto_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi ' + label + 'da com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = id;
        await axios.delete(API_URL + '/nuvemshop/' + projeto_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi excluída com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

let custom = {
  debug: async(id) => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // ENVIAR REQUISIÇÃO PARA O BACK-END
      let projeto_id = id;
      let debug = (await axios.get(API_URL + '/nuvemshop/debug/' + projeto_id)).data;
      // DEU CERTO
      swal.close();
      swal.fire({
        icon: 'success',
        title: 'Excelente!',
        html: '<code>' + JSON.stringify(debug) + '</code>',
        width: '90%'
      });
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  webhook: async(id) => {
    try {
       // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR INFORMAÇÕES DO PROJETO
      let projeto_id = id;
      let projeto = (await axios.get(API_URL + '/projetos/' + projeto_id)).data;
      // ISOLAR EM UM OBJETO OS DADOS DA INTEGRAÇÃO
      let nuvemshop = JSON.parse(projeto.integracoes).nuvemshop;
      // CARREGAR INFORMAÇÕES DO V_NUVEMSHOP
      let v_nuvemshop = (await axios.get(API_URL + '/nuvemshop/' + projeto_id)).data;
      let options = ['order_created', 'order_updated', 'order_paid', 'order_packed', 'order_fulfilled', 'order_cancelled'];
      // AVALIAR INTEGRAÇÕES ATIVAS
      if (v_nuvemshop) {
        options.forEach(webhook => {
          // RD STATION
          if (!v_nuvemshop.rdstation) {
            $('#rdstation_integracao').removeClass('d-none');
            $('#rdstation-' + webhook.replace(/_/g, '-')).prop('disabled', true);
          } else {
            $('#rdstation_integracao').addClass('d-none');
            $('#rdstation-' + webhook.replace(/_/g, '-')).prop('disabled', false);
          }
          // ACTIVECAMPAIGN
          if (!v_nuvemshop.activecampaign) {
            $('#activecampaign_integracao').removeClass('d-none');
            $('#activecampaign-' + webhook.replace(/_/g, '-')).prop('disabled', true);
          }
          else {
            $('#activecampaign_integracao').addClass('d-none');
            $('#activecampaign-' + webhook.replace(/_/g, '-')).prop('disabled', false);
          }
          // WHATSAPP
          if (!v_nuvemshop.whatsapp) {
            $('#whatsapp_integracao').removeClass('d-none');
            $('#whatsapp-' + webhook.replace(/_/g, '-')).prop('disabled', true);
          }
          else {
            $('#whatsapp_integracao').addClass('d-none');
            $('#whatsapp-' + webhook.replace(/_/g, '-')).prop('disabled', false);
          } 
          // SMS
          if (!v_nuvemshop.sms) {
            $('#sms_integracao').removeClass('d-none');
            $('#sms-' + webhook.replace(/_/g, '-')).prop('disabled', true);
          }
          else {
            $('#sms_integracao').addClass('d-none');
            $('#sms-' + webhook.replace(/_/g, '-')).prop('disabled', false);
          } 
        });
      }
      // AVALIAR WEBHOOKS DISPONÍVEIS E EXISTENTES
      if (nuvemshop.webhooks) {
        options.forEach(webhook => {
          // RD STATION
          if (nuvemshop.webhooks[webhook] && nuvemshop.webhooks[webhook].tags.includes('rdstation')) $('#rdstation-' + webhook.replace(/_/g, '-')).prop('checked', true);
          else $('#rdstation-' + webhook.replace(/_/g, '-')).prop('checked', false);
          // ACTIVECAMPAIGN
          if (nuvemshop.webhooks[webhook] && nuvemshop.webhooks[webhook].tags.includes('activecampaign')) $('#activecampaign-' + webhook.replace(/_/g, '-')).prop('checked', true);
          else $('#activecampaign-' + webhook.replace(/_/g, '-')).prop('checked', false);
          // WHATSAPP
          if (nuvemshop.webhooks[webhook] && nuvemshop.webhooks[webhook].tags.includes('whatsapp')) $('#whatsapp-' + webhook.replace(/_/g, '-')).prop('checked', true);
          else $('#whatsapp-' + webhook.replace(/_/g, '-')).prop('checked', false);
          // SMS
          if (nuvemshop.webhooks[webhook] && nuvemshop.webhooks[webhook].tags.includes('sms')) $('#sms-' + webhook.replace(/_/g, '-')).prop('checked', true);
          else $('#sms-' + webhook.replace(/_/g, '-')).prop('checked', false);
        });
      } else options.forEach(webhook => {
        // RD STATION
        $('#rdstation-' + webhook.replace(/_/g, '-')).prop('checked', false);
        // ACTIVECAMPAIGN
        $('#activecampaign-' + webhook.replace(/_/g, '-')).prop('checked', false);
        // WHATSAPP
        $('#whatsapp-' + webhook.replace(/_/g, '-')).prop('checked', false);
        // SMS
        $('#sms-' + webhook.replace(/_/g, '-')).prop('checked', false);
      });
      // GERENCIAR ATUALIZAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        let confirmacao = await swal.fire({
          icon: 'warning',
          title: 'Atenção!',
          text: 'Deseja mesmo atualizar esta integração?',
          showCancelButton: true,
          reverseButtons: true,
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Continuar',
          customClass: {
            confirmButton: 'btn btn-primary mt-0',
            cancelButton: 'btn btn-light mt-0'
          }
        });
        if (confirmacao.value) {
          try {
            // EXIBIR TELA DE CARREGAMENTO
            swal.fire({
              title: 'Aguarde...',
              text: 'Processando a solicitação.',
              onOpen: () => {
                swal.showLoading();
              },
              allowOutsideClick: false,
              allowEscapeKey: false
            });
            // MANIPULANDO OBJETO DOS EVENTOS HABILITADOS PARA CADA PLATAFORMA
            let rdstation = {};
            let activecampaign = {};
            let whatsapp = {};
            let sms = {};
            options.forEach(webhook => {
              rdstation[webhook] = $('#rdstation-' + webhook.replace('_', '-')).prop('checked');
              activecampaign[webhook] = $('#activecampaign-' + webhook.replace('_', '-')).prop('checked');
              whatsapp[webhook] = $('#whatsapp-' + webhook.replace('_', '-')).prop('checked');
              sms[webhook] = $('#sms-' + webhook.replace('_', '-')).prop('checked');
            });
            // OBJETO FINAL
            let webhooks = {};
            webhooks.rdstation = rdstation;
            webhooks.activecampaign = activecampaign;
            webhooks.whatsapp = whatsapp;
            webhooks.sms = sms;
            // ENVIA REQUISIÇÃO PARA O BACKEND
            await axios.put(API_URL + '/nuvemshop/webhooks/' + id, webhooks);
            datatable.reload();
            // DEU CERTO
            $('#modal').modal('hide');
            swal.close();
            swal.fire({
              icon: 'success',
              title: 'Excelente!',
              text: 'A integração foi atualizada com sucesso.'
            });
          }
          //
          catch(e) {
            console.log(e);
            swal.close();
            swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Algo errado não está certo.',
              footer: 'Confira o erro no log do navegador.'
            });
          }
        }
      });
      // EXIBIR MODAL
      swal.close();
      $('#modal-title').html('<a href="' + API_URL + '/nuvemshop/webhooks/' + id + '" target="_blank"><span class="label mr-2"><i class="icon-sm la la-bug"></i></span></a>' + nuvemshop.original_domain);
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  script: async(id) => {
    try {
       // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR INFORMAÇÕES DO PROJETO
      let projeto_id = id;
      let projeto = (await axios.get(API_URL + '/projetos/' + projeto_id)).data;
      // CARREGAR INFORMAÇÕES DO V_NUVEMSHOP
      let v_nuvemshop = (await axios.get(API_URL + '/nuvemshop/' + projeto_id)).data;
      let options = ['tracking_code_rd', 'tracking_code_ac', 'abandoned_cart'];
      // AVALIAR INTEGRAÇÕES ATIVAS
      if (v_nuvemshop) {
        options.forEach(script => {
          // RD STATION
          if (!v_nuvemshop.rdstation) {
            $('#script_rdstation_integracao').removeClass('d-none');
            $('#script-rd-' + script.replaceAll(/_/g, '-')).prop('disabled', true);
          } else {
            $('#script_rdstation_integracao').addClass('d-none');
            $('#script-rd-' + script.replaceAll(/_/g, '-')).prop('disabled', false);
          }
          // ACTIVECAMPAIGN
          if (!v_nuvemshop.activecampaign) {
            $('#script_activecampaign_integracao').removeClass('d-none');
            $('#script-activecampaign-' + script.replaceAll(/_/g, '-')).prop('disabled', true);
          }
          else {
            $('#script_activecampaign_integracao').addClass('d-none');
            $('#script-activecampaign-' + script.replaceAll(/_/g, '-')).prop('disabled', false);
          }
          // WHATSAPP
          if (!v_nuvemshop.whatsapp) {
            $('#script_whatsapp_integracao').removeClass('d-none');
            $('#script-wpp-' + script.replaceAll(/_/g, '-')).prop('disabled', true);
          }
          else {
            $('#script_whatsapp_integracao').addClass('d-none');
            $('#script-wpp-' + script.replaceAll(/_/g, '-')).prop('disabled', false);
          } 
          // SMS
          if (!v_nuvemshop.sms) {
            $('#script_sms_integracao').removeClass('d-none');
            $('#script-sms-' + script.replaceAll(/_/g, '-')).prop('disabled', true);
          }
          else {
            $('#script_sms_integracao').addClass('d-none');
            $('#script-sms-' + script.replaceAll(/_/g, '-')).prop('disabled', false);
          } 
        });
      }
      // ISOLAR EM UM OBJETO OS DADOS DA INTEGRAÇÃO
      let nuvemshop = JSON.parse(projeto.integracoes).nuvemshop;
      // AVALIAR SCRIPTS DISPONÍVEIS E EXISTENTES
      if (nuvemshop.scripts) {
        options.forEach(script => {
          // RD STATION
          if (nuvemshop.scripts[script] && nuvemshop.scripts[script].tags.includes('rdstation')) $('#script-rd-' + script.replaceAll(/_/g, '-')).prop('checked', true);
          else $('#script-rd-' + script.replaceAll(/_/g, '-')).prop('checked', false);
          // ACTIVECAMPAIGN
          if (nuvemshop.scripts[script] && nuvemshop.scripts[script].tags.includes('activecampaign')) $('#script-activecampaign-' + script.replaceAll(/_/g, '-')).prop('checked', true);
          else $('#script-activecampaign-' + script.replaceAll(/_/g, '-')).prop('checked', false);
          // WHATSAPP
          if (nuvemshop.scripts[script] && nuvemshop.scripts[script].tags.includes('whatsapp')) $('#script-wpp-' + script.replaceAll(/_/g, '-')).prop('checked', true);
          else $('#script-wpp-' + script.replaceAll(/_/g, '-')).prop('checked', false);
          // SMS
          if (nuvemshop.scripts[script] && nuvemshop.scripts[script].tags.includes('sms')) $('#script-sms-' + script.replaceAll(/_/g, '-')).prop('checked', true);
          else $('#script-sms-' + script.replaceAll(/_/g, '-')).prop('checked', false);
        });
      } else options.forEach(script => {
        // RD STATION
        $('#script-rd-' + script.replaceAll(/_/g, '-')).prop('checked', false);
        // ACTIVECAMPAIGN
        $('#script-activecampaign-' + script.replaceAll(/_/g, '-')).prop('checked', false);
        // WHATSAPP
        $('#script-wpp-' + script.replaceAll(/_/g, '-')).prop('checked', false);
        // SMS
        $('#script-sms-' + script.replaceAll(/_/g, '-')).prop('checked', false);
      });
      // GERENCIAR ATUALIZAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        let confirmacao = await swal.fire({
          icon: 'warning',
          title: 'Atenção!',
          text: 'Deseja mesmo atualizar esta integração?',
          showCancelButton: true,
          reverseButtons: true,
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Continuar',
          customClass: {
            confirmButton: 'btn btn-primary mt-0',
            cancelButton: 'btn btn-light mt-0'
          }
        });
        if (confirmacao.value) {
          try {
            // EXIBIR TELA DE CARREGAMENTO
            swal.fire({
              title: 'Aguarde...',
              text: 'Processando a solicitação.',
              onOpen: () => {
                swal.showLoading();
              },
              allowOutsideClick: false,
              allowEscapeKey: false
            });
            // MANIPULANDO OBJETO DOS EVENTOS HABILITADOS PARA CADA PLATAFORMA
            let rdstation = {};
            let activecampaign = {};
            let whatsapp = {};
            let sms = {};
            options.forEach(script => {
              rdstation[script] = $('#script-rd-' + script.replaceAll('_', '-')).prop('checked');
              activecampaign[script] = $('#script-activecampaign-' + script.replaceAll('_', '-')).prop('checked');
              whatsapp[script] = $('#script-wpp-' + script.replaceAll('_', '-')).prop('checked');
              sms[script] = $('#script-sms-' + script.replaceAll('_', '-')).prop('checked');
            });
            // OBJETO FINAL
            let scripts = {};
            scripts.rdstation = rdstation;
            scripts.activecampaign = activecampaign;
            scripts.whatsapp = whatsapp;
            scripts.sms = sms;
            // ENVIA REQUISIÇÃO PARA O BACKEND
            await axios.put(API_URL + '/nuvemshop/scripts/' + id, scripts);
            datatable.reload();
            // DEU CERTO
            $('#modal-scripts').modal('hide');
            swal.close();
            swal.fire({
              icon: 'success',
              title: 'Excelente!',
              text: 'A integração foi atualizada com sucesso.'
            });
          }
          //
          catch(e) {
            console.log(e);
            swal.close();
            swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Algo errado não está certo.',
              footer: 'Confira o erro no log do navegador.'
            });
          }
        }
      });
      // EXIBIR MODAL
      swal.close();
      $('#modal-scripts-title').html('<a href="' + API_URL + '/nuvemshop/scripts/' + id + '" target="_blank"><span class="label mr-2"><i class="icon-sm la la-bug"></i></span></a>' + nuvemshop.original_domain);
      $('#modal-scripts').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  log: async(id, label) => {
    $('#modal-log').off();
    // MATAR A TAELA ANTIGA AO FECHAR O MODAL
    $('#modal-log').on('hidden.bs.modal', function() {
      $('#kt_datatable_log').KTDatatable().destroy();
    });
    // CARREGAR A NOVA TABELA APENAS COM O MODAL ABERTO
    $('#modal-log').on('shown.bs.modal', function() {
      // RENDERIZAR A NOVA TABELA
      $('#kt_datatable_log').KTDatatable({
        // CARREGAR E MANIPULAR OS DADOS
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'GET',
              url: API_URL + '/nuvemshop/logs/' + id,
              map: (raw) => {
                raw.forEach(item => {
                  item.created_at = moment(item.created_at).subtract(3, 'hours').format('DD/MM/YYYY HH:mm:ss');
                });
                return raw;
              }
            }
          }
        },
        // DEFINIR LAYOUT
        layout: {
          scroll: false,
          footer: false
        },
        sortable: true,
        pagination: true,
        // DEFINIR FILTROS
        search: {
          input: $('#kt_datatable_search_query_log'),
          key: 'generalSearch'
        },
        // DEFINIR COLUNAS
        columns: [{
            field: 'id',
            title: '#',
            width: 50
          }, {
            field: 'descricao',
            title: 'Descrição',
            width: 600,
            template: (row) => {
              if (row.user_display_name) return row.descricao +  '<span class="label ml-3" data-toggle="tooltip" data-html="true" title="' + row.user_display_name + '<br>' + row.user_email + '"><i class="icon-sm la la-cog"></i></span>';
              else if (row.rdstation_event_type == 'ORDER_PLACED') return row.descricao + '<span class="label label-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-shopping-cart text-white"></i></span>';
              else if (row.rdstation_event_type == 'SALE') return row.descricao + '<span class="label label-success ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-dollar text-white"></i></span>';
              else if (row.rdstation_lead_id) return row.descricao +  '<span class="label label-light-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-user"></i></span>';
              else return row.descricao;
            }
          }, {
            field: 'origem',
            title: 'Origem',
            width: 75
          }, {
            field: 'tipo',
            title: 'Tipo',
            textAlign: 'center',
            width: 75,
            template: (row) => {
              return '<span class="label font-weight-bold label-lg label-light-' + row.tipo + ' label-inline w-100 text-uppercase">' + row.tipo + '</span>';
            }
          }, {
            field: 'created_at',
            title: 'Data',
            width: 200
          }
        ],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
      });
      // EXIBIR TOOLTIPS
      $('#kt_datatable_log').KTDatatable().on('datatable-on-layout-updated', function() {
        $('[data-toggle="tooltip"]').tooltip();
      });
    });
    // EXIBIR O MODAL
    $('#modal-log-title').html(label);
    $('#modal-log').modal('show');
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create', function() {
    api.create();
  });
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-id'), $(this).attr('data-label'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete($(this).attr('data-id'));
  });
  // CONFIGURAR CUSTOM
  $(document).on('click', '.btn-debug', function() {
    custom.debug($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-webhook', function() {
    custom.webhook($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-script', function() {
    custom.script($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-log', function() {
    custom.log($(this).attr('data-id'), $(this).attr('data-label'));
  });
  // AVALIAR SE O CARREGAMENTO É UM CALLBACK
  let params = new URLSearchParams(window.location.search);
  if (params.get('sucesso')) {
    let sucesso = (params.get('sucesso') === 'true');
    // DEU CERTO
    if (sucesso) swal.fire({
      icon: 'success',
      title: 'Excelente!',
      text: 'A integração foi adicionada com sucesso.',
    });
    // DEU ERRADO
    else swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Algo errado não está certo.',
      footer: 'Por favor, tente novamente.'
    });
    // LIMPAR A FLAG DA URL
    window.history.replaceState({}, document.title, location.protocol + '//' + location.host + location.pathname);
  }
});