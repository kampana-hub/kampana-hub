let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/pesquisas',
            map: (raw) => {
              raw.forEach(item => {
                item.competencia = item.mes + '/' + item.ano;
                //
                switch (item.status) {
                  case 0:
                    // RASCUNHO
                    item.status_label = 'light';
                    item.status_text = 'Aguardando';
                    break;
                  case 1:
                    // CONCLUÍDA
                    item.status_label = 'light-success';
                    item.status_text = 'Preenchida';
                    break;
                }
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
          field: 'id',
          title: '#',
          autoHide: false,
          width: 30
        }, {
          field: 'projeto_nome',
          title: 'Projeto',
          width: 200,
          template: (row) => {
            return row.projeto_nome + '<br><small>' + row.cliente_nome + '</small>';
          }
        }, {
          field: 'formulario_nome',
          title: 'Nome',
          autoHide: false,
          width: 150,
          template: (row) => {
            return '<span class="ellipsis">' + row.formulario_nome + '</span>';
          }
        }, {
          field: 'competencia',
          title: 'Mês/Ano',
          width: 125
        }, {
          field: 'status',
          title: 'Status',
          textAlign: 'center',
          width: 125,
          template: (row => '<span class="label font-weight-bold label-lg label-' + row.status_label + ' label-inline w-100">' + row.status_text + '</span>')
        }, {
          field: 'Actions',
          title: '',
          autoHide: false,
          sortable: false,
          width: 50,
          overflow: 'visible',
          template: (row) => {
            return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover py-2">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">\
                            ESCOLHA UMA OPÇÃO\
                        </li>' + (row.status == 0 ?
                        '<li class="navi-item">\
                            <span class="navi-link btn-copy" onclick="gui.copy(\'' + window.location.origin + '/pesquisas/' + row.id + '\')">\
                                <span class="navi-icon"><i class="la la-link"></i></span>\
                                <span class="navi-text">Copiar Link</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-update" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-edit"></i></span>\
                                <span class="navi-text">Editar</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-delete" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>' :
                        '<li class="navi-item">\
                            <a class="navi-link" href="' + window.location.origin + '/pesquisas/' + row.id + '" target="_blank">\
                                <span class="navi-icon"><i class="la la-eye"></i></span>\
                                <span class="navi-text">Visualizar</span>\
                            </a>\
                        </li>') +
                    '</ul>\
                </div>\
            </div>';
          }
        }
      ],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let gui = {
  init: async () => {
    // CARREGAR DADOS DE CLIENTES E PROJETOS
    let clientes = (await axios.get(API_URL + '/clientes')).data;
    // MONTAR LISTA DE PROJETOS PARA SELEÇÃO
    let projetos = {};
    clientes.filter(cliente => cliente.status).forEach(cliente => {
      projetos[cliente.nome] = {};
      cliente.projetos.filter(cliente => cliente.status).forEach(projeto => {
        projetos[cliente.nome][projeto.id] = projeto.nome;
      });
    });
    // CARREGAR DADOS DE FORMULÁRIOS
    let formularios = (await axios.get(API_URL + '/formularios')).data;
    // INICIALIAZR SELECT2 DE DATAS
    $('#mes').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    $('#ano').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    // INICIALIZAR SELELCT2 DOS PROJETOS
    Object.keys(projetos).forEach((cliente) => $('#projeto').append('<optgroup label="' + cliente + '">' + Object.entries(projetos[cliente]).map((projeto) => '<option value="' + projeto.shift() + '">' + projeto.pop() + '</option>') + '</optgroup>'));
    $('#projeto').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    // INICIALIZAR SELELCT2 DOS FORMULÁRIOS
    formularios.filter(formulario => formulario.status).forEach((formulario) => $('#formulario').append('<option value="' + formulario.id + '">' + formulario.nome + '</option>'));
    $('#formulario').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
  },
  copy: (text) => {
    var input = document.body.appendChild(document.createElement("input"));
    input.value = text;
    input.focus();
    input.select();
    document.execCommand('copy');
    input.parentNode.removeChild(input);
    swal.fire({
      icon: 'success',
      title: 'Excelente!',
      text: 'Link copiado para a área de transferência.'
    });
  }
}

let api = {
  create: () => {
    try {
      // REDEFINIR FORMULÁRIO
      $('#form :input').prop('disabled', false);
      $('#form').trigger('reset');
      $('#form').validate();
      // PREENCHER DATA AUTOMATICAMENTE
      $('#mes').val(new Date().getMonth() + 1);
      $('#ano').val(new Date().getFullYear());
      $('#mes, #ano, #formulario, #projeto').trigger('change');
      // GERENCIAR CRIAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        // VALIDAR FORMULÁRIO
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo adicionar esta pesquisa?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let pesquisa = {
                mes: $('#mes').val(),
                ano: $('#ano').val(),
                projeto_id: $('#projeto').val(),
                formulario_id: $('#formulario').val(),
                status: 0
              }
              // CHAMADA
              await axios.post(API_URL + '/pesquisas', pesquisa);
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'A pesquisa foi adicionada com sucesso.',
              });
            }
            //
            catch (e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      $('#modal-title').html('Adicionar Pesquisa');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  update: async (id) => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR DADOS DO CAMPANHA
      let pesquisa = (await axios.get(API_URL + '/pesquisas/' + id)).data;
      // REDEFINIR FORMULÁRIO
      $('#form').trigger('reset');
      $('#form').validate();
      // INSERIR DADOS DO CAMPANHA NO FORMULÁRIO
      $('#mes').val(pesquisa.mes).trigger('change');
      $('#ano').val(pesquisa.ano).trigger('change');
      $('#projeto').val(pesquisa.projeto_id).trigger('change');
      $('#formulario').val(pesquisa.formulario_id).trigger('change');
      // GERENCIAR ATUALIZAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo atualizar esta pesquisa?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let pesquisa = {
                mes: $('#mes').val(),
                ano: $('#ano').val(),
                projeto_id: $('#projeto').val(),
                formulario_id: $('#formulario').val(),
                status: 0
              }
              // CHAMADA
              await axios.patch(API_URL + '/pesquisas/' + id, pesquisa);
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'A pesquisa foi atualizada com sucesso.',
              });
            }
            //
            catch(e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      swal.close();
      $('#modal-title').html('Editar Pesquisa');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let pesquisa = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir esta pesquisa?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (pesquisa.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let pesquisa_id = id;
        await axios.delete(API_URL + '/pesquisas/' + pesquisa_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A pesquisa foi excluída com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
  // GUI
  gui.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create', function() {
    api.create();
  });
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete($(this).attr('data-id'));
  });
});