let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/tray',
            map: (raw) => {
              raw.forEach(item => {
                item.expires_at = moment(item.date_expiration_access_token).format('DD/MM/YYYY HH:mm:ss');
                item.is_valid   = moment(item.date_expiration_access_token).diff(moment(), 'minutes') > 0 ? true : false;
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
          field: 'projeto_id',
          title: '#',
          sortable: false,
          width: 20,
          type: 'number',
          selector: {
            class: ''
          },
          textAlign: 'center'
        }, {
          field: 'projeto_nome',
          title: 'Projeto',
          width: 250,
          template: (row) => {
            return row.projeto_nome + '<br><small>' + row.cliente_nome + '</small>';
          }
        }, {
          field: 'access_token',
          title: 'Token',
          width: 400,
          template: (row) => {
            let icon = row.is_valid ? '<span class="label label-success label-dot mr-2"></span>' : '<span class="label label-danger label-dot mr-2"></span>';
            return row.store_host + '<br>' + icon + '<small>Expira em ' + row.expires_at + '.</small>';
          }
        }, {
          field: 'status',
          title: 'Status',
          textAlign: 'center',
          width: 60,
          template: (row) => {
            if (row.status === 'true') return '<span class="label font-weight-bold label-lg label-light-success label-inline w-100">Ativa</span>';
            else return '<span class="label font-weight-bold label-lg label-light-danger label-inline w-100">Inativa</span>';
          }
        }, {
          field: 'Actions',
          title: '',
          sortable: false,
          width: 50,
          autoHide: false,
          overflow: 'visible',
          template: (row) => {
            return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">\
                            CONFIGURAÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-debug" data-id="' + row.projeto_id + '">\
                                <span class="navi-icon"><i class="la la-bug"></i></span>\
                                <span class="navi-text">Testar</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-update" data-id="' + row.projeto_id + '" data-label="' + (row.status === 'true' ? 'desabilita' : 'habilita') + '">\
                                <span class="navi-icon"><i class="la la-power-off"></i></span>\
                                <span class="navi-text">' + (row.status === 'true' ? 'Desabilitar' : 'Habilitar') + '</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-delete" data-id="' + row.projeto_id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>\
                        <li class="navi-separator my-2"></li>\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">\
                            OPERAÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-refresh" data-id="' + row.projeto_id + '">\
                                <span class="navi-icon"><i class="la la-refresh"></i></span>\
                                <span class="navi-text">Atualizar</span>\
                            </span>\
                        </li>' + (row.rdstation ?
                        '<li class="navi-item">\
                            <span class="navi-link btn-script" data-id="' + row.projeto_id + '">\
                                <span class="navi-icon"><i class="la la-code"></i></span>\
                                <span class="navi-text">Scripts</span>\
                            </span>\
                        </li>': '') + 
                        '<li class="navi-item">\
                            <span class="navi-link btn-log" data-id="' + row.projeto_id + '" data-label="' + row.store_host + '">\
                                <span class="navi-icon"><i class="la la-binoculars"></i></span>\
                                <span class="navi-text">Histórico</span>\
                            </span>\
                        </li>\
                    </ul>\
                </div>\
            </div>';
          }
        }
      ],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let api = {
  create: async () => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR DADOS DE CLIENTES E PROJETOS
      let clientes = (await axios.get(API_URL + '/clientes')).data;
      // MONTAR LISTA DE PROJETOS PARA SELEÇÃO
      let projetos = {};
      clientes.forEach(cliente => {
        projetos[cliente.nome] = {};
        cliente.projetos.forEach(projeto => {
          let integracoes = JSON.parse(projeto.integracoes);
          if (!(integracoes && integracoes.tray)) projetos[cliente.nome][projeto.id] = projeto.nome;
        });
        if ($.isEmptyObject(projetos[cliente.nome])) delete projetos[cliente.nome];
      });
      // SOLICITAR AO USUÁRIO QUAL PROJETO UTILIZAR
      swal.close();
      let projeto = await swal.fire({
        icon: 'question',
        title: 'Vamos lá!',
        html: 'Qual o endereço da loja?<input id="swal2-url" class="swal2-input form-control" type="text" placeholder="https://loja.com.br">Em qual projeto associar a integração?',
        input: 'select',
        inputOptions: projetos,
        inputPlaceholder: 'Selecionar',
        didOpen: () => $('#swal2-url').focus(),
        preConfirm: (value) => {
          return [
            // URL DA LOJA
            $('#swal2-url').val(),
            // ID DO PROJETO
            value
          ]
        },
        inputValidator: (value) => {
          return new Promise((resolve) => {
            // URL DA LOJA
            let regex = /(http|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
            if (!regex.test($('#swal2-url').val())) {
              setTimeout(() => $('#swal2-url').focus() , 10);
              resolve('Você precisa inserir uma URL válida.');
            }
            // ID DO PROJETO
            else {
              if (value) resolve();
              else resolve('Você precisa selecionar um conta.');
            }
          });
        },
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        footer: 'Ao continuar você será redirecionado para o Tray. Certifique-se de ter acesso à conta.',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0',
          footer: 'text-center'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = projeto.value;
        window.location.href = API_URL + '/tray/login/' + projeto.value[1] + '?shop=' + projeto.value[0].split('/').filter(Boolean).pop();
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  update: async (id, label) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo ' + label + 'r esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = id;
        await axios.put(API_URL + '/tray/' + projeto_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi ' + label + 'da com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = id;
        await axios.delete(API_URL + '/tray/' + projeto_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi excluída com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

let custom = {
  debug: async (id) => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // ENVIAR REQUISIÇÃO PARA O BACK-END
      let projeto_id = id;
      let debug = (await axios.get(API_URL + '/tray/debug/' + projeto_id)).data;
      // DEU CERTO
      swal.close();
      swal.fire({
        icon: 'success',
        title: 'Excelente!',
        html: '<code>' + JSON.stringify(debug) + '</code>',
        width: '90%'
      });
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  refresh: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo atualizar esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let projeto_id = id;
        await axios.get(API_URL + '/tray/refresh/' + projeto_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi atualizada com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  script: async(id) => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR INFORMAÇÕES DO PROJETO
      let projeto_id = id;
      let projeto = (await axios.get(API_URL + '/projetos/' + projeto_id)).data;
      // ISOLAR EM UM OBJETO OS DADOS DA INTEGRAÇÃO
      let tray = JSON.parse(projeto.integracoes).tray;
      // AVALIAR SCRIPTS DISPONÍVEIS E EXISTENTES
      let options = ['tracking_code'];
      if (tray.scripts) {
        options.forEach(script => {
          if (tray.scripts[script]) $('#script-rd-' + script.replace(/_/g, '-')).prop('checked', true);
          else $('#script-rd-' + script.replace(/_/g, '-')).prop('checked', false);
        });
      } else options.forEach(script => $('#script-rd-' + script.replace(/_/g, '-')).prop('checked', false));
      // GERENCIAR ATUALIZAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        let confirmacao = await swal.fire({
          icon: 'warning',
          title: 'Atenção!',
          text: 'Deseja mesmo atualizar esta integração?',
          showCancelButton: true,
          reverseButtons: true,
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Continuar',
          customClass: {
            confirmButton: 'btn btn-primary mt-0',
            cancelButton: 'btn btn-light mt-0'
          }
        });
        if (confirmacao.value) {
          try {
            // EXIBIR TELA DE CARREGAMENTO
            swal.fire({
              title: 'Aguarde...',
              text: 'Processando a solicitação.',
              onOpen: () => {
                swal.showLoading();
              },
              allowOutsideClick: false,
              allowEscapeKey: false
            });
            // ENVIAR REQUISIÇÃO PARA O BACK-END
            let scripts = {};
            options.forEach(script => {
              scripts[script] = $('#script-rd-' + script.replace('_', '-')).prop('checked');
            });
            console.log(scripts);
            await axios.put(API_URL + '/tray/scripts/' + id, scripts);
            datatable.reload();
            // DEU CERTO
            $('#modal-scripts').modal('hide');
            swal.close();
            swal.fire({
              icon: 'success',
              title: 'Excelente!',
              text: 'A integração foi atualizada com sucesso.'
            });
          }
          //
          catch(e) {
            console.log(e);
            swal.close();
            swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Algo errado não está certo.',
              footer: 'Confira o erro no log do navegador.'
            });
          }
        }
      });
      // EXIBIR MODAL
      swal.close();
      $('#modal-scripts-title').html('<a href="' + API_URL + '/tray/scripts/' + id + '" target="_blank"><span class="label mr-2"><i class="icon-sm la la-bug"></i></span></a>' + tray.auth.store_host);
      $('#modal-scripts').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  log: async (id, label) => {
    $('#modal-log').off();
    // MATAR A TAELA ANTIGA AO FECHAR O MODAL
    $('#modal-log').on('hidden.bs.modal', function() {
      $('#kt_datatable_log').KTDatatable().destroy();
    });
    // CARREGAR A NOVA TABELA APENAS COM O MODAL ABERTO
    $('#modal-log').on('shown.bs.modal', function() {
      // RENDERIZAR A NOVA TABELA
      $('#kt_datatable_log').KTDatatable({
        // CARREGAR E MANIPULAR OS DADOS
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'GET',
              url: API_URL + '/tray/logs/' + id,
              map: (raw) => {
                raw.forEach(item => {
                  item.created_at = moment(item.created_at).subtract(3, 'hours').format('DD/MM/YYYY HH:mm:ss');
                });
                return raw;
              }
            }
          }
        },
        // DEFINIR LAYOUT
        layout: {
          scroll: false,
          footer: false
        },
        sortable: true,
        pagination: true,
        // DEFINIR FILTROS
        search: {
          input: $('#kt_datatable_search_query_log'),
          key: 'generalSearch'
        },
        // DEFINIR COLUNAS
        columns: [{
            field: 'id',
            title: '#',
            width: 50
          }, {
            field: 'descricao',
            title: 'Descrição',
            width: 600,
            template: (row) => {
              console.log(row)
              if (row.user_display_name) return row.descricao +  '<span class="label ml-3" data-toggle="tooltip" data-html="true" title="' + row.user_display_name + '<br>' + row.user_email + '"><i class="icon-sm la la-cog"></i></span>';
              else if (row.rdstation_event_type == 'ORDER_PLACED') return row.descricao + '<span class="label label-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.tray_event + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-shopping-cart text-white"></i></span>';
              else if (row.rdstation_event_type == 'SALE') return row.descricao + '<span class="label label-success ml-3" data-toggle="tooltip" data-html="true" title="' + row.tray_event + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-dollar text-white"></i></span>';
              else if (row.rdstation_lead_id) return row.descricao +  '<span class="label label-light-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.tray_event + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-user"></i></span>';
              else return row.descricao;
            }
          }, {
            field: 'origem',
            title: 'Origem',
            width: 75
          }, {
            field: 'tipo',
            title: 'Tipo',
            textAlign: 'center',
            width: 75,
            template: (row) => {
              return '<span class="label font-weight-bold label-lg label-light-' + row.tipo + ' label-inline w-100 text-uppercase">' + row.tipo + '</span>';
            }
          }, {
            field: 'created_at',
            title: 'Data',
            width: 200
          }
        ],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
      });
      // EXIBIR TOOLTIPS
      $('#kt_datatable_log').KTDatatable().on('datatable-on-layout-updated', function() {
        $('[data-toggle="tooltip"]').tooltip();
      });
    });
    // EXIBIR O MODAL
    $('#modal-log-title').html(label);
    $('#modal-log').modal('show');
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create', function() {
    api.create();
  });
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-id'), $(this).attr('data-label'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete($(this).attr('data-id'));
  });
  // CONFIGURAR CUSTOM
  $(document).on('click', '.btn-debug', function() {
    custom.debug($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-refresh', function() {
    custom.refresh($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-script', function() {
    custom.script($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-log', function() {
    custom.log($(this).attr('data-id'), $(this).attr('data-label'));
  });
  // AVALIAR SE O CARREGAMENTO É UM CALLBACK
  let params = new URLSearchParams(window.location.search);
  if (params.get('sucesso')) {
    let sucesso = (params.get('sucesso') === 'true');
    // DEU CERTO
    if (sucesso) swal.fire({
      icon: 'success',
      title: 'Excelente!',
      text: 'A integração foi adicionada com sucesso.',
    });
    // DEU ERRADO
    else swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Algo errado não está certo.',
      footer: 'Por favor, tente novamente.'
    });
    // LIMPAR A FLAG DA URL
    window.history.replaceState({}, document.title, location.protocol + '//' + location.host + location.pathname);
  }
});