jQuery(document).ready(async () => {
  try {
    // EXIBIR TELA DE CARREGAMENTO
    swal.fire({
      title: 'Aguarde...',
      text: 'Processando a solicitação.',
      timer: 3000,
      onOpen: () => {
        swal.showLoading();
      },
      allowOutsideClick: false,
      allowEscapeKey: false
    });
    let relatorio = (await axios.get(API_URL + '/relatorios/' + id)).data;

    /*
     * ÚTEIS
     */

    // FUNÇÃO PARA VALIDAR URL
    const validarUrl = function(url) {
      const expressao = new RegExp('^(https?:\\/\\/)?((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*(\\?[;&a-z\\d%_.~+=-]*)?(\\#[-a-z\\d_]*)?$', 'i');
      return !!expressao.test(url);
    }

    /*
     * DATA
     */

    // CARREGAR DADOS DE CLIENTES E PROJETOS
    let clientes = (await axios.get(API_URL + '/clientes')).data;
    // MONTAR LISTA DE PROJETOS PARA SELEÇÃO
    let projetos = {};
    clientes.filter(cliente => cliente.status).forEach(cliente => {
      projetos[cliente.nome] = {};
      cliente.projetos.filter(cliente => cliente.status).forEach(projeto => {
        projetos[cliente.nome][projeto.id] = projeto.nome;
      });
    });
    // INICIALIAZR SELECT2 DE DATAS
    $('#mes').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    $('#mes').val(relatorio.mes ? relatorio.mes : new Date().getMonth() + 1);
    $('#ano').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    $('#ano').val(relatorio.ano ? relatorio.ano : new Date().getFullYear());
    // INICIALIZAR SELELCT2 DOS PROJETOS
    Object.keys(projetos).forEach((cliente) => $('#projeto').append('<optgroup label="' + cliente + '">' + Object.entries(projetos[cliente]).map((projeto) => '<option value="' + projeto.shift() + '">' + projeto.pop() + '</option>') + '</optgroup>'));
    $('#projeto').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    $('#projeto').val(relatorio.projeto_id ? relatorio.projeto_id : '');
    $('#mes, #ano, #projeto').trigger('change');
    // INICIALIZAR STATUS
    relatorio.status ? $('#status').prop('checked', true) : $('#status').prop('checked', false);

    /*
     * TAB 1 - DESTAQUES DO MÊS
     */

    // SALVAR OBJETO COM ALTERAÇÕES
    let relatorio_objeto = {};

    // 01 - VISÃO GERAL
    $('#visao-geral').val(relatorio.visao_geral ? relatorio.visao_geral : '');
    tinymce.init({
      selector: '#visao-geral',
      language: 'pt_BR',
      menubar: false,
      toolbar: ['undo redo | cut copy paste | fontsizeselect | bold italic | bullist numlist | alignleft aligncenter alignright | outdent indent | preview code'],
      statusbar: false,
      plugins: 'lists preview code'
    });

    // 02 - FUNIL DE VENDAS
    relatorio.funil = JSON.parse(relatorio.funil);
    $('#visitantes').val(relatorio.funil ? relatorio.funil.visitantes : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#leads').val(relatorio.funil ? relatorio.funil.leads : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#oportunidades').val(relatorio.funil ? relatorio.funil.oportunidades : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#vendas').val(relatorio.funil ? relatorio.funil.vendas : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#funil-vendas').val(relatorio.funil_comentarios ? relatorio.funil_comentarios : '');
    tinymce.init({
      selector: '#funil-vendas',
      language: 'pt_BR',
      menubar: false,
      toolbar: ['undo redo | cut copy paste | fontsizeselect | bold italic | bullist numlist | alignleft aligncenter alignright | outdent indent | preview code'],
      statusbar: false,
      plugins: 'lists preview code'
    });

    // 03 - PROCESSO DE COMPRA
    relatorio.processo_de_compra = JSON.parse(relatorio.processo_de_compra);
    $('#carrinho_criado').val(relatorio.processo_de_compra ? relatorio.processo_de_compra.carrinho_criado : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#inicio_compra').val(relatorio.processo_de_compra ? relatorio.processo_de_compra.inicio_compra : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#endereco_preenchido').val(relatorio.processo_de_compra ? relatorio.processo_de_compra.endereco_preenchido : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#pedido_criado').val(relatorio.processo_de_compra ? relatorio.processo_de_compra.pedido_criado : '').mask('000.000.000.000', {
      reverse: true
    });

    // 04 - MÉTRICAS DE AQUISIÇÃO
    $('#metricas-aquisicao').val(relatorio.aquisicao_comentarios ? relatorio.aquisicao_comentarios : '');
    tinymce.init({
      selector: '#metricas-aquisicao',
      language: 'pt_BR',
      menubar: false,
      toolbar: ['undo redo | cut copy paste | fontsizeselect | bold italic | bullist numlist | alignleft aligncenter alignright | outdent indent | preview code'],
      statusbar: false,
      plugins: 'lists preview code'
    });

    // 04.1 - VISÃO CONSOLIDADA
    relatorio.aquisicao_visao_consolidada = JSON.parse(relatorio.aquisicao_visao_consolidada);
    $('#pedidos_pagos').val(relatorio.aquisicao_visao_consolidada ? relatorio.aquisicao_visao_consolidada.pedidos_pagos.quantidade : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#pedidos_realizados').val(relatorio.aquisicao_visao_consolidada ? relatorio.aquisicao_visao_consolidada.pedidos_realizados.quantidade : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#faturamento_pago').val(relatorio.aquisicao_visao_consolidada ? relatorio.aquisicao_visao_consolidada.pedidos_pagos.faturamento : '').mask('000.000.000.000,00', {
      reverse: true
    });
    $('#ticket_medio_pago').val(relatorio.aquisicao_visao_consolidada ? parseFloat(relatorio.aquisicao_visao_consolidada.pedidos_pagos.faturamento / relatorio.aquisicao_visao_consolidada.pedidos_pagos.quantidade).toFixed(2) : '').mask('000.000.000.000,00', {
      reverse: true
    });
    $('#faturamento_realizado').val(relatorio.aquisicao_visao_consolidada ? relatorio.aquisicao_visao_consolidada.pedidos_realizados.faturamento : '').mask('000.000.000.000,00', {
      reverse: true
    });
    $('#ticket_medio_realizado').val(relatorio.aquisicao_visao_consolidada ? parseFloat(relatorio.aquisicao_visao_consolidada.pedidos_realizados.faturamento / relatorio.aquisicao_visao_consolidada.pedidos_realizados.quantidade).toFixed(2) : '').mask('000.000.000.000,00', {
      reverse: true
    });
    $('#pedidos_pagos, #faturamento_pago').on('blur', function() {
      $('#ticket_medio_pago').val(parseFloat($('#faturamento_pago').val().replace(/[^\d,]/g, '').replace(',', '.') / $('#pedidos_pagos').val().replace(/[^\d,]/g, '')).toLocaleString('pt-BR', {
        maximumFractionDigits: 2
      }));
    });
    $('#pedidos_realizados, #faturamento_realizado').on('blur', function() {
      $('#ticket_medio_realizado').val(parseFloat($('#faturamento_realizado').val().replace(/[^\d,]/g, '').replace(',', '.') / $('#pedidos_realizados').val().replace(/[^\d,]/g, '')).toLocaleString('pt-BR', {
        maximumFractionDigits: 2
      }));
    });

    // 04.2 - TRÁFEGO PAGO
    let trafego_table;
    let trafego_data = [];
    trafego_data = JSON.parse(relatorio.aquisicao_trafego_pago) ? JSON.parse(relatorio.aquisicao_trafego_pago) : [{
      tipo: '',
      pedidos: '',
      faturamento: '',
      investimento: ''
    }];
    trafego_data.forEach((item, index) => {
      trafego_data[index].tickt_medio = '=C' + (index + 1) + '/B' + (index + 1);
      trafego_data[index].roas = '=C' + (index + 1) + '/D' + (index + 1);
    })
    trafego_data.length == 1 ? $('#trafegoBtnDelRow').addClass('d-none') : '';
    trafego_table = jspreadsheet(document.getElementById('trafego_spreadsheet'), {
      data: trafego_data,
      columns: [{
          type: 'dropdown',
          title: 'Tipo',
          width: 100,
          source: [{
              'id': 'Facebook Ads',
              'name': 'Facebook Ads'
            },
            {
              'id': 'Google Ads',
              'name': 'Google Ads'
            },
            {
              'id': 'Pinterest Ads',
              'name': 'Pinterest Ads'
            },
            {
              'id': 'Linkedin Ads',
              'name': 'Linkedin Ads'
            }
          ]
        },
        {
          type: 'text',
          title: 'Pedidos',
          mask: '#.##,00',
          decimal: ',',
          width: 100,
        },
        {
          type: 'text',
          title: 'Faturamento (R$)',
          mask: '#.##0,00',
          width: 100,
        },
        {
          type: 'text',
          title: 'Investimento (R$)',
          mask: '#.##0,00',
          width: 100,
        },
        {
          type: 'text',
          title: 'Ticket Médio (R$)',
          mask: '#.##0,00',
          width: 100,
          readOnly: true
        },
        {
          type: 'text',
          title: 'ROAS',
          mask: '#.##0,00',
          width: 80,
          readOnly: true
        }
      ],
      oninsertrow: function() {
        if (trafego_table.getData().length > 1) {
          $('#trafegoBtnDelRow').removeClass('d-none');
        }
      },
      ondeleterow: function() {
        if (trafego_table.getData().length == 1) {
          $('#trafegoBtnDelRow').addClass('d-none');
        }
      }
    });
    //
    $('#trafegoBtnAddRow').on('click', function() {
      const ticket = '=C' + (trafego_table.getData().length + 1) + '/B' + (trafego_table.getData().length + 1);
      const roas = '=C' + (trafego_table.getData().length + 1) + '/D' + (trafego_table.getData().length + 1);
      trafego_table.insertRow(['', '', '', '', ticket, roas]);
    });
    //
    $('#trafegoBtnDelRow').on('click', function() {
      trafego_table.deleteRow();
    });

    /*
     * TAB 2 - MÍDIA PAGA
     */

    // 05 - MÍDIA PAGA
    relatorio.midia_paga = JSON.parse(relatorio.midia_paga) ? JSON.parse(relatorio.midia_paga) : [];
    // FUNÇÃO PARA FILTRAR DROPDOWN DA COLUNA
    const dropdownFilter = function(instance, cell, c, r, source) {
      let campanha_id = instance.jspreadsheet.getValueFromCoords(c - 8, r);
      if (campanha_id !== "") {
        return source.filter(function(item) {
          if (item.campanha_id == campanha_id) return true;
        })
      } else {
        return [];
      }
    };
    // SETANDO OS DROPDOWNS - MÍDIA PAGA
    let campanhas = [{
        'id': 'Tráfego',
        'name': 'Tráfego'
      },
      {
        'id': 'Interesses',
        'name': 'Interesses'
      },
      {
        'id': 'Look Alike',
        'name': 'Look Alike'
      },
      {
        'id': 'Público frio',
        'name': 'Público frio'
      },
      {
        'id': 'Remarketing',
        'name': 'Remarketing'
      },
      {
        'id': 'Cadastro',
        'name': 'Cadastro'
      },
      {
        'id': 'Alcance',
        'name': 'Alcance'
      },
      {
        'id': 'Envolvimento',
        'name': 'Envolvimento'
      }
    ];
    let indicadores = [{
        'id': 'Visualizações de Página',
        'campanha_id': 'Tráfego',
        'name': 'Visualizações de Página'
      },
      {
        'id': 'Adições ao Carrinho',
        'campanha_id': 'Interesses',
        'name': 'Adições ao Carrinho'
      },
      {
        'id': 'Adições ao Carrinho',
        'campanha_id': 'Look Alike',
        'name': 'Adições ao Carrinho'
      },
      {
        'id': 'Adições ao Carrinho',
        'campanha_id': 'Público frio',
        'name': 'Adições ao Carrinho'
      },
      {
        'id': 'Adições ao Carrinho',
        'campanha_id': 'Remarketing',
        'name': 'Adições ao Carrinho'
      },
      {
        'id': 'Compras',
        'campanha_id': 'Interesses',
        'name': 'Compras'
      },
      {
        'id': 'Compras',
        'campanha_id': 'Look Alike',
        'name': 'Compras'
      },
      {
        'id': 'Compras',
        'campanha_id': 'Público frio',
        'name': 'Compras'
      },
      {
        'id': 'Compras',
        'campanha_id': 'Remarketing',
        'name': 'Compras'
      },
      {
        'id': 'Leads',
        'campanha_id': 'Cadastro',
        'name': 'Leads'
      },
      {
        'id': 'CPM',
        'campanha_id': 'Alcance',
        'name': 'CPM'
      },
      {
        'id': 'Engajamentos',
        'campanha_id': 'Envolvimento',
        'name': 'Engajamentos'
      },
    ];
    let midia_paga_facebook_ads = {};
    let midia_paga_google_ads = {};
    let midia_paga_pinterest_ads = {};
    let midia_paga_linkedin_ads = {};
    let midiaPaga = function() {
      let n_item = 5;
      if (midia_paga_facebook_ads.table) {
        midia_paga_facebook_ads.anuncio = $('#midia_paga_facebook_ads_anuncio').val();
        midia_paga_facebook_ads.data = midia_paga_facebook_ads.table.getData();
        midia_paga_facebook_ads.table.destroy();
      }
      if (midia_paga_google_ads.table) {
        midia_paga_google_ads.anuncio = $('#midia_paga_google_ads_anuncio').val();
        midia_paga_google_ads.data = midia_paga_google_ads.table.getData();
        midia_paga_google_ads.table.destroy();
      }
      if (midia_paga_pinterest_ads.table) {
        midia_paga_pinterest_ads.anuncio = $('#midia_paga_pinterest_ads_anuncio').val();
        midia_paga_pinterest_ads.data = midia_paga_pinterest_ads.table.getData();
        midia_paga_pinterest_ads.table.destroy();
      }
      if (midia_paga_linkedin_ads.table) {
        midia_paga_linkedin_ads.anuncio = $('#midia_paga_linkedin_ads_anuncio').val();
        midia_paga_linkedin_ads.data = midia_paga_linkedin_ads.table.getData();
        midia_paga_linkedin_ads.table.destroy();
      }
      $('#padrao_bloco').removeClass('d-none');
      $('#facebook_ads_bloco').addClass('d-none');
      $('#google_ads_bloco').addClass('d-none');
      $('#pinterest_ads_bloco').addClass('d-none');
      $('#linkedin_ads_bloco').addClass('d-none');
      if (trafego_table.getData().length > 0) {
        trafego_table.getData().forEach((item, index) => {
          if (item[0] == 'Facebook Ads') {
            midia_paga_facebook_ads.tipo = 'Facebook Ads';
            $('#padrao_bloco').addClass('d-none');
            $('#facebook_ads_bloco').removeClass('d-none');
            $('#facebook_ads_titulo').html(n_item + (index == 0 ? '' : '.' + index) + '. Facebook Ads');
            let facebook_ads_data = [{
              tipo: '',
              alcance: '',
              impressoes: '',
              resultado: '',
              investimento: '',
              custo_por_opercacao: '=E1/D1',
              receita: '',
              roas: '=G1/E1',
              indicador: ''
            }];
            let facebook_ads_filter = relatorio.midia_paga.find(obj => obj.tipo == 'Facebook Ads');
            if (facebook_ads_filter) {
              midia_paga_facebook_ads.comentario = facebook_ads_filter.comentarios;
              !midia_paga_facebook_ads.anuncio ? midia_paga_facebook_ads.anuncio = facebook_ads_filter.ads_library : midia_paga_facebook_ads.anuncio = midia_paga_facebook_ads.anuncio;
              !midia_paga_facebook_ads.data ? facebook_ads_data = facebook_ads_filter.campanhas.map((item, index) => {
                return {
                  tipo: item.tipo,
                  alcance: item.alcance,
                  impressoes: item.impressoes,
                  resultado: item.resultado,
                  investimento: item.investimento,
                  custo_por_opercacao: '=E' + (index + 1) + '/D' + (index + 1),
                  receita: item.receita,
                  roas: '=G' + (index + 1) + '/E' + (index + 1),
                  indicador: item.resultado_indicador
                }
              }) : facebook_ads_data = midia_paga_facebook_ads.data;
            } else {
              midia_paga_facebook_ads.comentario = $('#midia-paga-facebook-ads').val();
              midia_paga_facebook_ads.anuncio = midia_paga_facebook_ads.anuncio;
              midia_paga_facebook_ads.table ? facebook_ads_data = midia_paga_facebook_ads.data : facebook_ads_data = [{
                tipo: '',
                alcance: '',
                impressoes: '',
                resultado: '',
                investimento: '',
                custo_por_opercacao: '=E1/D1',
                receita: '',
                roas: '=G1/E1',
                indicador: ''
              }];
            }
            $('#midia-paga-facebook-ads').val(midia_paga_facebook_ads.comentario ? midia_paga_facebook_ads.comentario : '');
            tinymce.init({
              selector: '#midia-paga-facebook-ads',
              language: 'pt_BR',
              menubar: false,
              toolbar: ['undo redo | cut copy paste | fontsizeselect | bold italic | bullist numlist | alignleft aligncenter alignright | outdent indent | preview code'],
              statusbar: false,
              plugins: 'lists preview code'
            });
            facebook_ads_data.length == 1 ? $('#midiaPagaFacebookAdsBtnDelRow').addClass('d-none') : '';
            midia_paga_facebook_ads.table = jspreadsheet(document.getElementById('midia_paga_facebook_ads_spreadsheet'), {
              data: facebook_ads_data,
              columns: [{
                  type: 'dropdown',
                  title: 'Campanha',
                  width: 90,
                  source: campanhas
                },
                {
                  type: 'text',
                  title: 'Alcance',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Impressões',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Resultado',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Investimento (R$)',
                  mask: '#.##0,00',
                  width: 85,
                },
                {
                  type: 'text',
                  title: 'Custo por Resultado (R$)',
                  mask: '#.##0,00',
                  width: 115,
                  readOnly: true
                },
                {
                  type: 'text',
                  title: 'Receita (R$)',
                  mask: '#.##0,00',
                  width: 85,
                },
                {
                  type: 'text',
                  title: 'ROAS',
                  mask: '#.##0,00',
                  width: 80,
                  readOnly: true
                },
                {
                  type: 'dropdown',
                  title: 'Indicador',
                  width: 95,
                  source: indicadores,
                  filter: dropdownFilter
                },
              ],
              oninsertrow: function() {
                if (midia_paga_facebook_ads.table.getData().length > 1) {
                  $('#midiaPagaFacebookAdsBtnDelRow').removeClass('d-none');
                }
              },
              ondeleterow: function(instance, cell, x, y, value) {
                if (midia_paga_facebook_ads.table.getData().length == 1) {
                  $('#midiaPagaFacebookAdsBtnDelRow').addClass('d-none');
                }
              }
            });
            $('#midia_paga_facebook_ads_anuncio').val(midia_paga_facebook_ads.anuncio ? midia_paga_facebook_ads.anuncio : '');
          } else if (item[0] == 'Google Ads') {
            midia_paga_google_ads.tipo = 'Google Ads';
            $('#padrao_bloco').addClass('d-none');
            $('#google_ads_bloco').removeClass('d-none');
            $('#google_ads_titulo').html(n_item + (index == 0 ? '' : '.' + index) + '. Google Ads');
            let google_ads_data = [{
              tipo: '',
              alcance: '',
              impressoes: '',
              resultado: '',
              investimento: '',
              custo_por_opercacao: '=E1/D1',
              receita: '',
              roas: '=G1/E1',
              indicador: ''
            }];
            let google_ads_filter = relatorio.midia_paga.find(obj => obj.tipo == 'Google Ads');
            if (google_ads_filter) {
              midia_paga_google_ads.comentario = google_ads_filter.comentarios;
              !midia_paga_google_ads.anuncio ? midia_paga_google_ads.anuncio = google_ads_filter.ads_library : midia_paga_google_ads.anuncio = midia_paga_google_ads.anuncio;
              !midia_paga_google_ads.data ? google_ads_data = google_ads_filter.campanhas.map((item, index) => {
                return {
                  tipo: item.tipo,
                  alcance: item.alcance,
                  impressoes: item.impressoes,
                  resultado: item.resultado,
                  investimento: item.investimento,
                  custo_por_opercacao: '=E' + (index + 1) + '/D' + (index + 1),
                  receita: item.receita,
                  roas: '=G' + (index + 1) + '/E' + (index + 1),
                  indicador: item.resultado_indicador
                }
              }) : google_ads_data = midia_paga_google_ads.data;
            } else {
              midia_paga_google_ads.comentario = $('#midia-paga-google-ads').val();
              midia_paga_google_ads.anuncio = midia_paga_google_ads.anuncio;
              midia_paga_google_ads.table ? google_ads_data = midia_paga_google_ads.data : google_ads_data = [{
                tipo: '',
                alcance: '',
                impressoes: '',
                resultado: '',
                investimento: '',
                custo_por_opercacao: '=E1/D1',
                receita: '',
                roas: '=G1/E1',
                indicador: ''
              }];
            }
            $('#midia-paga-google-ads').val(midia_paga_google_ads.comentario ? midia_paga_google_ads.comentario : '');
            tinymce.init({
              selector: '#midia-paga-google-ads',
              language: 'pt_BR',
              menubar: false,
              toolbar: ['undo redo | cut copy paste | fontsizeselect | bold italic | bullist numlist | alignleft aligncenter alignright | outdent indent | preview code'],
              statusbar: false,
              plugins: 'lists preview code'
            });
            google_ads_data.length == 1 ? $('#midiaPagaGoogleAdsBtnDelRow').addClass('d-none') : '';
            midia_paga_google_ads.table = jspreadsheet(document.getElementById('midia_paga_google_ads_spreadsheet'), {
              data: google_ads_data,
              columns: [{
                  type: 'dropdown',
                  title: 'Campanha',
                  width: 90,
                  source: campanhas
                },
                {
                  type: 'text',
                  title: 'Alcance',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Impressões',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Resultado',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Investimento (R$)',
                  mask: '#.##0,00',
                  width: 85,
                },
                {
                  type: 'text',
                  title: 'Custo por Resultado (R$)',
                  mask: '#.##0,00',
                  width: 115,
                  readOnly: true
                },
                {
                  type: 'text',
                  title: 'Receita (R$)',
                  mask: '#.##0,00',
                  width: 85,
                },
                {
                  type: 'text',
                  title: 'ROAS',
                  mask: '#.##0,00',
                  width: 80,
                  readOnly: true
                },
                {
                  type: 'dropdown',
                  title: 'Indicador',
                  width: 95,
                  source: indicadores,
                  filter: dropdownFilter
                },
              ],
              oninsertrow: function() {
                if (midia_paga_google_ads.table.getData().length > 1) {
                  $('#midiaPagaGoogleAdsBtnDelRow').removeClass('d-none');
                }
              },
              ondeleterow: function(instance, cell, x, y, value) {
                if (midia_paga_google_ads.table.getData().length == 1) {
                  $('#midiaPagaGoogleAdsBtnDelRow').addClass('d-none');
                }
              }
            });
            $('#midia_paga_google_ads_anuncio').val(midia_paga_google_ads.anuncio ? midia_paga_google_ads.anuncio : '');
          } else if (item[0] == 'Pinterest Ads') {
            midia_paga_pinterest_ads.tipo = 'Pinterest Ads';
            $('#padrao_bloco').addClass('d-none');
            $('#pinterest_ads_bloco').removeClass('d-none');
            $('#pinterest_ads_titulo').html(n_item + (index == 0 ? '' : '.' + index) + '. Pinterest Ads');
            let pinterest_ads_data = [{
              tipo: '',
              alcance: '',
              impressoes: '',
              resultado: '',
              investimento: '',
              custo_por_opercacao: '=E1/D1',
              receita: '',
              roas: '=G1/E1',
              indicador: ''
            }];
            let pinterest_ads_filter = relatorio.midia_paga.find(obj => obj.tipo == 'Pinterest Ads');
            if (pinterest_ads_filter) {
              midia_paga_pinterest_ads.comentario = pinterest_ads_filter.comentarios;
              !midia_paga_pinterest_ads.anuncio ? midia_paga_pinterest_ads.anuncio = pinterest_ads_filter.ads_library : midia_paga_pinterest_ads.anuncio = midia_paga_pinterest_ads.anuncio;
              !midia_paga_pinterest_ads.data ? pinterest_ads_data = pinterest_ads_filter.campanhas.map((item, index) => {
                return {
                  tipo: item.tipo,
                  alcance: item.alcance,
                  impressoes: item.impressoes,
                  resultado: item.resultado,
                  investimento: item.investimento,
                  custo_por_opercacao: '=E' + (index + 1) + '/D' + (index + 1),
                  receita: item.receita,
                  roas: '=G' + (index + 1) + '/E' + (index + 1),
                  indicador: item.resultado_indicador
                }
              }) : pinterest_ads_data = midia_paga_pinterest_ads.data;
            } else {
              midia_paga_pinterest_ads.comentario = $('#midia-paga-pinterest-ads').val();
              midia_paga_pinterest_ads.anuncio = midia_paga_pinterest_ads.anuncio;
              midia_paga_pinterest_ads.table ? pinterest_ads_data = midia_paga_pinterest_ads.data : pinterest_ads_data = [{
                tipo: '',
                alcance: '',
                impressoes: '',
                resultado: '',
                investimento: '',
                custo_por_opercacao: '=E1/D1',
                receita: '',
                roas: '=G1/E1',
                indicador: ''
              }];
            }
            $('#midia-paga-pinterest-ads').val(midia_paga_pinterest_ads.comentario ? midia_paga_pinterest_ads.comentario : '');
            tinymce.init({
              selector: '#midia-paga-pinterest-ads',
              language: 'pt_BR',
              menubar: false,
              toolbar: ['undo redo | cut copy paste | fontsizeselect | bold italic | bullist numlist | alignleft aligncenter alignright | outdent indent | preview code'],
              statusbar: false,
              plugins: 'lists preview code'
            });
            pinterest_ads_data.length == 1 ? $('#midiaPagaPinterestAdsBtnDelRow').addClass('d-none') : '';
            midia_paga_pinterest_ads.table = jspreadsheet(document.getElementById('midia_paga_pinterest_ads_spreadsheet'), {
              data: pinterest_ads_data,
              columns: [{
                  type: 'dropdown',
                  title: 'Campanha',
                  width: 90,
                  source: campanhas
                },
                {
                  type: 'text',
                  title: 'Alcance',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Impressões',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Resultado',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Investimento (R$)',
                  mask: '#.##0,00',
                  width: 85,
                },
                {
                  type: 'text',
                  title: 'Custo por Resultado (R$)',
                  mask: '#.##0,00',
                  width: 115,
                  readOnly: true
                },
                {
                  type: 'text',
                  title: 'Receita (R$)',
                  mask: '#.##0,00',
                  width: 85,
                },
                {
                  type: 'text',
                  title: 'ROAS',
                  mask: '#.##0,00',
                  width: 80,
                  readOnly: true
                },
                {
                  type: 'dropdown',
                  title: 'Indicador',
                  width: 95,
                  source: indicadores,
                  filter: dropdownFilter
                },
              ],
              oninsertrow: function() {
                if (midia_paga_pinterest_ads.table.getData().length > 1) {
                  $('#midiaPagaPinterestAdsBtnDelRow').removeClass('d-none');
                }
              },
              ondeleterow: function(instance, cell, x, y, value) {
                if (midia_paga_pinterest_ads.table.getData().length == 1) {
                  $('#midiaPagaPinterestAdsBtnDelRow').addClass('d-none');
                }
              }
            });
            $('#midia_paga_pinterest_ads_anuncio').val(midia_paga_pinterest_ads.anuncio ? midia_paga_pinterest_ads.anuncio : '');
          } else if (item[0] == 'Linkedin Ads') {
            midia_paga_linkedin_ads.tipo = 'Linkedin Ads';
            $('#padrao_bloco').addClass('d-none');
            $('#linkedin_ads_bloco').removeClass('d-none');
            $('#linkedin_ads_titulo').html(n_item + (index == 0 ? '' : '.' + index) + '. Linkedin Ads');
            let linkedin_ads_data = [{
              tipo: '',
              alcance: '',
              impressoes: '',
              resultado: '',
              investimento: '',
              custo_por_opercacao: '=E1/D1',
              receita: '',
              roas: '=G1/E1',
              indicador: ''
            }];
            let linkedin_ads_filter = relatorio.midia_paga.find(obj => obj.tipo == 'Linkedin Ads');
            if (linkedin_ads_filter) {
              midia_paga_linkedin_ads.comentario = linkedin_ads_filter.comentarios;
              !midia_paga_linkedin_ads.anuncio ? midia_paga_linkedin_ads.anuncio = linkedin_ads_filter.ads_library : midia_paga_linkedin_ads.anuncio = midia_paga_linkedin_ads.anuncio;
              !midia_paga_linkedin_ads.data ? linkedin_ads_data = linkedin_ads_filter.campanhas.map((item, index) => {
                return {
                  tipo: item.tipo,
                  alcance: item.alcance,
                  impressoes: item.impressoes,
                  resultado: item.resultado,
                  investimento: item.investimento,
                  custo_por_opercacao: '=E' + (index + 1) + '/D' + (index + 1),
                  receita: item.receita,
                  roas: '=G' + (index + 1) + '/E' + (index + 1),
                  indicador: item.resultado_indicador
                }
              }) : linkedin_ads_data = midia_paga_linkedin_ads.data;
            } else {
              midia_paga_linkedin_ads.comentario = $('#midia-paga-linkedin-ads').val();
              midia_paga_linkedin_ads.anuncio = midia_paga_linkedin_ads.anuncio;
              midia_paga_linkedin_ads.table ? linkedin_ads_data = midia_paga_linkedin_ads.data : linkedin_ads_data = [{
                tipo: '',
                alcance: '',
                impressoes: '',
                resultado: '',
                investimento: '',
                custo_por_opercacao: '=E1/D1',
                receita: '',
                roas: '=G1/E1',
                indicador: ''
              }];
            }
            $('#midia-paga-linkedin-ads').val(midia_paga_linkedin_ads.comentario ? midia_paga_linkedin_ads.comentario : '');
            tinymce.init({
              selector: '#midia-paga-linkedin-ads',
              language: 'pt_BR',
              menubar: false,
              toolbar: ['undo redo | cut copy paste | fontsizeselect | bold italic | bullist numlist | alignleft aligncenter alignright | outdent indent | preview code'],
              statusbar: false,
              plugins: 'lists preview code'
            });
            linkedin_ads_data.length == 1 ? $('#midiaPagaLinkedinAdsBtnDelRow').addClass('d-none') : '';
            midia_paga_linkedin_ads.table = jspreadsheet(document.getElementById('midia_paga_linkedin_ads_spreadsheet'), {
              data: linkedin_ads_data,
              columns: [{
                  type: 'dropdown',
                  title: 'Campanha',
                  width: 90,
                  source: campanhas
                },
                {
                  type: 'text',
                  title: 'Alcance',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Impressões',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Resultado',
                  mask: '#.##,00',
                  decimal: ',',
                  width: 75,
                },
                {
                  type: 'text',
                  title: 'Investimento (R$)',
                  mask: '#.##0,00',
                  width: 85,
                },
                {
                  type: 'text',
                  title: 'Custo por Resultado (R$)',
                  mask: '#.##0,00',
                  width: 115,
                  readOnly: true
                },
                {
                  type: 'text',
                  title: 'Receita (R$)',
                  mask: '#.##0,00',
                  width: 85,
                },
                {
                  type: 'text',
                  title: 'ROAS',
                  mask: '#.##0,00',
                  width: 80,
                  readOnly: true
                },
                {
                  type: 'dropdown',
                  title: 'Indicador',
                  width: 95,
                  source: indicadores,
                  filter: dropdownFilter
                },
              ],
              oninsertrow: function() {
                if (midia_paga_linkedin_ads.table.getData().length > 1) {
                  $('#midiaPagaLinkedinAdsBtnDelRow').removeClass('d-none');
                }
              },
              ondeleterow: function(instance, cell, x, y, value) {
                if (midia_paga_linkedin_ads.table.getData().length == 1) {
                  $('#midiaPagaLinkedinAdsBtnDelRow').addClass('d-none');
                }
              }
            });
            $('#midia_paga_linkedin_ads_anuncio').val(midia_paga_linkedin_ads.anuncio ? midia_paga_linkedin_ads.anuncio : '');
          }
        });
      }
    }
    relatorio.midia_paga ? midiaPaga() : '';
    // ATUALIZAR MÍDIA PAGA DE ACORDO COM TRÁFEGO PAGO
    $('.nav-link[href="#kt_tab_pane_2"').on('click', function() {
      midiaPaga();
    });
    // EVENTOS DE ADIÇÃO DE LINHAS PARA AS TABELAS
    // FACEBOOK
    $('#midiaPagaFacebookAdsBtnAddRow').on('click', function() {
      const custo_por_resultado = '=E' + (midia_paga_facebook_ads.table.getData().length + 1) + '/D' + (midia_paga_facebook_ads.table.getData().length + 1);
      const roas = '=G' + (midia_paga_facebook_ads.table.getData().length + 1) + '/E' + (midia_paga_facebook_ads.table.getData().length + 1);
      midia_paga_facebook_ads.table.insertRow(['', '', '', '', '', custo_por_resultado, '', roas, '']);
    });
    $('#midiaPagaFacebookAdsBtnDelRow').on('click', function() {
      midia_paga_facebook_ads.table.deleteRow();
    });
    // GOOGLE
    $('#midiaPagaGoogleAdsBtnAddRow').on('click', function() {
      const custo_por_resultado = '=E' + (midia_paga_google_ads.table.getData().length + 1) + '/D' + (midia_paga_google_ads.table.getData().length + 1);
      const roas = '=G' + (midia_paga_facebook_ads.table.getData().length + 1) + '/E' + (midia_paga_facebook_ads.table.getData().length + 1);
      midia_paga_google_ads.table.insertRow(['', '', '', '', '', custo_por_resultado, '', roas, '']);
    });
    $('#midiaPagaGoogleAdsBtnDelRow').on('click', function() {
      midia_paga_google_ads.table.deleteRow();
    });
    // PINTEREST
    $('#midiaPagaPinterestAdsBtnAddRow').on('click', function() {
      const custo_por_resultado = '=E' + (midia_paga_pinterest_ads.table.getData().length + 1) + '/D' + (midia_paga_pinterest_ads.table.getData().length + 1);
      const roas = '=G' + (midia_paga_facebook_ads.table.getData().length + 1) + '/E' + (midia_paga_facebook_ads.table.getData().length + 1);
      midia_paga_pinterest_ads.table.insertRow(['', '', '', '', '', custo_por_resultado, '', roas, '']);
    });
    $('#midiaPagaPinterestAdsBtnDelRow').on('click', function() {
      midia_paga_pinterest_ads.table.deleteRow();
    });
    // LINKEDIN
    $('#midiaPagaLinkedinAdsBtnAddRow').on('click', function() {
      const custo_por_resultado = '=E' + (midia_paga_linkedin_ads.table.getData().length + 1) + '/D' + (midia_paga_linkedin_ads.table.getData().length + 1);
      const roas = '=G' + (midia_paga_facebook_ads.table.getData().length + 1) + '/E' + (midia_paga_facebook_ads.table.getData().length + 1);
      midia_paga_linkedin_ads.table.insertRow(['', '', '', '', '', custo_por_resultado, '', roas, '']);
    });
    $('#midiaPagaLinkedinAdsBtnDelRow').on('click', function() {
      midia_paga_linkedin_ads.table.deleteRow();
    });

    /*
     * TAB 3 - LANDING PAGES
     */

    // 06 - LANDING PAGES
    $('#landing-pages').val(relatorio.landing_pages_comentarios ? relatorio.landing_pages_comentarios : '');
    tinymce.init({
      selector: '#landing-pages',
      language: 'pt_BR',
      menubar: false,
      toolbar: ['undo redo | cut copy paste | fontsizeselect | bold italic | bullist numlist | alignleft aligncenter alignright | outdent indent | preview code'],
      statusbar: false,
      plugins: 'lists preview code'
    });
    //
    let landing_pages_table;
    let landing_pages_data = [];
    relatorio.landing_pages = JSON.parse(relatorio.landing_pages);
    relatorio.landing_pages ? landing_pages_data = relatorio.landing_pages.colecoes : landing_pages_data = [{
      nome: '',
      visitante: '',
      leads: '',
      taxa_conversao: '=C1/B1'
    }];
    landing_pages_data = landing_pages_data.map((item, index) => {
      return {
        nome: item.nome,
        visitante: item.visitantes,
        leads: item.leads,
        taxa_conversao: '=(C' + (index + 1) + '/B' + (index + 1) + ')*100'
      }
    });
    landing_pages_data.length == 1 ? $('#landingPagesBtnDelRow').addClass('d-none') : '';
    landing_pages_table = jspreadsheet(document.getElementById('landing_pages_spreadsheet'), {
      data: landing_pages_data,
      columns: [{
          type: 'text',
          title: 'Nome',
          width: 120,
        },
        {
          type: 'text',
          title: 'Visitantes',
          mask: '#.##,00',
          decimal: ',',
          width: 100,
        },
        {
          type: 'text',
          title: 'Leads',
          mask: '#.##,00',
          decimal: ',',
          width: 100,
        },
        {
          type: 'number',
          title: 'Taxa de Conversão (%)',
          mask: '#.##0,00',
          width: 120,
          readOnly: true
        }
      ],
      oninsertrow: function() {
        if (landing_pages_table.getData().length > 1) {
          $('#landingPagesBtnDelRow').removeClass('d-none');
        }
      },
      ondeleterow: function(instance, cell, x, y, value) {
        if (landing_pages_table.getData().length == 1) {
          $('#landingPagesBtnDelRow').addClass('d-none');
        }
      }
    });
    $('#landingPagesBtnAddRow').on('click', function() {
      const taxa_conversao = '=(C' + (landing_pages_table.getData().length + 1) + '/B' + (landing_pages_table.getData().length + 1) + ')*100'
      landing_pages_table.insertRow(['', '', '', taxa_conversao]);
    });
    $('#landingPagesBtnDelRow').on('click', function() {
      landing_pages_table.deleteRow();
    });

    // 06.1 - DESTAQUES
    let landing_pages_destaques_table;
    let landing_pages_destaques_data = [];
    relatorio.landing_pages ? landing_pages_destaques_data = relatorio.landing_pages.destaques.map((item) => {
      return {
        urls: item
      }
    }) : landing_pages_destaques_data = [{
      urls: ''
    }];
    landing_pages_destaques_data.length == 1 ? $('#landingPagesDestaquesBtnDelRow').addClass('d-none') : '';
    landing_pages_destaques_table = jspreadsheet(document.getElementById('landing_pages_destaques_spreadsheet'), {
      data: landing_pages_destaques_data,
      columns: [{
        type: 'text',
        title: 'Urls Destaque',
        width: 1000,
      }],
      onchange: function(instance, cell, x, y, value) {
        if (x == 0) {
          if (validarUrl(value) === false) {
            swal.close();
            swal.fire({
              icon: 'warning',
              title: 'Url inválida!',
              text: 'A url "' + value + '" não possui formato válido.',
            });
          }
        }
      },
      oninsertrow: function() {
        if (landing_pages_destaques_table.getData().length > 1) {
          $('#landingPagesDestaquesBtnDelRow').removeClass('d-none');
        }
      },
      ondeleterow: function() {
        if (landing_pages_destaques_table.getData().length == 1) {
          $('#landingPagesDestaquesBtnDelRow').addClass('d-none');
        }
      }
    });
    $('#landingPagesDestaquesBtnAddRow').on('click', function() {
      landing_pages_destaques_table.insertRow();
    });
    $('#landingPagesDestaquesBtnDelRow').on('click', function() {
      landing_pages_destaques_table.deleteRow();
    });

    /*
     * TAB 4 - E-MAIL MARKETING
     */

    // 07 - E-MAIL MARKETING
    relatorio.email_marketing = JSON.parse(relatorio.email_marketing) ? JSON.parse(relatorio.email_marketing) : [];
    $('#email-mkt').val(relatorio.email_marketing_comentarios ? relatorio.email_marketing_comentarios : '');
    tinymce.init({
      selector: '#email-mkt',
      language: 'pt_BR',
      menubar: false,
      toolbar: ['undo redo | cut copy paste | fontsizeselect | bold italic | bullist numlist | alignleft aligncenter alignright | outdent indent | preview code'],
      statusbar: false,
      plugins: 'lists preview code'
    });

    // 07.1 - DESTAQUES
    let email_mkt_destaques_preview = '';
    let email_mkt_destaques_urls = [];
    let email_mkt_upload_urls = [];
    let email_mkt_uppyDrag;
    relatorio.email_marketing ? email_mkt_destaques_urls = relatorio.email_marketing.destaques : email_mkt_destaques_urls = [];
    relatorio.email_marketing.destaques ? relatorio.email_marketing.destaques.forEach((item, index) => {
      email_mkt_destaques_preview += '<div class="uppy-thumbnail-container email-marketing-destaques" data-value="' + item + '" data-id="' + index + '"><div class="uppy-thumbnail"><img src="' + item + '"/></div><span class="uppy-thumbnail-label">' + item.split('/').pop() + '</span><span class="form-text text-muted"><a class="kt-link" href="' + item + '" target="_blank"><i class="flaticon-download"></i></a></span><span data-id="' + index + '" class="uppy-remove-thumbnail"><i class="flaticon2-cancel-music"></i></span></div>';
    }) : '';
    $('#email_mkt_destaques' + ' .uppy-thumbnails').append(email_mkt_destaques_preview);
    email_mkt_uppyDrag = Uppy.Core({
      autoProceed: true,
      restrictions: {
        maxFileSize: 10000000, // 1mb
        maxNumberOfFiles: 10,
        minNumberOfFiles: 1,
        allowedFileTypes: ['image/*']
      },
      locale: Uppy.locales.pt_BR
    });
    email_mkt_uppyDrag.use(Uppy.DragDrop, {
      target: '#email_mkt_destaques' + ' .uppy-drag'
    });
    email_mkt_uppyDrag.use(Uppy.ProgressBar, {
      target: '#email_mkt_destaques' + ' .uppy-progress',
      hideUploadButton: false,
      hideAfterFinish: false
    });
    email_mkt_uppyDrag.use(Uppy.Informer, {
      target: '#email_mkt_destaques' + ' .uppy-informer'
    });
    email_mkt_uppyDrag.use(Uppy.Tus, {
      endpoint: 'https://master.tus.io/files/'
    });
    email_mkt_uppyDrag.on('complete', async function(file) {
      email_mkt_destaques_urls = [];
      let files_array = file.successful.map((item) => {
        return item.data;
      });
      let formData = new FormData();
      if (files_array.length > 0) {
        files_array.forEach((file) => {
          formData.append('files', file, file.name);
        });
        // CHAMADA
        email_mkt_upload_urls = (await axios.post(API_URL + '/upload-files/email-marketing-destaques', formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })).data;
      }
      let image_preview = "";
      $.each(file.successful, function(index, value) {
        let image_type = /image/;
        let thumbnail = "";
        if (image_type.test(value.type)) {
          thumbnail = '<div class="uppy-thumbnail"><img src="' + value.uploadURL + '"/></div>';
        }
        image_preview += '<div class="uppy-thumbnail-container email-marketing-destaques" data-value="' + email_mkt_upload_urls[index] + '" data-id="' + value.id + '">' + thumbnail + ' <span class="uppy-thumbnail-label">' + value.name + '</span><span data-id="' + value.id + '" class="uppy-remove-thumbnail"><i class="flaticon2-cancel-music"></i></span></div>';
      });
      $('#email_mkt_destaques' + ' .uppy-thumbnails').append(image_preview);
      $('.email-marketing-destaques').each(function() {
        email_mkt_destaques_urls.push($(this).data('value'));
      });
    });
    $(document).on('click', '#email_mkt_destaques' + ' .uppy-thumbnails .uppy-remove-thumbnail', function() {
      email_mkt_destaques_urls = [];
      let image_id = $(this).attr('data-id');
      email_mkt_uppyDrag.removeFile(image_id);
      $('#email_mkt_destaques' + ' .uppy-thumbnail-container[data-id="' + image_id + '"').remove();
      $('.email-marketing-destaques').each(function() {
        email_mkt_destaques_urls.push($(this).data('value'));
      });
    });
    // 07.2 - CAMPANHAS
    let email_marketing_table;
    let email_marketing_data = [];
    relatorio.email_marketing.campanhas ? relatorio.email_marketing.campanhas.length == 1 ? $('#emailMktBtnDelRow').addClass('d-none') : '' : relatorio.email_marketing.campanhas = [{
      nome: '',
      emails_entregues: '',
      taxa_de_entrega: '',
      taxa_de_abertura: '',
      taxa_de_cliques: ''
    }];
    relatorio.email_marketing.campanhas.length == 1 ? $('#emailMktBtnDelRow').addClass('d-none') : '';
    email_marketing_data = relatorio.email_marketing.campanhas.map((item) => {
      return {
        nome: item.nome,
        emails_entregues: item.emails_entregues,
        taxa_de_entrega: item.taxa_de_entrega * 100,
        taxa_de_abertura: item.taxa_de_abertura * 100,
        taxa_de_cliques: item.taxa_de_cliques * 100
      }
    })
    email_marketing_table = jspreadsheet(document.getElementById('email_mkt_spreadsheet'), {
      data: email_marketing_data,
      columns: [{
          type: 'text',
          title: 'Nome',
          width: 200,
        },
        {
          type: 'text',
          title: 'E-mails Entregues',
          mask: '#.##,00',
          decimal: ',',
          width: 100,
        },
        {
          type: 'number',
          title: 'Taxa de Entrega (%)',
          mask: '#.##0,00',
          width: 100,
        },
        {
          type: 'number',
          title: 'Taxa de Abertura (%)',
          mask: '#.##0,00',
          width: 100,
        },
        {
          type: 'number',
          title: 'Taxa de Cliques (%)',
          mask: '#.##0,00',
          width: 100,
        }
      ],
      oninsertrow: function() {
        if (email_marketing_table.getData().length > 1) {
          $('#emailMktBtnDelRow').removeClass('d-none');
        }
      },
      ondeleterow: function(instance, cell, x, y, value) {
        if (email_marketing_table.getData().length == 1) {
          $('#emailMktBtnDelRow').addClass('d-none');
        }
      }
    });
    //
    $('#emailMktBtnAddRow').on('click', function() {
      email_marketing_table.insertRow();
    });
    //
    $('#emailMktBtnDelRow').on('click', function() {
      email_marketing_table.deleteRow();
    });
    // UPLOAD DE CAMPANHAS - ARQUIVOS .XLSX
    $('#email_mkt_upload').on('change', function(event) {
      let selected_file = event.target.files[0];
      if (selected_file) {
        console.log(selected_file.name)
        let file_reader = new FileReader();
        file_reader.readAsBinaryString(selected_file);
        file_reader.onload = (event) => {
          let data = event.target.result;
          let workbook = XLSX.read(data, {
            type: 'binary'
          });
          workbook.SheetNames.forEach(sheet => {
            let object_array = JSON.stringify(XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheet]));
            if (Object.keys(JSON.parse(object_array).shift()).length != 5 && Object.keys(JSON.parse(object_array).shift()).length != 6) {
              swal.fire({
                icon: 'info',
                title: '<strong>O arquivo "' + selected_file.name + '" selecionado possui formatação inválida. Acesse o ' + '<a href="https://app.rdstation.com.br/emails/sent" target="_blank" style="color: blue">link</a>' + ' e faça o download do arquivo conforme o exemplo a seguir.</strong>',
                html: '<iframe width="920px" height="480px" src="' + window.location.origin + '/media/tutorial.mp4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
                confirmButtonText: 'Fechar',
                customClass: 'swal-wide'
              });
            } else {
              JSON.parse(object_array).forEach((item) => {
                if (Object.keys(item).length == 6) {
                  email_marketing_table.insertRow([item[Object.keys(item)[1]], item[Object.keys(item)[2]], item[Object.keys(item)[3]], item[Object.keys(item)[4]], item[Object.keys(item)[5]]], 1);
                } else if (Object.keys(item).length == 5) {
                  email_marketing_table.insertRow([item[Object.keys(item)[0]], item[Object.keys(item)[1]], item[Object.keys(item)[2]], item[Object.keys(item)[3]], item[Object.keys(item)[4]]], 1);
                }
              });
            }
          });
        }
      }
    });

    /*
     * TAB 5 - REDES SOCIAIS
     */

    // 08 - REDES SOCIAIS
    relatorio.redes_sociais = JSON.parse(relatorio.redes_sociais) ? JSON.parse(relatorio.redes_sociais) : '';
    $('#campanhas').val(relatorio.redes_sociais ? relatorio.redes_sociais.campanhas : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#videos_gifs').val(relatorio.redes_sociais ? relatorio.redes_sociais.videos_gifs : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#pecas_de_feed').val(relatorio.redes_sociais ? relatorio.redes_sociais.pecas_de_feed : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#pecas_de_stories').val(relatorio.redes_sociais ? relatorio.redes_sociais.pecas_de_stories : '').mask('000.000.000.000', {
      reverse: true
    });
    $('#total_de_criativos').val(relatorio.redes_sociais ? (parseInt($('#pecas_de_feed').val().replace(/[^\d,]/g, '')) + parseInt($('#pecas_de_stories').val().replace(/[^\d,]/g, '')) + parseInt($('#videos_gifs').val().replace(/[^\d,]/g, ''))).toLocaleString('pt-BR', {
      maximumFractionDigits: 2
    }) : '');
    $('#pecas_de_feed, #pecas_de_stories, #videos_gifs').on('blur', function() {
      $('#total_de_criativos').val((parseInt($('#pecas_de_feed').val().replace(/[^\d,]/g, '')) + parseInt($('#pecas_de_stories').val().replace(/[^\d,]/g, '')) + parseInt($('#videos_gifs').val().replace(/[^\d,]/g, ''))).toLocaleString('pt-BR', {
        maximumFractionDigits: 2
      }));
    });
    // 08.1 - DESTAQUES
    let redes_sociais_destaques_preview = '';
    let redes_sociais_upload_destaques = [];
    let redes_sociais_destaques = [];
    let redes_sociais_uppyDrag;
    relatorio.redes_sociais ? redes_sociais_destaques = relatorio.redes_sociais.destaques : redes_sociais_destaques = [];
    relatorio.redes_sociais.destaques ? relatorio.redes_sociais.destaques.forEach((item, index) => {
      redes_sociais_destaques_preview += '<div class="uppy-thumbnail-container redes-sociais-destaques" data-value="' + item + '" data-id="' + index + '"><div class="uppy-thumbnail"><img src="' + item + '"/></div><span class="uppy-thumbnail-label">' + item.split('/').pop() + '</span><span class="form-text text-muted"><a class="kt-link" href="' + item + '" target="_blank"><i class="flaticon-download"></i></a></span><span data-id="' + index + '" class="uppy-remove-thumbnail"><i class="flaticon2-cancel-music"></i></span></div>';
    }) : '';
    $('#redes_sociais_destaques' + ' .uppy-thumbnails').append(redes_sociais_destaques_preview);
    redes_sociais_uppyDrag = Uppy.Core({
      autoProceed: true,
      restrictions: {
        maxFileSize: 10000000, // 1mb
        maxNumberOfFiles: 10,
        minNumberOfFiles: 1,
        allowedFileTypes: ['image/*']
      },
      locale: Uppy.locales.pt_BR
    });
    redes_sociais_uppyDrag.use(Uppy.DragDrop, {
      target: '#redes_sociais_destaques' + ' .uppy-drag'
    });
    redes_sociais_uppyDrag.use(Uppy.ProgressBar, {
      target: '#redes_sociais_destaques' + ' .uppy-progress',
      hideUploadButton: false,
      hideAfterFinish: false
    });
    redes_sociais_uppyDrag.use(Uppy.Informer, {
      target: '#redes_sociais_destaques' + ' .uppy-informer'
    });
    redes_sociais_uppyDrag.use(Uppy.Tus, {
      endpoint: 'https://master.tus.io/files/'
    });
    redes_sociais_uppyDrag.on('complete', async function(file) {
      redes_sociais_destaques = [];
      let files_array = file.successful.map((item) => {
        return item.data;
      });
      let formData = new FormData();
      if (files_array.length > 0) {
        files_array.forEach((file) => {
          formData.append('files', file, file.name);
        });
        // CHAMADA
        redes_sociais_upload_destaques = (await axios.post(API_URL + '/upload-files/redes-sociais-destaques', formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })).data;
      }
      let image_preview = "";
      $.each(file.successful, function(index, value) {
        let image_type = /image/;
        let thumbnail = "";
        if (image_type.test(value.type)) {
          thumbnail = '<div class="uppy-thumbnail"><img src="' + value.uploadURL + '"/></div>';
        }
        image_preview += '<div class="uppy-thumbnail-container redes-sociais-destaques" data-value="' + redes_sociais_upload_destaques[index] + '" data-id="' + value.id + '">' + thumbnail + ' <span class="uppy-thumbnail-label">' + value.name + '</span><span data-id="' + value.id + '" class="uppy-remove-thumbnail"><i class="flaticon2-cancel-music"></i></span></div>';
      });
      $('#redes_sociais_destaques' + ' .uppy-thumbnails').append(image_preview);
      $('.redes-sociais-destaques').each(function() {
        redes_sociais_destaques.push($(this).data('value'));
      });
    });
    $(document).on('click', '#redes_sociais_destaques' + ' .uppy-thumbnails .uppy-remove-thumbnail', function() {
      redes_sociais_destaques = [];
      let image_id = $(this).attr('data-id');
      redes_sociais_uppyDrag.removeFile(image_id);
      $('#redes_sociais_destaques' + ' .uppy-thumbnail-container[data-id="' + image_id + '"').remove();
      $('.redes-sociais-destaques').each(function() {
        redes_sociais_destaques.push($(this).data('value'));
      });
    });

    /*
     * TAB 6 - PLANEJAMENTO
     */

    // 09 - PLANEJAMENTO
    $('#planejamento').val(relatorio.proximas_acoes ? relatorio.proximas_acoes : '');
    tinymce.init({
      selector: '#planejamento',
      language: 'pt_BR',
      menubar: false,
      toolbar: ['undo redo | cut copy paste | fontsizeselect | bold italic | bullist numlist | alignleft aligncenter alignright | outdent indent | preview code'],
      statusbar: false,
      plugins: 'lists preview code'
    });

    /*
     * SALVAR ALTERAÇÕES
     */

    $('.btn-update').on('click', async function() {
      let confirmacao = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja salvar as alterações neste relatório?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Salvar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      if (confirmacao.value) {
        try {
          // EXIBIR TELA DE CARREGAMENTO
          swal.fire({
            title: 'Aguarde...',
            text: 'Processando a solicitação.',
            onOpen: () => {
              swal.showLoading();
            },
            allowOutsideClick: false,
            allowEscapeKey: false
          });
          // PREPARAR OBJETO
          tinymce.triggerSave();
          relatorio_objeto = {

            /*
             * DATA E PROJETO
             */

            mes: $('#mes').val(),
            ano: $('#ano').val(),
            projeto_id: $('#projeto').val(),
            status: ($('#status').prop('checked') ? 1 : 0),

            /*
             * TAB 1
             */

            visao_geral: $('#visao-geral').val() ? $('#visao-geral').val() : '',
            funil: JSON.stringify({
              visitantes: $('#visitantes').val() ? parseInt($('#visitantes').val().replace(".", "")) : '',
              leads: $('#leads').val() ? parseInt($('#leads').val().replace(".", "")) : '',
              oportunidades: $('#oportunidades').val() ? parseInt($('#oportunidades').val().replace(".", "")) : '',
              vendas: $('#vendas').val() ? parseInt($('#vendas').val().replace(".", "")) : ''
            }),
            funil_comentarios: $('#funil-vendas').val() ? $('#funil-vendas').val() : '',
            processo_de_compra: JSON.stringify({
              carrinho_criado: $('#carrinho_criado').val() ? parseInt($('#carrinho_criado').val().replace(".", "")) : '',
              inicio_compra: $('#inicio_compra').val() ? parseInt($('#inicio_compra').val().replace(".", "")) : '',
              endereco_preenchido: $('#endereco_preenchido').val() ? parseInt($('#endereco_preenchido').val().replace(".", "")) : '',
              pedido_criado: $('#pedido_criado').val() ? parseInt($('#pedido_criado').val().replace(".", "")) : '',
            }),
            aquisicao_comentarios: $('#metricas-aquisicao').val() ? $('#metricas-aquisicao').val() : '',
            aquisicao_visao_consolidada: JSON.stringify({
              pedidos_pagos: {
                quantidade: $('#pedidos_pagos').val() ? parseInt($('#pedidos_pagos').val().replace(".", "")) : '',
                faturamento: $('#faturamento_pago').val() ? parseFloat($('#faturamento_pago').val().replace(".", "").replace(",", ".")) : ''
              },
              pedidos_realizados: {
                quantidade: $('#pedidos_realizados').val() ? parseInt($('#pedidos_realizados').val().replace(".", "")) : '',
                faturamento: $('#faturamento_realizado').val() ? parseFloat($('#faturamento_realizado').val().replace(".", "").replace(",", ".")) : ''
              }
            }),
            aquisicao_trafego_pago: trafego_table.getData() ? JSON.stringify(trafego_table.getData().map((item) => {
              return {
                tipo: item[0],
                pedidos: item[1],
                faturamento: item[2],
                investimento: item[3]
              }
            })) : [],

            /*
             * TAB 2
             */

            midia_paga: JSON.stringify([
              midia_paga_facebook_ads.tipo ? {
                tipo: midia_paga_facebook_ads.tipo,
                campanhas: midia_paga_facebook_ads.table ? midia_paga_facebook_ads.table.getData().map((item) => {
                  return {
                    tipo: item[0],
                    alcance: item[1],
                    impressoes: item[2],
                    resultado: item[3],
                    investimento: item[4],
                    receita: item[6],
                    resultado_indicador: item[8]
                  }
                }) : [],
                ads_library: $('#midia_paga_facebook_ads_anuncio').val() ? $('#midia_paga_facebook_ads_anuncio').val() : '',
                comentarios: $('#midia-paga-facebook-ads').val() ? $('#midia-paga-facebook-ads').val() : ''
              } : null,
              midia_paga_google_ads.tipo ? {
                tipo: midia_paga_google_ads.tipo,
                campanhas: midia_paga_google_ads.table ? midia_paga_google_ads.table.getData().map((item) => {
                  return {
                    tipo: item[0],
                    alcance: item[1],
                    impressoes: item[2],
                    resultado: item[3],
                    investimento: item[4],
                    receita: item[6],
                    resultado_indicador: item[8]
                  }
                }) : [],
                ads_library: $('#midia_paga_google_ads_anuncio').val() ? $('#midia_paga_google_ads_anuncio').val() : '',
                comentarios: $('#midia-paga-google-ads').val() ? $('#midia-paga-google-ads').val() : ''
              } : null,
              midia_paga_pinterest_ads.tipo ? {
                tipo: midia_paga_pinterest_ads.tipo,
                campanhas: midia_paga_pinterest_ads.table ? midia_paga_pinterest_ads.table.getData().map((item) => {
                  return {
                    tipo: item[0],
                    alcance: item[1],
                    impressoes: item[2],
                    resultado: item[3],
                    investimento: item[4],
                    receita: item[6],
                    resultado_indicador: item[8]
                  }
                }) : [],
                ads_library: $('#midia_paga_pinterest_ads_anuncio').val() ? $('#midia_paga_pinterest_ads_anuncio').val() : '',
                comentarios: $('#midia-paga-pinterest-ads').val() ? $('#midia-paga-pinterest-ads').val() : ''
              } : null,
              midia_paga_linkedin_ads.tipo ? {
                tipo: midia_paga_linkedin_ads.tipo,
                campanhas: midia_paga_linkedin_ads.table ? midia_paga_linkedin_ads.table.getData().map((item) => {
                  return {
                    tipo: item[0],
                    alcance: item[1],
                    impressoes: item[2],
                    resultado: item[3],
                    investimento: item[4],
                    receita: item[6],
                    resultado_indicador: item[8]
                  }
                }) : [],
                ads_library: $('#midia_paga_linkedin_ads_anuncio').val() ? $('#midia_paga_linkedin_ads_anuncio').val() : '',
                comentarios: $('#midia-paga-linkedin-ads').val() ? $('#midia-paga-linkedin-ads').val() : ''
              } : null,
            ].filter(function(item) {
              return item != null
            })),

            /*
             * TAB 3
             */

            landing_pages: JSON.stringify({
              colecoes: landing_pages_table.getData().map((item) => {
                return {
                  nome: item[0],
                  visitantes: item[1],
                  leads: item[2]
                }
              }),
              destaques: landing_pages_destaques_table.getData().map((item) => {
                return item[0]
              }),
            }),
            landing_pages_comentarios: $('#landing-pages').val() ? $('#landing-pages').val() : '',

            /*
             * TAB 4
             */

            email_marketing: JSON.stringify({
              campanhas: email_marketing_table.getData().map((item) => {
                return {
                  nome: item[0],
                  emails_entregues: item[1],
                  taxa_de_entrega: item[2] / 100,
                  taxa_de_abertura: item[3] / 100,
                  taxa_de_cliques: item[4] / 100
                }
              }),
              destaques: email_mkt_destaques_urls ? email_mkt_destaques_urls : []
            }),
            email_marketing_comentarios: $('#email-mkt').val() ? $('#email-mkt').val() : '',

            /*
             * TAB 5
             */

            redes_sociais: JSON.stringify({
              campanhas: $('#campanhas').val() ? parseInt($('#campanhas').val().replace(".", "")) : '',
              videos_gifs: $('#videos_gifs').val() ? parseInt($('#videos_gifs').val().replace(".", "")) : '',
              pecas_de_feed: $('#pecas_de_feed').val() ? parseInt($('#pecas_de_feed').val().replace(".", "")) : '',
              pecas_de_stories: $('#pecas_de_stories').val() ? parseInt($('#pecas_de_stories').val().replace(".", "")) : '',
              destaques: redes_sociais_destaques ? redes_sociais_destaques : []
            }),

            /*
             * TAB 6
             */

            proximas_acoes: $('#planejamento').val() ? $('#planejamento').val() : '',
          }

          // CHAMADA SALVAR OBJETO FINAL
          await axios.patch(API_URL + '/relatorios/' + id, relatorio_objeto);

          // DEU CERTO
          $('#modal').modal('hide');
          swal.close();
          swal.fire({
            icon: 'success',
            title: 'Excelente!',
            text: 'Relatório foi salvo com sucesso.',
            showCancelButton: true,
            confirmButtonText: 'Visualizar relatório',
            cancelButtonText: 'Fechar',
            reverseButtons: true,
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          }).then(function(e) {
            if (e.value) {
              window.open('/relatorios/' + id + '/visualizar/');
            }
          });
        }
        //
        catch (e) {
          console.log(e);
          swal.close();
          swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Algo errado não está certo.',
            footer: 'Confira o erro no log do navegador.'
          });
        }
      }
    });
  }
  // ERRO AO CARREGAR O RELATÓRIO
  catch (e) {
    console.log(e)
    swal.close();
    swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Algo errado não está certo.',
      footer: 'Confira o erro no log do navegador.'
    });
  }
});