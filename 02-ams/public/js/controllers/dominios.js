let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/dominios',
            map: (raw) => {
              raw.forEach(item => {
                item.validade = moment(item.validade).format('DD/MM/YYYY HH:mm:ss');
                item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
        field: 'cliente_nome',
        title: 'Cliente',
        autoHide: false,
        width: 230,
        template: (row) => {
          return row.cliente_nome
        }
      }, {
        field: 'projeto_nome',
        title: 'Projeto',
        autoHide: false,
        width: 120,
        template: (row) => {
          return row.projeto_nome
        }
      }, {
        field: 'dominio',
        title: 'Domínio',
        autoHide: false,
        width: 120,
        template: (row) => {
          return row.dominio
        }
      }, {
        field: 'validade',
        title: 'Validade',
        autoHide: false,
        width: 150,
        template: (row) => {
          return row.validade
        }
      }],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
    $('#kt_datatable').KTDatatable().sort('id', 'DESC');
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
});