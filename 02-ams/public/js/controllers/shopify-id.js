let datatable = {
  init: () => {
    // RENDERIZAR A NOVA TABELA
    $('#kt_datatable_log').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/shopify/logs/' + id,
            map: (raw) => {
              raw.forEach(item => {
                item.created_at = moment(item.created_at).subtract(3, 'hours').format('DD/MM/YYYY HH:mm:ss');
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query_log'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
        field: 'id',
        title: '#',
        width: 50
      }, {
        field: 'descricao',
        title: 'Descrição',
        width: 600,
        template: (row) => {
          console.log(row)
          if (row.user_display_name) return row.descricao + '<span class="label ml-3" data-toggle="tooltip" data-html="true" title="' + row.user_display_name + '<br>' + row.user_email + '"><i class="icon-sm la la-cog"></i></span>';
          else if (row.rdstation_event_type == 'ORDER_PLACED') return row.descricao + '<span class="label label-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.shopify_event + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-shopping-cart text-white"></i></span>';
          else if (row.rdstation_event_type == 'SALE') return row.descricao + '<span class="label label-success ml-3" data-toggle="tooltip" data-html="true" title="' + row.shopify_event + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-dollar text-white"></i></span>';
          else if (row.rdstation_lead_id) return row.descricao + '<span class="label label-light-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.shopify_event + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-user"></i></span>';
          else return row.descricao;
        }
      }, {
        field: 'origem',
        title: 'Origem',
        width: 75
      }, {
        field: 'tipo',
        title: 'Tipo',
        textAlign: 'center',
        width: 75,
        template: (row) => {
          return '<span class="label font-weight-bold label-lg label-light-' + row.tipo + ' label-inline w-100 text-uppercase">' + row.tipo + '</span>';
        }
      }, {
        field: 'created_at',
        title: 'Data',
        width: 200
      }],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
    // EXIBIR TOOLTIPS
    $('#kt_datatable_log').KTDatatable().on('datatable-on-layout-updated', function() {
      $('[data-toggle="tooltip"]').tooltip();
    });
  },
  reload: () => {
    $('#kt_datatable_log').KTDatatable().reload();
  }
}

let gui = {
  init: async () => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR INFORMAÇÕES DO PROJETO
      let projeto_id = id;
      let projeto = (await axios.get(API_URL + '/projetos/' + projeto_id)).data;
      // ISOLAR EM UM OBJETO OS DADOS DA INTEGRAÇÃO
      let shopify = JSON.parse(projeto.integracoes).shopify;
      // AVALIAR WEBHOOKS DISPONÍVEIS E EXISTENTES
      let options = ['customers_create', 'checkouts_create', 'checkouts_update', 'checkouts_delete', 'orders_create', 'orders_updated', 'orders_paid', 'orders_partially_fulfilled', 'orders_fulfilled', 'orders_cancelled'];
      if (shopify.webhooks) {
        options.forEach(webhook => {
          if (shopify.webhooks[webhook]) $('#shopify-' + webhook.replace(/_/g, '-')).prop('checked', true);
          else $('#shopify-' + webhook.replace(/_/g, '-')).prop('checked', false);
        });
      } else options.forEach(webhook => $('#shopify-' + webhook.replace(/_/g, '-')).prop('checked', false));
      // GERENCIAR ATUALIZAÇÃO DO REGISTRO
      $('.btn-save').on('click', async function() {
        let confirmacao = await swal.fire({
          icon: 'warning',
          title: 'Atenção!',
          text: 'Deseja mesmo atualizar esta integração?',
          showCancelButton: true,
          reverseButtons: true,
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Continuar',
          customClass: {
            confirmButton: 'btn btn-primary mt-0',
            cancelButton: 'btn btn-light mt-0'
          }
        });
        if (confirmacao.value) {
          try {
            // EXIBIR TELA DE CARREGAMENTO
            swal.fire({
              title: 'Aguarde...',
              text: 'Processando a solicitação.',
              onOpen: () => {
                swal.showLoading();
              },
              allowOutsideClick: false,
              allowEscapeKey: false
            });
            // ENVIAR REQUISIÇÃO PARA O BACK-END
            let webhooks = {};
            options.forEach(webhook => {
              webhooks[webhook] = $('#shopify-' + webhook.replace(/_/g, '-')).prop('checked');
            });
            await axios.put(API_URL + '/shopify/webhooks/' + id, webhooks);
            // DEU CERTO
            swal.close();
            await swal.fire({
              icon: 'success',
              title: 'Excelente!',
              text: 'A integração foi atualizada com sucesso.'
            });
            document.location.reload(true);
          }
          //
          catch (e) {
            console.log(e);
            swal.close();
            swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Algo errado não está certo.',
              footer: 'Confira o erro no log do navegador.'
            });
          }
        }
      });
      // CONTROLAR ABAS
      $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        if ($(this).attr('href') == '#kt_tab_pane_2') setTimeout(function() {
          datatable.reload();
        }, 500);
      });
      // LIBERAR PÁGINA
      swal.close();
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      await swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
      document.location.reload(true);
    }
  },
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
  // SALVAR ALTERAÇÕES
  gui.init();
});