jQuery(document).ready(async () => {
  let formulario = (await axios.get(API_URL + '/formularios/' + id)).data;
  // PARTE 1
  console.log(formulario)
  $('#nome').val(formulario.nome);
  $('#descricao').val(formulario.descricao);
  $('#status').prop('checked', formulario.status ? true : false);
  $('#privado').prop('checked', formulario.privado ? true : false);
  $('#status_email').prop('checked', formulario.status_email ? true : false);
  // PARTE 2
  let schema = JSON.parse(formulario.conteudo);
  Formio.builder(document.getElementById('formio'), schema).then(function(form) {
    // MONITORAR MUDANÇAS
    form.on('change', () => schema = form.schema);
  });
  // PARTE 3
  $('#email').val(formulario.email);
  tinymce.init({
    selector: '#email',
    language: 'pt_BR',
    menubar: false,
    toolbar: ['undo redo | cut copy paste | fontsizeselect | bold italic | bullist numlist | alignleft aligncenter alignright | outdent indent | preview code'],
    statusbar: false,
    plugins : 'lists preview code'
  });
  // CONFIGURAR AÇÕES - PRÉ-VISUALIZAR
  $(document).on('click', '.btn-preview', function() {
    Formio.createForm(document.getElementById('formio-preview'), schema, {
      language: 'pt',
      i18n: {
        'pt': {
          error: 'Corrija os seguintes erros antes de enviar.',
          required: '{{field}} * Obrigatório.',
          complete: 'Submissão Completa'
        }
      }
    }).then(function(form) {
      $('#modal').modal('show');
    });
  });
  // CONFIGURAR AÇÕES - LIMPAR MODAL AO FECHAR
  $('#modal').on('hidden.bs.modal', function() {
    $('#formio-preview').html('');
  });
  // CONFIGURAR AÇÕES - ATUALIZAR REGISTRO
  $(document).on('click', '.btn-update', async function() {
    if ($('#form').valid()) {
      let confirmacao = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo atualizar este formulário?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      if (confirmacao.value) {
        try {
          // EXIBIR TELA DE CARREGAMENTO
          swal.fire({
            title: 'Aguarde...',
            text: 'Processando a solicitação.',
            onOpen: () => {
              swal.showLoading();
            },
            allowOutsideClick: false,
            allowEscapeKey: false
          });
          // ENVIAR REQUISIÇÃO PARA O BACK-END
          let formulario = {
            nome: $('#nome').val(),
            descricao: $('#descricao').val(),
            conteudo: JSON.stringify(schema),
            status: $('#status').prop('checked'),
            privado: $('#privado').prop('checked'),
            status_email: $('#status_email').prop('checked'),
          }
          if (tinymce.get('email').getContent()) formulario.email = tinymce.get('email').getContent();
          await axios.patch(API_URL + '/formularios/' + id, formulario);
          swal.close();
          swal.fire({
            icon: 'success',
            title: 'Excelente!',
            text: 'O formulário foi atualizado com sucesso.',
          });
        }
        //
        catch (e) {
          console.log(e);
          swal.close();
          swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Algo errado não está certo.',
            footer: 'Confira o erro no log do navegador.'
          });
        }
      }
    }
  });
});