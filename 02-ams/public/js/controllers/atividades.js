let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/atividades',
            map: (raw) => {
              raw.forEach(item => {
                item.leader = JSON.parse(item.leader);
                item.squad = JSON.parse(item.squad) || [];
                //
                let datetime = item.data;
                //
                item.data = moment(datetime).format('DD/MM/YYYY');
                item.hora = moment(datetime).format('HH:mm:ss');
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: false,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
        field: 'id',
        title: 'Atividade',
        autoHide: false,
        template: (row) => {
          let template = '<small>' + row.id + '</small> - ' + row.nome;
          if (row.descricao) template += '<br><small>' + row.descricao + '</small>';
          template += '<br><span class="label label-sm label-light-success label-inline mr-2">' + row.leader.value + '</span>'
          if (row.squad.length > 0) template += row.squad.map(tag => '<span class="label label-sm label-light-info label-inline mr-2">' + tag.value + '</span>').join('');
          template += '<br><small class="d-inline-text d-sm-none">' + row.data + ' ' + row.hora + '</small>';
          return template;
        }
      }, {
        field: 'projeto_nome',
        title: 'Projeto',
        width: 180,
        responsive: {
          hidden: 'xs',
          visible: 'sm'
        }
      }, {
        field: 'data',
        title: 'Data',
        width: 80,
        template: (row) => {
          return row.data + '<br><small>' + row.hora + '</small>';
        },
        responsive: {
          hidden: 'xs',
          visible: 'sm'
        }
      }],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
    $('#kt_datatable').KTDatatable().sort('data', 'DESC');
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let tagify;
let tagify_input = document.getElementById('squad');

let gui = {
  init: async () => {
    // CARREGAR DADOS DE CLIENTES E PROJETOS
    let clientes = (await axios.get(API_URL + '/clientes')).data;
    // MONTAR LISTA DE PROJETOS PARA SELEÇÃO
    let projetos = {};
    clientes.filter(cliente => cliente.status).forEach(cliente => {
      projetos[cliente.nome] = {};
      cliente.projetos.filter(cliente => cliente.status).forEach(projeto => {
        projetos[cliente.nome][projeto.id] = projeto.nome;
      });
    });
    // INICIALIZAR SELELCT2 DOS PROJETOS
    Object.keys(projetos).forEach((cliente) => $('#projeto').append('<optgroup label="' + cliente + '">' + Object.entries(projetos[cliente]).map((projeto) => '<option value="' + projeto.shift() + '">' + projeto.pop() + '</option>') + '</optgroup>'));
    $('#projeto').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    // INICIALIZAR DATE TIME PICKER
    $('#data').datetimepicker({
      locale: 'pt-br',
      maxDate: moment()
    });
    // INICIALIZAR SQUAD
    let squad = (await axios.get(API_URL + '/back-office')).data;
    tagify = new Tagify(tagify_input, {
      enforceWhitelist: true,
      whitelist: squad.filter(user => user.user_id != auth0.user_id).map(user => {
        return {
          value: user.name,
          user_id: user.user_id
        }
      })
    });
  }
}

let api = {
  create: () => {
    try {
      tagify.removeAllTags();
      // REDEFINIR FORMULÁRIO
      $('#form').trigger('reset');
      $('#projeto').trigger('change');
      $('#form').validate();
      $('#data').find('input').val(moment().format('DD/MM/YYYY HH:mm'));
      // GERENCIAR CRIAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        // VALIDAR FORMULÁRIO
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo adicionar esta atividade?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let data = $('#data').find('input').val();
              //
              let dia = data.substring(0, 2);
              let mes = data.substring(3, 5);
              let ano = data.substring(6, 10);
              let hora = data.substring(11, 16);
              //
              let atividade = {
                projeto_id: $('#projeto').val(),
                nome: $('#nome').val(),
                descricao: $('#descricao').val() || null,
                data: ano + '-' + mes + '-' + dia + ' ' + hora,
                leader: JSON.stringify({
                  user_id: auth0.user_id,
                  value: auth0.name
                }),
                squad: $('#squad').val() ? $('#squad').val() : '[]',
              }
              await axios.post(API_URL + '/atividades/', atividade);
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'A atividade foi adicionada com sucesso.',
              });
            }
            //
            catch (e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      $('#modal-title').html('Adicionar Atividade');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  update: async (id) => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR DADOS DO CAMPANHA
      let pesquisa = (await axios.get(API_URL + '/pesquisas/' + id)).data;
      // REDEFINIR FORMULÁRIO
      $('#form').trigger('reset');
      $('#form').validate();
      // INSERIR DADOS DO CAMPANHA NO FORMULÁRIO
      $('#mes').val(pesquisa.mes).trigger('change');
      $('#ano').val(pesquisa.ano).trigger('change');
      $('#projeto').val(pesquisa.projeto_id).trigger('change');
      $('#formulario').val(pesquisa.formulario_id).trigger('change');
      // GERENCIAR ATUALIZAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo atualizar esta pesquisa?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let pesquisa = {
                mes: $('#mes').val(),
                ano: $('#ano').val(),
                projeto_id: $('#projeto').val(),
                formulario_id: $('#formulario').val(),
                status: 0
              }
              // CHAMADA
              await axios.patch(API_URL + '/pesquisas/' + id, pesquisa);
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'A pesquisa foi atualizada com sucesso.',
              });
            }
            //
            catch(e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      swal.close();
      $('#modal-title').html('Editar Pesquisa');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let pesquisa = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir esta pesquisa?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (pesquisa.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let pesquisa_id = id;
        await axios.delete(API_URL + '/pesquisas/' + pesquisa_id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A pesquisa foi excluída com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
  // GUI
  gui.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create', function() {
    api.create();
  });
  /*
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete($(this).attr('data-id'));
  });
  */
  setTimeout(function() {
    $('#kt_datatable').KTDatatable().reload();
  }, 3000);
});