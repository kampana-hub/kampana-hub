let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/credenciais',
            map: (raw) => {
              raw.forEach(item => {
                item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
      columns: [{
        field: 'projeto_nome',
        title: 'Projeto',
        autoHide: false,
        width: 120,
        template: (row) => {
          return row.projeto_nome
        }
      }, {
        field: 'nome',
        title: 'Nome',
        autoHide: false,
        width: 150,
        template: (row) => {
          return row.nome
        }
      }, {
        field: 'url',
        title: 'URL',
        autoHide: false,
        width: 350,
        template: (row) => '<a class="ellipsis" href="' + row.url + '" target="_blank">' + row.url + '</a><br><small>' + row.usuario + '</small>'
      }/*, {
        field: 'usuario',
        title: 'Usuário',
        autoHide: false,
        width: 150,
        template: (row) => {
          return row.usuario
        }
      }, {
        field: 'senha',
        title: 'Senha',
        autoHide: false,
        width: 150,
        template: (row) => {
          return row.senha
        }
      }, {
        field: 'data',
        title: 'Data',
        autoHide: false,
        width: 80,
        template: (row) => {
          return row.created_at
        }
      }*/, {
        field: 'Actions',
        title: '',
        sortable: false,
        width: 50,
        autoHide: false,
        overflow: 'visible',
        template: (row) => {
          return '\
            <div class="dropdown dropdown-inline">\
                <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                    <span class="svg-icon svg-icon-md">\
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <rect x="0" y="0" width="24" height="24"/>\
                                <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                            </g>\
                        </svg>\
                    </span>\
                </a>\
                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">\
                    <ul class="navi flex-column navi-hover ">\
                        <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">\
                            CONFIGURAÇÃO\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-update" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-pencil"></i></span>\
                                <span class="navi-text">Gerenciar</span>\
                            </span>\
                        </li>\
                        <li class="navi-item">\
                            <span class="navi-link btn-delete" data-id="' + row.id + '">\
                                <span class="navi-icon"><i class="la la-trash"></i></span>\
                                <span class="navi-text">Excluir</span>\
                            </span>\
                        </li>\
                    </ul>\
                </div>\
            </div>';
        }
      }],
      // TRADUÇÃO
      translate: kt_datatable_pt_br
    });
    $('#kt_datatable').KTDatatable().sort('id', 'DESC');
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let gui = {
  init: async () => {
    // CARREGAR DADOS DE CLIENTES E PROJETOS
    let clientes = (await axios.get(API_URL + '/clientes')).data;
    // MONTAR LISTA DE PROJETOS PARA SELEÇÃO
    let projetos = {};
    clientes.filter(cliente => cliente.status).forEach(cliente => {
      projetos[cliente.nome] = {};
      cliente.projetos.filter(cliente => cliente.status).forEach(projeto => {
        projetos[cliente.nome][projeto.id] = projeto.nome;
      });
    });
    // INICIALIZAR SELELCT2 DOS PROJETOS
    Object.keys(projetos).forEach((cliente) => $('#projeto').append('<optgroup label="' + cliente + '">' + Object.entries(projetos[cliente]).map((projeto) => '<option value="' + projeto.shift() + '">' + projeto.pop() + '</option>') + '</optgroup>'));
    $('#projeto').select2({
      placeholder: {
        id: '',
        text: 'Selecionar'
      },
      allowClear: true
    });
    // VISUALAR SENHAR
    $('#btnViewPass').on('click', function() {
      let senha = document.querySelector('#credencial-senha');
      let tipo_senha = senha.getAttribute('type') === 'password' ? 'text' : 'password';
      senha.setAttribute('type', tipo_senha);
      $('#btnViewPass').addClass('bi-eye-slash');
      this.classList.toggle('bi-eye');
    });
    // COPIAR SENHAR
    $('#btnCopyPass').on('click', function() {
      let senha = document.getElementById("credencial-senha");
      senha.select();
      senha.setSelectionRange(0, 99999);
      navigator.clipboard.writeText(senha.value);
      swal.fire({
        icon: 'success',
        text: JSON.stringify($('#credencial-senha').val()) + " copiado",
      });
    });
    // COPIAR USUÁRIO
    $('#btnCopyUser').on('click', function() {
      let usuario = document.getElementById("credencial-usuario");
      usuario.select();
      usuario.setSelectionRange(0, 99999);
      navigator.clipboard.writeText(usuario.value);
      swal.fire({
        icon: 'success',
        text: JSON.stringify($('#credencial-usuario').val()) + " copiado",
      });
    });
    // REDIRECIONAR URL
    $('#btnRdrtUrl').on('click', function() {
      window.open($('#credencial-url').val());
    });
  }
}

let api = {
  create: () => {
    try {
      // REDEFINIR FORMULÁRIO
      $('#form').trigger('reset');
      $('#form').validate();
      // GERENCIAR CRIAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        // VALIDAR FORMULÁRIO
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo adicionar esta Credencial?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let credencial = {
                projeto_id: $('#projeto').val(),
                nome: $('#credencial-nome').val(),
                usuario: $('#credencial-usuario').val(),
                url: $('#credencial-url').val(),
                senha: $('#credencial-senha').val(),
              }
              await axios.post(API_URL + '/credenciais/', credencial);
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'A credencial foi adicionada com sucesso.',
              });
            }
            //
            catch (e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      $('#modal-title').html('Adicionar Credencial');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  update: async (id) => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR DADOS DA CREDENCIAL
      let credencial = (await axios.get(API_URL + '/credenciais/' + id)).data;
      // REDEFINIR FORMULÁRIO
      $('#form').trigger('reset');
      $('#form').validate();
      // INSERIR DADOS DA CREDENCIAL NO FORMULÁRIO
      $('#projeto').val(credencial.projeto_id).trigger('change');
      $('#credencial-nome').val(credencial.nome);
      $('#credencial-url').val(credencial.url);
      $('#credencial-usuario').val(credencial.usuario);
      $('#credencial-senha').val(credencial.senha);
      // GERENCIAR ATUALIZAÇÃO DO REGISTRO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
        if ($('#form').valid()) {
          let confirmacao = await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'Deseja mesmo atualizar esta credencial?',
            showCancelButton: true,
            reverseButtons: true,
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Continuar',
            customClass: {
              confirmButton: 'btn btn-primary mt-0',
              cancelButton: 'btn btn-light mt-0'
            }
          });
          if (confirmacao.value) {
            try {
              // EXIBIR TELA DE CARREGAMENTO
              swal.fire({
                title: 'Aguarde...',
                text: 'Processando a solicitação.',
                onOpen: () => {
                  swal.showLoading();
                },
                allowOutsideClick: false,
                allowEscapeKey: false
              });
              // ENVIAR REQUISIÇÃO PARA O BACK-END
              let credencial = {
                projeto_id: $('#projeto').val(),
                nome: $('#credencial-nome').val(),
                usuario: $('#credencial-usuario').val(),
                url: $('#credencial-url').val(),
                senha: $('#credencial-senha').val(),
              }
              await axios.patch(API_URL + '/credenciais/' + id, credencial);
              datatable.reload();
              // DEU CERTO
              $('#modal').modal('hide');
              swal.close();
              swal.fire({
                icon: 'success',
                title: 'Excelente!',
                text: 'A credencial foi atualizado com sucesso.',
              });
            }
            //
            catch(e) {
              console.log(e);
              swal.close();
              swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo errado não está certo.',
                footer: 'Confira o erro no log do navegador.'
              });
            }
          }
        }
      });
      // EXIBIR MODAL
      swal.close();
      $('#modal-title').html('Editar Credencial');
      $('#modal').modal('show');
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async (id) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let cliente = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir esta credencial?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (cliente.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        await axios.delete(API_URL + '/credenciais/' + id);
        datatable.reload();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A credencial foi excluído com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR A TABELA
  datatable.init();
  // GUI
  gui.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-create', function() {
    $('#credencial-senha').attr('type', 'password');
    $('#btnCopyUser').addClass('d-none');
    $('#btnCopyPass').addClass('d-none');
    $('#btnViewPass').addClass('d-none');
    $('#btnRdrtUrl').addClass('d-none');
    api.create();
  });
  $(document).on('click', '.btn-update', function() {
    $('#btnCopyUser').removeClass('d-none');
    $('#btnCopyPass').removeClass('d-none');
    $('#btnViewPass').removeClass('d-none');
    $('#btnRdrtUrl').removeClass('d-none');
    api.update($(this).attr('data-id'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete($(this).attr('data-id'));
  });
});