let datatable;
let initKtDatatable = function(url) {
  datatable = $('#kt_datatable').KTDatatable({
    data: {
      type: 'remote',
      source: {
        read: {
          url: url,
          method: 'GET',
        },
      },
      pageSize: 25,
      serverPaging: false,
      serverFiltering: false,
      serverSorting: false,
    },
    layout: {
      scroll: false,
      footer: false
    },
    sortable: true,
    pagination: false,
    columns: [{
      field: 'id',
      title: '#',
      sortable: false,
      autoHide: false,
      width: 20,
      selector: {
        class: ''
      },
      textAlign: 'center',
    }, {
      field: 'name',
      title: 'Interesse',
      textAlign: 'left',
      autoHide: false,
      width: 230,
    }, {
      field: 'audience_size_upper_bound',
      title: 'Tamanho da Audiência',
      width: 200,
      template: function(row) {
        return '\
          <div class="skills" style="width: ' + row.audience_size_upper_bound / 3000000 + '%"></div>\
          <small style="font-weight: 900">' + (row.audience_size_upper_bound).toLocaleString('pt-BR') + '</small>\
        ';
      }
    }, {
      field: 'topic',
      title: 'Tópico',
      width: 150,
      textAlign: 'left',
    }, {
      field: 'Actions',
      title: '',
      sortable: false,
      width: 20,
      overflow: 'visible',
      textAlign: 'center',
      template: function(row) {
        return '\
          <div class="dropdown dropdown-inline">\
            <span><i id="copy_button" value ="' + row.name + '"class="fa fa-copy" style="cursor: pointer; color: rgb(140, 141, 141);"></i></span>\
          </div>\
        ';
      },
    }],
    translate: ""
  });
  $('#kt_datatable').KTDatatable().sort('audience_size_upper_bound', 'DESC');
  datatable.on(
    'datatable-on-check datatable-on-uncheck',
    function(e) {
      var checkedNodes = datatable.rows('.datatable-row-active').nodes();
      var count = checkedNodes.length;
      $('#kt_datatable_selected_records').html(count);
      if (count > 0) {
        $('#kt_datatable_group_action_form').collapse('show');
      } else {
        $('#kt_datatable_group_action_form').collapse('hide');
      }
    });
  //
  $(document).on('click', '#copy_button', function() {
    let name = $(this).attr('value');
    let el = document.createElement('textarea');
    el.value = name;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    swal.fire({
      icon: 'success',
      text: JSON.stringify(name) + " copiado",
    });
  });
  $('#kt_datatable_fetch_modal').on('show.bs.modal', function(e) {
    var selected_name = datatable.rows('.datatable-row-active').nodes();
    var ids = datatable.rows('.datatable-row-active').
    nodes().
    find('.checkbox > [type="checkbox"]').
    map(function(i, chk) {
      return $(chk).val();
    });
    var c = document.createDocumentFragment();
    for (var i = 0; i < ids.length; i++) {
      var width = $(window).width()
      var li = document.createElement('li');
      if (width > 800) {
        li.innerHTML = "<div style='margin-bottom: 10px'>" + selected_name[i].childNodes[1].innerText + "</div>";
      } else if (width <= 800) {
        li.innerHTML = "<div style='margin-bottom: 10px'>" + selected_name[i].childNodes[2].innerText + "</div>";
      }
      c.appendChild(li);
    }
    $('#kt_datatable_fetch_display').append(c);
  }).on('hide.bs.modal', function(e) {
    $('#kt_datatable_fetch_display').empty();
  });
};
//
$(document).ready(function() {
  $('#kt_datatable_search').collapse('show');
  $('#search_button').on('click', async function() {
    swal.fire({
      title: 'Aguarde',
      text: 'Carregando...',
      onOpen: function() {
        swal.showLoading();
      }
    });
    $('#kt_datatable_search').collapse('hide');
    try {
      $('#kt_datatable_search_query').prop('disabled', true);
      let search_input = $('#kt_datatable_search_query').val()
      let authorization = (await axios.get("https://graph.facebook.com/oauth/access_token?client_id=1785979331549869&client_secret=7d7b46a060e5679edc109d48ef992813&grant_type=client_credentials")).data;
      let interesses_url = "https://graph.facebook.com/search?type=adinterest&q=[" + search_input + "]&limit=10000&locale=pt_BR&access_token=" + authorization.access_token + "";
      let count = (await axios.get(interesses_url)).data;
      if (count.data) {
        $('#kt_datatable_search_reflash').collapse('show');
        $('#search_result').html(count.data.length + " Interesses Encontrados!");
        if (count.data.length > 0) {
          $('#kt_datatable_selected_results').collapse('show');
        } else {
          $('#kt_datatable_selected_results').collapse('hide');
        }
        initKtDatatable(interesses_url);
        swal.close();
        $('#reflash_button').on('click', function() {
          window.location.reload();
        });
      }
    } catch (e) {
      console.log(e);
    }
  });
});