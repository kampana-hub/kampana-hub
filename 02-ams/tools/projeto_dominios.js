const axios = require('axios');
const dotenv = require('dotenv');

dotenv.config({path:__dirname+'/./../../.env'});

async function main() {
	try {
		// 1 - LISTAR OS DOMÍNIOS DO PROJETO
		let dominios = (await axios.get(process.env.API_URL + '/projeto/12')).data.shift().dominios;
		//
		let objeto = [];
		dominios = JSON.parse(dominios);
		dominios.forEach(async (item) => {
			let rdap = (await axios.get('https://rdap.registro.br/domain/' + item)).data;
			objeto.push({
				nome: rdap.ldhName,
				responsavel: rdap.entities.shift().legalRepresentative,
				data_validade: (rdap.events.filter(item => { return item.eventAction == 'expiration' })).shift().eventDate
			});
			console.log(objeto);
		});
	} catch(e) {
		console.log(e)
	}
}

main();