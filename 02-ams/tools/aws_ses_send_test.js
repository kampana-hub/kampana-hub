const AWS = require('aws-sdk');
const axios = require('axios');
const dotenv = require('dotenv');

dotenv.config({path: __dirname + '/../.env'});

let calendario = {
  1:  'Janeiro',
  2:  'Fevereiro',
  3:  'Março',
  4:  'Abril',
  5:  'Maio',
  6:  'Junho',
  7:  'Julho',
  8:  'Agosto',
  9:  'Setembro',
  10: 'Outubro',
  11: 'Novembro',
  12: 'Dezembro'
}

async function main() {
  // ENTIDADE PRINCIPAL
  let pesquisa = (await axios.get(process.env.API_URL + '/pesquisa/29')).data.shift();
  // ENTIDADES DE APOIO
  let formulario = (await axios.get(process.env.API_URL + '/formulario/' + pesquisa.formulario_id)).data.shift();
  let projeto    = (await axios.get(process.env.API_URL + '/projeto/'    + pesquisa.projeto_id)).data.shift();
  let cliente    = (await axios.get(process.env.API_URL + '/cliente/'    + projeto.cliente_id)).data.shift();
  // MANIPULAÇÃO DE DADOS
  formulario.conteudo = JSON.parse(formulario.conteudo);
  pesquisa.respostas  = JSON.parse(pesquisa.respostas);
  // EXPRESSÃO REGULAR PARA ENCONTRAR VARIÁVEIS (https://www.regextester.com/94730)
  let regexp1 = RegExp('\{\{.*?\}\}', 'g');
  let regexp2 = RegExp('\_\_.*?\_\_', 'g');
  // DETERMINAR QUAIS VARIÁVEIS ESTÃO SENDO UTILIZADAS - EXCLUIR AS INTRÍNSECAS DA ENTIDADE FORMULÁRIO
  let matches = [];
  // PERGUNTA E RESPOSTA
  let temp1 = Array.from(formulario.email.matchAll(regexp1), m => m[0]);
  temp1 = temp1.toString().split('{{').filter(Boolean);
  temp1 = temp1.toString().split('}}');
  temp1 = temp1.toString().split(',').filter(Boolean);
  // APENAS RESPOSTA
  let temp2 = Array.from(formulario.email.matchAll(regexp2), m => m[0]).filter(match => match != '__mes__' && match != '__ano__');
  temp2 = temp2.toString().split('__').filter(Boolean);
  temp2 = temp2.toString().split(',').filter(Boolean);
  // AGRUPAR EM APENAS UM ARRAY
  matches = matches.concat(temp1, temp2);
  // INICIALIZAR OBJETO COM O CONTEÚDO QUE SERÁ UTILIZADO NO E-MAIL
  let conteudo = {};
  // ENCONTRAR AS INFORMAÇÕES DE CADA IUMA DAS VARIÁVEIS  
  matches.forEach(match => {
    // IDENTIFICAR O COMPONENTE
    let component = findNestedObj(formulario.conteudo.components, 'key', match);
    // ATRIBUIR A PERGUNTA
    conteudo[match] = {
      pergunta: component.label
    }
    // ATRIBUIR A RESPOSTA COM BASE NO TIPO DO COMPONENTE
    if (component.type == 'radio') {
      conteudo[match].resposta = component.values.find(item => item.value = pesquisa.respostas[match]).label;
    } else if (component.type == 'selectboxes') {
      let temp1 = [];
      Object.keys(pesquisa.respostas[match]).forEach(item => {
        if (pesquisa.respostas[match][item]) temp1.push(item);
      });
      //
      let temp2 = [];
      temp1.forEach(item => {
        temp2.push(component.values.find(component => component.value == item).label);
      });
      //
      conteudo[match].resposta = temp2.join(' / ')
    } else {
      conteudo[match].resposta = pesquisa.respostas[match] || 'Não informado';
    }
  });
  let email = formulario.email;
  // PARTE 1
  email = email.replaceAll('__mes__', calendario[pesquisa.mes]);
  email = email.replaceAll('__ano__', pesquisa.ano);
  // PARTE 2
  matches.forEach(match => {
    email = email.replaceAll('{{' + match + '}}', '<b>' + conteudo[match].pergunta + '</b><br><br>' + conteudo[match].resposta);
    email = email.replaceAll('__' + match + '__', conteudo[match].resposta);
  });
  /*
   * TESTE DE ENVIO
   */
  let params = {
    Destinations: [{
      Destination: {
        ToAddresses: [
          'jose.vieira@kampana.digital',
          'victor.marques@kampana.digital'
        ]
      },
      ReplacementTemplateData: JSON.stringify({
        subject: calendario[pesquisa.mes] + '/' + pesquisa.ano + ' - ' + formulario.nome + ' (' + projeto.nome +  ')',
        content: email
      })
    }],
    Source: 'Kampana Hub  <hub@kampana.digital>',
    Template: 'kampana_hub_01',
    DefaultTemplateData: JSON.stringify({
      subject: 'unknown',
      content: 'unknown'
    })
  };
  let aws = new AWS.SES({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_KEY,
    region: process.env.AWS_REGION,
    apiVersion: '2010-12-01'
  })
  let test = await aws.sendBulkTemplatedEmail(params).promise();
  console.log(test);
}

function findNestedObj(entireObj, keyToFind, valToFind) {
  let foundObj;
  JSON.stringify(entireObj, (_, nestedValue) => {
    if (nestedValue && nestedValue[keyToFind] === valToFind) foundObj = nestedValue;
    return nestedValue;
  });
  return foundObj;
}

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};

main();