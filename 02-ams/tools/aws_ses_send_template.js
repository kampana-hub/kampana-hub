// https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/ses-examples-creating-template.html
// https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/ses-examples-sending-email.html

// Load the AWS SDK for Node.js
var AWS    = require('aws-sdk');
var dotenv = require('dotenv');

dotenv.config();

var templateData = {
  subject: 'Esse é o assunto do e-mail :)',
  content: '<h1>Aqui vai o HTML do nosso e-mail.</h1>'
}

var defaultTemplateData = {
  subject: 'unknown',
  content: 'unknown'
}

var params = {
  Destinations: [{
    Destination: {
      ToAddresses: [
        'jose.vieira@kampana.digital'
      ]
    },
    ReplacementTemplateData: JSON.stringify(templateData)
  }],
  Source: 'Kampana Hub  <hub@kampana.digital>',
  Template: 'kampana_hub_01',
  DefaultTemplateData: JSON.stringify(defaultTemplateData)
};

// Create the promise and SES service object
var templatePromise = new AWS.SES({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: process.env.AWS_REGION,
  apiVersion: '2010-12-01'
}).sendBulkTemplatedEmail(params).promise();

// Handle promise's fulfilled/rejected states
templatePromise.then(
  function(data) {
    console.log(data);
  }).catch(
  function(err) {
    console.error(err, err.stack);
});