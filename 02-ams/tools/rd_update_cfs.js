const axios = require('axios');
const dotenv = require('dotenv');

dotenv.config({path:__dirname+'/./../../.env'});

async function main() {
  // 1 - LISTAR INTEGRAÇÕES ATUAIS
  let integracoes = (await axios.get(process.env.API_URL + '/v_rdstation')).data;
  // COMPARAR CFS QUE JÁ EXISTEM COM O PADRÃO
  let cfs_hub = [
    {
      api_identifier: 'cf_hub_abandoned_cart_url',
      data_type: 'STRING',
      label: {
        'pt-BR': 'URL do Carrinho Abandonado'
      },
      name: {
        'pt-BR': 'URL do Carrinho Abandonado'
      },
      presentation_type: 'URL_INPUT',
      validation_rules: {}
    },
    {
      api_identifier: 'cf_hub_abandoned_cart_size',
      data_type: 'INTEGER',
      label: {
        'pt-BR': 'Quantidade de Itens do Carrinho Abandonado'
      },
      name: {
        'pt-BR': 'Quantidade de Itens do Carrinho Abandonado'
      },
      presentation_type: 'NUMBER_INPUT',
      validation_rules: {}
    },
    {
      api_identifier: 'cf_hub_product_01_name',
      data_type: 'STRING',
      label: {
        'pt-BR': 'Nome do Produto 01'
      },
      name: {
        'pt-BR': 'Nome do Produto 01'
      },
      presentation_type: 'TEXT_INPUT',
      validation_rules: {}
    },
    {
      api_identifier: 'cf_hub_product_01_price',
      data_type: 'STRING',
      label: {
        'pt-BR': 'Preço do Produto 01'
      },
      name: {
        'pt-BR': 'Preço do Produto 01'
      },
      presentation_type: 'TEXT_INPUT',
      validation_rules: {}
    },
    {
      api_identifier: 'cf_hub_product_01_image_url',
      data_type: 'STRING',
      label: {
        'pt-BR': 'URL da Imagem do Produto 01'
      },
      name: {
        'pt-BR': 'URL da Imagem do Produto 01'
      },
      presentation_type: 'URL_INPUT',
      validation_rules: {}
    },
    {
      api_identifier: 'cf_hub_product_02_name',
      data_type: 'STRING',
      label: {
        'pt-BR': 'Nome do Produto 02'
      },
      name: {
        'pt-BR': 'Nome do Produto 02'
      },
      presentation_type: 'TEXT_INPUT',
      validation_rules: {}
    },
    {
      api_identifier: 'cf_hub_product_02_price',
      data_type: 'STRING',
      label: {
        'pt-BR': 'Preço do Produto 02'
      },
      name: {
        'pt-BR': 'Preço do Produto 02'
      },
      presentation_type: 'TEXT_INPUT',
      validation_rules: {}
    },
    {
      api_identifier: 'cf_hub_product_02_image_url',
      data_type: 'STRING',
      label: {
        'pt-BR': 'URL da Imagem do Produto 02'
      },
      name: {
        'pt-BR': 'URL da Imagem do Produto 02'
      },
      presentation_type: 'URL_INPUT',
      validation_rules: {}
    },
    {
      api_identifier: 'cf_hub_total_spent',
      data_type: 'STRING',
      label: {
        'pt-BR': 'Quantidade Total Gasta pelo Cliente'
      },
      name: {
        'pt-BR': 'Quantidade Total Gasta pelo Cliente'
      },
      presentation_type: 'TEXT_INPUT',
      validation_rules: {}
    },
    {
      api_identifier: 'cf_hub_order_count',
      data_type: 'INTEGER',
      label: {
        'pt-BR': 'Quantidade de Pedidos Realizados'
      },
      name: {
        'pt-BR': 'Quantidade de Pedidos Realizados'
      },
      presentation_type: 'NUMBER_INPUT',
      validation_rules: {}
    }
  ];
  // 2 - PARA CADA INTEGRAÇÃO
  integracoes.forEach(async (integracao, index) => {
    /*
    // LISTAR AS CFS PELO IDENTIFICADOR
    let cfs = (await axios.get(process.env.RD_BASE_URL + '/platform/contacts/fields', {
      headers: {
        'Authorization': 'Bearer ' + integracao.access_token
      }
    })).data.fields.map(item => { if(item.api_identifier == 'cf_hub_total_spent' || item.api_identifier == 'cf_hub_order_count') console.log(item) });
    */
    let cfs = (await axios.get(process.env.RD_BASE_URL + '/platform/contacts/fields', {
      headers: {
        'Authorization': 'Bearer ' + integracao.access_token
      }
    })).data.fields.map(item => { return item.api_identifier });
    let criar_cfs = [];
    cfs_hub.forEach((item, index) => {
      if(cfs.indexOf(cfs_hub[index].api_identifier) == -1) {
        criar_cfs.push(cfs_hub[index]);
      }
    });
    criar_cfs.forEach(async (cfs) => {
      try {
        let uuid =  (await axios.post(process.env.RD_BASE_URL + '/platform/contacts/fields',
          // OBJETO
          cfs,
          // AUTENTICAÇÃO
          {
            headers: {
              'Authorization': 'Bearer ' + integracao.access_token,
              'Content-Type': 'application/json'
            }
          }
        )).data.uuid;
      } catch(e) { console.log(e.response.data.errors); }
    });
  });
}

main();