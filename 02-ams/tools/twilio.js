const accountSid = 'AC119cfd0cca05f9e57f24840e8aaef695'; // process.env.TWILIO_ACCOUNT_SID;
const authToken = '943dbba57ab6688096a06f75b7851876'; // process.env.TWILIO_AUTH_TOKEN;

// require the Twilio module and create a REST client
const client = require('twilio')(accountSid, authToken);

client.messages
  .create({
    to: '+5562995030938',
    from: '+15592062898',
    body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
  })
  .then(message => console.log(message.sid));