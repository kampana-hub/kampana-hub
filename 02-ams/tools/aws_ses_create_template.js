// https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/ses-examples-creating-template.html
// https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/ses-examples-sending-email.html

// Load the AWS SDK for Node.js
var AWS    = require('aws-sdk');
var dotenv = require('dotenv');
var fs     = require('fs')

dotenv.config();

var html = fs.readFileSync('./public/email.html', 'utf8');

var params = {
  Template: {
    TemplateName: 'kampana_hub_01',
    SubjectPart: '{{subject}}',
    HtmlPart: html
  }
};

var templatePromise = new AWS.SES({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_KEY,
  region: process.env.AWS_REGION,
  apiVersion: '2010-12-01'
}).createTemplate(params).promise();

templatePromise.then(
  function(data) {
    console.log(data);
  }).catch(
  function(err) {
    console.error(err, err.stack);
});