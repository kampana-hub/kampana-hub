const mongoose = require('mongoose');

const linkSchema = new mongoose.Schema({
  url_code: {
    type: String,
  },
  long_url: {
    type: String,
  },
	short_url: {
    type: String,
  },
  click_count: {
    type: Number,
  },
	status: {
    type: Number,
  }
});

module.exports = linkSchema;