const mongoose = require('mongoose');

const leadSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
    lowercase: true,
  },
  personal_phone: {
    type: String,
  },
  mobile_phone: {
    type: String,
  },
  tags: {
    type: Array,
  },
  created_at: {
    type: Date,
    default: Date.now,
  }
});

module.exports = leadSchema;