const mongoose = require('mongoose');

const campanhaSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    lowercase: true,
  },
  nome: {
    type: String,
    required: true,
  },
  mensagem: {
    type: String,
    required: true
  },
  telefone: {
    type: String,
  },
  anexo: {
    type: String,
  },
  status: {
    type: Number,
  },
  created_at: {
    type: Date,
    default: Date.now,
  }
});

module.exports = campanhaSchema;