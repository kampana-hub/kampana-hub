const mongoose = require('mongoose');

//

const leads = mongoose.createConnection(
  'mongodb+srv://' + process.env.MDB_USER + ':' + process.env.MDB_PASS + '@' + process.env.MDB_CLUSTER + '/leads?retryWrites=true&w=majority',
  {
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true
  }
);

const campanhas = mongoose.createConnection(
  'mongodb+srv://' + process.env.MDB_USER + ':' + process.env.MDB_PASS + '@' + process.env.MDB_CLUSTER + '/campanhas?retryWrites=true&w=majority',
  {
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true
  }
);

const links = mongoose.createConnection(
 'mongodb+srv://' + process.env.MDB_USER + ':' + process.env.MDB_PASS + '@' + process.env.MDB_CLUSTER + '/links?retryWrites=true&w=majority',
  {
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true
  }
);

//

module.exports = {
  mdb_leads: leads, 
  mdb_campanhas: campanhas,
  mdb_links: links
};