/*
 * DEPENDENCIES
 */

const Auth0Strategy = require('passport-auth0');
const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const dotenv = require('dotenv');
const express = require('express');
const logger = require('morgan');
const passport = require('passport');
const path = require('path');
const session = require('express-session');

/*
 * INITIALIZATION
 */

dotenv.config({ path: __dirname + '/../.env' });

const app = express();

/*
 * AUTHENTICATION PARAMETERS
 */

const strategy = new Auth0Strategy({
      domain: process.env.AUTH0_DOMAIN,
      clientID: process.env.AUTH0_AMS_CLIENT_ID,
      clientSecret: process.env.AUTH0_AMS_CLIENT_SECRET,
      callbackURL: process.env.AUTH0_AMS_CALLBACK_URL
   },
   (accessToken, refreshToken, extraParams, profile, done) => {
      return done(null, profile);
   }
);

passport.use(strategy);

passport.serializeUser((user, done) => {
   done(null, user);
});
passport.deserializeUser((user, done) => {
   done(null, user);
});

app.use(
   session({
      secret: 'shhhhhhhhh',
      resave: false,
      saveUninitialized: false
   })
);

app.use((req, res, next) => {
   res.locals.loggedIn = false;
   if (req.session.passport && typeof req.session.passport.user != 'undefined') {
      res.locals.loggedIn = true;
   }
   next();
});

app.use(passport.initialize());
app.use(passport.session());

/*
 * EXPRESS STUFF
 */

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
   extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/*
 * ROUTES
 */

app.use('/', require('./routes/index'));

// INÍCIO

app.use('/api/dashboard'      , require('./routes/api/dashboard'));
app.use('/api/clientes'       , require('./routes/api/clientes'));
app.use('/api/projetos'       , require('./routes/api/projetos'));
app.use('/api/atividades'     , require('./routes/api/atividades'));
app.use('/api/usuarios'       , require('./routes/api/usuarios'));
app.use('/api/back-office'    , require('./routes/api/back-office'));

// INTEGRAÇÃO

app.use('/api/activecampaign' , require('./routes/api/activecampaign'));
app.use('/api/facebook'       , require('./routes/api/facebook'));
app.use('/api/google'         , require('./routes/api/google'));
app.use('/api/nuvemshop'      , require('./routes/api/nuvemshop'));
app.use('/api/rdstation'      , require('./routes/api/rdstation'));
app.use('/api/shopify'        , require('./routes/api/shopify'));
app.use('/api/sms'            , require('./routes/api/sms'));
app.use('/api/tray'           , require('./routes/api/tray'));
app.use('/api/whatsapp'       , require('./routes/api/whatsapp'));

// RELACIONAMENTO

app.use('/api/leads'          , require('./routes/api/leads'));
app.use('/api/campanhas'      , require('./routes/api/campanhas'));
app.use('/api/links'          , require('./routes/api/links'));

// GESTÃO DO SUCESSO

app.use('/api/formularios'  , require('./routes/api/formularios'));
app.use('/api/pesquisas'    , require('./routes/api/pesquisas'));
app.use('/api/relatorios'   , require('./routes/api/relatorios'));

// FERRAMENTAS

app.use('/api/credenciais'  , require('./routes/api/credenciais'));
app.use('/api/dominios'  , require('./routes/api/dominios'));

// ROTAS AUXILIARES

app.use('/api/upload-files' , require('./routes/api/upload-files'));

/*
 * ERROR HANDLING
 */

app.use((req, res, next) => {
   next(createError(404));
});

app.use((err, req, res, next) => {
   res.locals.message = err.message;
   res.locals.error = req.app.get('env') === 'development' ? err : {};
   res.status(err.status || 500);
   res.render('error', {
      message: err.message,
      error: res.locals.error,
      status: err.status || 500
   });
});

/*
 * SERVER START
 */

app.listen(4002, () => {
   console.log('[DEV] Kampana Hub - AMS | Listening on port 4002.');
});

module.exports = app;