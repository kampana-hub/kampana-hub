const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const moment = require('moment');
const passport = require('passport');
const querystring = require('querystring');
const router = express.Router();
const util = require('util');

/**
 * LAYOUT
 */

let layout = {
  canonical: process.env.HUB_AMS_URL,
  //
  prefix:    process.env.LAYOUT_PREFIX,
  //
  style:     process.env.LAYOUT_STYLE,
  logo:      process.env.LAYOUT_LOGO,
  favicon:   process.env.LAYOUT_FAVICON,
  //
  api_url:   process.env.LAYOUT_API_URL,
  aws_s3:    process.env.LAYOUT_AWS_S3
}

/**
 * INÍCIO
 */

router.get('/', ensureLoggedIn, (req, res, next) => {
  res.render('dashboard', {
    layout: layout,
    title: 'Dashboard',
    user: req.session.passport.user
  });
});

router.get('/clientes', ensureLoggedIn, (req, res, next) => {
  res.render('clientes', {
    layout: layout,
    title: 'Clientes',
    user: req.session.passport.user
  });
});

router.get('/projetos', ensureLoggedIn, (req, res, next) => {
  res.render('projetos', {
    layout: layout,
    title: 'Projetos',
    user: req.session.passport.user
  });
});

// FILTO PARA ALTERNAR ENTRE PROJETOS INTERNOS / EXTERNOS
router.get('/alternar', ensureLoggedIn, async (req, res, next) => {
  try {
    req.session.passport.user.externo = Math.abs(req.session.passport.user.externo - 1);
    res.redirect('back');
  } catch (e) {
    res.status(500).send('Erro ao alterar o filtro de projetos.');
  }
});
//

router.get('/projetos/:id', ensureLoggedIn, (req, res, next) => {
  res.render('projetos-id', {
    layout: layout,
    title: 'Projetos - ' + req.params.id,
    user: req.session.passport.user,
    id: req.params.id
  });
});

router.get('/atividades', ensureLoggedIn, (req, res, next) => {
  res.render('atividades', {
     layout: layout,
    title: 'Atividades',
    user: req.session.passport.user
  });
});

router.get('/usuarios', ensureLoggedIn, (req, res, next) => {
  res.render('usuarios', {
    layout: layout,
    title: 'Usuários',
    user: req.session.passport.user
  });
});

router.get('/back-office', ensureLoggedIn, (req, res, next) => {
  res.render('back-office', {
    layout: layout,
    title: 'Back-Office',
    user: req.session.passport.user
  });
});

/**
 * INTEGRAÇÃO
 */

router.get('/activecampaign', ensureLoggedIn, (req, res, next) => {
  res.render('activecampaign', {
    layout: layout,
    title: 'ActiveCampaign',
    user: req.session.passport.user
  });
});

router.get('/facebook', ensureLoggedIn, (req, res, next) => {
  res.render('facebook', {
    layout: layout,
    title: 'Facebook',
    user: req.session.passport.user
  });
});

router.get('/nuvemshop', ensureLoggedIn, (req, res, next) => {
  res.render('nuvemshop', {
    layout: layout,
    title: 'Nuvemshop',
    user: req.session.passport.user
  });
});

router.get('/rdstation', ensureLoggedIn, (req, res, next) => {
  res.render('rdstation', {
    layout: layout,
    title: 'RD Station',
    user: req.session.passport.user
  });
});

router.get('/shopify', ensureLoggedIn, (req, res, next) => {
  res.render('shopify', {
    layout: layout,
    title: 'Shopify',
    user: req.session.passport.user
  });
});

router.get('/sms', ensureLoggedIn, (req, res, next) => {
  res.render('sms', {
    layout: layout,
    title: 'SMS',
    user: req.session.passport.user
  });
});

/*
router.get('/shopify/:id', (req, res, next) => {
  res.render('shopify-id', {
    layout: layout,
    title: 'Shopify - ' + req.params.id,
    id: req.params.id
  });
});
*/

router.get('/tray', ensureLoggedIn, (req, res, next) => {
  res.render('tray', {
    layout: layout,
    title: 'Tray',
    user: req.session.passport.user
  });
});

router.get('/whatsapp', ensureLoggedIn, (req, res, next) => {
  res.render('whatsapp', {
    layout: layout,
    title: 'WhatsApp',
    user: req.session.passport.user
  });
});

/**
 * RELACIONAMENTO
 */

router.get('/links', ensureLoggedIn, (req, res, next) => {
  res.render('links', {
    layout: layout,
    title: 'Links',
    user: req.session.passport.user
  });
});

router.get('/leads', ensureLoggedIn, (req, res, next) => {
  res.render('leads', {
    layout: layout,
    title: 'Leads',
    user: req.session.passport.user
  });
});

router.get('/campanhas', ensureLoggedIn, (req, res, next) => {
  res.render('campanhas', {
    layout: layout,
    title: 'Campanhas',
    user: req.session.passport.user
  });
});

/**
 * GESTÃO DO SUCESSO
 */

router.get('/formularios', ensureLoggedIn, (req, res, next) => {
  res.render('formularios', {
    layout: layout,
    title: 'Formulários',
    user: req.session.passport.user,
  });
});

router.get('/formularios/:id', ensureLoggedIn, (req, res, next) => {
  res.render('formularios-id', {
    layout: layout,
    title: 'Formulários - ' + req.params.id,
    user: req.session.passport.user,
    id: req.params.id
  });
});

router.get('/pesquisas', ensureLoggedIn, (req, res, next) => {
  res.render('pesquisas', {
    layout: layout,
    title: 'Pesquisas',
    user: req.session.passport.user,
  });
});

router.get('/pesquisas/:id', (req, res, next) => {
  res.render('pesquisas-id', {
    layout: layout,
    title: 'Pesquisas - ' + req.params.id,
    id: req.params.id
  });
});

router.get('/relatorios', ensureLoggedIn, (req, res, next) => {
  res.render('relatorios', {
    layout: layout,
    title: 'Relatórios',
    user: req.session.passport.user,
  });
});

router.get('/relatorios/:id', ensureLoggedIn, (req, res, next) => {
  res.render('relatorios-editar', {
    layout: layout,
    title: 'Relatórios - ' + req.params.id,
    user: req.session.passport.user,
    id: req.params.id
  });
});

router.get('/relatorios/:id/visualizar', (req, res, next) => {
  res.render('relatorios-visualizar', {
    layout: layout,
    title: 'Relatórios - ' + req.params.id,
    id: req.params.id
  });
});

/**
 * FERRAMENTAS
 */

router.get('/credenciais', ensureLoggedIn, (req, res, next) => {
  res.render('credenciais', {
    layout: layout,
    title: 'Credenciais',
    user: req.session.passport.user
  });
});

router.get('/dominios', ensureLoggedIn, (req, res, next) => {
  res.render('dominios', {
    layout: layout,
    title: 'Domínios',
    user: req.session.passport.user
  });
});

router.get('/facebook-interests', ensureLoggedIn, (req, res, next) => {
  res.render('facebook-interests', {
    layout: layout,
    title: 'Facebook Interests'
  });
});

/**
 * AUTHENTICATION ROUTES
 */

router.get('/refresh', ensureLoggedIn, (req, res, next) => {
  res.render('refresh');
});

router.get('/watchdog', async (req, res, next) => {
  // USUÁRIO LOGADO
  if (req.session.passport) res.status(200).json({status: true});
  // USUÁRIO NÃO LOGADO
  else res.status(401).json({status: false});
});

router.get(
  '/login',
  passport.authenticate('auth0', {
    clientID: process.env.AUTH0_AMS_CLIENT_ID,
    domain: process.env.AUTH0_DOMAIN,
    redirectUri: process.env.AUTH0_AMS_CALLBACK_URL,
    audience: 'https://' + process.env.AUTH0_DOMAIN + '/userinfo',
    responseType: 'code',
    scope: 'openid email profile'
  }),
  (req, res) => {
    res.redirect('/');
  }
);

router.get(
  '/callback',
  passport.authenticate('auth0', {
    failureRedirect: '/'
  }),
  (req, res) => {
    let domain = process.env.AUTH0_DOMAIN.slice().split('.').shift()
    // IDENTIFICAR SE É ADMINISTRADOR
    req.session.passport.user.admin = req.session.passport.user._json['https://' + domain + ':auth0:com/app_metadata'].admin || false;
    if (!req.session.passport.user.admin) req.session.passport.user.projetos = req.session.passport.user._json['https://' + domain + ':auth0:com/app_metadata'].projetos || '';
    // IDENTIFICAR FUNÇÃO
    req.session.passport.user.funcao = req.session.passport.user._json['https://' + domain + ':auth0:com/app_metadata'].funcao;
    // ÚLTIMO LOGIN
    req.session.passport.user.ultimo_login = moment(req.session.passport.user._json.updated_at).format('DD/MM/YYYY hh:mm:ss');
    // DETERMINAR O TIPO DE PROJETO QUE SERÁ EXIBIDO
    req.session.passport.user.externo = 0;
    // REDIRECIONAR
    res.redirect(req.session.returnTo || '/');
  }
);

router.get('/logout', (req, res) => {
  req.logout();
  var logoutURL = new URL(
    util.format('https://%s/v2/logout', process.env.AUTH0_DOMAIN)
  );
  var searchString = querystring.stringify({
    client_id: process.env.AUTH0_AMS_CLIENT_ID,
    returnTo: process.AUTH0_AMS_LOGOUT_URL
  });
  logoutURL.search = searchString;
  res.redirect(logoutURL);
});

module.exports = router;