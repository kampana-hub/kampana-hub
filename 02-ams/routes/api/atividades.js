const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let url = process.env.API_URL + '/v_atividade';
    // BATCH SIZE
    let batch = 100;
    // NUMBER OF RECORDS
    let count = (await axios.get(url + '/count')).data.shift().no_of_rows;
    // NUMBER OF PAGES
    let pages = (count / batch).toFixed();
    // INDEX CONTROL
    let index = 0;
    // RESULTS VARIABLE
    let results = [];
    // ARRAY OF PROMISES
    let promiseArray = [];
    // CONTROLE DE PERMISSÕES
    if (!req.session.passport.user.admin) {
      url += '?_where=(projeto_id,in,' + req.session.passport.user.projetos + ')~or(gerente_id,eq,' + req.session.passport.user.user_id + ')';
    }
    // PARALLEL REQUESTS
    do {
      promiseArray.push(new Promise(async (resolve, reject) => {
        let response = (await axios.get(url + (!req.session.passport.user.admin ? '&' : '?') + '_sort=-id&_size=' + batch + '&_p=' + index++)).data;
        resolve(results = results.concat(response));
      }));
    } while (index <= pages);
    // 
    Promise.all(promiseArray).then(function() {
      res.json(results);
    });
  }
  //
  catch (e) {
    res.status(500).send('Erro ao consultar as atividades.');
  }
});

router.post('/', ensureLoggedIn, async (req, res, next) => {
  try {
    await axios.post(process.env.API_URL + '/atividade', req.body);
    res.status(200).send('OK');
  }
  //
  catch (e) {
    res.status(500).send('Erro ao criar a atividade.');
  }
});

router.patch('/:id', async (req, res, next) => {
  try {
    await axios.patch(process.env.API_URL + '/atividade/' + req.params.id, req.body);
    res.status(200).send('OK');
  }
  //
  catch (e) {
    res.status(500).send('Erro ao atualizar a atividade.');
  }
});

router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    await axios.delete(process.env.API_URL + '/atividade/' + req.params.id, req.body);
    res.status(200).send('OK');
  }
  //
  catch (e) {
    res.status(500).send('Erro ao excluir a atividade.');
  }
});

module.exports = router;