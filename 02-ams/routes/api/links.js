const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const shortid = require('shortid');
const validUrl = require('valid-url');

const router = express.Router();

const { mdb_links } = require('../../mdb');
const linkSchema = require('../../models/links');

router.get('/:id', async (req, res, next) => {
  try {
    let collection = mdb_links.model(req.params.id, linkSchema);
    res.json(await collection.find());
  } catch (e) {
    res.status(500).send('Erro ao consultar os links.');
  }
});

router.get('/:id/code/:url_code', async (req, res, next) => {
  try {
    let collection = mdb_links.model(req.params.id, linkSchema);
    let response = await collection.findOne({
      url_code: req.params.url_code
    });
    if (response) {
      let click_count = response.click_count;
      if (click_count >= process.env.URL_ALLOWED_CLICK) {
        await collection.findOneAndUpdate({
          url_code: req.params.url_code
        }, {
          status: 0
        });
        return res.status(500).send('O acesso para o link de codigo ' + req.params.url_code + ' ultrapassou o limite máximo de ' + process.env.URL_ALLOWED_CLICK + ' acessos!');
      }
      click_count++;
      await collection.findOneAndUpdate({
        url_code: req.params.url_code
      }, {
        click_count: click_count
      });
      return res.redirect(response.long_url);
    } else {
      return res.status(500).send('Link não encontrado para o codigo: ' + req.params.url_code);
    }
  } catch (e) {
    res.status(500).send('Erro ao acessar o link.');
  }
});

router.post('/:id', async (req, res, next) => {
  try {
    let collection = mdb_links.model(req.params.id, linkSchema);
    let base_url = process.env.API_PUBLIC_URL;
    //
    if (!validUrl.isUri(process.env.API_PUBLIC_URL)) return res.status(500).send('A Url:' + base_url + 'não é válida!');
    //
    if (validUrl.isUri(req.body.url)) {
      let url_code = shortid.generate();
      //
      let response = await collection.findOne({
        long_url: req.body.url
      });
      //
      if (response) {
        return res.status(200).send('Url já utilizada: \n' + response);
      } else {
        let short_url = base_url + '/api/links/' + req.params.id + '/code/' + url_code;
        let create_url = await collection.create({
          long_url: req.body.url,
          short_url: short_url,
          url_code: url_code,
          click_count: 0,
          status: req.body.status
        });
        //
        return res.status(201).send(create_url);
      }
    } else {
      res.status(400).send('Url inválida. Favor, entre com uma url válida!');
    }
  } catch (e) {
    res.status(500).send('Erro ao criar o link.');
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    // [...]
  } catch (e) {
    res.status(500).send('Erro ao criar o link.');
  }
});
module.exports = router;