const ActiveCampaign = require('activecampaign');
const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();
const url = require('url');

/**
 * BASIC ROUTES
 */

// LISTAR TODAS AS INTEGRAÇÕES
router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let integracoes = (await axios.get(process.env.API_URL + '/v_activecampaign')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(integracoes);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ALTERAR O STATUS DE UMA INTEGRAÇÃO
router.put('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // MUDAR  STATUS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.activecampaign.status = !projeto.integracoes.activecampaign.status;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    await axios.post(process.env.API_URL + '/log_activecampaign', {
      tipo: projeto.integracoes.activecampaign.status ? 'info' : 'warning',
      descricao: projeto.integracoes.activecampaign.status ? 'Integração habilitada com sucesso.' : 'Integração desabilitada com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_activecampaign', {
      tipo: 'danger',
      descricao: 'Erro na alteração do status da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// EXCLUIR UMA INTEGRAÇÃO
router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // REMOVER A INTEGRAÇÃO DO PROJETO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // DELETAR O DOMÍNIO REFERENTE AO CÓDIGO DE MONITORAMENTO
    if (projeto.integracoes.activecampaign.tracking_domain) {
      try {
        (await axios.delete(projeto.integracoes.activecampaign.url + '/api/3/siteTrackingDomains/' + projeto.integracoes.activecampaign.tracking_domain,
          // AUTENTICAÇÃO
          {
            headers: {
              'Api-Token': projeto.integracoes.activecampaign.key
            }
          }
        ));
      } catch(e) { console.log(e.response.data); }
    }
    // DELETAR TAGS
    let tags = ['pedido_criado', 'pedido_pago', 'carrinho_abandonado'];
    if (projeto.integracoes.activecampaign.tags) {
      for (let tag of tags) {
        try {
          (await axios.delete(projeto.integracoes.activecampaign.url + '/api/3/tags/' + projeto.integracoes.activecampaign.tags[tag],
            // AUTENTICAÇÃO
            {
              headers: {
                'Api-Token': projeto.integracoes.activecampaign.key
              }
            }
          ));
        } catch(e) { console.log(e.response.data); }
      }
    }
    // DELETAR CUSTOM FIELDS
    let cfs = ['cf_hub_abandoned_cart_url', 'cf_hub_abandoned_cart_size', 'cf_hub_product_01_name', 'cf_hub_product_01_price', 'cf_hub_product_01_image_url', 'cf_hub_product_02_name', 'cf_hub_product_02_price', 'cf_hub_product_02_image_url', 'cf_hub_total_spent', 'cf_hub_order_count'];
    if (projeto.integracoes.activecampaign.custom_fields) {
      for (let cf of cfs) {
        try {
          (await axios.delete(projeto.integracoes.activecampaign.url + '/api/3/fields/' + projeto.integracoes.activecampaign.custom_fields[cf],
            // AUTENTICAÇÃO
            {
              headers: {
                'Api-Token': projeto.integracoes.activecampaign.key
              }
            }
          ));
        } catch(e) { console.log(e.response.data); }
      }
    }
    // DELETAR CONEXÃO
    if (projeto.integracoes.activecampaign.connection) {
      try {
        (await axios.delete(projeto.integracoes.activecampaign.url + '/api/3/connections/' + projeto.integracoes.activecampaign.connection.id,
          // AUTENTICAÇÃO
          {
            headers: {
              'Api-Token': projeto.integracoes.activecampaign.key
            }
          }
        ));
      } catch(e) { console.log(e.response.data); }
    }
    delete projeto.integracoes.activecampaign;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_activecampaign', {
      tipo: 'warning',
      descricao: 'Integração excluída com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_activecampaign', {
      tipo: 'danger',
      descricao: 'Erro na exclusão da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/**
 * CUSTOM ROUTES
 */

// TESTAR UMA INTEGRAÇÃO
router.get('/debug/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA DE TESTES NA API DO ACTIVE CAMPAIGN
    let ac = new ActiveCampaign(projeto.integracoes.activecampaign.url, projeto.integracoes.activecampaign.key);
    // TEST API credentials
    ac.credentials_test().then(async result => {
      // successful request
      if (result.success) {
        // CRIAR O LOG
        await axios.post(process.env.API_URL + '/log_activecampaign', {
          tipo: 'info',
          descricao: 'Integração testada com sucesso.',
          origem: 'hub',
          extra: JSON.stringify({
            user: {
              id: req.session.passport.user.id,
              displayName: req.session.passport.user.displayName,
              email: req.session.passport.user.emails.slice().shift().value
            }
          }),
          projeto_id: req.params.id
        });
        // ENVIAR RESPOSTA POSITIVA
        res.json(result);
      } else {
        // INVALID ACCOUNT
      }
    }, async result => {
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_activecampaign', {
        tipo: 'danger',
        descricao: 'Erro no teste da integração.',
        origem: 'hub',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        projeto_id: req.params.id
      });
      // ENVIAR RESPOSTA NEGATIVA
      res.status(500).send(result);
    });
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_activecampaign', {
      tipo: 'danger',
      descricao: 'Erro no teste da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// LISTAR O LOG DE UMA INTEGRAÇÃO
router.get('/logs/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let logs = (await axios.get(process.env.API_URL + '/v_activecampaign_log?_where=(projeto_id,eq,' + req.params.id + ')')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(logs);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/**
 * AUTHENTICATION ROUTES
 */

// CRIAR UMA INTEGRAÇÃO
router.post('/login/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CRIAR INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.activecampaign = {
      name: projeto.nome,
      url: req.body.url,
      key: req.body.key,
      last_timestamp: Math.floor(+new Date() / 1000),
      status: true
    }
    // INCLUIR O CÓDIGO DE MONITORAMENTO
    try {
      let trackingcode_response = (await axios.get(projeto.integracoes.activecampaign.url + '/api/3/siteTracking/code',
        // AUTENTICAÇÃO
        {
          headers: {
            'Api-Token': projeto.integracoes.activecampaign.key
          }
        }
      )).data.replace('<script type="text/javascript">', '').replace('</script>', '');
      projeto.integracoes.activecampaign.tracking_code = trackingcode_response;
    } catch(e) { console.log(e.response.data); }
    // INCLUIR O DOMÍNIO DA LOJA PARA O CÓDIGO DE MONITORAMENTO
    if (projeto.integracoes.nuvemshop && projeto.integracoes.nuvemshop.original_domain) {
      try {
        let domain_response = (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/siteTrackingDomains',
          // OBJETO
          JSON.stringify({
            siteTrackingDomain: {
              name: '*.'  + projeto.integracoes.nuvemshop.original_domain
            }
          }),
          // AUTENTICAÇÃO
          {
            headers: {
              'Api-Token': projeto.integracoes.activecampaign.key
            }
          }
        )).data.siteTrackingDomain.name;
        projeto.integracoes.activecampaign.tracking_domain = domain_response;
      } catch(e) { console.log(e.response.data); }
    }
    // PREPARAR OBJETOS DOS CAMPOS CUSTOMIZADOS
    projeto.integracoes.activecampaign.tags = {};
    let tags = [
      {
        "tag":{
          "tag": "pedido_criado",
          "tagType": "contact",
          //"description": "Description"
        }
      },
      {
        "tag":{
          "tag": "pedido_pago",
          "tagType": "contact",
          //"description": "Description"
        }
      },
      {
        "tag":{
          "tag": "carrinho_abandonado",
          "tagType": "contact",
          //"description": "Description"
        }
      }
    ];
    // CRIAR TAGS
    for (let tag of tags) {
      try {
        let tags_response = (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/tags',
          // OBJETO
          tag,
          // AUTENTICAÇÃO
          {
            headers: {
              'Api-Token': projeto.integracoes.activecampaign.key
            }
          }
        )).data.tag;
        projeto.integracoes.activecampaign.tags[tags_response.tag] = tags_response.id;
      } catch(e) { console.log(e.response.data); }
    }
    // CRIAR OBJETO DA CONEXÃO
    projeto.integracoes.activecampaign.connection = {};
    let connectiondata = JSON.stringify({
      connection: {
        service: "Kampana Hub",
        externalid: projeto.nome.replace(" ", "") + projeto.id,
        name: projeto.nome,
        logoUrl: "https://kampana.digital/wp-content/uploads/2021/02/logo-mini.png",
        linkUrl: "https://kampana.digital/"
      }
    });
    // CRIAR CONEXÃO PARA EVENTOS DO ECOMMERCE
    try {
      let connection_response = (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/connections',
        // OBJETO
        connectiondata,
        // AUTENTICAÇÃO
        {
          headers: {
            'Api-Token': projeto.integracoes.activecampaign.key
          }
        }
      )).data.connection;
      projeto.integracoes.activecampaign.connection = connection_response;
    } catch(e) { console.log(e.response.data); }
    // PREPARAR OBJETOS DOS CAMPOS CUSTOMIZADOS
    projeto.integracoes.activecampaign.custom_fields = {};
    let cfs = [
      {
        "field": {
          "type": "text",
          "title": "cf_hub_abandoned_cart_url",
          "descript": "Hub - URL do Carrinho Abandonado",
        }
      },
      {
        "field": {
          "type": "text",
          "title": "cf_hub_abandoned_cart_size",
          "descript": "Hub - Quantidade de Itens do Carrinho Abandonado",
        }
      },
      {
        "field": {
          "type": "text",
          "title": "cf_hub_product_01_name",
          "descript": "Hub - Nome do Produto 01",
        }
      },
      {
        "field": {
          "type": "text",
          "title": "cf_hub_product_01_price",
          "descript": "Hub - Preço do Produto 01",
        }
      },
      {
        "field": {
          "type": "text",
          "title": "cf_hub_product_01_image_url",
          "descript": "Hub - URL da Imagem do Produto 01",
        }
      },
      {
        "field": {
          "type": "text",
          "title": "cf_hub_product_02_name",
          "descript": "Hub - Nome do Produto 02",
        }
      },
      {
        "field": {
          "type": "text",
          "title": "cf_hub_product_02_price",
          "descript": "Hub - Preço do Produto 02",
        }
      },
      {
        "field": {
          "type": "text",
          "title": "cf_hub_product_02_image_url",
          "descript": "Hub - URL da Imagem do Produto 02",
        }
      },
      {
        "field": {
          "type": "text",
          "title": "cf_hub_total_spent",
          "descript": "Hub - Quantidade Total Gasta pelo Cliente",
        }
      },
      {
        "field": {
          "type": "text",
          "title": "cf_hub_order_count",
          "descript": "Hub - Quantidade de Pedidos Realizados",
        }
      }
    ];
    // CRIAR CAMPOS CUSTOMIZADOS
    for (let cf of cfs) {
      try {
        let cfs_response = (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/fields',
          // OBJETO
          cf,
          // AUTENTICAÇÃO
          {
            headers: {
              'Api-Token': projeto.integracoes.activecampaign.key
            }
          }
        )).data.field;
        projeto.integracoes.activecampaign.custom_fields[cfs_response.title] = cfs_response.id;
      } catch(e) { console.log(e.response.data); }
    }
    //
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    await axios.post(process.env.API_URL + '/log_activecampaign', {
      tipo: 'info',
      descricao: 'Integração criada com sucesso (' + projeto.integracoes.activecampaign.name + ').',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_activecampaign', {
      tipo: 'danger',
      descricao: 'Erro na criação da integração.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    //
    res.status(500).send('Not Ok');
  }
});

module.exports = router;