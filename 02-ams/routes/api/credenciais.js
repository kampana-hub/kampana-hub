const axios = require('axios');
const Cryptr = require('cryptr');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

const cryptr = new Cryptr(process.env.CRYPTR_KEY);

router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let url = '/v_credencial?';
    // CONTROLE DE PERMISSÕES
    if (!req.session.passport.user.admin) {
      url += '&_where=(projeto_id,in,' + req.session.passport.user.projetos + ')~or(gerente_id,eq,' + req.session.passport.user.user_id + ')';
    }
    // BATCH SIZE
    let batch = 100;
    // NUMBER OF RECORDS
    let count = (await axios.get(process.env.API_URL + '/v_credencial/count')).data.shift().no_of_rows;
    // NUMBER OF PAGES
    let pages = (count / batch).toFixed();
    // INDEX CONTROL
    let index = 0;
    // RESULTS VARIABLE
    let results = [];
    // ARRAY OF PROMISES
    let promiseArray = [];
    // PARALLEL REQUESTS
    do {
      promiseArray.push(new Promise(async (resolve, reject) => {
        let response = (await axios.get(process.env.API_URL + url + '&_sort=-id&_size=' + batch + '&_p=' + index++)).data;
        resolve(results = results.concat(response));
      }));
    } while (index <= pages);
    // 
    await Promise.all(promiseArray);
    res.json(results);
  }
  //
  catch (e) {
    res.status(500).send('Erro ao consultar as credenciais.');
  }
});

router.get('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let raw = (await axios.get(process.env.API_URL + '/credencial/' + req.params.id)).data;
    if (raw.length == 0) res.json(null);
    else {
      let credencial = raw.shift();
      //
      credencial.senha = cryptr.decrypt(credencial.senha);
      //
      res.json(credencial);
    }
  }
  //
  catch (e) {
    res.status(500).send('Erro ao consultar a credencial.');
  }
});

router.post('/', ensureLoggedIn, async (req, res, next) => {
  try {
    req.body.senha = cryptr.encrypt(req.body.senha);
    //
    await axios.post(process.env.API_URL + '/credencial', req.body);
    //
    res.status(200).send('OK');
  }
  //
  catch (e) {
    res.status(500).send('Erro ao criar a credencial.');
  }
});

router.patch('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    req.body.senha = cryptr.encrypt(req.body.senha);
    //
    await axios.patch(process.env.API_URL + '/credencial/' + req.params.id, req.body);
    //
    res.status(200).send('OK');
  }
  //
  catch (e) {
    res.status(500).send('Erro ao atualizar a credencial.');
  }
});

router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    await axios.delete(process.env.API_URL + '/credencial/' + req.params.id, req.body);
    res.status(200).send('OK');
  }
  //
  catch (e) {
    res.status(500).send('Erro ao excluir a credencial.');
  }
});

module.exports = router;