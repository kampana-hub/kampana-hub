const axios = require('axios');
const auth0 = require('auth0');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

//

let management = {};

router.use((req, res, next) => {
  let authentication = new auth0.AuthenticationClient({
    domain: process.env.AUTH0_DOMAIN,
    clientId: process.env.AUTH0_MANAGEMENT_CLIENT_ID,
    clientSecret: process.env.AUTH0_MANAGEMENT_CLIENT_SECRET
  }).clientCredentialsGrant({
    audience: 'https://' + process.env.AUTH0_DOMAIN + '/api/v2/',
    scope: 'read:users'
  }, (err, response) => {
    management = new auth0.ManagementClient({
      token: response.access_token,
      domain: process.env.AUTH0_DOMAIN
    });
    next();
  });
});

//

router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let url = '/v_relatorio?';
    // CONTROLE DE PERMISSÕES
    if (!req.session.passport.user.admin) {
      url += '&_where=(projeto_id,in,' + req.session.passport.user.projetos + ')~or(gerente_id,eq,' + req.session.passport.user.user_id + ')';
    }
    // BATCH SIZE
    let batch = 100;
    // NUMBER OF RECORDS
    let count = (await axios.get(process.env.API_URL + '/v_relatorio/count')).data.shift().no_of_rows;
    // NUMBER OF PAGES
    let pages = (count / batch).toFixed();
    // INDEX CONTROL
    let index = 0;
    // RESULTS VARIABLE
    let results = [];
    // ARRAY OF PROMISES
    let promiseArray = [];
    // PARALLEL REQUESTS
    do {
      promiseArray.push(new Promise(async (resolve, reject) => {
        let response = (await axios.get(process.env.API_URL + url + '&_sort=-id&_size=' + batch + '&_p=' + index++)).data;
        resolve(results = results.concat(response));
      }));
    } while (index <= pages);
    // 
    await Promise.all(promiseArray);
    res.json(results);
  } catch (e) {
    res.status(500).send('Erro ao consultar os relatórios.');
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    let raw = (await axios.get(process.env.API_URL + '/relatorio/' + req.params.id)).data;
    if (raw.length == 0) res.json(null);
    else {
      let relatorio = raw.shift();
      //
      relatorio.projeto = (await axios.get(process.env.API_URL + '/projeto/' + relatorio.projeto_id)).data.shift();
      relatorio.cliente = (await axios.get(process.env.API_URL + '/cliente/' + relatorio.projeto.cliente_id)).data.shift();
      //
      relatorio.atividades = (await axios.get(process.env.API_URL + '/v_atividade?_size=100&_where=(projeto_id,eq,' + relatorio.projeto_id + ')~and(mes,eq,' + relatorio.mes + ')~and(ano,eq,' + relatorio.ano + ')')).data;
      //
      management.getUser({id: relatorio.projeto.gerente_id}, (err, user) => {
        if (err) res.status(500).send('Erro ao consultar o relatório.');
        else {
          relatorio.gerente = user;
          res.json(relatorio);
        }
      });
    }
  } catch (e) {
    res.status(500).send('Erro ao consultar o relatório.');
  }
});

router.post('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let relatorio = await axios.post(process.env.API_URL + '/relatorio', req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao criar o relatório.');
  }
});

router.patch('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let relatorio = await axios.patch(process.env.API_URL + '/relatorio/' + req.params.id, req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao atualizar o relatório.');
  }
});

router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let relatorio = await axios.delete(process.env.API_URL + '/relatorio/' + req.params.id, req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao excluir o relatório.');
  }
});

module.exports = router;