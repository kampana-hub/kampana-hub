const auth0 = require('auth0');
const axios = require('axios');
const cors = require('cors');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

//

let management = {};

router.use((req, res, next) => {
  let authentication = new auth0.AuthenticationClient({
    domain: process.env.AUTH0_DOMAIN,
    clientId: process.env.AUTH0_MANAGEMENT_CLIENT_ID,
    clientSecret: process.env.AUTH0_MANAGEMENT_CLIENT_SECRET
  }).clientCredentialsGrant({
    audience: 'https://' + process.env.AUTH0_DOMAIN + '/api/v2/',
    scope: 'read:users'
  }, (err, response) => {
    management = new auth0.ManagementClient({
      token: response.access_token,
      domain: process.env.AUTH0_DOMAIN
    });
    next();
  });
});

//

router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let projetos = [];
    {
      let url = '/projeto?_where=(externo,eq,' + req.session.passport.user.externo + ')';
      // CONTROLE DE PERMISSÕES
      if (!req.session.passport.user.admin) {
        url += '~and((id,in,' + req.session.passport.user.projetos + ')~or(gerente_id,eq,' + req.session.passport.user.user_id + '))';
      }
      // BATCH SIZE
      let batch = 100;
      // NUMBER OF RECORDS
      let count = (await axios.get(process.env.API_URL + '/projeto/count')).data.shift().no_of_rows;
      // NUMBER OF PAGES
      let pages = (count / batch).toFixed();
      // INDEX CONTROL
      let index = 0;
      // RESULTS VARIABLE
      let results = [];
      // ARRAY OF PROMISES
      let promiseArray = [];
      // PARALLEL REQUESTS
      do {
        promiseArray.push(new Promise(async (resolve, reject) => {
          let response = (await axios.get(process.env.API_URL + url + '&_sort=-id&_size=' + batch + '&_p=' + index++)).data;
          resolve(results = results.concat(response));
        }));
      } while (index <= pages);
      // 
      await Promise.all(promiseArray);
      projetos = results;
    }
    let gerentes = [];
    let promises = [];
    // CARREGAR OS DADOS DOS GERENTES
    promises.push(new Promise(async (resolve, reject) => {
      management.getUsers((err, users) => {
        if (err) res.status(500).send('Erro ao consultar os usuários.');
        else resolve(gerentes = users.filter(user => user.identities.filter(identity => identity.connection === process.env.AUTH0_AMS_CONNECTION).length > 0));
      });
    }));
    // ENCONTRAR O CLIENTE DE CADA PROJETO
    projetos.forEach(async (projeto) => {
      promises.push(new Promise(async (resolve, reject) => {
        resolve(projeto.cliente = (await axios.get(process.env.API_URL + '/cliente?_where=(id,eq,' + projeto.cliente_id + ')')).data.shift());
      }));
    });
    // PREPARAR PARA ENVIO
    Promise.all(promises).then(() => {
      // MAPEAR GERENTE DE ACORDO COM O PROJETO
      projetos.forEach(projeto => {
        if (projeto.gerente_id) projeto.gerente_nome = gerentes.find(gerente => gerente.user_id == projeto.gerente_id).name;
      });
      // ENVIAR
      res.json(projetos);
    });
  } catch (e) {
    console.log(e);
    res.status(500).send('Erro ao consultar os projetos.');
  }
});

// REMOVIDO O ensureLoggedIn PARA A INTEGRAÇÃO COM O SHOPIFY - PENSAR NUM MÉTODO ALTERNATIVO
router.get('/:id', cors(), async (req, res, next) => {
  try {
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data;
    if (projeto.length == 0) res.json(null);
    else res.json(projeto.shift());
  } catch (e) {
    res.status(500).send('Erro ao consultar o projeto.');
  }
});

router.get('/:id/atividades', ensureLoggedIn, async (req, res, next) => {
  try {
    let atividade = (await axios.get(process.env.API_URL + '/atividade?_size=100&_where=(projeto_id,eq,' + req.params.id + ')')).data;
    if (atividade.length == 0) res.json([]);
    else res.json(atividade);
  } catch (e) {
    res.status(500).send('Erro ao consultar as atividades do projeto.');
  }
});

router.get('/:id/relatorios', ensureLoggedIn, async (req, res, next) => {
  try {
    let relatorios = (await axios.get(process.env.API_URL + '/relatorio?_where=(projeto_id,eq,' + req.params.id + ')')).data;
    if (relatorios.length == 0) res.json([]);
    else res.json(relatorios);
  } catch (e) {
    res.status(500).send('Erro ao consultar os relatórios do projeto.');
  }
});

router.get('/:id/pesquisas', ensureLoggedIn, async (req, res, next) => {
  try {
    let pesquisas = (await axios.get(process.env.API_URL + '/v_pesquisa?_where=(projeto_id,eq,' + req.params.id + ')')).data;
    if (pesquisas.length == 0) res.json([]);
    else res.json(pesquisas);
  } catch (e) {
    res.status(500).send('Erro ao consultar as pesquisas do projeto.');
  }
});

router.get('/:id/credenciais', ensureLoggedIn, async (req, res, next) => {
  try {
    let credenciais = (await axios.get(process.env.API_URL + '/v_credencial?_where=(projeto_id,eq,' + req.params.id + ')')).data;
    if (credenciais.length == 0) res.json([]);
    else res.json(credenciais);
  } catch (e) {
    res.status(500).send('Erro ao consultar as credenciais do projeto.');
  }
});

router.post('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let projeto = await axios.post(process.env.API_URL + '/projeto', req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao criar o projeto.');
  }
});

router.patch('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let projeto = await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao atualizar o projeto.');
  }
});

router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let projeto = await axios.delete(process.env.API_URL + '/projeto/' + req.params.id, req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao excluir o projeto.');
  }
});

module.exports = router;