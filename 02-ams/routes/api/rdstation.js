const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();
const url = require('url');

/**
 * BASIC ROUTES
 */

// LISTAR TODAS AS INTEGRAÇÕES
router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let integracoes = (await axios.get(process.env.API_URL + '/v_rdstation')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(integracoes);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ALTERAR O STATUS DE UMA INTEGRAÇÃO
router.put('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // MUDAR  STATUS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.rdstation.status = !projeto.integracoes.rdstation.status;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: projeto.integracoes.rdstation.status ? 'info' : 'warning',
      descricao: projeto.integracoes.rdstation.status ? 'Integração habilitada com sucesso.' : 'Integração desabilitada com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'danger',
      descricao: 'Erro na alteração do status da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// EXCLUIR UMA INTEGRAÇÃO
router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // REMOVER A INTEGRAÇÃO DO PROJETO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    delete projeto.integracoes.rdstation;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'warning',
      descricao: 'Integração excluída com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'danger',
      descricao: 'Erro na exclusão da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/**
 * CUSTOM ROUTES
 */

// TESTAR UMA INTEGRAÇÃO
router.get('/debug/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA DE TESTES NA API DO RD STATION
    let debug = (await axios.get(process.env.RD_BASE_URL + '/marketing/account_info', {
      headers: {
        'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token
      }
    })).data;
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'info',
      descricao: 'Integração testada com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.json(debug);
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'danger',
      descricao: 'Erro no teste da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ATUALIZAR UMA INTEGRAÇÃO
router.get('/refresh/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA DE ATUALIZAÇÃO NA API DO RD STATION    
    projeto.integracoes.rdstation.access_token = (await axios.post(process.env.RD_BASE_URL + '/auth/token', {
      client_id: process.env.RD_CLIENT_ID,
      client_secret: process.env.RD_CLIENT_SECRET,
      refresh_token: projeto.integracoes.rdstation.refresh_token
    })).data.access_token;
    projeto.integracoes.rdstation.last_timestamp = Math.floor(+new Date() / 1000);
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'info',
      descricao: 'Integração atualizada com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'danger',
      descricao: 'Erro na atualização da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// LISTAR WEBHOOKS DE UMA INTEGRAÇÃO
router.get('/webhooks/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA NA API DO RD STATION
    let debug = (await axios.get(process.env.RD_BASE_URL + '/integrations/webhooks', {
      headers: {
        'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token
      }
    })).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(debug);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ATUALIZAR WEBHOOKS DE UMA INTEGRAÇÃO
router.put('/webhooks/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // COMPARAR REQUISIÇÃO COM DADOS ATUAIS
    projeto.integracoes.rdstation.webhooks = projeto.integracoes.rdstation.webhooks || {};
    let options = ['converted', 'marked_opportunity'];
    let chamadas = [];
    //
    let integracoes_habilitadas = [];
    let integracoes_desabilitadas = [];
    //
    options.forEach(webhook => {
      // EXISTE ESSA INTEGRAÇÃO CONFIGUADA HOJE?
      if (projeto.integracoes.rdstation.webhooks[webhook]) {
        // ELA DEVE SER REMOVIDA?
        if (!req.body[webhook]) chamadas.push(new Promise(async (resolve, reject) => {
          // SUBIR NA FILA DE CHAMADAS À API
          let chamada = (await axios.delete(process.env.RD_BASE_URL + '/integrations/webhooks/' + projeto.integracoes.rdstation.webhooks[webhook].uuid, {
            headers: {
              'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token
            }
          })).data;
          integracoes_desabilitadas.push(webhook);
          resolve(delete projeto.integracoes.rdstation.webhooks[webhook]);
        }));
      }
      //
      else {
        // ELA DEVE SER CRIADA?
        if (req.body[webhook]) chamadas.push(new Promise(async (resolve, reject) => {
          // SUBIR NA FILA DE CHAMADAS À API
          let chamada = (await axios.post(process.env.RD_BASE_URL + '/integrations/webhooks',
            // OBJETO
            {
              event_type: 'WEBHOOK.' + webhook.toUpperCase(),
              entity_type: 'CONTACT',
              // event_identifiers: [],
              url: process.env.HUB_API_URL + '/webhooks/rdstation/leads/' + req.params.id,
              http_method: 'POST',
              // include_relations: ['COMPANY', 'CONTACT_FUNNEL']
            },
            // AUTENTICAÇÃO
            {
              headers: {
                'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token,
                'Content-Type': 'application/json'
              }
            }
          )).data;
          integracoes_habilitadas.push(webhook);
          resolve(projeto.integracoes.rdstation.webhooks[webhook] = chamada);
        }));
      }
    });
    // REALIZAR CHAMADAS NA API DO RD STATION
    Promise.all(chamadas).then(async () => {
      if (Object.entries(projeto.integracoes.rdstation.webhooks).length === 0) delete projeto.integracoes.rdstation.webhooks;
      projeto.integracoes = JSON.stringify(projeto.integracoes);
      // ATUALIZAR PROJETO
      await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
        integracoes: projeto.integracoes
      });
      // CRIAR O LOG
      let descricao = '';
      if (integracoes_habilitadas.length > 0 ) descricao = descricao + 'Eventos habilitados: ' + integracoes_habilitadas.join(', ') + '. ';
      if (integracoes_desabilitadas.length > 0) descricao = descricao + 'Eventos desabilitados: ' + integracoes_desabilitadas.join(', ') + '.';
      await axios.post(process.env.API_URL + '/log_rdstation', {
        tipo: 'warning',
        descricao: descricao,
        origem: 'hub',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        projeto_id: req.params.id
      });
      // ENVIAR RESPOSTA POSITIVA
      res.status(200).send('Ok');
    });
  }
  //
  catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'danger',
      descricao: 'Erro na atualização dos eventos.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// LISTAR O LOG DE UMA INTEGRAÇÃO
router.get('/logs/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let logs = (await axios.get(process.env.API_URL + '/v_rdstation_log?_where=(projeto_id,eq,' + req.params.id + ')')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(logs);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// LISTAR CUSTOM FIELDS DE UMA INTEGRAÇÃO
router.get('/cfs/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA NA API DO RD STATION
    let debug = (await axios.get(process.env.RD_BASE_URL + '/platform/contacts/fields', {
      headers: {
        'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token
      }
    })).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(debug);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/**
 * AUTHENTICATION ROUTES
 */

router.get('/login/:id', ensureLoggedIn, async (req, res, next) => {
  // ARMAZENAR NA SESSÃO O ID DO PROJETO QUE SERÁ VINCULADO
  req.session.projeto_id = req.params.id;
  // REDIRECIONAR PARA O AMBIENTE DE AUTORIZAÇÃO DO RD STATION
  res.redirect(process.env.RD_BASE_URL + '/auth/dialog' + '?client_id=' + process.env.RD_CLIENT_ID + '&redirect_uri=' + process.env.RD_CALLBACK_URL);
});

router.get('/callback', async (req, res, next) => {
  // USUÁRIO LOGADO - OPERAÇÃO REALIZADA NO AMS
  if (req.session.passport) {
    try {
      // SOLICITAR TOKENS COM BASE NO CÓDIGO RETORNADO
      let tokens = (await axios.post(process.env.RD_BASE_URL + '/auth/token', {
        client_id: process.env.RD_CLIENT_ID,
        client_secret: process.env.RD_CLIENT_SECRET,
        code: req.query.code
      })).data;
      // CARREGAR PROJETO
      let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.projeto_id)).data.shift();
      // INCLUIR A INTEGRAÇÃO NO PROJETO
      projeto.integracoes = JSON.parse(projeto.integracoes) || {};
      projeto.integracoes.rdstation = tokens;
      projeto.integracoes.rdstation.last_timestamp = Math.floor(+new Date() / 1000);
      projeto.integracoes.rdstation.status = true;
      // INCLUIR TAMBÉM O NOME DA CONTA
      projeto.integracoes.rdstation.name = (await axios.get(process.env.RD_BASE_URL + '/marketing/account_info', {
        headers: {
          'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token
        }
      })).data.name;
      // INCLUIR TAMBÉM O TRACKING CODE
      projeto.integracoes.rdstation.tracking_code = (await axios.get(process.env.RD_BASE_URL + '/marketing/tracking_code', {
        headers: {
          'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token
        }
      })).data.path;
      /**/
        // PREPARAR OBJETOS DOS CAMPOS CUSTOMIZADOS
        projeto.integracoes.rdstation.custom_fields = {};
        let cfs = [
          {
            api_identifier: 'cf_hub_abandoned_cart_url',
            data_type: 'STRING',
            label: {
              'pt-BR': 'Hub - URL do Carrinho Abandonado'
            },
            name: {
              'pt-BR': 'Hub - URL do Carrinho Abandonado'
            },
            presentation_type: 'URL_INPUT',
            validation_rules: {}
          },
          {
            api_identifier: 'cf_hub_abandoned_cart_size',
            data_type: 'INTEGER',
            label: {
              'pt-BR': 'Hub - Quantidade de Itens do Carrinho Abandonado'
            },
            name: {
              'pt-BR': 'Hub - Quantidade de Itens do Carrinho Abandonado'
            },
            presentation_type: 'NUMBER_INPUT',
            validation_rules: {}
          },
          {
            api_identifier: 'cf_hub_product_01_name',
            data_type: 'STRING',
            label: {
              'pt-BR': 'Hub - Nome do Produto 01'
            },
            name: {
              'pt-BR': 'Hub - Nome do Produto 01'
            },
            presentation_type: 'TEXT_INPUT',
            validation_rules: {}
          },
          {
            api_identifier: 'cf_hub_product_01_price',
            data_type: 'STRING',
            label: {
              'pt-BR': 'Hub - Preço do Produto 01'
            },
            name: {
              'pt-BR': 'Hub - Preço do Produto 01'
            },
            presentation_type: 'TEXT_INPUT',
            validation_rules: {}
          },
          {
            api_identifier: 'cf_hub_product_01_image_url',
            data_type: 'STRING',
            label: {
              'pt-BR': 'Hub - URL da Imagem do Produto 01'
            },
            name: {
              'pt-BR': 'Hub - URL da Imagem do Produto 01'
            },
            presentation_type: 'URL_INPUT',
            validation_rules: {}
          },
          {
            api_identifier: 'cf_hub_product_02_name',
            data_type: 'STRING',
            label: {
              'pt-BR': 'Hub - Nome do Produto 02'
            },
            name: {
              'pt-BR': 'Hub - Nome do Produto 02'
            },
            presentation_type: 'TEXT_INPUT',
            validation_rules: {}
          },
          {
            api_identifier: 'cf_hub_product_02_price',
            data_type: 'STRING',
            label: {
              'pt-BR': 'Hub - Preço do Produto 02'
            },
            name: {
              'pt-BR': 'Hub - Preço do Produto 02'
            },
            presentation_type: 'TEXT_INPUT',
            validation_rules: {}
          },
          {
            api_identifier: 'cf_hub_product_02_image_url',
            data_type: 'STRING',
            label: {
              'pt-BR': 'Hub - URL da Imagem do Produto 02'
            },
            name: {
              'pt-BR': 'Hub - URL da Imagem do Produto 02'
            },
            presentation_type: 'URL_INPUT',
            validation_rules: {}
          },
          {
            api_identifier: 'cf_hub_total_spent',
            data_type: 'STRING',
            label: {
              'pt-BR': 'Hub - Quantidade Total Gasta pelo Cliente'
            },
            name: {
              'pt-BR': 'Hub - Quantidade Total Gasta pelo Cliente'
            },
            presentation_type: 'TEXT_INPUT',
            validation_rules: {}
          },
          {
            api_identifier: 'cf_hub_order_count',
            data_type: 'INTEGER',
            label: {
              'pt-BR': 'Hub - Quantidade de Pedidos Realizados'
            },
            name: {
              'pt-BR': 'Hub - Quantidade de Pedidos Realizados'
            },
            presentation_type: 'NUMBER_INPUT',
            validation_rules: {}
          }
        ];
        // CRIAR CAMPOS CUSTOMIZADOS
        for (let cf of cfs) {
          try {
            let uuid =  (await axios.post(process.env.RD_BASE_URL + '/platform/contacts/fields',
              // OBJETO
              cf,
              // AUTENTICAÇÃO
              {
                headers: {
                  'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token,
                  'Content-Type': 'application/json'
                }
              }
            )).data.uuid;
            projeto.integracoes.rdstation.custom_fields[cf.api_identifier] = uuid;
          } catch(e) { console.log(e.response.data); }
        }
      /**/
      // ATUALIZAR PROJETO
      projeto.integracoes = JSON.stringify(projeto.integracoes);
      await axios.patch(process.env.API_URL + '/projeto/' + req.session.projeto_id, {
        integracoes: projeto.integracoes
      });
      // CRIAR O LOG
      projeto.integracoes = JSON.parse(projeto.integracoes) || {};
      await axios.post(process.env.API_URL + '/log_rdstation', {
        tipo: 'info',
        descricao: 'Integração criada com sucesso (' + projeto.integracoes.rdstation.name + ').',
        origem: 'hub',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        projeto_id: req.session.projeto_id
      });
      // REDIRECIONAR O USUÁRIO PARA A TELA DE CADASTRO COM MENASGEM DE SUCESSO
      res.redirect('/rdstation?sucesso=true');
    }
    //
    catch (e) {
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_rdstation', {
        tipo: 'danger',
        descricao: 'Erro na criação da integração.',
        origem: 'hub',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        projeto_id: req.session.projeto_id
      });
      // REDIRECIONAR O USUÁRIO PARA A TELA DE CADASTRO COM MENASGEM DE ERRO
      res.redirect('/rdstation?sucesso=false');
    }
  }
  // USUÁRIO NÃO LOGADO - OPERAÇÃO REALIZADA NO APP
  else {
    res.redirect(process.env.HUB_APP_URL + '/api/rdstation/callback?' + url.parse(req.url).query);
  }
});

module.exports = router;