const auth0 = require('auth0');
const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const multer = require('multer');
const router = express.Router();

//

let management = {};

router.use((req, res, next) => {
  let authentication = new auth0.AuthenticationClient({
    domain: process.env.AUTH0_DOMAIN,
    clientId: process.env.AUTH0_MANAGEMENT_CLIENT_ID,
    clientSecret: process.env.AUTH0_MANAGEMENT_CLIENT_SECRET
  }).clientCredentialsGrant({
    audience: 'https://' + process.env.AUTH0_DOMAIN + '/api/v2/',
    scope: 'read:users'
  }, (err, response) => {
    management = new auth0.ManagementClient({
      token: response.access_token,
      domain: process.env.AUTH0_DOMAIN
    });
    next();
  });
});

//

router.get('/', ensureLoggedIn, (req, res, next) => {
  management.getUsers((err, users) => {
    if (err) res.status(500).send('Erro ao consultar os usuários.');
    else res.json(users.filter(user => user.identities.filter(identity => identity.connection === process.env.AUTH0_APP_CONNECTION).length > 0));
  });
});

router.get('/logs/:id', ensureLoggedIn, (req, res, next) => {
  management.getUserLogs({
    id: req.params.id
  }, (err, logs) => {
    if (err) res.status(500).send('Erro ao consultar os logs do usuário.');
    else res.json(logs);
  });
});

router.get('/:id', ensureLoggedIn, (req, res, next) => {
  management.getUser({
    id: req.params.id
  }, (err, user) => {
    if (err) res.status(500).send('Erro ao consultar o usuário.');
    else res.json(user);
  });
});

// IMAGEM DE PERFIL
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/cache');
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + '-' + Date.now() + '-' + file.originalname);
  }
});

let fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') cb(null, true);
  else cb(null, false);
}

let upload = multer({
  storage: storage,
  limits: {
    fileSize: 480 * 480
  },
  fileFilter: fileFilter
});

router.post('/', ensureLoggedIn, upload.single('file'), (req, res, next) => {
  let user = {
    connection: process.env.AUTH0_APP_CONNECTION,
    name: req.body.nome,
    email: req.body.email,
    password: 'Kampana@2021',
    blocked: req.body.status ? false : true,
    user_metadata: {
      nascimento: req.body.nascimento,
      telefone: req.body.telefone
    },
    app_metadata: {
      projetos: req.body.projetos
    }
  };
  if (req.file) user.picture = process.env.API_PUBLIC_URL + '/cache/' + req.file.filename;
  management.createUser(user, (err, user) => {
    if (err) res.status(500).send('Erro ao criar o usuário.');
    else res.status(200).send('OK');
  });
});

router.put('/password/:id', ensureLoggedIn, (req, res, next) => {
  management.updateUser({
    id: req.params.id
  }, {
    password: req.body.password,
  }, (err, user) => {
    if (err) res.status(500).send('Erro ao alterar a senha do usuário.');
    else res.status(200).send('OK');
  });
});

router.put('/:id', ensureLoggedIn, upload.single('img_perfil'), (req, res, next) => {
  let user = {
    name: req.body.nome,
    email: req.body.email,
    blocked: req.body.status ? false : true,
    user_metadata: {
      nascimento: req.body.nascimento,
      telefone: req.body.telefone
    },
    app_metadata: {
      projetos: req.body.projetos
    }
  };
  if (req.file) user.picture = process.env.API_PUBLIC_URL + '/cache/' + req.file.filename;
  management.updateUser({
    id: req.params.id
  }, user, (err, user) => {
    if (err) res.status(500).send('Erro ao atualizar o usuário.');
    else res.status(200).send('OK');
  });
});

router.delete('/:id', ensureLoggedIn, (req, res, next) => {
  management.deleteUser({
    id: req.params.id
  }, (err, user) => {
    if (err) res.status(500).json(err);
    else res.json(user);
  });
});

module.exports = router;