const AWS = require('aws-sdk');
const auth0 = require('auth0');
const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const path = require('path');
const fs = require('fs');
const us2 = require('url-safe-string');
const multer = require('multer');
const router = express.Router();
const dotenv = require('dotenv');

const safe = new us2();
dotenv.config();

router.get('/', ensureLoggedIn, (req, res, next) => {
  res.status(200).send('OK');
});

// FILE
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/cache');
  },
  filename: (req, file, cb) => {
    let extension = path.extname(file.originalname);
    let filename = safe.generate(file.fieldname) + '-' + (+ new Date()) + extension;
    cb(null, filename);
  }
});

let fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') cb(null, true);
  else cb(null, false);
}

let upload = multer({
  storage: storage,
  fileFilter: fileFilter
});

router.post('/:bucket_name', ensureLoggedIn, upload.array('files'), async (req, res, next) => {
  let bucket_name = req.params.bucket_name;
  let s3upload = null;
  let files = [];
  try {
    if (req.files) {
      let s3Client = new AWS.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_KEY,
        region: process.env.AWS_REGION
      });
      req.files.forEach(async (item) => {
        let fileStream = fs.createReadStream(item.path);
        s3upload = await s3Client.upload({
          Bucket: process.env.AWS_BUCKET + '/' + bucket_name,
          Key: item.filename,
          Body: fileStream,
        }).promise();
      });
      files = req.files.map((item) => { return process.env.AWS_PUBLIC_URL + '/' + bucket_name + '/' + item.filename })
    }
    res.json(files);
  } catch (e) {
    console.log(e);
    res.status(500).send('Erro ao carregar o anexo da campanha.');
  }
});

/*
router.put('/:id', ensureLoggedIn, upload.single('img_perfil'), (req, res, next) => {
  if (req.file) user.picture = process.env.API_PUBLIC_URL + '/cache/' + req.file.filename;
  res.status(200).send('OK');
});
*/

module.exports = router;