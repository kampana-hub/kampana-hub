const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let resposta = {};
    // PROJETOS
    let projetos = [];
    {
      let url = '/projeto?_where=(externo,eq,' + req.session.passport.user.externo + ')';
      // CONTROLE DE PERMISSÕES
      if (!req.session.passport.user.admin) {
        url += '~and((id,in,' + req.session.passport.user.projetos + ')~or(gerente_id,eq,' + req.session.passport.user.user_id + '))';
      }
      // BATCH SIZE
      let batch = 100;
      // NUMBER OF RECORDS
      let count = (await axios.get(process.env.API_URL + '/projeto/count')).data.shift().no_of_rows;
      // NUMBER OF PAGES
      let pages = (count / batch).toFixed();
      // INDEX CONTROL
      let index = 0;
      // RESULTS VARIABLE
      let results = [];
      // ARRAY OF PROMISES
      let promiseArray = [];
      // PARALLEL REQUESTS
      do {
        promiseArray.push(new Promise(async (resolve, reject) => {
          let response = (await axios.get(process.env.API_URL + url + '&_sort=-id&_size=' + batch + '&_p=' + index++)).data;
          resolve(results = results.concat(response));
        }));
      } while (index <= pages);
      // 
      await Promise.all(promiseArray);
      projetos = results;
    }
    resposta.projetos = {
      ativos:   projetos.filter(projeto =>  projeto.status).length,
      inatviso: projetos.filter(projeto => !projeto.status).length,
    };
    // CLIENTES
    resposta.clientes = {
      ativos:   [...new Set(projetos.filter(projeto =>  projeto.status).map(projeto => projeto.cliente_id))].length,
      inativos: [...new Set(projetos.filter(projeto => !projeto.status).map(projeto => projeto.cliente_id))].length
    };
    // MRR
    resposta.mrr = {
      ativo: projetos.filter(projeto =>  projeto.status).map(projeto => {
        let comercial = JSON.parse(projeto.comercial);
        //
        if (comercial && comercial.kampana && comercial.kampana.mrr) return comercial.kampana.mrr;
        else return 0;
      }).reduce((a, b) => a + b, 0),
      inativo: projetos.filter(projeto => !projeto.status).map(projeto => {
        let comercial = JSON.parse(projeto.comercial);
        //
        if (comercial && comercial.kampana && comercial.kampana.mrr) return comercial.kampana.mrr;
        else return 0;
      }).reduce((a, b) => a + b, 0)
    };
    // PESQUISAS & NPS
    let pesquisas = [];
    {
      let url = '/pesquisa?_where=(projeto_id,in,' + projetos.map(projeto => projeto.id).toString() + ')';
      // BATCH SIZE
      let batch = 100;
      // NUMBER OF RECORDS
      let count = (await axios.get(process.env.API_URL + '/pesquisa/count')).data.shift().no_of_rows;
      // NUMBER OF PAGES
      let pages = (count / batch).toFixed();
      // INDEX CONTROL
      let index = 0;
      // RESULTS VARIABLE
      let results = [];
      // ARRAY OF PROMISES
      let promiseArray = [];
      // PARALLEL REQUESTS
      do {
        promiseArray.push(new Promise(async (resolve, reject) => {
          let response = (await axios.get(process.env.API_URL + url + '&_sort=-id&_size=' + batch + '&_p=' + index++)).data;
          resolve(results = results.concat(response));
        }));
      } while (index <= pages);
      // 
      await Promise.all(promiseArray);
      pesquisas = results;
    }
    resposta.nps = [];
    {
      let pesquisas_nps = pesquisas.filter(pesquisa => pesquisa.formulario_id == 1);
      let competencia = [...new Set(pesquisas_nps.map(item => item.mes + '/' + item.ano))];
      competencia.forEach(competencia => {
        let mes = competencia.slice().split('/').shift();
        let ano = competencia.slice().split('/').pop();
        // FILTRAR PESQUISAS DO MÊS/ANO
        let pesquisas_competencia = pesquisas_nps.filter(pesquisa => pesquisa.ano == ano && pesquisa.mes == mes);
        // PROMOTERS
        let promoters = pesquisas_competencia.filter(pesquisa => pesquisa.status && JSON.parse(pesquisa.respostas).questao_09 >= 9);
        // PASSIVES
        let passives = pesquisas_competencia.filter(pesquisa => pesquisa.status && JSON.parse(pesquisa.respostas).questao_09 >= 7 && JSON.parse(pesquisa.respostas).questao_09 < 9);
        // DETRACTORS
        let detractors = pesquisas_competencia.filter(pesquisa => pesquisa.status && JSON.parse(pesquisa.respostas).questao_09 < 7);
        // NO SHOW
        let noshow = pesquisas_competencia.filter(pesquisa => !pesquisa.status);
        resposta.nps.push({
          mes: mes,
          ano: ano,
          promoters: promoters.length,
          passives: passives.length,
          detractors: detractors.length,
          nps: parseFloat((((promoters.length - detractors.length) / (promoters.length + passives.length + detractors.length)) * 100).toFixed(2)) || 0,
          nps_noshow: parseFloat((((promoters.length - detractors.length - noshow.length) / (promoters.length + passives.length + detractors.length + noshow.length)) * 100).toFixed(2)) || 0
        });
      });
      resposta.nps.reverse();
    }
    // RESPOSTA FINAL
    res.json(resposta);
  } catch (e) {
    console.log(e);
    res.status(500).send('Erro ao consultar os dados do dashboard.');
  }
});

module.exports = router;