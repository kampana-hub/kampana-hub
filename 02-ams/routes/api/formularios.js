const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let formularios = (await axios.get(process.env.API_URL + '/formulario')).data;
    res.json(formularios);
  } catch (e) {
    res.status(500).send('Erro ao consultar os formulários.');
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    let formulario = (await axios.get(process.env.API_URL + '/formulario/' + req.params.id)).data;
    if (formulario.length == 0) res.json(null);
    else res.json(formulario.shift());
  } catch (e) {
    res.status(500).send('Erro ao consultar o formulário.');
  }
});

router.post('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let formulario = await axios.post(process.env.API_URL + '/formulario', req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao criar o formulário.');
  }
});

router.patch('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let formulario = await axios.patch(process.env.API_URL + '/formulario/' + req.params.id, req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao atualizar o formulário.');
  }
});

router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let formulario = await axios.delete(process.env.API_URL + '/formulario/' + req.params.id, req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao excluir o formulário.');
  }
});

module.exports = router;