const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

/**
 * AUTHENTICATION ROUTES
 */

router.get('/login/:id', ensureLoggedIn, async (req, res, next) => {
  // ARMAZENAR NA SESSÃO O ID DO PROJETO QUE SERÁ VINCULADO
  req.session.projeto_id = req.params.id;
  // REDIRECIONAR PARA O AMBIENTE DE AUTORIZAÇÃO DO GOOGLE
  res.redirect(
    process.env.G_AUTH_URL +
    '?client_id=' + process.env.G_AUTH_CLIENT_ID +
    '&redirect_uri=' + process.env.G_AUTH_CALLBACK_URL +
    '&response_type=code&' +
    'scope=https://www.googleapis.com/auth/analytics.readonly'
  );
});

router.get('/callback', ensureLoggedIn, async (req, res, next) => {
  console.log(req.query);
});

module.exports = router;