const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let clientes = [];
    {
      // BATCH SIZE
      let batch = 100;
      // NUMBER OF RECORDS
      let count = (await axios.get(process.env.API_URL + '/cliente/count')).data.shift().no_of_rows;
      // NUMBER OF PAGES
      let pages = (count / batch).toFixed();
      // INDEX CONTROL
      let index = 0;
      // RESULTS VARIABLE
      let results = [];
      // ARRAY OF PROMISES
      let promiseArray = [];
      // PARALLEL REQUESTS
      do {
        promiseArray.push(new Promise(async (resolve, reject) => {
          let response = (await axios.get(process.env.API_URL + '/cliente?_sort=-id&_size=' + batch + '&_p=' + index++)).data;
          resolve(results = results.concat(response));
        }));
      } while (index <= pages);
      // 
      await Promise.all(promiseArray);
      clientes = results;
    }
    let projetos = [];
    clientes.forEach(async (cliente) => {
      projetos.push(new Promise(async (resolve, reject) => {
        resolve(cliente.projetos = (await axios.get(process.env.API_URL + '/cliente/' + cliente.id + '/projeto?_sort=nome')).data);
      }));
    });
    Promise.all(projetos).then(() => {
      // CONTROLE DE PERMISSÕES
      if (!req.session.passport.user.admin) res.json(clientes.filter(cliente => cliente.projetos.find(projeto => ((projeto.gerente_id == req.session.passport.user.user_id) || req.session.passport.user.projetos.split(',').includes(projeto.id.toString())))));
      else res.json(clientes);
    });
  } catch (e) {
    res.status(500).send('Erro ao consultar os clientes.');
  }
});

router.get('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let cliente = (await axios.get(process.env.API_URL + '/cliente/' + req.params.id)).data;
    if (cliente.length == 0) res.json(null);
    else res.json(cliente.shift());
  } catch (e) {
    res.status(500).send('Erro ao consultar o cliente.');
  }
});

router.post('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let cliente = await axios.post(process.env.API_URL + '/cliente', req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao criar o cliente.');
  }
});

router.patch('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let cliente = await axios.patch(process.env.API_URL + '/cliente/' + req.params.id, req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao atualizar o cliente.');
  }
});

router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let cliente = await axios.delete(process.env.API_URL + '/cliente/' + req.params.id, req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao excluir o cliente.');
  }
});

module.exports = router;