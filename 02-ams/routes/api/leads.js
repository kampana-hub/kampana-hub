const axios = require('axios');
const csvToJson = require('csvtojson');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const multer = require('multer');
const router = express.Router();

const { mdb_leads } = require('../../mdb');
const leadSchema = require('../../models/leads');

router.get('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let collection = mdb_leads.model(req.params.id, leadSchema);
    //
    if (req.query.tags) res.json(await collection.find({
      tags: {
        $all: req.query.tags
      }
    }));
    //
    else res.json(await collection.find());
  } catch (e) {
    res.status(500).send('Erro ao consultar os leads.');
  }
});

router.get('/tags/:id', async (req, res, next) => {
  let collection = mdb_leads.model(req.params.id, leadSchema);
  //
  try {
    let tags = [];
    // BUSCA TODAS AS TAGS DO PROJETO
    let filter = await collection.find().select('tags');
    // ADICIONA TAGS EM UM ARRAY
    filter.forEach((obj) => {
      if (obj.tags) obj.tags.forEach((item) => {
        tags.push(item);
      });
    });
    // TRATA DUPLICIDADE ENTRE AS TAGS
    let new_tags = tags.filter((obj, index) => tags.indexOf(obj) === index);
    //
    return res.json(new_tags);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/**
 * IMPORTAÇÃO MANUAL VIA CSV
 */

let dir = 'public/cache/';
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, dir);
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + '-' + Date.now() + '-' + file.originalname);
  }
});
let upload = multer({
  storage
});

router.post('/:id', upload.single('file'), async (req, res, next) => {
  let collection = mdb_leads.model(req.params.id, leadSchema);
  let filePath = dir + req.file.filename;
  //
  try {
    if (!req.file) res.status(404).send('Arquivo não encontrado!');
    else {
      //
      let result = await csvToJson().fromFile(filePath);
      result.forEach(async (item) => {
        await collection.findOneAndUpdate({
          email: item.Email
        }, {
          name: item.Nome,
          email: item.Email,
          personal_phone: item.Telefone,
          mobile_phone: item.Celular,
          tags: item.Tags.split(',')
        }, {
          upsert: true
        });
      });
      // ENVIAR RESPOSTA POSITIVA
      res.status(200).send('Ok');
    }
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

module.exports = router;