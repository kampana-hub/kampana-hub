const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

/**
 * BASIC ROUTES
 */

// LISTAR TODAS AS INTEGRAÇÕES
router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let integracoes = (await axios.get(process.env.API_URL + '/v_facebook')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(integracoes);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ALTERAR O STATUS DE UMA INTEGRAÇÃO
router.put('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // MUDAR  STATUS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.facebook.status = !projeto.integracoes.facebook.status;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// EXCLUIR UMA INTEGRAÇÃO
router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // REMOVER A INTEGRAÇÃO DO PROJETO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    delete projeto.integracoes.facebook;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/**
 * CUSTOM ROUTES
 */

// TESTAR UMA INTEGRAÇÃO
router.get('/debug/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA DE TESTES NA API DO FACEBOOK
    let debug = (await axios.get(process.env.FB_BASE_URL + '/me' +
                                '?fields=id,name' + 
                                '&access_token=' + projeto.integracoes.facebook.access_token
    )).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(debug);
  } catch(e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/**
 * AUTHENTICATION ROUTES
 */

router.get('/login/:id', ensureLoggedIn, async (req, res, next) => {
  // ARMAZENAR NA SESSÃO O ID DO PROJETO QUE SERÁ VINCULADO
  req.session.projeto_id = req.params.id;
  // REDIRECIONAR PARA O AMBIENTE DE AUTORIZAÇÃO DO FACEBOOK
  res.redirect(process.env.FB_AUTH_URL + 
               '/dialog/oauth?client_id=' + process.env.FB_CLIENT_ID +
               '&redirect_uri=' + process.env.FB_CALLBACK_URL +
               '&state=' + '123' +
               '&scope=ads_management,ads_read,read_insights');
});

router.get('/callback', ensureLoggedIn, async (req, res, next) => {
  try {
    // SOLICITAR TOKENS COM BASE NO CÓDIGO RETORNADO
    let url = process.env.FB_BASE_URL +
              '/oauth/access_token?client_id=' + process.env.FB_CLIENT_ID +
              '&redirect_uri=' + process.env.FB_CALLBACK_URL +
              '&client_secret=' + process.env.FB_CLIENT_SECRET +
              '&code=' + req.query.code;
    //
    let tokens = (await axios.get(url)).data;
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.projeto_id)).data.shift();
    // INCLUIR A INTEGRAÇÃO NO PROJETO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.facebook = tokens;
    projeto.integracoes.facebook.last_timestamp = Math.floor(+new Date() / 1000);
    projeto.integracoes.facebook.status = true;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.projeto_id, {
      integracoes: projeto.integracoes
    });
    // REDIRECIONAR O USUÁRIO PARA A TELA DE CADASTRO COM MENASGEM DE SUCESSO
    res.redirect('/facebook?sucesso=true');
  } catch (e) {
    // REDIRECIONAR O USUÁRIO PARA A TELA DE CADASTRO COM MENASGEM DE ERRO
    res.redirect('/facebook?sucesso=false');
  }
});

module.exports = router;