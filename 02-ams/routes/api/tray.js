const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

/**
 * BASIC ROUTES
 */

// LISTAR TODAS AS INTEGRAÇÕES
router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let integracoes = (await axios.get(process.env.API_URL + '/v_tray')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(integracoes);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ALTERAR O STATUS DE UMA INTEGRAÇÃO
router.put('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // MUDAR  STATUS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.tray.status = !projeto.integracoes.tray.status;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    await axios.post(process.env.API_URL + '/log_tray', {
      tipo: projeto.integracoes.tray.status ? 'info' : 'warning',
      descricao: projeto.integracoes.tray.status ? 'Integração habilitada com sucesso.' : 'Integração desabilitada com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_tray', {
      tipo: 'danger',
      descricao: 'Erro na alteração do status da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// EXCLUIR UMA INTEGRAÇÃO
router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // REMOVER A INTEGRAÇÃO DO PROJETO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    delete projeto.integracoes.tray;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_tray', {
      tipo: 'warning',
      descricao: 'Integração excluída com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_tray', {
      tipo: 'danger',
      descricao: 'Erro na exclusão da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/**
 * CUSTOM ROUTES
 */

// TESTAR UMA INTEGRAÇÃO
router.get('/debug/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA DE TESTES NA API DO TRAY
    let debug = (await axios.get(projeto.integracoes.tray.auth.api_address + '/info?access_token=' + projeto.integracoes.tray.access_token)).data;
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_tray', {
      tipo: 'info',
      descricao: 'Integração testada com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.json(debug);
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_tray', {
      tipo: 'danger',
      descricao: 'Erro no teste da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ATUALIZAR UMA INTEGRAÇÃO
router.get('/refresh/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA DE ATUALIZAÇÃO NA API DO TRAY
    let auth = projeto.integracoes.tray.auth;
    projeto.integracoes.tray = (await axios.get(projeto.integracoes.tray.auth.api_address + '/auth?refresh_token=' + projeto.integracoes.tray.refresh_token)).data
    projeto.integracoes.tray.auth = auth;
    projeto.integracoes.tray.last_timestamp = Math.floor(+new Date() / 1000);
    projeto.integracoes.tray.status = true;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_tray', {
      tipo: 'info',
      descricao: 'Integração atualizada com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    console.log(e.response.data);
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_tray', {
      tipo: 'danger',
      descricao: 'Erro na atualização da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// LISTAR SCRIPTS DE UMA INTEGRAÇÃO
router.get('/scripts/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA NA API DO TRAY
    let debug = (await axios.get(projeto.integracoes.tray.auth.api_address + '/external_scripts?access_token=' + projeto.integracoes.tray.access_token)).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(debug);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ATUALIZAR SCRIPTS DE UMA INTEGRAÇÃO
router.put('/scripts/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // COMPARAR REQUISIÇÃO COM DADOS ATUAIS
    projeto.integracoes.tray.scripts = projeto.integracoes.tray.scripts || {};
    projeto.integracoes.rdstation.custom_fields = projeto.integracoes.rdstation.custom_fields || {};
    let options = ['tracking_code'];
    let chamadas = [];
    //
    let integracoes_habilitadas = [];
    let integracoes_desabilitadas = [];
    //
    options.forEach(script => {
      // EXISTE ESSA INTEGRAÇÃO CONFIGUADA HOJE?
      if (projeto.integracoes.tray.scripts[script]) {
        // ELA DEVE SER REMOVIDA?
        if (!req.body[script]) chamadas.push(new Promise(async (resolve, reject) => {
          // SUBIR NA FILA DE CHAMADAS À API
          let chamada = (await axios.delete(projeto.integracoes.tray.auth.api_address + '/external_scripts/' + projeto.integracoes.tray.scripts[script].id + '?access_token=' + projeto.integracoes.tray.access_token)).data;
          integracoes_desabilitadas.push(script);
          resolve(delete projeto.integracoes.tray.scripts[script]);
        }));
      }
      //
      else {
        // ELA DEVE SER CRIADA?
        if (req.body[script]) chamadas.push(new Promise(async (resolve, reject) => {
          let objeto = {};
          // DETERMINAR OBJETO
          if (script === 'tracking_code') {
            objeto = {
              ExternalScript: { 
                source: projeto.integracoes.rdstation.tracking_code
              }
            }
          }
          // SUBIR NA FILA DE CHAMADAS À API
          let chamada = (await axios.post(projeto.integracoes.tray.auth.api_address + '/external_scripts/?access_token=' + projeto.integracoes.tray.access_token, objeto)).data;
          integracoes_habilitadas.push(script);
          resolve(projeto.integracoes.tray.scripts[script] = chamada);
        }));
      }
    });
    // REALIZAR CHAMADAS NA API DO TRAY
    Promise.all(chamadas).then(async () => {
      if (Object.entries(projeto.integracoes.tray.scripts).length === 0) delete projeto.integracoes.tray.scripts;
      projeto.integracoes = JSON.stringify(projeto.integracoes);
      // ATUALIZAR PROJETO
      await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
        integracoes: projeto.integracoes
      });
      // CRIAR O LOG
      let descricao = '';
      if (integracoes_habilitadas.length > 0 ) descricao = descricao + 'Scripts habilitados: ' + integracoes_habilitadas.join(', ') + '. ';
      if (integracoes_desabilitadas.length > 0) descricao = descricao + 'Scripts desabilitados: ' + integracoes_desabilitadas.join(', ') + '.';
      await axios.post(process.env.API_URL + '/log_tray', {
        tipo: 'warning',
        descricao: descricao,
        origem: 'hub',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        projeto_id: req.params.id
      });
      // ENVIAR RESPOSTA POSITIVA
      res.status(200).send('Ok');
    });
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_tray', {
      tipo: 'danger',
      descricao: 'Erro na atualização dos scripts.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// LISTAR O LOG DE UMA INTEGRAÇÃO
router.get('/logs/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let logs = (await axios.get(process.env.API_URL + '/v_tray_log?_where=(projeto_id,eq,' + req.params.id + ')&_size=100&_sort=-id')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(logs);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/**
 * AUTHENTICATION ROUTES
 */

router.get('/login/:id', ensureLoggedIn, async (req, res, next) => {
  // ARMAZENAR NA SESSÃO O ID DO PROJETO QUE SERÁ VINCULADO
  req.session.projeto_id = req.params.id;
  // REDIRECIONAR PARA O AMBIENTE DE AUTORIZAÇÃO DO TRAY
  res.redirect('https://' + req.query.shop + '/auth.php?response_type=code&consumer_key=' + process.env.TRAY_CONSUMER_KEY + '&callback=' + process.env.HUB_AMS_URL + '/api/tray/callback');
});

router.get('/callback', async (req, res, next) => {
  try {
    // SOLICITAR TOKENS COM BASE NO CÓDIGO RETORNADO
    let tray = (await axios.post(req.query.api_address + '/auth', {
      consumer_key: process.env.TRAY_CONSUMER_KEY,
      consumer_secret: process.env.TRAY_CONSUMER_SECRET,
      code: req.query.code
    })).data;
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.projeto_id)).data.shift();
    // INCLUIR A INTEGRAÇÃO NO PROJETO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.tray = tray;
    projeto.integracoes.tray.auth = req.query;
    projeto.integracoes.tray.last_timestamp = Math.floor(+new Date() / 1000);
    projeto.integracoes.tray.status = true;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.projeto_id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    await axios.post(process.env.API_URL + '/log_tray', {
      tipo: 'info',
      descricao: 'Integração criada com sucesso (' + projeto.integracoes.tray.auth.store_host + ').',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.projeto_id
    });
    // REDIRECIONAR O USUÁRIO PARA A TELA DE CADASTRO COM MENASGEM DE SUCESSO
    res.redirect('/tray?sucesso=true');
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_tray', {
      tipo: 'danger',
      descricao: 'Erro na criação da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.projeto_id
    });
    // REDIRECIONAR O USUÁRIO PARA A TELA DE CADASTRO COM MENASGEM DE ERRO
    res.redirect('/tray?sucesso=false');
  }
});

module.exports = router;