const AWS = require('aws-sdk');
const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const multer  = require('multer')
const path = require('path');
const us2 = require('url-safe-string');

const router = express.Router();
const upload = multer();

const { mdb_campanhas } = require('../../mdb');
const campanhaSchema = require('../../models/campanhas');

const { mdb_leads } = require('../../mdb');
const leadSchema = require('../../models/leads')

/**
 * CUSTOM ROUTES
 */

// LISTAR O LOG DE UMA CAMPANHA
router.get('/logs/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let logs = (await axios.get(process.env.API_URL + '/v_campanha_log?_where=(campanha_id,eq,' + req.params.id + ')')).data;
    res.json(logs);
  } catch (e) {
    res.status(500).send('Erro ao consultar o log da campanha.');
  }
});

// LISTAR A FILA DE UMA CAMPANHA
router.get('/filas/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let campanha_collection = mdb_campanhas.model(req.params.id, campanhaSchema);
    res.json(await campanha_collection.find());
  } catch (e) {
    res.status(500).send('Erro ao consultar a fila da campanha.');
  }
});

/**
 * BASIC ROUTES
 */

// LISTAR TODAS AS CAMPANHAS
router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let campanhas = (await axios.get(process.env.API_URL + '/v_campanha')).data;
    res.json(campanhas);
  } catch (e) {
    res.status(500).send('Erro ao consultar as campanhas.');
  }
});

// LISTAR AS INFORMAÇÕES DE UMA CAMPANHA
router.get('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let campanha = (await axios.get(process.env.API_URL + '/campanha/' + req.params.id)).data;
    if (campanha.length == 0) res.json(null);
    else res.json(campanha.shift());
  } catch (e) {
    res.status(500).send('Erro ao consultar a campanha.');
  }
});

// CRIAR UMA CAMPANHA
router.post('/', ensureLoggedIn, upload.single('file'), async (req, res, next) => {
  let s3upload = null;
  if (req.file) {
    let s3Client = new AWS.S3({
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_KEY,
      region: process.env.AWS_REGION
    });
    try {
      let safe = new us2();
      let extension = path.extname(req.file.originalname);
      let filename = safe.generate(req.body.nome) + '-' + (+ new Date()) + extension;
      //
      s3upload = await s3Client.upload({
        Bucket: process.env.AWS_BUCKET + '/campanhas',
        Key: filename,
        Body: req.file.buffer,
        ContentType: req.file.mimetype
      }).promise();
      //
    } catch (e) {
      res.status(500).send('Erro ao carregar o anexo da campanha.');
    }
  }
  try {
    // AVALIAR SE EXISTE IMAGEM ASSOCIADA
    if (req.file && s3upload) req.body.extra = JSON.stringify({
      attachment: s3upload.key
    });
    // CRIAR O REGISTRO DA CAMPANHA NO BANCO
    let campanha = (await axios.post(process.env.API_URL + '/campanha', req.body)).data;
    let tags = JSON.parse(req.body.tags || '[]');
    // SE A CAMPANHA ESTIVER PUBLICADA CRIAR TAMBÉM A FILA NO MONGO
    if (req.body.status_id == 2) {
      let lead_collection = mdb_leads.model(req.body.projeto_id, leadSchema);
      let leads;
      // FILTRAR POR TAGS
      if (tags.length > 0) leads = await lead_collection.find({
          tags: {
            $all: tags
          }
        });
      // TRAZER TODOS OS REGISTROS
      else leads = await lead_collection.find();
      // CRIAR E ENVIAR O OBJETO COM TODOS OS REGISTROS DA CAMPANHA
      let campanha_collection = mdb_campanhas.model(campanha.insertId.toString(), campanhaSchema);
      await campanha_collection.create(leads.filter(lead => lead.mobile_phone || lead.personal_phone).map(lead => {
        // TRATAR MENSAGEM
        let mensagem = req.body.mensagem ;
        if (mensagem.includes('{nome_completo}') || mensagem.includes('{primeiro_nome}') || mensagem.includes('{sobrenome}')) {
          let nome = lead.name.split(' ').map(nome => nome.charAt(0).toUpperCase() + nome.slice(1));
          //
          mensagem = mensagem.replace('{nome_completo}', nome.join(' '));
          mensagem = mensagem.replace('{primeiro_nome}', nome.slice().shift());
          mensagem = mensagem.replace('{sobrenome}', nome.slice(1).join(' '));
        }
        // TRATAR TELEFONE
        let telefone = (lead.mobile_phone || lead.personal_phone).replace(/\D/g, '');
        if (telefone.startsWith('55')) telefone = telefone.replace('55', '');
        if (telefone.length == 11) telefone = telefone.slice(0, 2) + telefone.slice(3, telefone.length);
        telefone = '55' + telefone;
        // MAPEAR OBJETO
        return {
          email: lead.email,
          nome: lead.name,
          mensagem: mensagem,
          telefone: telefone,
          status: 0,
          anexo: s3upload ? 'https://kampana-hub.s3-sa-east-1.amazonaws.com/' + s3upload.key : null
        }
      }));
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_campanha', {
        tipo: 'warning',
        descricao: 'Campanha criada (agendada) com sucesso.',
        origem: 'hub',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        campanha_id: campanha.insertId.toString()
      });
      res.status(200).send('OK');
    }
    // DO CONTRÁRIO SALVAR APENAS COMO RASCUNHO
    else {
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_campanha', {
        tipo: 'info',
        descricao: 'Campanha criada (rascunho) com sucesso.',
        origem: 'hub',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        campanha_id: campanha.insertId.toString()
      });
      res.status(200).send('OK');
    }
  } catch (e) {
    res.status(500).send('Erro ao criar a campanha.');
  }
});

// ATUALIZAR UMA CAMPANHA
router.patch('/:id', ensureLoggedIn, upload.single('file'), async (req, res, next) => {
  let s3upload = null;
  if (req.file) {
    let s3Client = new AWS.S3({
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_KEY,
      region: process.env.AWS_REGION
    });
    try {
      let safe = new us2();
      let extension = path.extname(req.file.originalname);
      let filename = safe.generate(req.body.nome) + '-' + (+ new Date()) + extension;
      //
      s3upload = await s3Client.upload({
        Bucket: process.env.AWS_BUCKET + '/campanhas',
        Key: filename,
        Body: req.file.buffer,
        ContentType: req.file.mimetype
      }).promise();
      //
    } catch (e) {
      res.status(500).send('Erro ao carregar o anexo da campanha.');
    }
  }
  try {
    // AVALIAR SE EXISTE IMAGEM ASSOCIADA
    if (req.file && s3upload) req.body.extra = JSON.stringify({
      attachment: s3upload.key
    });
    //
    let campanha = await axios.patch(process.env.API_URL + '/campanha/' + req.params.id, req.body);
    campanha = await axios.get(process.env.API_URL + '/campanha/' + req.params.id);
    let tags = JSON.parse(req.body.tags || '[]');
    // SE A CAMPANHA ESTIVER PUBLICADA CRIAR TAMBÉM A FILA NO MONGO
    if (req.body.status_id == 2) {
      let lead_collection = mdb_leads.model(req.body.projeto_id, leadSchema);
      let leads;
      // FILTRAR POR TAGS
      if (tags.length > 0) leads = await lead_collection.find({
          tags: {
            $all: tags
          }
        });
      // TRAZER TODOS OS REGISTROS
      else leads = await lead_collection.find();
      // CRIAR E ENVIAR O OBJETO COM TODOS OS REGISTROS DA CAMPANHA
      let campanha_collection = mdb_campanhas.model(req.params.id, campanhaSchema);
      await campanha_collection.create(leads.filter(lead => lead.mobile_phone || lead.personal_phone).map(lead => {
        // TRATAR MENSAGEM
        let mensagem = req.body.mensagem ;
        if (mensagem.includes('{nome_completo}') || mensagem.includes('{primeiro_nome}') || mensagem.includes('{sobrenome}')) {
          let nome = lead.name.split(' ').map(nome => nome.charAt(0).toUpperCase() + nome.slice(1));
          //
          mensagem = mensagem.replace('{nome_completo}', nome.join(' '));
          mensagem = mensagem.replace('{primeiro_nome}', nome.slice().shift());
          mensagem = mensagem.replace('{sobrenome}', nome.slice(1).join(' '));
        }
        // TRATAR TELEFONE
        let telefone = (lead.mobile_phone || lead.personal_phone).replace(/\D/g, '');
        if (telefone.startsWith('55')) telefone = telefone.replace('55', '');
        if (telefone.length == 11) telefone = telefone.slice(0, 2) + telefone.slice(3, telefone.length);
        telefone = '55' + telefone;
        // MAPEAR OBJETO
        return {
          email: lead.email,
          nome: lead.name,
          mensagem: mensagem,
          telefone: telefone,
          status: 0,
          anexo: s3upload.key ? 'https://kampana-hub.s3-sa-east-1.amazonaws.com/' + s3upload.key : null
        }
      }));
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_campanha', {
        tipo: 'warning',
        descricao: 'Campanha atualizada (agendada) com sucesso.',
        origem: 'hub',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        campanha_id: req.params.id
      });
      res.status(200).send('OK');
    }
    // SE A CAMPAMNHA VAI SER RESUMIDA AJUSTAR OS ERROS NO MONGO
    if (req.body.status_id == 3) {
      let campanha_collection = mdb_campanhas.model(req.params.id, campanhaSchema);
      await campanha_collection.updateMany({
        'status': 2
      }, {
        '$set': {
          'status': 0
        }
      }, {
        'multi': true
      });
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_campanha', {
        tipo: 'warning',
        descricao: 'Campanha resumida com sucesso.',
        origem: 'hub',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        campanha_id: req.params.id
      });
      res.status(200).send('OK');
    }
    // DO CONTRÁRIO SALVAR APENAS COMO RASCUNHO
    else {
      // CRIAR O LOG
      let descricao;
      //
      if (req.body.status_id == 1) descricao = 'Campanha atualizada (rascunho) com sucesso.';
      else if (req.body.status_id == 7) descricao = 'Campanha cancelada com sucesso.'
      else descricao = 'Campanha atualizada com sucesso.';
      //
      await axios.post(process.env.API_URL + '/log_campanha', {
        tipo: 'info',
        descricao: descricao,
        origem: 'hub',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        campanha_id: req.params.id
      });
      res.status(200).send('OK');
    }
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_campanha', {
      tipo: 'danger',
      descricao: 'Erro na atualização da campanha.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      campanha_id: req.params.id
    });
    res.status(500).send('Erro ao atualizar a campanha.');
  }
});

// EXCLUIR UMA CAMPANHA
router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let campanha = await axios.delete(process.env.API_URL + '/campanha/' + req.params.id, req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao excluir a campanha.');
  }
});

module.exports = router;