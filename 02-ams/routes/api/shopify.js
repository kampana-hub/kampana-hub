const axios = require('axios');
const cors = require('cors');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

/*
 * BASIC ROUTES
 */

// LISTAR TODAS AS INTEGRAÇÕES
router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let integracoes = (await axios.get(process.env.API_URL + '/v_shopify')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(integracoes);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ALTERAR O STATUS DE UMA INTEGRAÇÃO
router.put('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // MUDAR  STATUS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.shopify.status = !projeto.integracoes.shopify.status;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    await axios.post(process.env.API_URL + '/log_shopify', {
      tipo: projeto.integracoes.shopify.status ? 'info' : 'warning',
      descricao: projeto.integracoes.shopify.status ? 'Integração habilitada com sucesso.' : 'Integração desabilitada com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_shopify', {
      tipo: 'danger',
      descricao: 'Erro na alteração do status da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// EXCLUIR UMA INTEGRAÇÃO
router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // REMOVER A INTEGRAÇÃO DO PROJETO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    delete projeto.integracoes.shopify;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_shopify', {
      tipo: 'warning',
      descricao: 'Integração excluída com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_shopify', {
      tipo: 'danger',
      descricao: 'Erro na exclusão da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/**
 * CUSTOM ROUTES
 */

// TESTAR UMA INTEGRAÇÃO
router.get('/debug/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA DE TESTES NA API DO SHOPIFY
    let debug = (await axios.get(process.env.SHOPIFY_AUTH_URL.replace('{shop}', projeto.integracoes.shopify.shop) + '/access_scopes.json', {
      headers: {
        'X-Shopify-Access-Token': projeto.integracoes.shopify.access_token
      }
    })).data;
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_shopify', {
      tipo: 'info',
      descricao: 'Integração testada com sucesso.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.json(debug);
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_shopify', {
      tipo: 'danger',
      descricao: 'Erro no teste da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// LISTAR WEBHOOKS DE UMA INTEGRAÇÃO
router.get('/webhooks/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA DE TESTES NA API DO SHOPIFY
    let debug = (await axios.get(process.env.SHOPIFY_BASE_URL.replace('{shop}', projeto.integracoes.shopify.shop) + '/webhooks.json', {
      headers: {
        'X-Shopify-Access-Token': projeto.integracoes.shopify.access_token
      }
    })).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(debug);
  } catch(e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ATUALIZAR WEBHOOKS DE UMA INTEGRAÇÃO
// REMOVIDO O ensureLoggedIn PARA A INTEGRAÇÃO COM O SHOPIFY - PENSAR NUM MÉTODO ALTERNATIVO
router.put('/webhooks/:id', cors(), async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // COMPARAR REQUISIÇÃO COM DADOS ATUAIS
    projeto.integracoes.shopify.webhooks = projeto.integracoes.shopify.webhooks || {};
    let options = ['customers_create', 'checkouts_create', 'checkouts_update', 'checkouts_delete', 'orders_create', 'orders_updated', 'orders_paid', 'orders_partially_fulfilled', 'orders_fulfilled', 'orders_cancelled'];
    let chamadas = [];
    //
    let integracoes_habilitadas = [];
    let integracoes_desabilitadas = [];
    //
    options.forEach(webhook => {
      // EXISTE ESSA INTEGRAÇÃO CONFIGUADA HOJE?
      if (projeto.integracoes.shopify.webhooks[webhook]) {
        // ELA DEVE SER REMOVIDA?
        if (!req.body[webhook]) chamadas.push(new Promise(async (resolve, reject) => {
          // SUBIR NA FILA DE CHAMADAS À API
          let chamada = (await axios.delete(process.env.SHOPIFY_BASE_URL.replace('{shop}', projeto.integracoes.shopify.shop) + '/webhooks/' + projeto.integracoes.shopify.webhooks[webhook].id + '.json', {
            headers: {
              'X-Shopify-Access-Token': projeto.integracoes.shopify.access_token
            }
          })).data;
          integracoes_desabilitadas.push(webhook);
          resolve(delete projeto.integracoes.shopify.webhooks[webhook]);
        }));
      }
      //
      else {
        // ELA DEVE SER CRIADA?
        if (req.body[webhook]) chamadas.push(new Promise(async (resolve, reject) => {
          // SUBIR NA FILA DE CHAMADAS À API
          let chamada = (await axios.post(process.env.SHOPIFY_BASE_URL.replace('{shop}', projeto.integracoes.shopify.shop) + '/webhooks.json',
            // OBJETO
            {
              webhook: {
                topic: webhook.replace('_', '/'),
                address: process.env.HUB_API_URL + '/webhooks/shopify/' + webhook.substring(0, webhook.search('_')) + '/' + req.params.id,
                format: 'json'
              }
            },
            // AUTENTICAÇÃO
            {
              headers: {
                'X-Shopify-Access-Token': projeto.integracoes.shopify.access_token
              }
            }
          )).data.webhook;
          integracoes_habilitadas.push(webhook);
          resolve(projeto.integracoes.shopify.webhooks[webhook] = chamada);
        }));
      }
    });
    // REALIZAR CHAMADAS NA API DO SHOPIFY
    Promise.all(chamadas).then(async () => {
      if (Object.entries(projeto.integracoes.shopify.webhooks).length === 0) delete projeto.integracoes.shopify.webhooks;
      projeto.integracoes = JSON.stringify(projeto.integracoes);
      // ATUALIZAR PROJETO
      await axios.patch(process.env.API_URL + '/projeto/' + req.params.id, {
        integracoes: projeto.integracoes
      });
      // CRIAR O LOG
      let descricao = '';
      if (integracoes_habilitadas.length > 0 ) descricao = descricao + 'Eventos habilitados: ' + integracoes_habilitadas.join(', ') + '. ';
      if (integracoes_desabilitadas.length > 0) descricao = descricao + 'Eventos desabilitados: ' + integracoes_desabilitadas.join(', ') + '.';
      /*await axios.post(process.env.API_URL + '/log_shopify', {
        tipo: 'warning',
        descricao: descricao,
        origem: 'hub',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        projeto_id: req.params.id
      });
      */
      // ENVIAR RESPOSTA POSITIVA
      res.status(200).send('Ok');
    });
    //
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// LISTAR O LOG DE UMA INTEGRAÇÃO
// REMOVIDO O ensureLoggedIn PARA A INTEGRAÇÃO COM O SHOPIFY - PENSAR NUM MÉTODO ALTERNATIVO
router.get('/logs/:id', cors(), async (req, res, next) => {
  try {
    let logs = (await axios.get(process.env.API_URL + '/v_shopify_log?_where=(projeto_id,eq,' + req.params.id + ')&_size=100&_sort=-id')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(logs);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// https://shopify.dev/docs/admin-api/rest/reference/events/webhook#create-2021-01

/*
    POST /admin/api/2021-01/webhooks.json
    {
      "webhook": {
        "topic": "orders/create",
        "address": "https://whatever.hostname.com/",
        "format": "json"
      }
    }
*/

/**
 * AUTHENTICATION ROUTES
 */

router.get(['/login', '/login/:id'], async (req, res, next) => {
  // ARMAZENAR NA SESSÃO O ID DO PROJETO QUE SERÁ VINCULADO
  req.session.projeto_id = req.params.id || '12';
  // VERIFICAR SE PARA O PROJETO JÁ EXISTE UMA INTEGRAÇÃO CONFIGURADA
  let integracao = (await axios.get(process.env.API_URL + '/v_shopify?_where=(projeto_id,eq,' + req.session.projeto_id + ')')).data;
  // SE NÃO EXISTE ENTÃO REDIRECIONAR PARA A AUTENTICAÇÃO DO SHOPIFY
  if (integracao.length == 0) res.redirect(
    process.env.SHOPIFY_AUTH_URL.replace('{shop}', req.query.shop) + 
    '/authorize?client_id=' + process.env.SHOPIFY_CLIENT_ID +
    '&scope=read_orders' +
    '&redirect_uri=' + process.env.SHOPIFY_CALLBACK_URL +
    '&state=123'
  );
  // SE EXISTE ENTÃO EXIBIR A PÁGINA DE CONFIGURAÇÕES
  else res.redirect('/shopify/' + req.session.projeto_id); // res.json(integracao.shift());
});

router.get('/callback', async (req, res, next) => {
  try {
    // SOLICITAR TOKENS COM BASE NO CÓDIGO RETORNADO
    let tokens = (await axios.post(process.env.SHOPIFY_AUTH_URL.replace('{shop}', req.query.shop) + '/access_token', {
      client_id: process.env.SHOPIFY_CLIENT_ID,
      client_secret: process.env.SHOPIFY_CLIENT_SECRET,
      code: req.query.code
    })).data;
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.projeto_id)).data.shift();
    // INCLUIR A INTEGRAÇÃO NO PROJETO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.shopify = tokens;
    projeto.integracoes.shopify.shop = req.query.shop;
    projeto.integracoes.shopify.last_timestamp = Math.floor(+new Date() / 1000);
    projeto.integracoes.shopify.status = true;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.projeto_id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    await axios.post(process.env.API_URL + '/log_shopify', {
      tipo: 'info',
      descricao: 'Integração criada com sucesso (' + projeto.integracoes.shopify.shop + ').',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.projeto_id
    });
    // REDIRECIONAR O USUÁRIO PARA A TELA DE CADASTRO COM MENASGEM DE SUCESSO
    // res.redirect('/shopify?sucesso=true');
    res.redirect('https://' + projeto.integracoes.shopify.shop + '/admin/apps/kampana-hub');
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_shopify', {
      tipo: 'danger',
      descricao: 'Erro na criação da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.projeto_id
    });
    // REDIRECIONAR O USUÁRIO PARA A TELA DE CADASTRO COM MENASGEM DE ERRO
    res.redirect('/shopify?sucesso=false');
  }
});

module.exports = router;