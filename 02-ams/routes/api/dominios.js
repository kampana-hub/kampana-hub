const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

router.get('/', ensureLoggedIn, async (req, res, next) => {
  let validade = async (dominio) => {
    //.COM.BR
    return await ((await axios.get('https://rdap.registro.br/domain/' + dominio)).data.events.filter(item => {
      return item.eventAction == 'expiration'
    })).shift().eventDate;
  }
  try {
    let url = '/v_dominio?';
    // CONTROLE DE PERMISSÕES
    if (!req.session.passport.user.admin) {
      url += '&_where=(projeto_id,in,' + req.session.passport.user.projetos + ')~or(gerente_id,eq,' + req.session.passport.user.user_id + ')';
    }
    // BATCH SIZE
    let batch = 100;
    // NUMBER OF RECORDS
    let count = (await axios.get(process.env.API_URL + '/v_dominio/count')).data.shift().no_of_rows;
    // NUMBER OF PAGES
    let pages = (count / batch).toFixed();
    // INDEX CONTROL
    let index = 0;
    // RESULTS VARIABLE
    let results = [];
    // ARRAY OF PROMISES
    let promiseArray = [];
    // PARALLEL REQUESTS
    do {
      promiseArray.push(new Promise(async (resolve, reject) => {
        let response = (await axios.get(process.env.API_URL + url + '&_size=' + batch + '&_p=' + index++)).data;
        response = await Promise.all(response.map(async item => {
          if (item.dominio) return {
            cliente_id: item.cliente_id,
            cliente_nome: item.cliente_nome,
            created_at: item.created_at,
            dominio: item.dominio,
            projeto_id: item.projeto_id,
            projeto_nome: item.projeto_nome,
            validade: await validade(item.dominio)
          }
        }))
        resolve(results = results.concat(response));
      }));
    } while (index <= pages);
    // 
    await Promise.all(promiseArray);
    res.json(results);
  } catch (e) {
    res.status(500).send('Erro ao consultar os domínios.');
  }
});

module.exports = router;