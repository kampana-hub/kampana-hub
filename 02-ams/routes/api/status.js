const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const pm2 = require('pm2');
const router = express.Router();

router.get('/', ensureLoggedIn, async (req, res, next) => {
  pm2.describe('kampana-hub-cron', (err, data) => {
    if (err) res.status(500).send(err);
    else res.json(data);
  });
});

module.exports = router;