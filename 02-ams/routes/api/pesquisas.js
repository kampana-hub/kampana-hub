const axios = require('axios');
const AWS = require('aws-sdk');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const router = express.Router();

router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let url = process.env.API_URL + '/v_pesquisa';
    // BATCH SIZE
    let batch = 100;
    // NUMBER OF RECORDS
    let count = (await axios.get(url + '/count')).data.shift().no_of_rows;
    // NUMBER OF PAGES
    let pages = (count / batch).toFixed();
    // INDEX CONTROL
    let index = 0;
    // RESULTS VARIABLE
    let results = [];
    // ARRAY OF PROMISES
    let promiseArray = [];
    // CONTROLE DE PERMISSÕES
    if (!req.session.passport.user.admin) {
      url += '?_where=(projeto_id,in,' + req.session.passport.user.projetos + ')~or(gerente_id,eq,' + req.session.passport.user.user_id + ')';
    }
    // PARALLEL REQUESTS
    do {
      promiseArray.push(new Promise(async (resolve, reject) => {
        let response = (await axios.get(url + (!req.session.passport.user.admin ? '&' : '?') + '_sort=-id&_size=' + batch + '&_p=' + index++)).data;
        resolve(results = results.concat(response));
      }));
    } while (index <= pages);
    // 
    Promise.all(promiseArray).then(function() {
      res.json(results);
    });
  } catch (e) {
    res.status(500).send('Erro ao consultar as pesquisas.');
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    let raw = (await axios.get(process.env.API_URL + '/pesquisa/' + req.params.id)).data;
    if (raw.length == 0) res.json(null);
    else {
      let pesquisa = raw.shift();
      //
      // ?_fields=customerNumber,checkNumber
      //
      pesquisa.formulario = (await axios.get(process.env.API_URL + '/formulario/' + pesquisa.formulario_id)).data.shift();
      pesquisa.projeto = (await axios.get(process.env.API_URL + '/projeto/' + pesquisa.projeto_id)).data.shift();
      pesquisa.cliente = (await axios.get(process.env.API_URL + '/cliente/' + pesquisa.projeto.cliente_id)).data.shift();
      //
      res.json(pesquisa);
    }
  } catch (e) {
    res.status(500).send('Erro ao consultar a pesquisa.');
  }
});

router.post('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let pesquisa = await axios.post(process.env.API_URL + '/pesquisa', req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao criar a pesquisa.');
  }
});

router.patch('/:id', async (req, res, next) => {
  let calendario = {
    1:  'Janeiro',
    2:  'Fevereiro',
    3:  'Março',
    4:  'Abril',
    5:  'Maio',
    6:  'Junho',
    7:  'Julho',
    8:  'Agosto',
    9:  'Setembro',
    10: 'Outubro',
    11: 'Novembro',
    12: 'Dezembro'
  }

  function findNestedObj(entireObj, keyToFind, valToFind) {
    let foundObj;
    JSON.stringify(entireObj, (_, nestedValue) => {
      if (nestedValue && nestedValue[keyToFind] === valToFind) foundObj = nestedValue;
      return nestedValue;
    });
    return foundObj;
  }

  String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
  };
  
  try {
    // CHAMADA
    await axios.patch(process.env.API_URL + '/pesquisa/' + req.params.id, req.body);
    // ENTIDADE PRINCIPAL
    let pesquisa = (await axios.get(process.env.API_URL + '/pesquisa/' + req.params.id)).data.shift();
    if(pesquisa.status == 1) {
      // ENTIDADES DE APOIO
      let formulario = (await axios.get(process.env.API_URL + '/formulario/' + pesquisa.formulario_id)).data.shift();
      let projeto    = (await axios.get(process.env.API_URL + '/projeto/'    + pesquisa.projeto_id)).data.shift();
      let cliente    = (await axios.get(process.env.API_URL + '/cliente/'    + projeto.cliente_id)).data.shift();
      if(formulario.status_email == 1 && formulario.email) {
        // MANIPULAÇÃO DE DADOS
        formulario.conteudo = JSON.parse(formulario.conteudo);
        pesquisa.respostas  = JSON.parse(pesquisa.respostas);
        // EXPRESSÃO REGULAR PARA ENCONTRAR VARIÁVEIS (https://www.regextester.com/94730)
        let regexp1 = RegExp('\{\{.*?\}\}', 'g');
        let regexp2 = RegExp('\_\_.*?\_\_', 'g');
        // DETERMINAR QUAIS VARIÁVEIS ESTÃO SENDO UTILIZADAS - EXCLUIR AS INTRÍNSECAS DA ENTIDADE FORMULÁRIO
        let matches = [];
        // PERGUNTA E RESPOSTA
        let temp1 = Array.from(formulario.email.matchAll(regexp1), m => m[0]);
        temp1 = temp1.toString().split('{{').filter(Boolean);
        temp1 = temp1.toString().split('}}');
        temp1 = temp1.toString().split(',').filter(Boolean);
        // APENAS RESPOSTA
        let temp2 = Array.from(formulario.email.matchAll(regexp2), m => m[0]).filter(match => match != '__mes__' && match != '__ano__');
        temp2 = temp2.toString().split('__').filter(Boolean);
        temp2 = temp2.toString().split(',').filter(Boolean);
        // AGRUPAR EM APENAS UM ARRAY
        matches = matches.concat(temp1, temp2);
        // INICIALIZAR OBJETO COM O CONTEÚDO QUE SERÁ UTILIZADO NO E-MAIL
        let conteudo = {};
        // ENCONTRAR AS INFORMAÇÕES DE CADA IUMA DAS VARIÁVEIS  
        matches.forEach(match => {
          // IDENTIFICAR O COMPONENTE
          let component = findNestedObj(formulario.conteudo.components, 'key', match);
          // ATRIBUIR A PERGUNTA
          conteudo[match] = {
            pergunta: component.label
          }
          // ATRIBUIR A RESPOSTA COM BASE NO TIPO DO COMPONENTE
          if (component.type == 'radio') {
            conteudo[match].resposta = component.values.find(item => item.value = pesquisa.respostas[match]).label;
          } else if (component.type == 'selectboxes') {
            let temp1 = [];
            Object.keys(pesquisa.respostas[match]).forEach(item => {
              if (pesquisa.respostas[match][item]) temp1.push(item);
            });
            //
            let temp2 = [];
            temp1.forEach(item => {
              temp2.push(component.values.find(component => component.value == item).label);
            });
            //
            conteudo[match].resposta = temp2.join(' / ')
          } else {
            conteudo[match].resposta = pesquisa.respostas[match] || 'Não informado';
          }
        });
        let email = formulario.email;
        // PARTE 1
        email = email.replaceAll('__mes__', calendario[pesquisa.mes]);
        email = email.replaceAll('__ano__', pesquisa.ano);
        // PARTE 2
        matches.forEach(match => {
          email = email.replaceAll('{{' + match + '}}', '<b>' + conteudo[match].pergunta + '</b><br><br>' + conteudo[match].resposta);
          email = email.replaceAll('__' + match + '__', conteudo[match].resposta);
        });
        /*
         * TESTE DE ENVIO
         */
        let params = {
          Destinations: [{
            Destination: {
              ToAddresses: [pesquisa.respostas.email],
              CcAddresses: ['dev@kampana.digital']
            },
            ReplacementTemplateData: JSON.stringify({
              subject: calendario[pesquisa.mes] + '/' + pesquisa.ano + ' - ' + formulario.nome + ' (' + projeto.nome +  ')',
              content: email
            })
          }],
          Source: 'Kampana Hub  <hub@kampana.digital>',
          Template: 'kampana_hub_01',
          DefaultTemplateData: JSON.stringify({
            subject: 'unknown',
            content: 'unknown'
          })
        };
        let aws = new AWS.SES({
          accessKeyId: process.env.AWS_ACCESS_KEY_ID,
          secretAccessKey: process.env.AWS_SECRET_KEY,
          region: process.env.AWS_REGION,
          apiVersion: '2010-12-01'
        })
        let test = await aws.sendBulkTemplatedEmail(params).promise();
      }
    }
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao atualizar a pesquisa.');
  }
});

router.delete('/:id', ensureLoggedIn, async (req, res, next) => {
  try {
    let pesquisa = await axios.delete(process.env.API_URL + '/pesquisa/' + req.params.id, req.body);
    res.status(200).send('OK');
  } catch (e) {
    res.status(500).send('Erro ao excluir a pesquisa.');
  }
});

module.exports = router;