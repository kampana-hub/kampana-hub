/*/
 * DEPENDENCIES
/*/

const cookieParser = require('cookie-parser');
const cors         = require('cors');
const dotenv       = require('dotenv');
const express      = require('express');
const logger       = require('morgan');
const path         = require('path');

/*/
 * EXPRESS INITIALIZATION
/*/

dotenv.config({ path: __dirname + '/../.env' });

const app = express();

/*/
 * EXPRESS PARAMETERS
/*/

app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

/*/
 * ROUTES
/*/

app.use('/', require('./routes/index'));

// NUVEMSHOP

app.use('/webhooks/nuvemshop/orders'    , require('./routes/webhooks/nuvemshop/orders'));
app.use('/webhooks/nuvemshop/checkouts' , require('./routes/webhooks/nuvemshop/checkouts'));
// LGPD
app.use('/webhooks/nuvemshop/customers' , require('./routes/webhooks/nuvemshop/customers'));
app.use('/webhooks/nuvemshop/store'     , require('./routes/webhooks/nuvemshop/store'));

// RD STATION

app.use('/webhooks/rdstation/leads'     , require('./routes/webhooks/rdstation/leads'));

// SHOPIFY

app.use('/webhooks/shopify/checkouts'   , require('./routes/webhooks/shopify/checkouts'));
app.use('/webhooks/shopify/customers'   , require('./routes/webhooks/shopify/customers'));
app.use('/webhooks/shopify/orders'      , require('./routes/webhooks/shopify/orders'));
app.use('/webhooks/shopify/shop'        , require('./routes/webhooks/shopify/shop'));

// TRAY

app.use('/webhooks/tray'                , require('./routes/webhooks/tray/notifications'));

/*/
 * SERVER INITIALIZATION
/*/

app.listen(4001, () => {
   console.log('[DEV] Kampana Hub - API | Listening on port 4001...');
});

module.exports = app;