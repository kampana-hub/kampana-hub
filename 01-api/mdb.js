const mongoose = require('mongoose');

//

const rdstation_leads = mongoose.createConnection(
  'mongodb+srv://' + process.env.MDB_USER + ':' + process.env.MDB_PASS + '@' + process.env.MDB_CLUSTER + '/rdstation?retryWrites=true&w=majority',
  {
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true
  }
);

const nuvemshop_checkouts = mongoose.createConnection(
  'mongodb+srv://' + process.env.MDB_USER + ':' + process.env.MDB_PASS + '@' + process.env.MDB_CLUSTER + '/nuvemshop?retryWrites=true&w=majority',
  {
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true
  }
);

//

module.exports = {
  mdb: {
    rdstation: {
      leads: rdstation_leads
    },
    nuvemshop: {
      checkouts: nuvemshop_checkouts
    }
  }
};