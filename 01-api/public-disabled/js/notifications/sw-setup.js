// https://api.kampana.digital/js/notifications/test.html

/*
 * GLOBAL VARIABLES
 */

let swRegistration = null;
let isSubscribed   = false;

const applicationServerPublicKey = '';

/*
 * SERVICE WORKER REGISTRATION
 */

if ('serviceWorker' in navigator && 'PushManager' in window) {
  if (navigator.serviceWorker.controller) {
    console.log('[PWA] ACTIVE SERVICE WORKER FOUND - NO NEED TO REGISTER');
    navigator.serviceWorker.ready.then(function(swReg) {
      swRegistration = swReg;
      checkSubscription();
    });
  }
	//
	else {
    console.log('[PWA] ACTIVE SERVICE WORKER NOT FOUND - NEED TO REGISTER');
    navigator.serviceWorker
      .register('https://api.kampana.digital/js/notifications/sw.js', {
        scope: './'
      })
      .then(function(swReg) {
        console.log('[PWA] SERVICE WORKER REGISTERED');
        swRegistration = swReg;
        checkSubscription();
      });
  }
}
//
else {
  console.log('[PWA] PUSH MESSAGING IS NOT SUPORTED');
}

/*
 * CHECK FOR A VALID SUBSCRIPTION
 */

function checkSubscription() {
  swRegistration.pushManager.getSubscription().then(function(subscription) {
    isSubscribed = (subscription ? true : false);
    if (isSubscribed) {
      console.log('[PWA] USER IS SUBSCRIBED - NEED TO CHECK FOR VALID CREDENTIALS ON SERVER');
      // NOW CHECK IF EVERYTHING IS OK ON THE SERVER SIDE
      // IF NOT, SUBSCRIBE USER SAME WAY
      fetch('/api/push/subscriptions', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        }
      })
      .then(response => response.json())
      .then(function(response) {
        var valid = response.find(x => x.endpoint === subscription.endpoint);
        if (!valid) {
          console.log('[PWA] VALID CREDENTIAL NOT FOUND - NEED TO SUBSCRIBE');
          subscribeUser();
        } else {
          console.log('[PWA] VALID CREDENTIAL FOUND - NO NEED TO SUBSCRIBE');
        }
      });
    }
    //
    else {
      console.log('[PWA] USER IS NOT SUBSCRIBED - NEED TO SUBSCRIBE');
      subscribeUser();
    }
  });
}

/*
 * SUBSCRIBE USER AND SAVE DATA ON SERVER
 */

function subscribeUser() {
  const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
  swRegistration.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: applicationServerKey
  })
  .then(function(subscription) {
    console.log('[PWA] USER LOCALLY SUBSCRIBED - PROCEED TO SERVER SUBSCRIPTION');
    isSubscribed = true;
    fetch('/api/push/subscriptions', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(subscription),
    })
    .then(response => response.json())
    .then(json => console.log('[PWA] USER REMOTELY SUBSCRIBED ON SERVER'));
  })
  .catch(function(error) {
    console.log('[PWA] FAILED TO REMOTELY SUBSCRIBE ON SERVER');
    console.log(error);
  });
}

/* 
 * UNSUBSCRIBE USER
 */

function unsubscribeUser() {
  swRegistration.pushManager.getSubscription().then(function(subscription) {
    if (isSubscribed) {
      subscription.unsubscribe().then(function(successful) {
        console.log('[PWA] USER SUCCESSFULLY UNSUBSCRIBED');
      }).catch(function(error) {
        console.log('[PWA] FAILED TO UNSUBSCRIBE USER');
        console.log(error);
      })
    } else {
      console.log('[PWA] USER IS NOT SUBSCRIBED - NO NEED TO UNSUBSCRIBE');
    }
  });
}

/*
 * JUST AN AUXILIARY FUNCION
 */

function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');
  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);
  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}