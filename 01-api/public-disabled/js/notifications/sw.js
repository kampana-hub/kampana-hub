// PUSH

let url = '';

self.addEventListener('push', function(event) {
  console.log('[Service Worker] Push Received.');
  console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);
  const notification = JSON.parse(event.data.text());
  url = notification.url;
  const notificationPromise = self.registration.showNotification(notification.subject, {
    body: notification.body,
    icon: 'assets/img/icons/notifications/512x512.png',
    badge: 'assets/img/icons/notifications/badge.png'
  });
  event.waitUntil(notificationPromise);
});
self.addEventListener('notificationclick', function(event) {
  console.log('[Service Worker] Notification click Received.');
  event.notification.close();
  event.waitUntil(
    clients.openWindow(url)
  );
});