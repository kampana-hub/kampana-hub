(function() {
  function is_email(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }
  function abandoned_cart() {
    // INFO KAMPANA
    console.log('%c O código de monitoramento do Kampana Hub (abandoned-cart.js) está ativado. 🚀', [
      'font-size: 12px', 
      'font-family: monospace', 
      'background: purple', 
      'display: inline-block', 
      'color: white', 
      'padding: 8px 19px', 
      'border: 1px dashed;' 
    ].join(';'));
    // PREPARAR OBJETO DO CHECKOUT
    let checkout = {
      id:     LS.cart.id,
      status: 'store',
      items:  LS.cart.items
    };
    // USUÁRIO ESTÁ NA LOJA
    if (LS.template) {
      // AVALIAR SE O CLIENTE ESTÁ LOGADO E SE POSSUI ITENS NO CARRINHO
      if (LS.customer && LS.cart.items.length > 0) {
        checkout.customer = {
          id: LS.customer
        };
        // ENVIAR PARA O BACK-END
        $.ajax({
          url: '{{HUB_API_URL}}' + '/webhooks/nuvemshop/checkouts/' + LS.store.id,
          type: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          async: true,
          data: JSON.stringify(checkout),
          success: function(response) {
            console.log(response);
          },
          error: function(response) {
            console.error(response);
          }
        });
      }
    }
    // USUÁRIO ESTÁ NO CHECKOUT
    else {
      // ENCONTRAR PRODUCT_ID
      let lineItems = [];
      try {
        let meta = JSON.parse($('#__NEXT_DATA__').html());
        lineItems = meta.props.initialState.order.order.cart.lineItems;
      } catch(e) { console.log('Não é possível encontrar o product_id dos produtos.') }
      if (lineItems.length > 0) {
        checkout.items.forEach(item => {
          let lineItem = lineItems.find(lineItem => lineItem.variant_id == item.variant_id);
          if (lineItem) item.product_id = lineItem.product_id;
        })
      }
      //
      let path = window.location.pathname.split('/').filter(Boolean);
      if (path[2] == 'start') {
        checkout.url = window.location.href;
        // ASSOCIAR AOS EVENTOS DE MUDANÇA
        $(document).on('change', 'form .form-control', function() {
          checkout.customer = {
            // email: '',
            // name:  '',
            // phone: '',
            // city:  '',
            // state: '',
            country: 'Brasil'
          };
          if ($('input[name="contact.email"]').val()              && $('input[name="contact.email"]').val().length > 0)              checkout.customer.email = $('input[name="contact.email"]').val();
          if ($('input[name="shippingAddress.first_name"]').val() && $('input[name="shippingAddress.first_name"]').val().length > 0) checkout.customer.name  = $('input[name="shippingAddress.first_name"]').val() + ($('input[name="shippingAddress.last_name"]').val() ? ' ' + $('input[name="shippingAddress.last_name"]').val() : '');
          if ($('input[name="shippingAddress.phone"]').val()      && $('input[name="shippingAddress.phone"]').val().length > 0)      checkout.customer.phone = $('input[name="shippingAddress.phone"]').val();
          if ($('input[name="shippingAddress.city"]').val()       && $('input[name="shippingAddress.city"]').val().length > 0)       checkout.customer.city  = $('input[name="shippingAddress.city"]').val();
          if ($('input[name="shippingAddress.state"]').val()      && $('input[name="shippingAddress.state"]').val().length > 0)      checkout.customer.state = $('input[name="shippingAddress.state"]').val();
          // ENVIAR PARA O BACK-END
          if (is_email($('input[name="contact.email"]').val())) {
            $.ajax({
              url: '{{HUB_API_URL}}' + '/webhooks/nuvemshop/checkouts/' +  LS.store.id,
              type: 'POST',
              headers: {
                'Content-Type': 'application/json'
              },
              async: true,
              data: JSON.stringify(checkout),
              success: function(response) {
                console.log(response);
              },
              error: function(response) {
                console.error(response);
              }
            });
          }
        });
        // TRIGGER DO PRIMEIRO CARREGAMENTO
        $(document).ready(function() {
          $('input[name="contact.email"]').trigger('change');
        });
      }
    }
  }
  // PREPARAR SCRIPT
  if (window.jQuery) {
    abandoned_cart();
  }
  else {
    let script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = '//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js';
    script.onload = () => abandoned_cart();
    // CARREGAR SCRIPT
    document.head.appendChild(script);
  }
})();