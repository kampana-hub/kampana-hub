const axios = require('axios');
const express = require('express');
const fs = require('fs');
const JavaScriptObfuscator = require('javascript-obfuscator');
const router = express.Router();

router.get('/js/nuvemshop/abandoned-cart.js', function(req, res, next) {
  fs.readFile(__dirname + '/../scripts/abandoned-cart.js', 'utf8', (err, data) => {
    let script = data.replaceAll('{{HUB_API_URL}}', process.env.HUB_API_URL);
    let obfuscator = JavaScriptObfuscator.obfuscate(script);
    //
    res.setHeader('content-type', 'text/javascript');
    res.setHeader('Cache-Control', 'max-age=5');
    //
    res.write(obfuscator.getObfuscatedCode());
    res.end();
  });
});

// https://api.kampana.digital/js/activecampaign/trakcing-code.js?projeto_id=3

router.get('/js/activecampaign/tracking-code.js', async function(req, res, next) {
  let projeto_id = req.query.projeto_id;
  let projeto = (await axios.get(process.env.API_URL + '/projeto/' + projeto_id)).data.shift();
  projeto.integracoes = JSON.parse(projeto.integracoes) || {};
  if (projeto.integracoes.activecampaign && projeto.integracoes.activecampaign.tracking_code) {
    res.setHeader('content-type', 'text/javascript');
    res.setHeader('Cache-Control', 'max-age=5');
    //
    res.write(projeto.integracoes.activecampaign.tracking_code);
    res.end();
  }
});


module.exports = router;