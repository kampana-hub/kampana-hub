const axios = require('axios');
const express = require('express');
const router = express.Router();

// CONFIGURAR MONGO DB

const { mdb } = require('../../../mdb');
const leadSchema = require('../../../models/rdstation/leads');

router.post('/:id', async (req, res, next) => {
  // SALVAR LEAD
  try {
    let collection = mdb.rdstation.leads.model(req.params.id, leadSchema);
    let lead = req.body.leads.shift();
    await collection.findOneAndUpdate({
      email: lead.email
    }, {
      name: lead.name,
      email: lead.email,
      personal_phone: lead.personal_phone,
      mobile_phone: lead.mobile_phone,
      tags: lead.tags
    }, {
      upsert: true
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'success',
      descricao: 'Lead enviado para o Hub com sucesso.',
      origem: 'api',
      extra: JSON.stringify({
        rdstation: {
          lead_id: lead.email
        }
      }),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'danger',
      descricao: 'Erro ao enviar lead para o Hub.',
      origem: 'api',
      extra: JSON.stringify(req.body),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

module.exports = router;