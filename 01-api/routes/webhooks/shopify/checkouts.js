const axios = require('axios');
const express = require('express');
const router = express.Router();

router.post('/:id', async (req, res, next) => {
  /*/
   *
   * INFORMAÇÕES ENVIADAS PELO SHOPIFY
   *
   * After you configure a webhook subscription, the events that you specified will trigger a webhook notification each time they occur.
   * This notification contains a JSON payload, and HTTP headers that provide context.
   * For example, the orders/create webhook includes the following headers:
   *
   * X-Shopify-Topic: checkouts/create
   * X-Shopify-Hmac-Sha256: XWmrwMey6OsLMeiZKwP4FppHH3cmAiiJJAweH5Jo4bM=
   * X-Shopify-Shop-Domain: johns-apparel.myshopify.com
   * X-Shopify-API-Version: 2021-01
   * X-Shopify-Webhook-Id: b54557e4-bdd9-4b37-8a5f-bf7d70bcd043
   *
   * https://shopify.dev/docs/admin-api/rest/reference/events/webhook
   *
  /*/
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    projeto.integracoes = JSON.parse(projeto.integracoes);
    // MONTAR O OBJETO DO LEAD
    let lead_event = {};
    let lead = {
      name: req.body.customer ? req.body.customer.first_name + ' ' +  req.body.customer.last_name : '',
      personal_phone: req.body.customer ? req.body.customer.phone : '',
      city: req.body.customer ? req.body.customer.default_address.city : '',
      state: req.body.customer ? req.body.customer.default_address.province : '',
      country: req.body.customer ? req.body.customer.default_address.country : '',
      cf_hub_order_count     : req.body.customer.orders_count,
      cf_hub_total_spent     : req.body.customer.total_spent,
      cf_hub_product_01_name : req.body.line_items.length > 0 ? req.body.line_items[0].name  : '',
      cf_hub_product_01_price: req.body.line_items.length > 0 ? req.body.line_items[0].price : '',
      cf_hub_product_02_name : req.body.line_items.length > 1 ? req.body.line_items[1].name  : '',
      cf_hub_product_02_price: req.body.line_items.length > 1 ? req.body.line_items[1].price : '',
      tags: ['shopify', 'kampana-hub', req.headers['x-shopify-topic'].replace('/', '-')],
    };
    // ATUALIZAR O LEAD
    let chamada = (await axios.patch(process.env.RD_BASE_URL + '/platform/contacts/email:' + req.body.email,
      // OBJETO
      lead,
      // AUTENTICAÇÃO
      {
        headers: {
          'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token,
          'Content-Type': 'application/json'
        }
      }
    )).data;
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_shopify', {
      tipo: 'success',
      descricao: 'Lead enviado para o RD Station com sucesso.',
      origem: 'api',
      extra: JSON.stringify({
        shopify: req.body,
        event: req.headers['x-shopify-topic'],
        rdstation: {
          account: projeto.integracoes.rdstation.name,
          lead_id: req.body.email
        }
      }),
      projeto_id: req.params.id
    });
    // MONTAR O OBJETO DA CONVERSÃO
    let event = {
      event_family:'CDP'
    }
    // AVALIAR SE HOUVE CONVERSÃO
    switch (req.headers['x-shopify-topic']) {
      case 'checkouts/create':
        event.event_type = 'CART_ABANDONED';
        //
        lead_event.name = lead.name ? lead.name : '';
        lead_event.email = req.body.customer ? req.body.customer.email : '';
        lead_event.cf_cart_id = req.body.id.toString();
        lead_event.cf_cart_total_items = req.body.line_items ? parseInt(req.body.line_items.length) : 0;
        lead_event.cf_cart_status = req.body.source_name;
        //
        event.payload = lead_event;
        //
        break;
      // case 'checkouts/updated':
    }
    // CRIAR EVENTO DE CONVERSÃO
    if (event.event_type) {
      try {
        let chamada = (await axios.post(process.env.RD_BASE_URL + '/platform/events',
          // OBJETO
          event,
          // AUTENTICAÇÃO
          {
            headers: {
              'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token,
              'Content-Type': 'application/json'
            }
          }
        )).data;
        // CRIAR O LOG
        await axios.post(process.env.API_URL + '/log_shopify', {
          tipo: 'success',
          descricao: 'Evento de conversão enviado para o RD Station com sucesso.',
          origem: 'api',
          extra: JSON.stringify({
            shopify: req.body,
            event: req.headers['x-shopify-topic'],
            rdstation: {
              account: projeto.integracoes.rdstation.name,
              lead_id: lead.email,
              event_type: event.event_type
            }
          }),
          projeto_id: req.params.id
        });
        // ENVIAR RESPOSTA POSITIVA
        res.status(200).send('OK');
      } catch(e) {
        // CRIAR O LOG
        await axios.post(process.env.API_URL + '/log_shopify', {
          tipo: 'danger',
          descricao: 'Erro ao enviar envento de conversão para o RD Station.',
          origem: 'api',
          extra: JSON.stringify({
            shopify: req.body,
            rdstation: {
              account: projeto.integracoes.rdstation.name,
              lead_id: lead.email,
              event_type: event.event_type,
            }
          }),
          projeto_id: req.params.id
        });
        // ENVIAR RESPOSTA NEGATIVA
        res.status(500).send(e);
      }
    }
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_shopify', {
      tipo: 'danger',
      descricao: 'Erro ao preparar o envio dos dados para o RD Station.',
      origem: 'api',
      extra: JSON.stringify(req.body),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

module.exports = router;