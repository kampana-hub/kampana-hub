const axios = require('axios');
const express = require('express');
const router = express.Router();

router.post('/:id', async (req, res, next) => {
  /*/
   *
   * INFORMAÇÕES ENVIADAS PELO SHOPIFY
   *
   * After you configure a webhook subscription, the events that you specified will trigger a webhook notification each time      * they occur.
   * This notification contains a JSON payload, and HTTP headers that provide context.
   * For example, the orders/create webhook includes the following headers:
   *
   * X-Shopify-Topic: customers/create
   * X-Shopify-Hmac-Sha256: XWmrwMey6OsLMeiZKwP4FppHH3cmAiiJJAweH5Jo4bM=
   * X-Shopify-Shop-Domain: johns-apparel.myshopify.com
   * X-Shopify-API-Version: 2021-01
   * X-Shopify-Webhook-Id: b54557e4-bdd9-4b37-8a5f-bf7d70bcd043
   *
   * https://shopify.dev/docs/admin-api/rest/reference/events/webhook
   *
  /*/
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    projeto.integracoes = JSON.parse(projeto.integracoes);
    // MONTAR O OBJETO DO LEAD
    let lead = {
      name: req.body ? req.body.first_name + ' ' +  req.body.last_name : '',
      //email: req.body ? req.body.email : '',
      // bio: '',
      // website: '',
      // job_title: '',
      personal_phone: req.body ? req.body.phone : '',
      // mobile_phone: '',
      city: req.body.default_address ? req.body.default_address.city : '',
      state: req.body.default_address ? req.body.default_address.province : '',
      country: req.body.default_address ? req.body.default_address.country : '',
      tags: ['shopify', 'kampana-hub', req.headers['x-shopify-topic'].replace('/', '-'), 'novo-cadastro'],
      // twitter: '',
      // facebook: '',
      // linkedin: '',
      // extra_emails: []
    };
    // CRIAR/ATUALIZAR LEAD
    try {
      let chamada = (await axios.patch(process.env.RD_BASE_URL + '/platform/contacts/email:' + req.body.email,
        // OBJETO
        lead,
        // AUTENTICAÇÃO
        {
          headers: {
            'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token,
            'Content-Type': 'application/json'
          }
        }
      )).data;
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_shopify', {
        tipo: 'success',
        descricao: 'Lead enviado para o RD Station com sucesso.',
        origem: 'api',
        extra: JSON.stringify({
          shopify: req.body,
          event: req.headers['x-shopify-topic'],
          rdstation: {
            account: projeto.integracoes.rdstation.name,
            lead_id: req.body.email
          }
        }),
        projeto_id: req.params.id
      });
      // ENVIAR RESPOSTA POSITIVA
      res.status(200).send('OK');
    } catch(e) {
      console.log(e.response.data);
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_shopify', {
        tipo: 'danger',
        descricao: 'Erro ao enviar lead para o RD Station.',
        origem: 'api',
        extra: JSON.stringify({
          shopify: req.body,
          rdstation: {
            account: projeto.integracoes.rdstation.name,
            lead_id: req.body.email
          }
        }),
        projeto_id: req.params.id
      });
      // ENVIAR RESPOSTA NEGATIVA
      res.status(500).send(e);
    }
  } catch (e) {
    console.log(e.response.data);
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_shopify', {
      tipo: 'danger',
      descricao: 'Erro ao preparar o envio dos dados para o RD Station.',
      origem: 'api',
      extra: JSON.stringify(req.body),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

module.exports = router;