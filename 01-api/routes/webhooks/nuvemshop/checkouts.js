const axios   = require('axios');
const express = require('express');
const router  = express.Router();

// CONFIGURAR MONGO DB

const { mdb } = require('../../../mdb');
const checkoutSchema = require('../../../models/nuvemshop/checkouts');

//

router.post('/:id', async (req, res, next) => {
  try {
    // IDENTIFICAR OS DADOS DA REQUISIÇÃO
    let checkout = req.body;
    let user_id  = req.params.id;
    // CARREGAR OS DADOS NECESSÁRIOS
    let nuvemshop = (await axios.get(process.env.API_URL + '/v_nuvemshop?_where=(user_id,eq,' + user_id + ')')).data.shift();
    let projeto   = (await axios.get(process.env.API_URL + '/projeto/' + nuvemshop.projeto_id)).data.shift();
    // MANIPULAR O OBJETO RECEBIDO
    checkout.user_id = user_id;
    checkout.projeto_id = nuvemshop.projeto_id;
    // ENVIAR CARRINHO ABANDONADO PARA O HUB
    let collection = mdb.nuvemshop.checkouts.model('checkout', checkoutSchema);
    await collection.findOneAndUpdate({id: checkout.id}, checkout, {upsert: true, setDefaultsOnInsert: true});
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_nuvemshop', {
      tipo: 'success',
      descricao: 'Carrinho abandonado enviado para o Hub com sucesso.',
      origem: 'api',
      extra: JSON.stringify({
        nuvemshop: req.body,
        /*rdstation: {
          account: JSON.parse(projeto.integracoes).rdstation.name,
          lead_id: checkout.customer ? checkout.customer.email : null
        }*/
      }),
      projeto_id: nuvemshop.projeto_id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Carrinho abandonado enviado para o Hub com sucesso.');
  }
  //
  catch (e) {
    console.log(e);
    // IDENTIFICAR OS DADOS DA REQUISIÇÃO
    let user_id  = req.params.id;
    // CARREGAR OS DADOS NECESSÁRIOS
    let nuvemshop = (await axios.get(process.env.API_URL + '/v_nuvemshop?_where=(user_id,eq,' + user_id + ')')).data.shift();
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_nuvemshop', {
      tipo: 'danger',
      descricao: 'Erro ao enviar carrinho abandonado para o Hub.',
      origem: 'api',
      extra: JSON.stringify(req.body),
      projeto_id: nuvemshop.projeto_id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send('Erro ao enviar carrinho abandonado para o Hub.');
  }
});

module.exports = router;