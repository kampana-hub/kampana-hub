const axios = require('axios');
const express = require('express');
const router = express.Router();

router.post('/redact', async (req, res, next) => {
  res.status(200).send('OK');
});

router.post('/data_request', async (req, res, next) => {
  res.status(200).send('OK');
});

module.exports = router;