const ActiveCampaign = require('activecampaign');
const axios          = require('axios');
const express        = require('express');
const router         = express.Router();

router.post('/:id', async (req, res, next) => {
  console.log(req.body);
  /*/
   *
   * INFORMAÇÕES ENVIADAS PELO NUVEMSHOP
   *
   * let store_id = req.body.store_id;
   * let event    = req.body.event;
   * let order_id = req.body.id;
   *
  /*/
  let tag;
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.params.id)).data.shift();
    // CARREGAR APP - NUVEMSHOP
    let app = (await axios.get(process.env.API_URL + '/app_nuvemshop/' + projeto.app_nuvemshop_id)).data.shift();
    projeto.integracoes = JSON.parse(projeto.integracoes);
    // CONSULTAR PEDIDO NA API DO NUVEMSHOP
    let order = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/orders/' + req.body.id, {
      headers: {
        'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
        'User-Agent': app.user_agent
      }
    })).data;
    //
    let chamadas = [];
    // EXISTE ESSA INTEGRAÇÃO CONFIGUADA HOJE?
    if (projeto.integracoes.nuvemshop.webhooks && projeto.integracoes.nuvemshop.webhooks[req.body.event.replace('/', '_')]) {
      // POSSUI A TAG "rdstation"?
      if (projeto.integracoes.nuvemshop.webhooks[req.body.event.replace('/', '_')].tags.includes('rdstation')) chamadas.push(new Promise(async (resolve, reject) => {
        tag = 'RD Station';
        // MONTAR O OBJETO DO LEAD
        let lead = {
          name: order.customer.name,
          // email: order.customer.email,
          // bio: '',
          // website: '',
          // job_title: '',
          personal_phone: order.customer.phone,
          // mobile_phone: '',
          city: order.customer.default_address.city,
          state: order.customer.default_address.province,
          country: order.customer.default_address.country,
          // twitter: '',
          // facebook: '',
          // linkedin: '',
          tags: ['nuvemshop', 'kampana-hub', req.body.event.replace('/', '-')],
          // extra_emails: []
          // cf_hub_order_count: '',
          cf_hub_total_spent: (order.customer.total_spent).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }),
          cf_hub_product_01_name : order.products.length > 0 ? order.products[0].name  : '',
          cf_hub_product_01_price: order.products.length > 0 ? (order.products[0].price).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) : '',
          cf_hub_product_02_name : order.products.length > 1 ? order.products[1].name  : '',
          cf_hub_product_02_price: order.products.length > 1 ? (order.products[1].price).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })  : ''
        };
        // ATUALIZAR LEAD NO RD STATION
        try {
          let chamada = (await axios.patch(process.env.RD_BASE_URL + '/platform/contacts/email:' + order.customer.email,
            // OBJETO
            lead,
            // AUTENTICAÇÃO
            {
              headers: {
                'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token,
                'Content-Type': 'application/json'
              }
            }
          )).data;
          // CRIAR O LOG
          await axios.post(process.env.API_URL + '/log_nuvemshop', {
            tipo: 'success',
            descricao: 'Lead enviado para o RD Station com sucesso.',
            origem: 'api',
            extra: JSON.stringify({
              nuvemshop: req.body,
              rdstation: {
                account: projeto.integracoes.rdstation.name,
                lead_id: order.customer.email
              }
            }),
            projeto_id: req.params.id
          });
        }
        // ERRO NA ATUALIZAÇÃO DO LEAD
        catch(e) {
          console.log(e);
          // CRIAR O LOG
          await axios.post(process.env.API_URL + '/log_nuvemshop', {
            tipo: 'danger',
            descricao: 'Erro ao enviar lead para o RD Station.',
            origem: 'api',
            extra: JSON.stringify({
              nuvemshop: req.body,
              rdstation: {
                account: projeto.integracoes.rdstation.name,
                lead_id: order.customer.email
              }
            }),
            projeto_id: req.params.id
          });
        }
        // MONTAR O OBJETO DOS EVENTOS
        let events = [];
        let event_label = '';
        switch (req.body.event) {
          case 'order/created':
            event_label = 'Pedido Realizado';
            let payment_method = [{key: 'credit_card', value: 'Credit Card'}].find(method => method.key == order.payment_details.method);
            //
            if (payment_method) order.payment_details.method = payment_method.value;
            else order.payment_details.method = 'Others'
            //
            events.unshift({
              event_type: 'ORDER_PLACED',
              event_family: 'CDP',
              //
              payload: {
                name: order.customer.name,
                email: order.customer.email,
                cf_order_id: order.id.toString(),
                cf_order_total_items: order.products.length,
                cf_order_status: order.payment_status,
                cf_order_payment_method: order.payment_details.method,
                cf_order_payment_amount: parseFloat(order.total)
              }
            });
            //
            break;
          case 'order/paid':
            event_label = 'Pedido Pago';
            events.unshift({
              event_type: 'SALE',
              event_family: 'CDP',
              //
              payload: {
                email: order.customer.email,
                funnel_name: 'default',
                value: parseFloat(order.total)
              }
            });
            break;
          // case 'order/packed':
          // case 'order/fulfilled':
          // case 'order/cancelled':
          // case 'order/updated':
        }
        // AVALIAR O ENVIO DE EVENTOS
        if (events.length > 0) {
          // INSERIR OPORTUNIDADE NO OBJETO DE EVENTOS
          events.unshift({
            event_family:'CDP',
            event_type: 'OPPORTUNITY',
            payload: {
              email: order.customer.email,
              funnel_name: 'default'
            }
          });
          // ENVIAR EVENTOS
          try {
            let chamada = (await axios.post(process.env.RD_BASE_URL + '/platform/events/batch',
              // OBJETO
              events,
              // AUTENTICAÇÃO
              {
                headers: {
                  'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token,
                  'Content-Type': 'application/json'
                }
              }
            )).data;
            // CRIAR O LOG
            await axios.post(process.env.API_URL + '/log_nuvemshop', {
              tipo: 'success',
              descricao: 'Evento (' + event_label + ') enviado para o RD Station com sucesso.',
              origem: 'api',
              extra: JSON.stringify({
                nuvemshop: req.body,
                rdstation: {
                  account: projeto.integracoes.rdstation.name,
                  lead_id: lead.email,
                  event_type: req.body.event
                }
              }),
              projeto_id: req.params.id
            });
          }
          // ERRO NO ENVIO DOS EVENTOS
          catch(e) {
            console.log(e);
            // CRIAR O LOG
            await axios.post(process.env.API_URL + '/log_nuvemshop', {
              tipo: 'danger',
              descricao: 'Erro ao enviar evento (' + event_label + ') para o RD Station.',
              origem: 'api',
              extra: JSON.stringify({
                nuvemshop: req.body,
                rdstation: {
                  account: projeto.integracoes.rdstation.name,
                  lead_id: lead.email,
                  event_type: req.body.event
                }
              }),
              projeto_id: req.params.id
            });
          }
        }
        // 
        resolve();
      }));
      // POSSUI A TAG "activecampaign"?
      if (projeto.integracoes.nuvemshop.webhooks[req.body.event.replace('/', '_')].tags.includes('activecampaign')) chamadas.push(new Promise(async (resolve, reject) => {
        tag = 'ActiveCampaign';
        // https://developers.activecampaign.com/reference#create-order
        // https://help.activecampaign.com/hc/pt-br/articles/360001045964-Vis%C3%A3o-Geral-de-Deep-Data-de-Carrinhos-Abandonados#abandoned-cart-email-content-block
        // MONTAR O OBJETO DO CONTACT
        let contactdata = JSON.stringify({
          contact: {
            email: order.customer.email,
            firstName: order.customer.name,
            lastName: "",
            phone: order.customer.phone,
            fieldValues: [
              {
                field: parseInt(projeto.integracoes.activecampaign.custom_fields.cf_hub_total_spent),
                value: order.customer.total_spent ? parseInt(order.customer.total_spent.replace('.', '')) : ''
              },
              {
                field: parseInt(projeto.integracoes.activecampaign.custom_fields.cf_hub_product_01_name),
                value: order.products.length > 0 ? order.products[0].name : ''
              },
              {
                field: parseInt(projeto.integracoes.activecampaign.custom_fields.cf_hub_product_01_price),
                value: order.products.length > 0 ? parseInt(order.products[0].price.replace('.', '')) : ''
              },
              {
                field: parseInt(projeto.integracoes.activecampaign.custom_fields.cf_hub_product_02_name),
                value: order.products.length > 1 ? order.products[1].name : ''
              },
              { 
                field: parseInt(projeto.integracoes.activecampaign.custom_fields.cf_hub_product_02_price),
                value: order.products.length > 1 ? parseInt(order.products[1].price.replace('.', '')) : ''
              }
            ]
          }
        });
        // ATUALIZAR CONTACT NA ACTIVECAMPAIGN
        let contact_response;
        try {
          contact_response = (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/contact/sync',
            // OBJETO
            contactdata,
            // AUTENTICAÇÃO
            {
              headers: {
                'Api-Token': projeto.integracoes.activecampaign.key
              }
            }
          )).data.contact.id;
          // CRIAR O LOG
          await axios.post(process.env.API_URL + '/log_nuvemshop', {
            tipo: 'success',
            descricao: 'Contact enviado para a ActiveCampaign com sucesso.',
            origem: 'api',
            extra: JSON.stringify({
              nuvemshop: req.body,
              activecampaign: {
                account: projeto.integracoes.activecampaign.name,
                contact_id: order.customer.email
              }
            }),
            projeto_id: req.params.id
          });
        } 
        // ERRO NA ATUALIZAÇÃO DO LEAD
        catch(e) {
          console.log(e);
          // CRIAR O LOG
          await axios.post(process.env.API_URL + '/log_nuvemshop', {
            tipo: 'success',
            descricao: 'Erro ao enviar o contact para a ActiveCampaign.',
            origem: 'api',
            extra: JSON.stringify({
              nuvemshop: req.body,
              activecampaign: {
                account: projeto.integracoes.activecampaign.name,
                contact_id: order.customer.email
              }
            }),
            projeto_id: req.params.id
          });
        }
        // MONTAR O OBJETO DOS EVENTOS
        let events = {};
        let event_label = '';
        let custormer = {};
        let contacttag = {};
        // MONTAR VETOR DOS PRODUTOS
        let order_products = [];
        order_products = order.products.map(item => {
          return {
            externalid: item.product_id,
            name: item.name,
            price: (parseInt(item.price.replace('.', ''))),
            quantity: parseInt(item.quantity),
            //category: '',
            //sku: item.sku ? item.sku : '',
            //description: '',
            imageUrl: item.image.src,
            //productUrl: ''
          }
        });
        let contact_tags;
        switch (req.body.event) {
          case 'order/created':
            event_label = 'Pedido Realizado';
            // MONTAR OBJETO PARA ATUALIZAR TAG DO CONTACT
            contacttag = JSON.stringify({
              contactTag: {
                contact: contact_response,
                tag: parseInt(projeto.integracoes.activecampaign.tags.pedido_criado),
              }
            });
            // CHAMADA PARA ATUALIZAR TAG DO CONTACT
            try {
              (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/contactTags',
                // OBJETO
                contacttag,
                // AUTENTICAÇÃO
                {
                  headers: {
                    'Api-Token': projeto.integracoes.activecampaign.key
                  }
                }
              ));
            } catch(e) { console.log(e.response.data); }
            // CHAMADA PARA BUSCAR AS TAGS DO CONTACT
            contact_tags = (await axios.get(projeto.integracoes.activecampaign.url + '/api/3/contacts/' + contact_response + '/contactTags',
              // AUTENTICAÇÃO
              {
                headers: {
                  'Api-Token': projeto.integracoes.activecampaign.key
                }
              }
            )).data.contactTags;
            //
            if (contact_tags.length > 0) {
              contact_tags.forEach(async item => {
                if (item.tag == projeto.integracoes.activecampaign.tags.carrinho_abandonado) {
                  // CHAMADA PARA REMOVER TAG DE 'carrinho_abandonado' CASO EXISTA
                  try {
                    (await axios.delete(projeto.integracoes.activecampaign.url + '/api/3/contactTags/' + item.id,
                      // AUTENTICAÇÃO
                      {
                        headers: {
                          'Api-Token': projeto.integracoes.activecampaign.key
                        }
                      }
                    ));
                  } catch(e) { console.log(e.response.data); }
                }
              });
            }
            // MONTAR OBJETO DO CUSTOMER ASSOCIADO A ORDER
            custormer = JSON.stringify({
              ecomCustomer: {
                connectionid: parseInt(projeto.integracoes.activecampaign.connection.id),
                externalid: parseInt(order.customer.id),
                email: order.customer.email,
                acceptsMarketing: "1",
                totalRevenue: parseInt(order.customer.total_spent.replace('.', '')),
              }
            });
            // CHAMADA PARA CRIAR CUSTOMER
            let customer_response;
            try {
              // VERIFICAR SE JÁ EXISTE UM CUSTOMER ASSOCIADO A ESSE EMAIL
              let customer_id = (await axios.get(projeto.integracoes.activecampaign.url + '/api/3/ecomCustomers?filters[email]=' + order.customer.email,
                // AUTENTICAÇÃO
                {
                  headers: {
                    'Api-Token': projeto.integracoes.activecampaign.key
                  }
                }
              )).data.ecomCustomers;
              console.log(customer_id);
              if (customer_id.length > 0) {
                customer_id.forEach((item) => {
                  if (item.connection) customer_response = item.id;
                }); 
              }
              else {
                customer_response = (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/ecomCustomers',
                  // OBJETO
                  custormer,
                  // AUTENTICAÇÃO
                  {
                    headers: {
                      'Api-Token': projeto.integracoes.activecampaign.key
                    }
                  }
                )).data.ecomCustomer.id;
              }
              console.log(customer_response);
            } catch(e) { console.log(e.response.data); }
            // MONTAR OBJETO DA ORDER
            events = JSON.stringify({
              ecomOrder: {
                externalid: order.id,
                source: "1",
                email: order.customer.email,
                orderProducts: order_products,
                //orderUrl: "https://example.com/orders/3246315233",
                externalCreatedDate: order.created_at,
                externalUpdatedDate: order.updated_at,
                //shippingMethod: "UPS Ground",
                totalPrice: parseInt(order.total.replace('.', '')),
                shippingAmount: parseInt(order.shipping_cost_customer.replace('.', '')),
                discountAmount: parseInt(order.discount.replace('.', '')),
                currency: "BRL",//order.currency,
                orderNumber: order.number,
                connectionid: parseInt(projeto.integracoes.activecampaign.connection.id),
                customerid: parseInt(customer_response)
              }
            });
            // AVALIAR O ENVIO DE EVENTOS
            if (events != '{}') {
              // ENVIAR EVENTOS
              try {
                let chamada = (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/ecomOrders',
                  // OBJETO
                  events,
                  // AUTENTICAÇÃO
                  {
                    headers: {
                      'Api-Token': projeto.integracoes.activecampaign.key
                    }
                  }
                )).data;
                // CRIAR O LOG
                await axios.post(process.env.API_URL + '/log_nuvemshop', {
                  tipo: 'success',
                  descricao: 'Evento (' + event_label + ') enviado para a ActiveCampaign com sucesso.',
                  origem: 'api',
                  extra: JSON.stringify({
                    nuvemshop: req.body,
                    activecampaign: {
                      account: projeto.integracoes.activecampaign.name,
                      contact_id: order.customer.email,
                      event_type: req.body.event
                    }
                  }),
                  projeto_id: req.params.id
                });
              }
              // ERRO NO ENVIO DOS EVENTOS
              catch(e) {
                console.log(e.response.data);
                // CRIAR O LOG
                await axios.post(process.env.API_URL + '/log_nuvemshop', {
                  tipo: 'danger',
                  descricao: 'Erro ao enviar evento (' + event_label + ') para a ActiveCampaign.',
                  origem: 'api',
                  extra: JSON.stringify({
                    nuvemshop: req.body,
                    activecampaign: {
                      account: projeto.integracoes.activecampaign.name,
                      contact_id: order.customer.email,
                      event_type: req.body.event
                    }
                  }),
                  projeto_id: req.params.id
                });
              }
            }
            //
            break;
          case 'order/paid':
            event_label = 'Pedido Pago';
            // CHAMADA PARA BUSCAR AS TAGS DO CONTACT
            contact_tags = (await axios.get(projeto.integracoes.activecampaign.url + '/api/3/contacts/' + contact_response + '/contactTags',
              // AUTENTICAÇÃO
              {
                headers: {
                  'Api-Token': projeto.integracoes.activecampaign.key
                }
              }
            )).data.contactTags;
            //
            if (contact_tags.length > 0) {
              contact_tags.forEach(async item => {
                if (item.tag == projeto.integracoes.activecampaign.tags.pedido_criado) {
                  // CHAMADA PARA REMOVER TAG DE 'pedido_criado' CASO EXISTA
                  try {
                    (await axios.delete(projeto.integracoes.activecampaign.url + '/api/3/contactTags/' + item.id,
                      // AUTENTICAÇÃO
                      {
                        headers: {
                          'Api-Token': projeto.integracoes.activecampaign.key
                        }
                      }
                    ));
                  } catch(e) { console.log(e.response.data); }
                }
              });
            }
            // MONTAR OBJETO PARA ATUALIZAR TAG DO CONTACT
            contacttag = JSON.stringify({
              contactTag: {
                contact: contact_response,
                tag: parseInt(projeto.integracoes.activecampaign.tags.pedido_pago),
              }
            });
            // CHAMADA PARA ATUALIZAR TAG DO CONTACT
            try {
              (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/contactTags',
                // OBJETO
                contacttag,
                // AUTENTICAÇÃO
                {
                  headers: {
                    'Api-Token': projeto.integracoes.activecampaign.key
                  }
                }
              ));
            } catch(e) { console.log(e.response.data); }
            break;
          // case 'order/packed':
          // case 'order/fulfilled':
          // case 'order/cancelled':
          // case 'order/updated':
        }
        resolve();
      }));
      // POSSUI A TAG "whatsapp"?
      if (projeto.integracoes.nuvemshop.webhooks[req.body.event.replace('/', '_')].tags.includes('whatsapp')) chamadas.push(new Promise(async (resolve, reject) => {
        tag = 'Whatsapp';
        // BUSCAR INFORMAÇÕES DA LOJA
        let loja = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/store', {
          headers: {
            'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
            'User-Agent': app.user_agent
          }
        })).data;
        // MONTAR O OBJETO DOS EVENTOS
        let event = {};
        let event_label = '';
        switch (req.body.event) {
          case 'order/created':
            event_label = 'Pedido Realizado';
            // MONTAR OBJETO
            event.telefone = order.customer.phone ? order.customer.phone : order.billing_phone;
            event.mensagem = 'Olá, *' + order.customer.name + '*!\n\n' +
              'Aqui é da _' + projeto.nome + '_, e estou passando pra dizer que recebemos seu pedido! ❤️\n\n' +
              'Assim que você realizar o pagamento, iremos providenciar o envio. 😉\n\n' + 
              'Clique no link abaixo para acompanhar seu pedido e finalizar o pagamento!\n\n' + loja.url_with_protocol + '/account/orders/' + order.id;
            //
            break;
          case 'order/paid':
            event_label = 'Pedido Pago';
            // MONTAR OBJETO
            event.telefone = order.customer.phone ? order.customer.phone : order.billing_phone;
            event.mensagem = 'Olá, *' + order.customer.name + '*! O pagamento do seu pedido *#' + order.number + '* acabou de ser aprovado. ✅\n\n' +
              'Em breve voltarei para te contar mais sobre a entrega. 🚚\n\n' +
              'Ah, e você pode acompanhar o status do seu pedido pelo nosso site, ou acessando o link abaixo! 📲\n\n' + 
              loja.url_with_protocol + '/account/orders/' + order.id + '\n\n' +
              'Até logo! 😉';
            break;
          // case 'order/packed':
          case 'order/fulfilled':
            event_label = 'Pedido Enviado';
            // MONTAR OBJETO
            event.telefone = order.customer.phone ? order.customer.phone : order.billing_phone;
            event.mensagem = 'Olá, *' + order.customer.name + '*! O seu pedido *#' + order.number + '* acabou de ser enviado. 🚚\n\n' +
              'Você pode acompanhar o status do envio pelo nosso site, ou acessando o link abaixo! 📲\n\n' +
              loja.url_with_protocol + '/account/orders/' + order.id + '\n\n' +
              'Até logo! 😉';
          // case 'order/cancelled':
          // case 'order/updated':
        }
        // ENVIAR EVENTOS
        console.log(event);
        try {
          // CHAMADA PARA PRODUÇÃO - ROTA /sendText | CHAMADA PARA TESTE - ROTA /sendTextDemo
          let chamada = (await axios.post(process.env.WPP_BASE_URL + '/' + projeto.integracoes.whatsapp.sender_id + '/sendTextDemo',
            // OBJETO
            event,
            // AUTENTICAÇÃO
            {
              headers: {
                'Authorization': process.env.WPP_API_PREFIX + ' ' + process.env.WPP_API_KEY,
                'Content-Type': 'application/json'
              }
            }
          )).data;
          // CRIAR O LOG
          await axios.post(process.env.API_URL + '/log_nuvemshop', {
            tipo: 'success',
            descricao: 'Evento (' + event_label + ') enviado para o Whatsapp com sucesso.',
            origem: 'api',
            extra: JSON.stringify({
              nuvemshop: req.body,
              whatsapp: {
                sender_id: projeto.integracoes.whatsapp.sender_id,
                whatsapp: order.customer.phone ? order.customer.phone : order.billing_phone,
                event_type: req.body.event
              }
            }),
            projeto_id: req.params.id
          });
        }
        // ERRO NO ENVIO DOS EVENTOS
        catch(e) {
          console.log(e.response);
          // CRIAR O LOG
          await axios.post(process.env.API_URL + '/log_nuvemshop', {
            tipo: 'danger',
            descricao: 'Erro ao enviar evento (' + event_label + ') para o Whatsapp. O número ' + order.customer.phone + ' não é Whatsapp.',
            origem: 'api',
            extra: JSON.stringify({
              nuvemshop: req.body,
              whatsapp: {
                sender_id: projeto.integracoes.whatsapp.sender_id,
                whatsapp: order.customer.phone ? order.customer.phone : order.billing_phone,
                event_type: req.body.event
              }
            }),
            projeto_id: req.params.id
          });
        }
        // 
        resolve();
      }));      
      // POSSUI A TAG "sms"?
      if (projeto.integracoes.nuvemshop.webhooks[req.body.event.replace('/', '_')].tags.includes('sms')) chamadas.push(new Promise(async (resolve, reject) => {
        tag = 'Envio de SMS';
        // BUSCAR INFORMAÇÕES DA LOJA
        let loja = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/store', {
          headers: {
            'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
            'User-Agent': app.user_agent
          }
        })).data;
        // MONTAR O OBJETO DOS EVENTOS
        let event = {};
        let event_label = '';
        switch (req.body.event) {
          case 'order/created':
            event_label = 'Pedido Realizado';
            // MONTAR OBJETO
            //event.numberPhone = order.customer.phone ? order.customer.phone : order.billing_phone;
            event.numberPhone = '62981079144';
            event.message = 'Olá, ' + order.customer.name + '! Aqui é da ' + projeto.nome + ', e estou passando pra dizer que recebemos seu pedido!';
              /*
              'Assim que você realizar o pagamento, iremos providenciar o envio. 😉\n\n' + 
              'Clique no link abaixo para acompanhar seu pedido e finalizar o pagamento!\n\n' + loja.url_with_protocol + '/account/orders/' + order.id;
              */
            event.flashSms = false;
            //
            break;
          case 'order/paid':
            event_label = 'Pedido Pago';
            // MONTAR OBJETO
            //event.numberPhone = order.customer.phone ? order.customer.phone : order.billing_phone;
            event.numberPhone = '62981079144';
            event.message = 'Olá, ' + order.customer.name + '! O pagamento do seu pedido #' + order.number + ' acabou de ser aprovado. Em breve voltarei para te contar mais sobre a entrega.';
              /*
              'Ah, e você pode acompanhar o status do seu pedido pelo nosso site, ou acessando o link abaixo! 📲\n\n' + 
              loja.url_with_protocol + '/account/orders/' + order.id + '\n\n' +
              'Até logo! 😉';
              */
            event.flashSms = false;
            //
            break;
          // case 'order/packed':
          case 'order/fulfilled':
            event_label = 'Pedido Enviado';
            // MONTAR OBJETO
            //event.numberPhone = order.customer.phone ? order.customer.phone : order.billing_phone;
            event.numberPhone = '62981079144';
            event.message = 'Olá, ' + order.customer.name + '! O seu pedido #' + order.number + ' acabou de ser enviado.';
              /*
              'Você pode acompanhar o status do envio pelo nosso site, ou acessando o link abaixo! 📲\n\n' +
              loja.url_with_protocol + '/account/orders/' + order.id + '\n\n' +
              'Até logo! 😉';
              */
            event.flashSms = false;
            //
          // case 'order/cancelled':
          // case 'order/updated':
        }
        // ENVIAR EVENTOS
        try {
          // CHAMADA
          let chamada = (await axios.post(process.env.N_BASE_URL + '/sms?napikey=' + process.env.N_API_KEY,
            // OBJETO
            event,
            // AUTENTICAÇÃO
            {
              headers: {
                '-': 'Authorization: Bearer ' + process.env.N_API_TOKEN,
                'Content-Type': 'application/json'
              }
            }
          )).data;
          // CRIAR O LOG
          await axios.post(process.env.API_URL + '/log_nuvemshop', {
            tipo: 'success',
            descricao: 'Evento (' + event_label + ') enviado por SMS com sucesso.',
            origem: 'api',
            extra: JSON.stringify({
              nuvemshop: req.body,
              sms: {
                sms_number: order.customer.phone ? order.customer.phone : order.billing_phone,
                event_type: req.body.event
              }
            }),
            projeto_id: req.params.id
          });
        }
        // ERRO NO ENVIO DOS EVENTOS
        catch(e) {
          console.log(e.response);
          // CRIAR O LOG
          await axios.post(process.env.API_URL + '/log_nuvemshop', {
            tipo: 'danger',
            descricao: 'Erro ao enviar evento (' + event_label + ') por SMS.',
            origem: 'api',
            extra: JSON.stringify({
              nuvemshop: req.body,
              sms: {
                sms_number: order.customer.phone ? order.customer.phone : order.billing_phone,
                event_type: req.body.event
              }
            }),
            projeto_id: req.params.id
          });
        }
        // 
        resolve();
      }));
    }
    // REALIZAR CHAMADAS
    Promise.all(chamadas);
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  }
  //
  catch(e) {
    console.log(e);
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_nuvemshop', {
      tipo: 'danger',
      descricao: 'Erro ao preparar o envio dos dados para o ' + tag + '.',
      origem: 'api',
      extra: JSON.stringify(req.body),
      projeto_id: req.params.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// order.status == 'open'
// order.status == 'closed'
// order.status == 'cancelled'

// order.payment_status == 'pending'
// order.payment_status == 'authorized'
// order.payment_status == 'paid'
// order.payment_status == 'voided'
// order.payment_status == 'refunded'
// order.payment_status == 'abandoned'

// order.gateway == 'offline'
// order.gateway == 'mercadopago'
// order.gateway == 'pagseguro'
// order.gateway == 'cielo'
// order.gateway == 'moip'
// order.gateway == 'boleto_paghiper'
// order.gateway == 'payu'
// order.gateway == 'todopago'
// order.gateway == 'not-provided'

// order.shipping_type == 'ship'
// order.shipping_type == 'pickup'

// order.shipping_method == 'branch'
// order.shipping_method == 'correios'
// order.shipping_method == 'correo-argentino'
// order.shipping_method == 'oca-partner-ar'
// order.shipping_method == 'table'
// order.shipping_method == 'not-provided'    

// order.payment_details.method == 'credit_card' -> 'Cartão de Crédito'
// order.payment_details.method == 'offline'     -> 'Boleto?'

module.exports = router;