const axios = require('axios');
const express = require('express');
const router = express.Router();

router.post('/:id', async (req, res, next) => {
  /*/
   *
	 * INFORMAÇÕES ENVIADAS PELO TRAY (https://developers.tray.com.br)
   *
	 * scope_name - Nome do escopo notificado.
	 * scope_id   - Código do escopo da notificação.
	 * seller_id  - Código do vendedor.
	 * act        - Ação realizada.
   *
  /*/
  // CARREGAR OBJETO - INTEGRAÇÃO
  let integracao = (await axios.get(process.env.API_URL + '/v_tray')).data.find(item => item.store_id == req.body.seller_id);
  if (integracao) {
    // CARREGAR OBJETO - PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + integracao.projeto_id)).data.shift();
    projeto.integracoes = JSON.parse(projeto.integracoes);
    //
    let order;
    try {
      if (req.body.scope_name == 'order' && req.body.act == 'update') {
        // CARREGAR OBJETO - ORDER
        order = (await axios.get(integracao.api_host + '/orders/' + req.body.scope_id + '/complete?access_token=' + integracao.access_token)).data.Order;
        if (order.status == 'AGUARDANDO PAGAMENTO' || order.status == 'AGUARDANDO YAPAY' || order.status == 'A ENVIAR') {
          // console.log(order);
          // MONTAR O OBJETO DO LEAD
          let lead_event = {};
          let lead = {
            name: order.Customer ? order.Customer.name : '',
            personal_phone: order.Customer ? order.Customer.cellphone : '',
            city: order.Customer ? order.Customer.city : '',
            state: order.Customer ? order.Customer.state : '',
            country: order.Customer ? order.Customer.country : '',
            cf_hub_order_count: order.Customer ? parseInt(order.Customer.total_orders) : '',
            //cf_hub_total_spent       : order.Customer.total_spent,
            cf_hub_product_01_name: order.ProductsSold.length > 0 ? order.ProductsSold[0].ProductsSold.original_name : '',
            cf_hub_product_01_price: order.ProductsSold.length > 0 ? order.ProductsSold[0].ProductsSold.price : '',
            cf_hub_product_01_image_url: order.ProductsSold.length > 0 ? order.ProductsSold[0].ProductsSold.ProductSoldImage.length > 0 ? order.ProductsSold[0].ProductsSold.ProductSoldImage[0].https : '' : '',
            cf_hub_product_02_name: order.ProductsSold.length > 1 ? order.ProductsSold[1].ProductsSold.original_name : '',
            cf_hub_product_02_price: order.ProductsSold.length > 1 ? order.ProductsSold[1].ProductsSold.price : '',
            cf_hub_product_02_image_url: order.ProductsSold.length > 1 ? order.ProductsSold[1].ProductsSold.ProductSoldImage.length > 0 ? order.ProductsSold[0].ProductsSold.ProductSoldImage[0].https : '' : '',
            tags: ['tray', 'kampana-hub', order.status == 'A ENVIAR' ? 'order-paid' : 'order-create'],
          };
          // ATUALIZAR O LEAD
          let chamada = (await axios.patch(process.env.RD_BASE_URL + '/platform/contacts/email:' + order.Customer.email,
            // OBJETO
            lead,
            // AUTENTICAÇÃO
            {
              headers: {
                'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token,
                'Content-Type': 'application/json'
              }
            }
          )).data;
          // CRIAR O LOG
          await axios.post(process.env.API_URL + '/log_tray', {
            tipo: 'success',
            descricao: 'Lead enviado para o RD Station com sucesso.',
            origem: 'api',
            extra: JSON.stringify({
              tray: order,
              event: req.body.scope_name + '/' + req.body.act,
              rdstation: {
                account: projeto.integracoes.rdstation.name,
                lead_id: order.Customer.email ? order.Customer.email : ''
              }
            }),
            projeto_id: integracao.projeto_id
          });
          // MONTAR O OBJETO DA CONVERSÃO
          let event = {
            event_family: 'CDP'
          }
          // AVALIAR SE HOUVE CONVERSÃO
          switch (order.status) {
            case 'AGUARDANDO PAGAMENTO':
            case 'AGUARDANDO YAPAY':
              event.event_type = 'ORDER_PLACED';
              //
              lead_event.name = lead.name ? lead.name : '';
              lead_event.email = order.Customer.email ? order.Customer.email : '';
              lead_event.cf_order_id = order.id.toString();
              lead_event.cf_order_total_items = order.ProductsSold.length;
              lead_event.cf_order_status = order.status;
              lead_event.cf_order_payment_method = order.payment_method.split(" ")[0] == 'Cartão' ? 'Credit Card' : order.payment_method.split(" ")[0] == 'Boleto' ? 'Invoice' : 'Others';
              lead_event.cf_order_payment_amount = parseFloat(order.total);
              //
              event.payload = lead_event;
              //
              break;
            case 'A ENVIAR':
              event.event_type = 'SALE';
              //
              lead_event.email = order.Customer.email;
              lead_event.funnel_name = 'default';
              lead_event.value = parseFloat(order.total);
              //
              event.payload = lead_event;
              //
              break;
              // case 'order/cancelled':
              // case 'order/fulfilled':
              // case 'order/partially_fulfilled':
              // case 'order/updated':
          }
          // CRIAR EVENTO DE CONVERSÃO
          if (event.event_type) {
            try {
              let chamada = (await axios.post(process.env.RD_BASE_URL + '/platform/events',
                // OBJETO
                event,
                // AUTENTICAÇÃO
                {
                  headers: {
                    'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token,
                    'Content-Type': 'application/json'
                  }
                }
              )).data;
              // CRIAR O LOG
              await axios.post(process.env.API_URL + '/log_tray', {
                tipo: 'success',
                descricao: 'Evento de conversão enviado para o RD Station com sucesso.',
                origem: 'api',
                extra: JSON.stringify({
                  tray: order,
                  event: req.body.scope_name + '/' + req.body.act,
                  rdstation: {
                    account: projeto.integracoes.rdstation.name,
                    lead_id: order.Customer.email ? order.Customer.email : '',
                    event_type: event.event_type
                  }
                }),
                projeto_id: integracao.projeto_id
              });
              // ENVIAR RESPOSTA POSITIVA
              res.status(200).send('OK');
            } catch (e) {
              console.log(e.response.data);
              // CRIAR O LOG
              await axios.post(process.env.API_URL + '/log_tray', {
                tipo: 'danger',
                descricao: 'Erro ao enviar envento de conversão para o RD Station.',
                origem: 'api',
                extra: JSON.stringify({
                  tray: order,
                  event: req.body.scope_name + '/' + req.body.act,
                  rdstation: {
                    account: projeto.integracoes.rdstation.name,
                    lead_id: order.Customer.email ? order.Customer.email : '',
                    event_type: event.event_type
                  }
                }),
                projeto_id: integracao.projeto_id
              });
              // ENVIAR RESPOSTA NEGATIVA
              res.status(500).send(e);
            }
          }
        }
      }
    } catch (e) {
      console.log(e.response.data);
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_tray', {
        tipo: 'danger',
        descricao: 'Erro ao preparar o envio dos dados para o RD Station.',
        origem: 'api',
        extra: JSON.stringify(order),
        projeto_id: integracao.projeto_id
      });
      // ENVIAR RESPOSTA NEGATIVA
      res.status(500).send(e);
    }
  }
  else res.status(200).send('OK');
});

module.exports = router;