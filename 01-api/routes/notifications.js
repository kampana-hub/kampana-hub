// SERVER

const express = require('express');
const router = express.Router();
const webpush = require('web-push');

// SETUP

webpush.setVapidDetails('dev@kampana.digital', process.env.VAPID_PUBLIC_KEY, process.env.VAPID_PRIVATE_KEY);

// SUBSCRIPTIONS

router.post('/subscriptions', function(req, res, next) {
  /*
	var subscriptions = (JSON.parse(req.user._json.user.user_fields.push) ? JSON.parse(req.user._json.user.user_fields.push) : []);
  subscriptions.push(req.body);
  zendeskClient.users.update(req.user._json.user.id, {
    'user': {
      'user_fields': {
        'push': JSON.stringify(subscriptions)
      }
    }
  }, function(err, call, user) {
    if (err) res.json('erro');
    else {
      session.getData(req.user._json.user).then(function(user) {
        req.user._json.user = user;
        res.json('success');
      });
    }
  });
	*/
	console.log(req.body);
	res.status(200).send('OK');
});

router.delete('/subscriptions', function(req, res, next) {
  /*
	zendeskClient.users.update(req.user._json.user.id, {
    'user': {
      'user_fields': {
        'push': ''
      }
    }
  }, function(err, call, user) {
    if (err) res.json('erro');
    else {
      res.json('success');
    }
  });
	*/
});

router.get('/subscriptions', function(req, res, next) {
	/*
  (JSON.parse(req.user._json.user.user_fields.push) ? res.json(JSON.parse(req.user._json.user.user_fields.push)) : res.json([]));
	*/
});

// DEBUG

router.get('/debug', function(req, res, next) {
  /*
	session.getData(req.user._json.user).then(function(user) {
    req.user._json.user = user;
    const subscriptions = (JSON.parse(req.user._json.user.user_fields.push) ? JSON.parse(req.user._json.user.user_fields.push) : []);
    subscriptions.forEach(function(subscription) {
      webpush.sendNotification(subscription, 'Teste de Notificação');
    });
    res.json('success');
  });
	*/
});

//

module.exports = router;