let datatable = {
  init: () => {
    // INSTANCIAR TABELA 
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/rdstation/logs',
            map: (raw) => {
              raw.forEach(item => {
                item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
        columns: [{
            field: 'id',
            title: '#',
            width: 50
          }, {
            field: 'descricao',
            title: 'Descrição',
            width: 600,
            template: (row) => {
              if (row.user_display_name) return row.descricao +  '<span class="label ml-3" data-toggle="tooltip" data-html="true" title="' + row.user_display_name + '<br>' + row.user_email + '"><i class="icon-sm la la-cog"></i></span>';
              // else if (row.rdstation_event_type == 'ORDER_PLACED') return row.descricao + '<span class="label label-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-shopping-cart text-white"></i></span>';
              // else if (row.rdstation_event_type == 'SALE') return row.descricao + '<span class="label label-success ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-dollar text-white"></i></span>';
              // else if (row.rdstation_lead_id) return row.descricao +  '<span class="label label-light-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-user"></i></span>';
              else return row.descricao;
            }
          }, {
            field: 'origem',
            title: 'Origem',
            width: 75
          }, {
            field: 'tipo',
            title: 'Tipo',
            textAlign: 'center',
            width: 75,
            template: (row) => {
              return '<span class="label font-weight-bold label-lg label-light-' + row.tipo + ' label-inline w-100 text-uppercase">' + row.tipo + '</span>';
            }
          }, {
            field: 'created_at',
            title: 'Data',
            width: 200
          }
        ],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
    });
    // EXIBIR TOOLTIPS
    $('#kt_datatable').KTDatatable().on('datatable-on-layout-updated', function() {
      $('[data-toggle="tooltip"]').tooltip();
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let gui = {
  init: async() => {
    $('.btn-debug, .btn-update, .btn-refresh, .btn-delete').prop('disabled', true);
    //
    let rdstation = (await axios.get(API_URL + '/rdstation')).data;
    if (rdstation) {
      // EXPIRES_AT = LAST_TIMESTAMP + EXPIRES_IN
      let expires_at = parseInt(rdstation.last_timestamp) + parseInt(rdstation.expires_in);
      rdstation.expires_at = moment.unix(expires_at).format('DD/MM/YYYY HH:mm:ss');
      // IS_VALID
      rdstation.is_valid = parseInt(rdstation.current_timestamp) < expires_at ? true : false;
      if (rdstation.is_valid) {
        // $('#card-status[class*="bg-"]').removeClass((index, css) =>  (css.match(/(^|\s)bg-\S+/g) || []).join(' '));
        // $('#card-status').addClass('bg-success');
        $('#kt_content').css('border-top', '10px solid ' + KTAppSettings.colors.theme.base.success);
        // LIBERAR TODOS OS COMANDOS
        $('.btn-debug, .btn-update, .btn-refresh, .btn-delete').prop('disabled', false);
      }
      else {
        // $('#card-status[class*="bg-"]').removeClass((index, css) =>  (css.match(/(^|\s)bg-\S+/g) || []).join(' '));
        // $('#card-status').addClass('bg-warning');
        $('#kt_content').css('border-top', '10px solid ' + KTAppSettings.colors.theme.base.warning);
        // ATUALIZAR INTEGRAÇÃO
        $('.btn-update, .btn-refresh, .btn-delete').prop('disabled', false);
      }
      // CONTROLE DE STATUS
      if (rdstation.status === 'true') {
        // DESABILITAR
        $('.btn-update').attr('data-label', 'desabilita');
        $('.btn-update span').html('Desabilitar');
        $('#alerta-status-rdstation').addClass('d-none');
      }
      //
      else {
        // $('#card-status[class*="bg-"]').removeClass((index, css) =>  (css.match(/(^|\s)bg-\S+/g) || []).join(' '));
        // $('#card-status').addClass('bg-warning');
        // HABILITAR
        $('.btn-update').attr('data-label', 'habilita');
        $('.btn-update span').html('Habilitar');
        $('#alerta-status-rdstation').removeClass('d-none');
      }
      // PONTOS DE INTEGRAÇÃO
      let nuvemshop = (await axios.get(API_URL + '/nuvemshop')).data;
      let integracoes;
      if (nuvemshop) integracoes = JSON.parse(nuvemshop.raw.integracoes);
      if (integracoes && integracoes.rdstation.status == true) {
        $('.webhook, .script').prop('disabled', false);
        $('#alerta-nuvemshop-rdstation').addClass('d-none');
        // AVALIAR WEBHOOKS DISPONÍVEIS E EXISTENTES
        let webhook_options = ['order_created', 'order_updated', 'order_paid', 'order_packed', 'order_fulfilled', 'order_cancelled'];
        if (integracoes.nuvemshop.webhooks) {
          webhook_options.forEach(webhook => {
            if (integracoes.nuvemshop.webhooks[webhook] && integracoes.nuvemshop.webhooks[webhook].tags.includes('rdstation')) $('#' + webhook.replace(/_/g, '-')).prop('checked', true);
            else $('#' + webhook.replace(/_/g, '-')).prop('checked', false);
          });
        } else webhook_options.forEach(webhook => $('#' + webhook.replace(/_/g, '-')).prop('checked', false));
        $('.webhook').on('change', async function() {
          try {
            // EXIBIR TELA DE CARREGAMENTO
            swal.fire({
              title: 'Aguarde...',
              text: 'Processando a solicitação.',
              onOpen: () => {
                swal.showLoading();
              },
              allowOutsideClick: false,
              allowEscapeKey: false
            });
            // ENVIAR REQUISIÇÃO PARA O BACK-END
            let webhooks = {};
            webhook_options.forEach(webhook => {
              webhooks[webhook] = $('#' + webhook.replace('_', '-')).prop('checked');
            });
            await axios.put(API_URL + '/nuvemshop/webhooks', {
              webhooks: webhooks,
              tag: 'rdstation'
            });
            datatable.reload();
            // DEU CERTO
            $('#modal').modal('hide');
            swal.close();
            swal.fire({
              icon: 'success',
              title: 'Excelente!',
              text: 'A integração foi atualizada com sucesso.'
            });
          }
          //
          catch(e) {
            console.log(e);
            swal.close();
            swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Algo errado não está certo.',
              footer: 'Confira o erro no log do navegador.'
            });
          }
        });
        // AVALIAR SCRIPTS DISPONÍVEIS E EXISTENTES
        let script_options = ['tracking_code_rd', 'abandoned_cart'];
        if (integracoes.nuvemshop.scripts) {
          script_options.forEach(script => {
            if (integracoes.nuvemshop.scripts[script] && integracoes.nuvemshop.scripts[script].tags.includes('rdstation')) $('#' + script.replaceAll(/_/g, '-')).prop('checked', true);
            else $('#' + script.replaceAll(/_/g, '-')).prop('checked', false);
          });
        } else script_options.forEach(script => $('#' + script.replaceAll(/_/g, '-')).prop('checked', false));
        $('.script').on('change', async function() {
          try {
            // EXIBIR TELA DE CARREGAMENTO
            swal.fire({
              title: 'Aguarde...',
              text: 'Processando a solicitação.',
              onOpen: () => {
                swal.showLoading();
              },
              allowOutsideClick: false,
              allowEscapeKey: false
            });
            // ENVIAR REQUISIÇÃO PARA O BACK-END
            let scripts = {};
            script_options.forEach(script => {
              scripts[script] = $('#' + script.replaceAll('_', '-')).prop('checked');
            });
            await axios.put(API_URL + '/nuvemshop/scripts', {
              scripts: scripts,
              tag: 'rdstation'
            });
            datatable.reload();
            // DEU CERTO
            $('#modal').modal('hide');
            swal.close();
            swal.fire({
              icon: 'success',
              title: 'Excelente!',
              text: 'A integração foi atualizada com sucesso.'
            });
          }
          //
          catch(e) {
            console.log(e);
            swal.close();
            swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Algo errado não está certo.',
              footer: 'Confira o erro no log do navegador.'
            });
          }
        });
      } else {
        $('.webhook, .script').prop('disabled', true);
        $('#alerta-nuvemshop-rdstation').removeClass('d-none');
      }
    }
    // CRIAR INTEGRAÇÃO
    else {
      // $('#card-status[class*="bg-"]').removeClass((index, css) =>  (css.match(/(^|\s)bg-\S+/g) || []).join(' '));
      // $('#card-status').addClass('bg-danger');
      $('#kt_content').css('border-top', '10px solid ' + KTAppSettings.colors.theme.base.danger);
      // CONFIGURAR INTEGRAÇÃO!
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Nenhuma Conta Configurada',
        text: 'Para que o Kampana Hub funcione corretamente, você precisa associar uma conta do RD Station.',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        footer: 'Ao continuar você será redirecionado para o RD Station. Certifique-se de ter acesso à conta.',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0',
          footer: 'text-center'
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        window.location.href = API_URL + '/rdstation/login';
      }
      //
      else window.location.href = '/';
    }
  }
}

let api = {
  update: async (label) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo ' + label + 'r esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        await axios.put(API_URL + '/rdstation');
        datatable.reload();
        gui.init();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi ' + label + 'da com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async () => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        await axios.delete(API_URL + '/rdstation');
        datatable.reload();
        gui.init();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi excluída com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

let custom = {
  debug: async() => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // ENVIAR REQUISIÇÃO PARA O BACK-END
      let debug = (await axios.get(API_URL + '/rdstation/debug')).data;
      datatable.reload();
      // DEU CERTO
      swal.close();
      swal.fire({
        icon: 'success',
        title: 'Excelente!',
        html: '<code>' + JSON.stringify(debug) + '</code>'
      });
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  refresh: async () => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo atualizar esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        await axios.get(API_URL + '/rdstation/refresh');
        datatable.reload();
        gui.init();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi atualizada com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR
  datatable.init();
  gui.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-label'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete();
  });
  // CONFIGURAR CUSTOM
  $(document).on('click', '.btn-debug', function() {
    custom.debug();
  });
  $(document).on('click', '.btn-refresh', function() {
    custom.refresh();
  });
  // AVALIAR SE O CARREGAMENTO É UM CALLBACK
  let params = new URLSearchParams(window.location.search);
  if (params.get('sucesso')) {
    let sucesso = (params.get('sucesso') === 'true');
    // DEU CERTO
    if (sucesso) swal.fire({
      icon: 'success',
      title: 'Excelente!',
      text: 'A integração foi adicionada com sucesso.',
    });
    // DEU ERRADO
    else swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Algo errado não está certo.',
      footer: 'Por favor, tente novamente.'
    });
    // LIMPAR A FLAG DA URL
    window.history.replaceState({}, document.title, location.protocol + '//' + location.host + location.pathname);
  }
});