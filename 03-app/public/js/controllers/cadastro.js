let _wizardObj;
let KTWizard5 = function() {
  let _wizardEl;
  let _formEl;
  let _validations = [];
  //
  let _initWizard = function() {
    // INITIALIZATION
    _wizardObj = new KTWizard(_wizardEl, {
      startStep: 1, // initial active step number
      clickableSteps: false // allow step clicking
    });
    // VALIDATION
    _wizardObj.on('change', function(wizard) {
      // BANNER NUVEM
      if (wizard.getNewStep() == 1) $('#banner-nuvem').removeClass('d-none');
      else $('#banner-nuvem').addClass('d-none')
      //
      if (wizard.getStep() > wizard.getNewStep()) return;
      //
      let validator = _validations[wizard.getStep() - 1];
      if (validator) {
        validator.validate().then(function(status) {
          if (status == 'Valid') {
            wizard.goTo(wizard.getNewStep());
          }
          //
          else {
            swal.fire({
              icon: 'error',
              text: 'Parece que foram detectados alguns erros, tente novamente.',
              buttonsStyling: false,
              confirmButtonText: 'Entendido',
              customClass: {
                confirmButton: 'btn font-weight-bold btn-light'
              }
            }).then(function() {
              KTUtil.scrollTop();
            });
          }
        });
      }
      return false;
    });
    // CHANGE EVENT
    _wizardObj.on('changed', function(wizard) {
      KTUtil.scrollTop();
    });
    // SUBMIT EVENT
    _wizardObj.on('submit', async function(wizard) {
      try {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // PREPARAR OBJETO
        let object = {};
        $('#kt_form').find('[name*="_"]').each(function() { 
          object[$(this).attr('name')] = $(this).val();
        });
        // AVALIAR SE EXISTE UM CODE PRA SESSÃO
        let params = new URLSearchParams(window.location.search);
        if (params.get('code')) object.code = params.get('code');
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let data = (await axios.post(API_URL + '/cadastro', object)).data;
        // DEU CERTO
      	stripe.redirectToCheckout({ sessionId: data.sessionId });
      }
      catch(e) {
        swal.close();
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Algo errado não está certo.',
          footer: 'Nossa equipe já foi informada do erro.'
        });
      }
    });
  }
  //
  let _initValidation = function() {
    // Step 1
    _validations.push(FormValidation.formValidation(
      _formEl, {
        fields: {
          usuario_nome: {
            validators: {
              notEmpty: {
                message: '<small>O campo <strong>Nome</strong> é obrigatório.</small>'
              }
            }
          },
          usuario_email: {
            validators: {
              notEmpty: {
                message: '<small>O campo <strong>E-mail</strong> é obrigatório.</small>'
              },
              emailAddress: {
                message: '<small>O valor de <strong>E-mail</strong> não é um endereço válido.</small>'
              }
            }
          },
          usuario_senha: {
            validators: {
              notEmpty: {
                message: '<small>O campo <strong>Senha</strong> é obrigatório.</small>'
              },
              stringLength: {
                min: 8,
                message: '<small>O valor de <strong>Senha</strong> deve ter pelo menos 8 caracteres.</small>'
              }
            }
          },
          usuario_senha_c: {
            validators: {
              notEmpty: {
                message: '<small>O campo <strong>Confirmação de Senha</strong> é obrigatório.</small>'
              },
              identical: {
                message: '<small>O valor de <strong>Confirmação de Senha</strong> não confere.</small>',
                compare: function() {
                  return _formEl.querySelector('[name="usuario_senha"]').value;
                }
              }
            }
          }
        },
        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap({
            //eleInvalidClass: '',
            eleValidClass: '',
          })
        }
      }
    ));
    // Step 2
    _validations.push(FormValidation.formValidation(
      _formEl, {
        fields: {
          usuario_telefone: {
            validators: {
              notEmpty: {
                message: '<small>O campo <strong>Telefone</strong> é obrigatório.</small>'
              }
            }
          },
          usuario_nascimento: {
            validators: {
              notEmpty: {
                message: '<small>O campo <strong>Data de Nascimento</strong> é obrigatório.</small>'
              }
            }
          },
          cliente_tipo: {
            validators: {
              notEmpty: {
                message: '<small>O campo <strong>Tipo de Conta</strong> é obrigatório.</small>'
              }
            }
          },
          cliente_id_mf: {
            validators: {
              notEmpty: {
                message: '<small>O campo <strong>CPF / CNPJ</strong> é obrigatório.</small>'
              }
            }
          },
          cliente_nome: {
            validators: {
              callback: {
                message: '<small>O campo <strong>Nome da Empresa</strong> é obrigatório.</small>',
                callback: function(input) {
                  if (_formEl.querySelector('[name="cliente_tipo"]').value == 'PJ') {
                    if (_formEl.querySelector('[name="cliente_nome"]').value) return true;
                    else return false;
                  }
                  else return true;
                }
              }
            }
          }
        },
        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap({
            //eleInvalidClass: '',
            eleValidClass: '',
          })
        }
      }
    ));
    /*
    // Step 3
    _validations.push(FormValidation.formValidation(
      _formEl, {
        fields: {
        },
        plugins: {
          trigger: new FormValidation.plugins.Trigger(),
          // Bootstrap Framework Integration
          bootstrap: new FormValidation.plugins.Bootstrap({
            //eleInvalidClass: '',
            eleValidClass: '',
          })
        }
      }
    ));
    */
    // https://formvalidation.io/
  }
  //
  return {
    init: function() {
      _wizardEl = KTUtil.getById('kt_wizard');
      _formEl = KTUtil.getById('kt_form');
      _initWizard();
      _initValidation();
    }
  };
}();

let gui = {
  init: () => {
    // MÁSCARA DO TELEFONE
    let SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };
    let spOptions = {
      onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      },
      clearIfNotMatch: true
    };
    $('[name="usuario_telefone"]').mask(SPMaskBehavior, spOptions);
    // DATA DE NASCIMENTO
    $('[name="usuario_nascimento"]').datetimepicker({
      locale: 'pt-br',
      format: 'L',
      maxDate: moment()
    });
    $('[name="usuario_nascimento"]').mask('00/00/0000', {
      clearIfNotMatch: true
    });
    $('[name="usuario_nascimento"]').val('')
    // TIPO DE CLIENTE
    $('[name="cliente_tipo"]').on('change', function() {
      // LIMPAR CAMPO
      $('[name="cliente_id_mf"]').val('');
      $('[name="cliente_nome"]').val('');
      // OPÇÕES DA MÁSCARA
      let options = {
        clearIfNotMatch: true,
        everse: true
      }
      // ALTERAR MÁSCARA
      if ($(this).val() == 'PF') {
        $('[name="cliente_id_mf"]').mask('000.000.000-00', options);
        $('[name="cliente_id_mf"]').attr('placeholder', 'Ex.: 123.456.789-00');
        $('[name="cliente_id_mf"]').prop('disabled', false);
        //
        $('[name="cliente_nome"]').attr('placeholder', '');
        $('[name="cliente_nome"]').prop('disabled', true);
      }
      else if ($(this).val() == 'PJ') {
        $('[name="cliente_id_mf"]').mask('00.000.000/0000-00', options);
        $('[name="cliente_id_mf"]').attr('placeholder', 'Ex.: 12.345.456/0001-00');
        $('[name="cliente_id_mf"]').prop('disabled', false);
        //
        $('[name="cliente_nome"]').attr('placeholder', 'Ex.: Loja do João');
        $('[name="cliente_nome"]').prop('disabled', false);
      }
      else {
        $('[name="cliente_id_mf"]').unmask();
        $('[name="cliente_id_mf"]').attr('placeholder', '');
        $('[name="cliente_id_mf"]').prop('disabled', true);
        //
        $('[name="cliente_nome"]').attr('placeholder', '');
        $('[name="cliente_nome"]').prop('disabled', true);
      }
    });
    // VERIFICAR SE O E-MAIL JÁ ESTÁ EM USO
    $('[name="usuario_email"]').on('change', async function() {
      if ($(this).val().length > 0 && /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($(this).val())) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // REALIZAR CONSULTA
        let flag = (await axios.get(API_URL + '/cadastro/consultar-usuario/' + $(this).val())).data;
        // DETERMINAR
        swal.close();
        if (flag) {
          await swal.fire({
            icon: 'warning',
            title: 'Atenção!',
            text: 'O E-mail informado já está cadastrado.',
            footer: 'Por favor revise a informação e tente novamente.'
          });
          $(this).val('');
          $(this).focus();
        }
        else $('[name="usuario_senha"]').focus();
      }
    });
    // VERIFICAR SE O CPF/CNPJ JÁ ESTÁ EM USO
    $('[name="cliente_id_mf"]').on('change', function() {
      setTimeout(async function() {
        if ($('[name="cliente_id_mf"]').val().length > 0) {
          // EXIBIR TELA DE CARREGAMENTO
          swal.fire({
            title: 'Aguarde...',
            text: 'Processando a solicitação.',
            onOpen: () => {
              swal.showLoading();
            },
            allowOutsideClick: false,
            allowEscapeKey: false
          });
          // REALIZAR CONSULTA
          let flag = (await axios.get(API_URL + '/cadastro/consultar-cliente/' + $('[name="cliente_id_mf"]').val().replace(/\D/g, ''))).data;
          // DETERMINAR
          swal.close();
          if (flag) {
            await swal.fire({
              icon: 'warning',
              title: 'Atenção!',
              text: 'O CPF/CNPJ informado já está cadastrado.',
              footer: 'Por favor revise a informação e tente novamente.'
            });
            $('[name="cliente_id_mf"]').val('');
            $('[name="cliente_id_mf"]').focus();
          }
          else {
            if ($('[name="cliente_tipo"]').val() == 'PF') $('[data-wizard-type="action-next"]').focus();
            else $('[name="cliente_nome"]').focus();
          }
        }
      }, 100);
    })
  }
}

jQuery(document).ready(async function() {
  KTWizard5.init();
  //
  gui.init();
  //
  let params = new URLSearchParams(window.location.search);
  if (params.get('code')) {
    $('#banner-nuvem').removeClass('d-none');
    // BUSCAR INFORMAÇÕES DA LOJA;
    let cache_nuvemshop = (await axios.get(API_URL + '/cache-nuvemshop/' + params.get('code'))).data;
    if (cache_nuvemshop) {
      let store = JSON.parse(cache_nuvemshop.store) || {};
      store.contact_email ? $('[name="usuario_email"]').val(store.contact_email) : '';
      store.phone ? $('[name="usuario_telefone"]').val(store.phone.replace(/\D/g, '').replace('55', '').replace(/^(\d{2})(\d{5})?(\d{4})?/, "($1) $2-$3")) : '';
      if (store.business_id && store.business_id.length == 14) {
        $('[name="cliente_tipo"]').val('PJ');
        $('[name="cliente_tipo"]').change();
        $('[name="cliente_id_mf"]').val(store.business_id.replace(/\D/g, '').replace(/^(\d{2})(\d{3})?(\d{3})?(\d{4})?(\d{2})?/, "$1.$2.$3/$4-$5"));
        $('[name="cliente_nome"]').val(store.name.pt);
      } else if (store.business_id && store.business_id.length == 11) {
        $('[name="cliente_tipo"]').val('PF');
        $('[name="cliente_tipo"]').change();
        $('[name="cliente_id_mf"]').val(store.business_id.replace(/\D/g, '').replace(/^(\d{3})(\d{3})?(\d{3})?(\d{2})?/, "$1.$2.$3-$4"));
      }
    }
  }
  /*
  if (localStorage.getItem('signup')) {
    let signup = JSON.parse(localStorage.getItem('signup'));
    for (let [key, value] of Object.entries(signup.object)) {
      $('[name="' + key + '"]').val(value);
      _wizardObj.goTo(signup.step);
    }
  }
  */
});