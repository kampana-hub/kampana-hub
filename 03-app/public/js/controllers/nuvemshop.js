let datatable = {
  init: () => {
    // INSTANCIAR TABELA 
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/nuvemshop/logs',
            map: (raw) => {
              raw.forEach(item => {
                item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
                /*
                item.tipo_label = [{
                  key: 'info',
                  value: 'Informe'
                }, {
                  key: 'warning',
                  value: 'Atenção'
                }, {
                  key: 'danger',
                  value: 'Erro'
                }, {
                  key: 'success',
                  value: 'Sucesso'
                }].find(item => item.key == item.tipo).value;
                */
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
        columns: [{
            field: 'id',
            title: '#',
            width: 50
          }, {
            field: 'descricao',
            title: 'Descrição',
            width: 600,
            template: (row) => {
              if (row.user_display_name) return row.descricao +  '<span class="label ml-3" data-toggle="tooltip" data-html="true" title="' + row.user_display_name + '<br>' + row.user_email + '"><i class="icon-sm la la-cog"></i></span>';
              // else if (row.nuvemshop_event_type == 'ORDER_PLACED') return row.descricao + '<span class="label label-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.nuvemshop_lead_id + '"><i class="icon-sm la la-shopping-cart text-white"></i></span>';
              // else if (row.nuvemshop_event_type == 'SALE') return row.descricao + '<span class="label label-success ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.nuvemshop_lead_id + '"><i class="icon-sm la la-dollar text-white"></i></span>';
              // else if (row.nuvemshop_lead_id) return row.descricao +  '<span class="label label-light-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.nuvemshop_lead_id + '"><i class="icon-sm la la-user"></i></span>';
              else return row.descricao;
            }
          }, {
            field: 'origem',
            title: 'Origem',
            width: 75
          }, {
            field: 'tipo',
            title: 'Tipo',
            textAlign: 'center',
            width: 75,
            template: (row) => {
              return '<span class="label font-weight-bold label-lg label-light-' + row.tipo + ' label-inline w-100 text-uppercase">' + row.tipo + '</span>';
            }
          }, {
            field: 'created_at',
            title: 'Data',
            width: 200
          }
        ],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
    });
    // EXIBIR TOOLTIPS
    $('#kt_datatable').KTDatatable().on('datatable-on-layout-updated', function() {
      $('[data-toggle="tooltip"]').tooltip();
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let gui = {
  init: async() => {
    $('.btn-debug, .btn-update, .btn-refresh, .btn-delete').prop('disabled', true);
    //
    let nuvemshop = (await axios.get(API_URL + '/nuvemshop')).data;
    if (nuvemshop) {
      let integracoes = JSON.parse(nuvemshop.raw.integracoes);
      // $('#card-status[class*="bg-"]').removeClass((index, css) =>  (css.match(/(^|\s)bg-\S+/g) || []).join(' '));
      // $('#card-status').addClass('bg-success');
      $('#kt_content').css('border-top', '10px solid ' + KTAppSettings.colors.theme.base.success);
      $('.logo-nuvemshop').attr('href', '//' + nuvemshop.original_domain)
      // LIBERAR TODOS OS COMANDOS
      $('.btn-debug, .btn-update, .btn-refresh, .btn-delete').prop('disabled', false);
      // CONTROLE DE STATUS
      if (nuvemshop.status === 'true') {
        // DESABILITAR
        $('.btn-update').attr('data-label', 'desabilita');
        $('.btn-update span').html('Desabilitar');
      }
      //
      else {
        // $('#card-status[class*="bg-"]').removeClass((index, css) =>  (css.match(/(^|\s)bg-\S+/g) || []).join(' '));
        // $('#card-status').addClass('bg-warning');
        $('#kt_content').css('border-top', '10px solid ' + KTAppSettings.colors.theme.base.warning);
        // HABILITAR
        $('.btn-update').attr('data-label', 'habilita');
        $('.btn-update span').html('Habilitar');
      }
    }
    // CRIAR INTEGRAÇÃO
    else {
      // $('#card-status[class*="bg-"]').removeClass((index, css) =>  (css.match(/(^|\s)bg-\S+/g) || []).join(' '));
      // $('#card-status').addClass('bg-danger');
      $('#kt_content').css('border-top', '10px solid ' + KTAppSettings.colors.theme.base.danger);
      // CONFIGURAR INTEGRAÇÃO!
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Nenhuma Conta Configurada',
        text: 'Para que o Kampana Hub funcione corretamente, você precisa associar uma loja da Nuvemshop.',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        footer: 'Ao continuar você será redirecionado para a Nuvemshop. Certifique-se de ter acesso à loja.',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0',
          footer: 'text-center'
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        window.location.href = API_URL + '/nuvemshop/login';
      }
      //
      else window.location.href = '/';
    }
  }
}

let api = {
  update: async (label) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo ' + label + 'r esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        await axios.put(API_URL + '/nuvemshop');
        datatable.reload();
        gui.init();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi ' + label + 'da com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async () => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        await axios.delete(API_URL + '/nuvemshop');
        datatable.reload();
        gui.init();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi excluída com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

let custom = {
  debug: async() => {
    try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // ENVIAR REQUISIÇÃO PARA O BACK-END
      let debug = (await axios.get(API_URL + '/nuvemshop/debug')).data;
      datatable.reload();
      // DEU CERTO
      swal.close();
      swal.fire({
        icon: 'success',
        title: 'Excelente!',
        html: '<div class="p-5 border"><img class="max-h-30px mb-2" src="' + debug.logo +'" alt="' + debug.name.pt +'"><br><strong class="mb-2">' + debug.name.pt +'</strong><br>' + debug.url_with_protocol + '</div>'
      });
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR
  datatable.init();
  gui.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-label'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete();
  });
  // CONFIGURAR CUSTOM
  $(document).on('click', '.btn-debug', function() {
    custom.debug();
  });
  // AVALIAR SE O CARREGAMENTO É UM CALLBACK
  let params = new URLSearchParams(window.location.search);
  if (params.get('sucesso')) {
    let sucesso = (params.get('sucesso') === 'true');
    // DEU CERTO
    if (sucesso) swal.fire({
      icon: 'success',
      title: 'Excelente!',
      text: 'A integração foi adicionada com sucesso.',
    });
    // DEU ERRADO
    else swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Algo errado não está certo.',
      footer: 'Por favor, tente novamente.'
    });
    // LIMPAR A FLAG DA URL
    window.history.replaceState({}, document.title, location.protocol + '//' + location.host + location.pathname);
  }
});