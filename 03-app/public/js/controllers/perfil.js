let uppyDashboard;

let gui = {
  init: async () => {
		let mphone = (phone) => {
			phone = phone.replace(/\D/g,'');
			phone = phone.replace(/^(\d)/,'+$1');
			phone = phone.replace(/(.{3})(\d)/,'$1($2');
			phone = phone.replace(/(.{6})(\d)/,'$1)$2');
			//
			if (phone.length == 12) phone=phone.replace(/(.{1})$/,'-$1');
			else if (phone.length == 13) phone = phone.replace(/(.{2})$/,'-$1');
			else if (phone.length == 14) phone = phone.replace(/(.{3})$/,'-$1');
			else if (phone.length == 15) phone = phone.replace(/(.{4})$/,'-$1');
			else if (phone.length > 15) phone = phone.replace(/(.{4})$/,'-$1');
			//
			return phone.replace('(', ' (').replace(')', ') ');
		}
    // MÁSCARA DO TELEFONE
    let SPMaskBehavior = function (val) {
      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    };
    let spOptions = {
      onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      },
      clearIfNotMatch: true
    };
    $('[name="usuario_telefone"]').mask(SPMaskBehavior, spOptions);
    // DATA DE NASCIMENTO
    $('[name="usuario_nascimento"]').datetimepicker({
      locale: 'pt-br',
      format: 'L',
      //maxDate: moment()
    });
    $('[name="usuario_nascimento"]').mask('00/00/0000', {
      clearIfNotMatch: true
    });
    $('[name="usuario_nascimento"]').val('')
    // AVATAR
    uppyDashboard = Uppy.Core({
      restrictions: {
        maxFileSize: 1000000, // 1mb
        maxNumberOfFiles: 1,
        minNumberOfFiles: 1
      },
      locale: Uppy.locales.pt_BR
    });
    uppyDashboard.use(Uppy.Dashboard, {
      proudlyDisplayPoweredByUppy: false,
      target: '#kt_uppy .uppy-dashboard',
      inline: false,
      replaceTargetContent: true,
      height: 470,
      browserBackButtonClose: true,
      trigger: ' .uppy-btn'
    });
    uppyDashboard.use(Uppy.Webcam, {
      target: Uppy.Dashboard,
      modes: [
        'picture'
      ],
      videoConstraints: {
        facingMode: 'user',
        width: { min: 480, ideal: 800, max: 1080 },
        height: { min: 480, ideal: 800, max: 1080 }
      }
    });
    uppyDashboard.on('file-added', (file) => {
      var reader  = new FileReader();
      reader.onloadend = function() {
        $('#kt_user_edit_avatar .image-input-wrapper').attr('style', 'background-image: url("' + reader.result + '")');
      }
      reader.readAsDataURL(file.data);
    });
    uppyDashboard.on('file-removed', (file, reason) => {
      $('#kt_user_edit_avatar .image-input-wrapper').attr('style', 'background-image: url("/media/users/blank.png")');
    });
    $('.uppy-btn-remove').on('click', function() {
      uppyDashboard.cancelAll();
    });
		//
		try {
      // EXIBIR TELA DE CARREGAMENTO
      swal.fire({
        title: 'Aguarde...',
        text: 'Processando a solicitação.',
        onOpen: () => {
          swal.showLoading();
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // CARREGAR DADOS DO USUÁRIO
      let usuario = (await axios.get(API_URL + '/usuarios/' + auth0.user_id)).data;
			swal.close();
      // INSERIR DADOS DO USUÁRIO NO FORMULÁRIO
      $('#kt_user_edit_avatar .image-input-wrapper').attr('style', 'background-image: url("' + usuario.picture + '")');
      $('[name="usuario_nome"]').val(usuario.name);
      $('[name="usuario_email"]').val(usuario.email);
    	$('[name="usuario_telefone"]').val($('[name="usuario_telefone"]').masked(usuario.user_metadata.telefone.replace('55', '')));
      $('[name="usuario_nascimento"]').val(moment(usuario.user_metadata.nascimento).format('DD/MM/YYYY'));
			$('[name="cliente_tipo"]').val(usuario.cliente.tipo);
			$('[name="cliente_id_mf"]').val(usuario.cliente.id_mf);
			$('[name="cliente_nome"]').val(usuario.cliente.nome);
			// REDEFINIR FORMULÁRIO
      $('.btn-save').off();
      $('.btn-save').on('click', async function() {
				let confirmacao = await swal.fire({
					icon: 'warning',
					title: 'Atenção!',
					text: 'Deseja mesmo atualizar seu usuário?',
					showCancelButton: true,
					reverseButtons: true,
					cancelButtonText: 'Cancelar',
					confirmButtonText: 'Continuar',
					customClass: {
						confirmButton: 'btn btn-primary mt-0',
						cancelButton: 'btn btn-light mt-0'
					}
				});
				if (confirmacao.value) {
					try {
						// EXIBIR TELA DE CARREGAMENTO
						swal.fire({
							title: 'Aguarde...',
							text: 'Processando a solicitação.',
							onOpen: () => {
								swal.showLoading();
							},
							allowOutsideClick: false,
							allowEscapeKey: false
						});
						// ENVIAR REQUISIÇÃO PARA O BACK-END
						let nascimento = $('[name="usuario_nascimento"]').val();
						//
						let dia  = nascimento.substring(0, 2);
						let mes  = nascimento.substring(3, 5);
						let ano  = nascimento.substring(6, 10);
						let hora = nascimento.substring(11, 16);
						//
						let usuario = {
							nome: $('[name="usuario_nome"]').val(),
							telefone: '55' + $('[name="usuario_telefone"]').cleanVal(),
							nascimento: ano + '-' + mes + '-' + dia,
						}
						let formData = new FormData();
						// DADOS
						Object.keys(usuario).forEach(function(key) {
							formData.append(key, usuario[key]);
						});
						// ANEXO
						if (uppyDashboard.getFiles().length > 0) formData.append('file', uppyDashboard.getFiles()[0].data);
						// CHAMADA
						const chamada = (await axios.put(API_URL + '/usuarios/' + auth0.user_id, formData, {
							headers: {
								'Content-Type': 'multipart/form-data'
							}
						})).data;
						console.log(chamada);
						document.getElementById('img-perfil').style.backgroundImage='url(' + chamada.picture + ')';
						// DEU CERTO
						swal.close();
						swal.fire({
							icon: 'success',
							title: 'Excelente!',
							text: 'O usuario foi atualizado com sucesso.',
						});
					}
					//
					catch(e) {
						console.log(e);
						swal.close();
						swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: 'Algo errado não está certo.',
							footer: 'Confira o erro no log do navegador.'
						});
					}
				}
      });
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  gui.init();
});