let datatable = {
  init: () => {
    $('#kt_datatable').KTDatatable({
        // CARREGAR E MANIPULAR OS DADOS
        data: {
          type: 'remote',
          source: {
            read: {
              method: 'GET',
              url: API_URL + '/nuvemshop/logs',
              map: (raw) => {
                raw.forEach(item => {
                  item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
                });
                return raw;
              }
            }
          }
        },
        // DEFINIR LAYOUT
        layout: {
          scroll: false,
          footer: false
        },
        sortable: true,
        pagination: true,
        // DEFINIR FILTROS
        search: {
          input: $('#kt_datatable_search_query'),
          key: 'generalSearch'
        },
        // DEFINIR COLUNAS
        columns: [{
            field: 'id',
            title: '#',
            width: 50
          }, {
            field: 'descricao',
            title: 'Descrição',
            width: 600,
            template: (row) => {
              if (row.user_display_name) return row.descricao +  '<span class="label ml-3" data-toggle="tooltip" data-html="true" title="' + row.user_display_name + '<br>' + row.user_email + '"><i class="icon-sm la la-cog"></i></span>';
              else if (row.rdstation_event_type == 'ORDER_PLACED') return row.descricao + '<span class="label label-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-shopping-cart text-white"></i></span>';
              else if (row.rdstation_event_type == 'SALE') return row.descricao + '<span class="label label-success ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-dollar text-white"></i></span>';
              else if (row.rdstation_lead_id) return row.descricao +  '<span class="label label-light-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-user"></i></span>';
              else return row.descricao;
            }
          }, {
            field: 'origem',
            title: 'Origem',
            width: 75
          }, {
            field: 'tipo',
            title: 'Tipo',
            textAlign: 'center',
            width: 75,
            template: (row) => {
              return '<span class="label font-weight-bold label-lg label-light-' + row.tipo + ' label-inline w-100 text-uppercase">' + row.tipo + '</span>';
            }
          }, {
            field: 'created_at',
            title: 'Data',
            width: 200
          }
        ],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
      });
    $('#kt_datatable').KTDatatable().on('datatable-on-layout-updated', function() {
      $('[data-toggle="tooltip"]').tooltip();
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

jQuery(document).ready(async () => {

  let dashboard = (await axios.get(API_URL + '/dashboard')).data; console.log(dashboard);
  
  /*/
   * PAINEL DE CONTROLE - E-COMMERCE (NUVEMSHOP)
  /*/
  
  // E-COMMERCE CONFIGURADO  
  if (dashboard.nuvemshop) {
    $('#bridge-ecommerce').attr('href', '/nuvemshop');
    // INTEGRAÇÃO ATIVA
    if (dashboard.nuvemshop.status === "true") {
      $('#bridge-ecommerce').parent().removeClass((index, css) => (css.match (/(^|\s)bg-\S+/g) || []).join(' '))
      $('#bridge-ecommerce').parent().removeClass((index, css) => (css.match (/(^|\s)border\S+/g) || []).join(' '))
      $('#bridge-ecommerce').parent().addClass('bg-white border border-success');
      //
      $('#bridge-ecommerce span').after('<div class="mt-1"><small class="text-dark small-60"><strong>Tudo certo!</strong> Aproveite todas as <u>integrações</u> disponíveis.</small></div>');
    }
    // INTEGRAÇÃO INATIVA
    else {
      $('#bridge-ecommerce').parent().removeClass((index, css) => (css.match (/(^|\s)bg-\S+/g) || []).join(' '))
      $('#bridge-ecommerce').parent().removeClass((index, css) => (css.match (/(^|\s)border\S+/g) || []).join(' '))
      $('#bridge-ecommerce').parent().addClass('bg-white border border-warning');
      //
      $('#bridge-ecommerce span').after('<div class="mt-1"><small class="text-dark small-60"><strong>Atenção!</strong> A conexão foi <u>desabilitada</u>.</small></div>');
    }
  }
  // E-COMMERCE NÃO CONFIGURADO
  else {
    $('#bridge-ecommerce').attr('href', '/api/nuvemshop/login');
    //
    $('#bridge-ecommerce').parent().removeClass((index, css) => (css.match (/(^|\s)bg-\S+/g) || []).join(' '))
    $('#bridge-ecommerce').parent().removeClass((index, css) => (css.match (/(^|\s)border\S+/g) || []).join(' '))
    $('#bridge-ecommerce').parent().addClass('bg-danger-o-40 border border-danger');
    //
    $('#bridge-ecommerce span').after('<div class="mt-1"><small class="text-danger small-60"><strong>NÃO CONECTADO</strong></small><i class="flaticon2-warning text-danger ml-2"></i><br><small class="text-dark"><u>Clique para resolver.</u></small></div>');
  }
  
  /*/
   * PAINEL DE CONTROLE - FINANCEIRO (STRIPE)
   * https://stripe.com/docs/api/subscriptions/object
  /*/
  
  // ASSINATURA ENCONTRADA 
  if (dashboard.assinaturas && dashboard.assinaturas.length > 0) {
    $('#bridge-stripe').attr('href', '/financeiro');
    // EM PERÍODO DE TRIAL
    if (dashboard.assinaturas.find(assinatura => assinatura.status == 'trialing')) {
      let assinatura = dashboard.assinaturas.find(assinatura => assinatura.status == 'trialing');
      //
      $('#bridge-stripe').parent().removeClass((index, css) => (css.match (/(^|\s)bg-\S+/g) || []).join(' '));
      $('#bridge-stripe').parent().removeClass((index, css) => (css.match (/(^|\s)border\S+/g) || []).join(' '));
      $('#bridge-stripe').parent().addClass('bg-white border border-warning');
      //
      $('#bridge-stripe span').after('<div class="mt-1"><small class="small-60 text-dark"><strong>Atenção!</strong> O período de testes se encerra em <u>' + moment.unix(assinatura.trial_end).format('DD/MM/YYYY') + '</u>.</small></div>');
    }
    // ASSINATURA ATIVA
    else if (dashboard.assinaturas.find(assinatura => assinatura.status == 'active')) {
      let assinatura = dashboard.assinaturas.find(assinatura => assinatura.status == 'active');
      //
      $('#bridge-stripe').parent().removeClass((index, css) => (css.match (/(^|\s)bg-\S+/g) || []).join(' '));
      $('#bridge-stripe').parent().removeClass((index, css) => (css.match (/(^|\s)border\S+/g) || []).join(' '));
      $('#bridge-stripe').parent().addClass('bg-white border border-success');
      //
      $('#bridge-stripe span').after('<div class="mt-1"><small class="small-60 text-dark"><strong>Tudo certo!</strong> A próxima cobrança acontecerá em <u>' + moment.unix(assinatura.current_period_end).format('DD/MM/YYYY') + '</u>.</small></div>');
    }
    // PENDÊNCIA / ERRO DE COBRANÇA / ETC
    else {
      $('#bridge-stripe').parent().removeClass((index, css) => (css.match (/(^|\s)bg-\S+/g) || []).join(' '));
      $('#bridge-stripe').parent().removeClass((index, css) => (css.match (/(^|\s)border\S+/g) || []).join(' '));
      $('#bridge-stripe').parent().addClass('bg-danger-o-40 border border-danger');
      //
      $('#bridge-stripe span').after('<div class="mt-1"><small class="text-danger small-60"><strong>PENDÊNCIA</strong></small><i class="flaticon2-warning text-danger ml-2"></i><br><small class="text-dark"><u>Clique para resolver.</u></small></div>');
    }
  }
  // NENHUMA ASSINATURA ENCONTRADA
  else {
    $('#bridge-stripe').attr('href', '/assinatura');
    //
    $('#bridge-stripe').parent().removeClass((index, css) => (css.match (/(^|\s)bg-\S+/g) || []).join(' '));
    $('#bridge-stripe').parent().removeClass((index, css) => (css.match (/(^|\s)border\S+/g) || []).join(' '));
    $('#bridge-stripe').parent().addClass('bg-danger-o-40 border border-danger');
    //
    $('#bridge-stripe span').after('<div class="mt-1"><small class="text-danger small-60"><strong>NENHUM PLANO ATIVO</strong></small><i class="flaticon2-warning text-danger ml-2"></i><br><small class="text-dark"><u>Clique para resolver.</u></small></div>');
  }
  
  /*/
   * INTEGRAÇÕES HABILITADAS / DISPONÍVEIS
  /*/
  
  // ACTIVECAMPAIGN CONFIGURADO
  if (dashboard.activecampaign) {
    // INTEGRAÇÃO ATIVA
    if (dashboard.activecampaign.status === 'true') {
      $('#bridge-activecampaign .card-header').addClass('bg-success');
      $('#bridge-activecampaign .card-title').html('Conectado<i class="flaticon2-correct text-success ml-2"></i>');
      $('#bridge-activecampaign .card-subtitle').html('A conta está conectada.');
    }
    // INTEGRAÇÃO INATIVA
    else {
      $('#bridge-activecampaign .card-header').addClass('bg-danger');
      $('#bridge-activecampaign .card-title').html('Inativo<i class="flaticon2-warning text-danger ml-2"></i>');
      $('#bridge-activecampaign .card-subtitle').html('A integração está inativa.<br><strong>Clique aqui para resolver.</strong>');
    }
    //
    $('#ad-activecampaign').addClass('d-none');
    $('#bridge-activecampaign').removeClass('d-none');
  }
  // ACTIVECAMPAIGN NÃO CONFIGURADO
  else {
    $('#bridge-activecampaign').addClass('d-none');
    $('#ad-activecampaign').removeClass('d-none');
  }
  
  // RD STATION CONFIGURADO
  if (dashboard.rdstation) {
    // INTEGRAÇÃO ATIVA
    if (dashboard.rdstation.status === 'true') {
      let expires_at = parseInt(dashboard.rdstation.last_timestamp) + parseInt(dashboard.rdstation.expires_in);
      let is_valid   = parseInt(dashboard.rdstation.current_timestamp) < expires_at ? true : false;
      // INTEGRAÇÃO VÁLIDA
      if (is_valid) {
        $('#bridge-rdstation .card-header').addClass('bg-success');
        $('#bridge-rdstation .card-title').html('Conectado<i class="flaticon2-correct text-success ml-2"></i>');
        $('#bridge-rdstation .card-subtitle').html('A conta <strong>' + dashboard.rdstation.name + '</strong> está conectada.');
      }
      // INTEGRAÇÃO EXPIRADA
      else {
        $('#bridge-rdstation .card-header').addClass('bg-warning');
        $('#bridge-rdstation .card-title').html('Expirado<i class="flaticon2-warning text-warning ml-2"></i>');
        $('#bridge-rdstation .card-subtitle').html('A integração está expirada.<br><strong>Clique <a href="/rdstation">aqui</a> para resolver.</strong>');
      }
    }
    // INTEGRAÇÃO INATIVA
    else {
      $('#bridge-rdstation .card-header').addClass('bg-danger');
      $('#bridge-rdstation .card-title').html('Inativo<i class="flaticon2-warning text-danger ml-2"></i>');
      $('#bridge-rdstation .card-subtitle').html('A integração está inativa.<br><strong>Clique aqui para resolver.</strong>');
    }
    //
    $('#ad-rdstation').addClass('d-none');
    $('#bridge-rdstation').removeClass('d-none');
  }
  // RD STATION NÃO CONFIGURADO
  else {
    $('#bridge-rdstation').addClass('d-none');
    $('#ad-rdstation').removeClass('d-none');
  }
  
  // SMS CONFIGURADO
  if (dashboard.sms) {
    //
  }
  // SMS NÃO CONFIGURADO
  else {
    $('#bridge-sms').addClass('d-none');
    $('#ad-sms').removeClass('d-none');
  }
  
  // WHATSAPP CONFIGURADO
  if (dashboard.whatsapp) {
    // INTEGRAÇÃO ATIVA
    if (dashboard.whatsapp.status === 'true') {
      $('#bridge-whatsapp .card-header').addClass('bg-success');
      $('#bridge-whatsapp .card-title').html('Conectado<i class="flaticon2-correct text-success ml-2"></i>');
      $('#bridge-whatsapp .card-subtitle').html('A conta <strong>' + dashboard.whatsapp.sender_id + '</strong> está associada.');
    }
    // INTEGRAÇÃO INATIVA
    else {
      $('#bridge-whatsapp .card-header').addClass('bg-danger');
      $('#bridge-whatsapp .card-title').html('Inativo<i class="flaticon2-warning text-danger ml-2"></i>');
      $('#bridge-whatsapp .card-subtitle').html('A integração está inativa.<br><strong>Clique <a href="/whatsapp">aqui</a> para resolver.</strong>');
    }
    //
    $('#ad-whatsapp').addClass('d-none');
    $('#bridge-whatsapp').removeClass('d-none');
  }
  // WHATSAPP NÃO CONFIGURADO
  else {
    $('#bridge-whatsapp').addClass('d-none');
    $('#ad-whatsapp').removeClass('d-none');
  }
  
  /*
  if (!(dashboard.nuvemshop && (dashboard.nuvemshop.activecampaign || dashboard.nuvemshop.rdstation || dashboard.nuvemshop.sms || dashboard.nuvemshop.whatsapp))) {
    $('#integracoes-habilitadas').addClass('d-none');
    swal.fire({
      title: 'Bem-vindo(a) ao Kampana Hub! 🎉',
      html: '<div class="text-left">\
              <p>Aqui você integra e automatiza sua Nuvemshop em menos de 2 minutos, sem precisar escrever uma única linha de código. Vamos começar?</p>\
              <p><strong>1 - Conecte sua loja da Nuvemshop</strong>. Para isso, basta clicar na caixinha Nuvemshop aqui na página inicial.</p>\
              <p><strong>2 - Selecione um plano</strong>, também clicando na respectiva caixinha  do Financeiro aqui na página inicial ou no menu.</p>\
              <p><strong>3 - Habilite as integrações</strong>, que determinam em quais situações a Nuvemshop deve enviar dados para as plataformas.</p>\
              <p class="mb-0">Agora é só aproveitar: você pode trabalhar com todas as informações direto da plataforma desejada, traçando as melhores estratégias de acordo com o seu negócio.</p>\
            </div>',
      confirmButtonText: '<small>Clique para</small> Começar 🚀',
      customClass: {
        confirmButton: 'btn btn-primary',
      },
      padding: '10px',
      width: 1100
    });
  }
  //
  else $('#integracoes-habilitadas').removeClass('d-none');
  */
  
  if (!dashboard.nuvemshop) {
    $('.bridge').addClass('bridge-disabled');
    $('.ad').addClass('ad-disabled');
  }
  else {
    $('.bridge').removeClass('bridge-disabled');
    $('.ad').removeClass('ad-disabled');
  }
  
});