let datatable = {
  init: () => {
    // INSTANCIAR TABELA 
    $('#kt_datatable').KTDatatable({
      // CARREGAR E MANIPULAR OS DADOS
      data: {
        type: 'remote',
        source: {
          read: {
            method: 'GET',
            url: API_URL + '/sms/logs',
            map: (raw) => {
              raw.forEach(item => {
                item.created_at = moment(item.created_at).format('DD/MM/YYYY HH:mm:ss');
              });
              return raw;
            }
          }
        }
      },
      // DEFINIR LAYOUT
      layout: {
        scroll: false,
        footer: false
      },
      sortable: true,
      pagination: true,
      // DEFINIR FILTROS
      search: {
        input: $('#kt_datatable_search_query'),
        key: 'generalSearch'
      },
      // DEFINIR COLUNAS
        columns: [{
            field: 'id',
            title: '#',
            width: 50
          }, {
            field: 'descricao',
            title: 'Descrição',
            width: 600,
            template: (row) => {
              if (row.user_display_name) return row.descricao +  '<span class="label ml-3" data-toggle="tooltip" data-html="true" title="' + row.user_display_name + '<br>' + row.user_email + '"><i class="icon-sm la la-cog"></i></span>';
              // else if (row.rdstation_event_type == 'ORDER_PLACED') return row.descricao + '<span class="label label-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-shopping-cart text-white"></i></span>';
              // else if (row.rdstation_event_type == 'SALE') return row.descricao + '<span class="label label-success ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-dollar text-white"></i></span>';
              // else if (row.rdstation_lead_id) return row.descricao +  '<span class="label label-light-info ml-3" data-toggle="tooltip" data-html="true" title="' + row.nuvemshop_id + '<br>' + row.rdstation_lead_id + '"><i class="icon-sm la la-user"></i></span>';
              else return row.descricao;
            }
          }, {
            field: 'origem',
            title: 'Origem',
            width: 75
          }, {
            field: 'tipo',
            title: 'Tipo',
            textAlign: 'center',
            width: 75,
            template: (row) => {
              return '<span class="label font-weight-bold label-lg label-light-' + row.tipo + ' label-inline w-100 text-uppercase">' + row.tipo + '</span>';
            }
          }, {
            field: 'created_at',
            title: 'Data',
            width: 200
          }
        ],
        // TRADUÇÃO
        translate: kt_datatable_pt_br
    });
    // EXIBIR TOOLTIPS
    $('#kt_datatable').KTDatatable().on('datatable-on-layout-updated', function() {
      $('[data-toggle="tooltip"]').tooltip();
    });
  },
  reload: () => {
    $('#kt_datatable').KTDatatable().reload();
  }
}

let gui = {
  mphone: (phone) => {
    phone = phone.replace(/\D/g,'');
    phone = phone.replace(/^(\d)/,'+$1');
    phone = phone.replace(/(.{3})(\d)/,'$1($2');
    phone = phone.replace(/(.{6})(\d)/,'$1)$2');
    //
    if (phone.length == 12) phone=phone.replace(/(.{1})$/,'-$1');
    else if (phone.length == 13) phone = phone.replace(/(.{2})$/,'-$1');
    else if (phone.length == 14) phone = phone.replace(/(.{3})$/,'-$1');
    else if (phone.length == 15) phone = phone.replace(/(.{4})$/,'-$1');
    else if (phone.length > 15) phone = phone.replace(/(.{4})$/,'-$1');
    //
    return phone.replace('(', ' (').replace(')', ') ');
  },
  init: async() => {
    $('.btn-debug, .btn-update, .btn-delete').prop('disabled', true);
    //
    let sms = (await axios.get(API_URL + '/sms')).data;
    if (sms) {
      // CONTROLE DE STATUS
      if (sms.status == 'true') {
        // $('#card-status[class*="bg-"]').removeClass((index, css) =>  (css.match(/(^|\s)bg-\S+/g) || []).join(' '));
        // $('#card-status').addClass('bg-success');
        $('#kt_content').css('border-top', '10px solid ' + KTAppSettings.colors.theme.base.success);
        // LIBERAR TODOS OS COMANDOS
        $('.btn-debug, .btn-update, .btn-delete').prop('disabled', false);
        // DESABILITAR
        $('.btn-update').attr('data-label', 'desabilita');
        $('.btn-update span').html('Desabilitar');
        $('#alerta-status-sms').addClass('d-none');
      }
      else {
        // $('#card-status[class*="bg-"]').removeClass((index, css) =>  (css.match(/(^|\s)bg-\S+/g) || []).join(' '));
        // $('#card-status').addClass('bg-warning');
        $('#kt_content').css('border-top', '10px solid ' + KTAppSettings.colors.theme.base.warning);
        // ATUALIZAR INTEGRAÇÃO
        $('.btn-update, .btn-delete').prop('disabled', false);
        // $('#card-status[class*="bg-"]').removeClass((index, css) =>  (css.match(/(^|\s)bg-\S+/g) || []).join(' '));
        // $('#card-status').addClass('bg-warning');
        // HABILITAR
        $('.btn-update').attr('data-label', 'habilita');
        $('.btn-update span').html('Habilitar');
        $('#alerta-status-sms').removeClass('d-none');
      }
      // PONTOS DE INTEGRAÇÃO
      let nuvemshop = (await axios.get(API_URL + '/nuvemshop')).data;
      let integracoes;
      if (nuvemshop) integracoes = JSON.parse(nuvemshop.raw.integracoes);
      if (integracoes && integracoes.sms.status == true) {
        $('.webhook, .script').prop('disabled', false);
        $('#alerta-nuvemshop-sms').addClass('d-none');
        // AVALIAR WEBHOOKS DISPONÍVEIS E EXISTENTES
        let webhook_options = ['order_created', 'order_updated', 'order_paid', 'order_packed', 'order_fulfilled', 'order_cancelled'];
        if (integracoes.nuvemshop.webhooks) {
          webhook_options.forEach(webhook => {
            if (integracoes.nuvemshop.webhooks[webhook] && integracoes.nuvemshop.webhooks[webhook].tags.includes('sms')) $('#' + webhook.replace(/_/g, '-')).prop('checked', true);
            else $('#' + webhook.replace(/_/g, '-')).prop('checked', false);
          });
        } else webhook_options.forEach(webhook => $('#' + webhook.replace(/_/g, '-')).prop('checked', false));
        $('.webhook').on('change', async function() {
          try {
            // EXIBIR TELA DE CARREGAMENTO
            swal.fire({
              title: 'Aguarde...',
              text: 'Processando a solicitação.',
              onOpen: () => {
                swal.showLoading();
              },
              allowOutsideClick: false,
              allowEscapeKey: false
            });
            // ENVIAR REQUISIÇÃO PARA O BACK-END
            let webhooks = {};
            webhook_options.forEach(webhook => {
              webhooks[webhook] = $('#' + webhook.replace('_', '-')).prop('checked');
            });
            await axios.put(API_URL + '/nuvemshop/webhooks', {
              webhooks: webhooks,
              tag: 'sms'
            });
            datatable.reload();
            // DEU CERTO
            $('#modal').modal('hide');
            swal.close();
            swal.fire({
              icon: 'success',
              title: 'Excelente!',
              text: 'A integração foi atualizada com sucesso.'
            });
          }
          //
          catch(e) {
            console.log(e);
            swal.close();
            swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Algo errado não está certo.',
              footer: 'Confira o erro no log do navegador.'
            });
          }
        });
        // AVALIAR SCRIPTS DISPONÍVEIS E EXISTENTES
        let script_options = ['abandoned_cart', 'tracking_code'];
        if (integracoes.nuvemshop.scripts) {
          script_options.forEach(script => {
            if (integracoes.nuvemshop.scripts[script] && integracoes.nuvemshop.scripts[script].tags.includes('sms')) $('#' + script.replace(/_/g, '-')).prop('checked', true);
            else $('#' + script.replace(/_/g, '-')).prop('checked', false);
          });
        } else script_options.forEach(script => $('#' + script.replace(/_/g, '-')).prop('checked', false));
        $('.script').on('change', async function() {
          try {
            // EXIBIR TELA DE CARREGAMENTO
            swal.fire({
              title: 'Aguarde...',
              text: 'Processando a solicitação.',
              onOpen: () => {
                swal.showLoading();
              },
              allowOutsideClick: false,
              allowEscapeKey: false
            });
            // ENVIAR REQUISIÇÃO PARA O BACK-END
            let scripts = {};
            script_options.forEach(script => {
              scripts[script] = $('#' + script.replace('_', '-')).prop('checked');
            });
            await axios.put(API_URL + '/nuvemshop/scripts', {
              scripts: scripts,
              tag: 'sms'
            });
            datatable.reload();
            // DEU CERTO
            $('#modal').modal('hide');
            swal.close();
            swal.fire({
              icon: 'success',
              title: 'Excelente!',
              text: 'A integração foi atualizada com sucesso.'
            });
          }
          //
          catch(e) {
            console.log(e);
            swal.close();
            swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Algo errado não está certo.',
              footer: 'Confira o erro no log do navegador.'
            });
          }
        });
      } 
      else {
        $('.webhook, .script').prop('disabled', true);
        $('#alerta-nuvemshop-sms').removeClass('d-none');
      }
    }
    // CRIAR INTEGRAÇÃO
    else {
      // $('#card-status[class*="bg-"]').removeClass((index, css) =>  (css.match(/(^|\s)bg-\S+/g) || []).join(' '));
      // $('#card-status').addClass('bg-danger');
      $('#kt_content').css('border-top', '10px solid ' + KTAppSettings.colors.theme.base.danger);
      // CONFIGURAR INTEGRAÇÃO!
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Nenhuma Integração Configurada',
        text: 'Para que o Kampana Hub funcione corretamente, você precisa realizar a integração com o Envio de SMS.',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        //footer: 'Atenção! O seu plano atual não permite a personalização da conta comercial do Whatsapp que será configurado a integração. Portanto, será utilizado a conta comercial padrão Kampana Hub.',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0',
          footer: 'text-center'
        },
        allowOutsideClick: false,
        allowEscapeKey: false
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        try {
          // EXIBIR TELA DE CARREGAMENTO
          swal.fire({
            title: 'Aguarde...',
            text: 'Processando a solicitação.',
            onOpen: () => {
              swal.showLoading();
            },
            allowOutsideClick: false,
            allowEscapeKey: false
          });
          // ENVIAR REQUISIÇÃO PARA O BACK-END 
          let sms = (await axios.post(API_URL + '/sms/login')).data;
          // DEU BOM
          swal.close();
          swal.fire({
            icon: 'success',
            title: 'Excelente!',
            text: 'A integração foi adicionada com sucesso.'
          });
          datatable.reload();
          gui.init();
        }
        //
        catch (e) {
          console.log(e);
          swal.close();
          swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Algo errado não está certo.',
            footer: 'Confira o erro no log do navegador.'
          });
        }
      }
      //
      else window.location.href = '/';
    }
  }
}

let api = {
  update: async (label) => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo ' + label + 'r esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        await axios.put(API_URL + '/sms');
        datatable.reload();
        gui.init();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi ' + label + 'da com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  },
  delete: async () => {
    try {
      // SOLICITAR AO USUÁRIO A CONFIRMAÇÃO
      let projeto = await swal.fire({
        icon: 'warning',
        title: 'Atenção!',
        text: 'Deseja mesmo excluir esta integração?',
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Continuar',
        customClass: {
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      // VALIDAR CONTINUAÇÃO
      if (projeto.value) {
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        await axios.delete(API_URL + '/sms');
        datatable.reload();
        gui.init();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          text: 'A integração foi excluída com sucesso.',
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

let custom = {
  debug: async(id) => {
    try {
      // PERGUNTAR O TELEFONE
      let temp = await swal.fire({
        icon: 'question',
        title: 'Vamos lá!',
        text: 'Para qual número enviar a mensagem?',
        input: 'text',
        inputPlaceholder: '(62) 99503-0938',
        didOpen: () => {
          let SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          };
          let spOptions = {
            onKeyPress: function(val, e, field, options) {
              field.mask(SPMaskBehavior.apply({}, arguments), options);
            },
            clearIfNotMatch: true
          };
          $('.phone').mask(SPMaskBehavior, spOptions);
        },
        inputValidator: (value) => {
          return new Promise((resolve) => {
            if (value) resolve();
            else resolve('Você precisa inserir um número.');
          });
        },
        showCancelButton: true,
        reverseButtons: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Prosseguir',
        customClass: {
          input: 'phone',
          confirmButton: 'btn btn-primary mt-0',
          cancelButton: 'btn btn-light mt-0'
        }
      });
      if (temp.value) {
        let phone = temp.value.replace(/\D/g, '');
        // if (phone.length == 11) phone = phone.slice(0, 2) + phone.slice(3, phone.length);
        phone = '55' + phone;
        // EXIBIR TELA DE CARREGAMENTO
        swal.fire({
          title: 'Aguarde...',
          text: 'Processando a solicitação.',
          onOpen: () => {
            swal.showLoading();
          },
          allowOutsideClick: false,
          allowEscapeKey: false
        });
        // ENVIAR REQUISIÇÃO PARA O BACK-END
        let debug = (await axios.post(API_URL + '/sms/debug', {
          numberPhone: phone,
          message: 'Esta é uma mensagem de teste do Kampana Hub. 😊',
          flashSms: false
        })).data;
        //
        datatable.reload();
        gui.init();
        // DEU CERTO
        swal.close();
        swal.fire({
          icon: 'success',
          title: 'Excelente!',
          html: '<code>' + JSON.stringify(debug) + '</code>',
          width: '90%'
        });
      }
    }
    //
    catch (e) {
      console.log(e);
      swal.close();
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Algo errado não está certo.',
        footer: 'Confira o erro no log do navegador.'
      });
    }
  }
}

jQuery(document).ready(() => {
  // INICIALIZAR
  datatable.init();
  gui.init();
  // CONFIGURAR CRUD
  $(document).on('click', '.btn-update', function() {
    api.update($(this).attr('data-label'));
  });
  $(document).on('click', '.btn-delete', function() {
    api.delete();
  });
  // CONFIGURAR CUSTOM
  $(document).on('click', '.btn-debug', function() {
    custom.debug();
  });
  // AVALIAR SE O CARREGAMENTO É UM CALLBACK
  let params = new URLSearchParams(window.location.search);
  if (params.get('sucesso')) {
    let sucesso = (params.get('sucesso') === 'true');
    // DEU CERTO
    if (sucesso) swal.fire({
      icon: 'success',
      title: 'Excelente!',
      text: 'A integração foi adicionada com sucesso.',
    });
    // DEU ERRADO
    else swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Algo errado não está certo.',
      footer: 'Por favor, tente novamente.'
    });
    // LIMPAR A FLAG DA URL
    window.history.replaceState({}, document.title, location.protocol + '//' + location.host + location.pathname);
  }
});