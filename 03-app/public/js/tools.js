var tools = {
  cpf: {
    isValid: function(cpf) {
      cpf = cpf.replace(/[^\d]+/g, '');
      var invalid = ['00000000000', '11111111111', '22222222222', '33333333333', '44444444444', '55555555555', '66666666666', '77777777777', '88888888888', '99999999999'];
      if (cpf == '' || cpf.length != 11 || invalid.some(x => x == cpf)) return false;
      var add = 0;
      for (i = 0; i < 9; i++) {
        add += parseInt(cpf.charAt(i)) * (10 - i);
      }
      var rev = 11 - (add % 11);
      if (rev == 10 || rev == 11) rev = 0;
      if (rev != parseInt(cpf.charAt(9))) return false;
      add = 0;
      for (i = 0; i < 10; i++) {
        add += parseInt(cpf.charAt(i)) * (11 - i);
      }
      rev = 11 - (add % 11);
      if (rev == 10 || rev == 11) rev = 0;
      if (rev != parseInt(cpf.charAt(10))) return false;
      return true;
    },
    getQuality: function(cpf) {
      return new Promise(function(resolve, reject) {
        if (tools.cpf.isValid(cpf)) {
          $.ajax({
            url: 'https://crm.ojc.com.br:4433/InternalApi/api/pessoafisica/' + cpf,
            type: 'GET',
            headers: {
              'Content-Type': 'application/json'
            },
            success: function(response) {
              if (!response.erro) resolve(response);
              else resolve(null);
            },
            error: function(response) {
              resolve(null);
            }
          });
        } else resolve(null);
      });
    }
  },
  cnpj: {
    isValid: function(cnpj) {
      cnpj = cnpj.replace(/[^\d]+/g, '');
      if (cnpj == '') return false;
      if (cnpj.length != 14) return false;
      if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
        return false;
      var tamanho = cnpj.length - 2
      var numeros = cnpj.substring(0, tamanho);
      var digitos = cnpj.substring(tamanho);
      var soma = 0;
      var pos = tamanho - 7;
      for (var i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2) pos = 9;
      }
      var resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(0)) return false;
      tamanho = tamanho + 1;
      numeros = cnpj.substring(0, tamanho);
      soma = 0;
      pos = tamanho - 7;
      for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2) pos = 9;
      }
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(1)) return false;
      return true;
    }
  },
  cep: {
    isValid: function(cep) {
      var validacep = /^[0-9]{8}$/;
      // CEP VÁLIDO
      if (validacep.test(cep)) return true
      // CEP INVÁLIDO
      else return false;
    },
    getQuality: function(cep) {
      return new Promise(function(resolve, reject) {
        if (tools.cep.isValid(cep)) {
          $.ajax({
            url: 'https://viacep.com.br/ws/' + cep + '/json',
            type: 'GET',
            headers: {
              'Content-Type': 'application/json'
            },
            success: function(response) {
              if (!response.erro) resolve(response);
              else resolve(null);
            },
            error: function(response) {
              resolve(null);
            }
          });
        } else resolve(null);
      });
    }
  }
}