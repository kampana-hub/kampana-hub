const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const passport = require('passport');
const querystring = require('querystring');
const router = express.Router();
const stripe = require('stripe')(process.env.STP_SECRET_KEY);
const util = require('util');

/**
 * LAYOUT
 */

let layout = {
  canonical: process.env.HUB_AMS_URL,
  //
  prefix:    process.env.LAYOUT_PREFIX,
  //
  style:     process.env.LAYOUT_STYLE,
  logo:      process.env.LAYOUT_LOGO,
  favicon:   process.env.LAYOUT_FAVICON,
  //
  api_url:   process.env.LAYOUT_API_URL,
  aws_s3:    process.env.LAYOUT_AWS_S3,
  //
  gtag:      process.env.LAYOUT_GTAG === 'TRUE'
}

/**
 * INÍCIO
 */

router.get('/activecampaign', ensureLoggedIn, (req, res, next) => {
  res.render('activecampaign', {
    layout: layout,
    title: 'ActiveCampaign',
    user: req.session.passport.user
  });
});

router.get('/', ensureLoggedIn, async (req, res, next) => {
  res.render('dashboard', {
    layout: layout,
    title: 'Dashboard',
    user: req.session.passport.user,
  });
});

router.get('/perfil', ensureLoggedIn, (req, res, next) => {
  res.render('perfil', {
    layout: layout,
    title: 'Minha Conta',
    user: req.session.passport.user
  });
});

router.get('/rdstation', ensureLoggedIn, (req, res, next) => {
  res.render('rdstation', {
    layout: layout,
    title: 'RD Station',
    user: req.session.passport.user
  });
});

router.get('/nuvemshop', ensureLoggedIn, (req, res, next) => {
  res.render('nuvemshop', {
    layout: layout,
    title: 'Nuvemshop',
    user: req.session.passport.user
  });
});

router.get('/whatsapp', ensureLoggedIn, (req, res, next) => {
  res.render('whatsapp', {
    layout: layout,
    title: 'Whatsapp',
    user: req.session.passport.user
  });
});

router.get('/sms', ensureLoggedIn, (req, res, next) => {
  res.render('sms', {
    layout: layout,
    title: 'SMS',
    user: req.session.passport.user
  });
});

/*
 *
 */

router.get('/financeiro', ensureLoggedIn, async (req, res, next) => {
  if (req.session.kampana.cliente.id_stripe) {
    let portal = await stripe.billingPortal.sessions.create({
      customer: req.session.kampana.cliente.id_stripe,
      return_url: process.env.HUB_APP_URL
    });
    res.redirect(portal.url);
  }
  //
  else {
    let session = await stripe.checkout.sessions.create({
      mode: 'subscription',
      payment_method_types: ['card'],
      // customer: req.session.kampana.cliente.id_stripe,
      customer_email: req.session.passport.user.emails.slice().shift().value,
      line_items: [{
        price: process.env.STP_PRICE_ID,
        quantity: 1
      }],
      subscription_data: {
        trial_period_days: 7
      },
      success_url: process.env.HUB_APP_URL,
      cancel_url: process.env.HUB_APP_URL
    });
    // RENDERIZAR A PÁGINA
    res.render('assinatura', {
      layout: layout,
      sessionId: session.id,
      stripe: process.env.STP_PUBLIC_KEY
    });
  }
});

/**/
router.get('/assinatura', ensureLoggedIn, async (req, res, next) => {
  let session = {};
  // PRODUTO ESTÁTICO - ASSINATURA RD + NUVEMSHOP
  if (req.session.kampana.cliente.id_stripe) {
    session = await stripe.checkout.sessions.create({
      mode: 'subscription',
      payment_method_types: ['card'],
      customer: req.session.kampana.cliente.id_stripe,
      line_items: [{
        price: process.env.STP_PRICE_ID,
        quantity: 1
      }],
      success_url: process.env.HUB_APP_URL,
      cancel_url: process.env.HUB_APP_URL
    });
  }
  //
  else {
    session = await stripe.checkout.sessions.create({
      mode: 'subscription',
      payment_method_types: ['card'],
      // customer: req.session.kampana.cliente.id_stripe,
      customer_email: req.session.passport.user.emails.slice().shift().value,
      line_items: [{
        price: process.env.STP_PRICE_ID,
        quantity: 1
      }],
      subscription_data: {
        trial_period_days: 7
      },
      success_url: process.env.HUB_APP_URL,
      cancel_url: process.env.HUB_APP_URL
    });
  }
  // RENDERIZAR A PÁGINA
  res.render('assinatura', {
    layout: layout,
    sessionId: session.id,
    stripe: process.env.STP_PUBLIC_KEY
  });
});
/**/

/**
 * SIGNUP ROUTE
 */

router.get('/cadastro', (req, res, next) => {
  res.render('cadastro', {
    layout: layout,
    title: 'Cadastro',
    stripe: process.env.STP_PUBLIC_KEY
  });
});

/**
 * AUTHENTICATION ROUTES
 */

router.get(
  '/login',
  passport.authenticate('auth0', {
    clientID: process.env.AUTH0_APP_CLIENT_ID,
    domain: process.env.AUTH0_DOMAIN,
    redirectUri: process.env.AUTH0_APP_CALLBACK_URL,
    audience: 'https://' + process.env.AUTH0_DOMAIN + '/userinfo',
    responseType: 'code',
    scope: 'openid email profile'
  }),
  (req, res) => {
    res.redirect('/');
  }
);

router.get(
  '/callback',
  passport.authenticate('auth0', {
    failureRedirect: '/'
  }),
  async (req, res) => {
    let domain = process.env.AUTH0_DOMAIN.slice().split('.').shift()
    // CRIAR OBJETO KAMPANA
    let kampana = {};
    // CARREGAR OS DADOS DO PRIMEIRO PROJETO
    kampana.projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.passport.user._json['https://' + domain + ':auth0:com/app_metadata'].projetos.slice())).data.shift();
    // CARREGAR O CLIENTE DO PRIMEIRO PROJETO
    kampana.cliente = (await axios.get(process.env.API_URL + '/cliente/' + kampana.projeto.cliente_id)).data.shift();
    // SALVAR OBJETO NA SESSÃO
    req.session.kampana = kampana;
    // REDIRECIONAR
    res.redirect(req.session.returnTo || '/');
  }
);

router.get('/logout', (req, res) => {
  req.logout();
  var logoutURL = new URL(
    util.format('https://%s/v2/logout', process.env.AUTH0_DOMAIN)
  );
  var searchString = querystring.stringify({
    client_id: process.env.AUTH0_APP_CLIENT_ID,
    returnTo: process.AUTH0_APP_LOGOUT_URL
  });
  logoutURL.search = searchString;
  res.redirect(logoutURL);
});

module.exports = router;