const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');

const router = express.Router();

/*/
 * BASIC ROUTES
/*/

// LER AS INFORMAÇÕES DE UMA INTEGRAÇÃO
router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let nuvemshop = (await axios.get(process.env.API_URL + '/v_nuvemshop?_where=(projeto_id,eq,' + req.session.kampana.projeto.id + ')')).data.shift();
    if (nuvemshop) nuvemshop.raw = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // ENVIAR RESPOSTA POSITIVA
    res.json(nuvemshop);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ALTERAR O STATUS DE UMA INTEGRAÇÃO
router.put('/', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // MUDAR  STATUS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.nuvemshop.status = !projeto.integracoes.nuvemshop.status;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    await axios.post(process.env.API_URL + '/log_nuvemshop', {
      tipo: projeto.integracoes.nuvemshop.status ? 'info' : 'warning',
      descricao: projeto.integracoes.nuvemshop.status ? 'Integração habilitada com sucesso.' : 'Integração desabilitada com sucesso.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_nuvemshop', {
      tipo: 'danger',
      descricao: 'Erro na alteração do status da integração.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// EXCLUIR UMA INTEGRAÇÃO
router.delete('/', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // REMOVER A INTEGRAÇÃO DO PROJETO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    delete projeto.integracoes.nuvemshop;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_nuvemshop', {
      tipo: 'warning',
      descricao: 'Integração excluída com sucesso.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_nuvemshop', {
      tipo: 'danger',
      descricao: 'Erro na exclusão da integração.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/*/
 * CUSTOM ROUTES
/*/

// TESTAR UMA INTEGRAÇÃO
router.get('/debug', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // CARREGAR APP - NUVEMSHOP
    let app = (await axios.get(process.env.API_URL + '/app_nuvemshop/' + projeto.app_nuvemshop_id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA DE TESTES NA API DO NUVEMSHOP
    let debug = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/store', {
      headers: {
        'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
        'User-Agent': app.user_agent
      }
    })).data;
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_nuvemshop', {
      tipo: 'info',
      descricao: 'Integração testada com sucesso.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.json(debug);
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_nuvemshop', {
      tipo: 'danger',
      descricao: 'Erro no teste da integração.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// LISTAR O LOG DE UMA INTEGRAÇÃO
router.get('/logs', ensureLoggedIn, async (req, res, next) => {
  try {
    let logs = (await axios.get(process.env.API_URL + '/v_nuvemshop_log?_where=(projeto_id,eq,' + req.session.kampana.projeto.id + ')&_sort=-id')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(logs);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ATUALIZAR WEBHOOKS DE UMA INTEGRAÇÃO
router.put('/webhooks', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // CARREGAR APP - NUVEMSHOP
    let app = (await axios.get(process.env.API_URL + '/app_nuvemshop/' + projeto.app_nuvemshop_id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // COMPARAR REQUISIÇÃO COM DADOS ATUAIS
    projeto.integracoes.nuvemshop.webhooks = projeto.integracoes.nuvemshop.webhooks || {};
    let options = ['order_created', 'order_updated', 'order_paid', 'order_packed', 'order_fulfilled', 'order_cancelled'];
    let chamadas = [];
    //
    let integracoes_habilitadas = [];
    let integracoes_desabilitadas = [];
    let tags_habilitadas = [];
    let tags_desabilitadas = [];
    //
    options.forEach(webhook => {
      // EXISTE ESSA INTEGRAÇÃO CONFIGUADA HOJE?
      if (projeto.integracoes.nuvemshop.webhooks[webhook]) {
        // ELA DEVE SER REMOVIDA? SOMENTE DEVE SER REMOVIDA CASO ESTEJA DESABILITADA EM TODAS AS PLATAFORMAS (RD STATION, ACTIVE, WHATSAPP ou SMS)
        if (!req.body.webhooks[webhook] && projeto.integracoes.nuvemshop.webhooks[webhook].tags.length == 1) chamadas.push(new Promise(async (resolve, reject) => {
          // SUBIR NA FILA DE CHAMADAS À API
          let chamada = (await axios.delete(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/webhooks/' + projeto.integracoes.nuvemshop.webhooks[webhook].id, {
            headers: {
              'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
              'User-Agent': app.user_agent
            }
          })).data;
          integracoes_desabilitadas.push(webhook);
          tags_desabilitadas = [req.body.tag];
          resolve(delete projeto.integracoes.nuvemshop.webhooks[webhook]);
        }));
        else if (!req.body.webhooks[webhook]) {
          chamadas.push(new Promise(async (resolve, reject) => {
            // SETANDO O ATRIBUTOS DAS TAGS
            let tags = projeto.integracoes.nuvemshop.webhooks[webhook].tags;
            // TAG - RD STATION
            if (req.body.tag == 'rdstation') tags.indexOf('rdstation') >= 0 ? tags.splice(tags.indexOf('rdstation'), 1) : '';
            // TAG - ACTIVECAMPAIGN
            if (req.body.tag == 'activecampaign') tags.indexOf('activecampaign') >= 0 ? tags.splice(tags.indexOf('activecampaign'), 1) : '';
            // TAG - WHATSAPP
            if (req.body.tag == 'whatsapp') tags.indexOf('whatsapp') >= 0 ? tags.splice(tags.indexOf('whatsapp'), 1) : '';
            // TAG - SMS
            if (req.body.tag == 'sms') tags.indexOf('sms') >= 0 ? tags.splice(tags.indexOf('sms'), 1) : '';
            // LOG
            integracoes_desabilitadas.push(webhook);
            tags_desabilitadas = tags;
            resolve(projeto.integracoes.nuvemshop.webhooks[webhook].tags = tags);
          }));
        }  
        else {
          chamadas.push(new Promise(async (resolve, reject) => {
            // SETANDO O ATRIBUTOS DAS TAGS
            let tags = projeto.integracoes.nuvemshop.webhooks[webhook].tags;
            // TAG - RD STATION
            if (req.body.webhooks[webhook] && req.body.tag == 'rdstation') tags.indexOf('rdstation') === -1 ? tags.push('rdstation') : '';
            // TAG - ACTIVECAMPAIGN
            if (req.body.webhooks[webhook] && req.body.tag == 'activecampaign') tags.indexOf('activecampaign') === -1 ? tags.push('activecampaign') : '';
            // TAG - WHATSAPP
            if (req.body.webhooks[webhook] && req.body.tag == 'whatsapp') tags.indexOf('whatsapp') === -1 ? tags.push('whatsapp') : '';
            // TAG - SMS
            if (req.body.webhooks[webhook] && req.body.tag == 'sms') tags.indexOf('sms') === -1 ? tags.push('sms') : '';
            // LOG
            integracoes_habilitadas.push(webhook);
            tags_habilitadas = tags;
            resolve(projeto.integracoes.nuvemshop.webhooks[webhook].tags = tags);
          }));
        }
      }
      //
      else {
        // ELA DEVE SER CRIADA?
        if (req.body.webhooks[webhook]) chamadas.push(new Promise(async (resolve, reject) => {
          // SUBIR NA FILA DE CHAMADAS À API
          let chamada = (await axios.post(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/webhooks',
            // OBJETO
            {
              event: webhook.replace('_', '/'),
              url: process.env.HUB_API_URL + '/webhooks/nuvemshop/orders/' + req.session.kampana.projeto.id
            },
            // AUTENTICAÇÃO
            {
              headers: {
                'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
                'User-Agent': app.user_agent
              }
            }
          )).data;
          // SETANDO O ATRIBUTOS DAS TAGS
          chamada.tags = [];
          // TAG - RD STATION
          if (req.body.webhooks[webhook] && req.body.tag == 'rdstation') chamada.tags.indexOf('rdstation') === -1 ? chamada.tags.push('rdstation') : '';
          // TAG - ACTIVECAMPAIGN
          if (req.body.webhooks[webhook] && req.body.tag == 'activecampaign') chamada.tags.indexOf('activecampaign') === -1 ? chamada.tags.push('activecampaign') : '';
          // TAG - WHATSAPP
          if (req.body.webhooks[webhook] && req.body.tag == 'whatsapp') chamada.tags.indexOf('whatsapp') === -1 ? chamada.tags.push('whatsapp') : '';
          // TAG - SMS
          if (req.body.webhooks[webhook] && req.body.tag == 'sms') chamada.tags.indexOf('sms') === -1 ? chamada.tags.push('sms') : '';
          // LOG
          integracoes_habilitadas.push(webhook);
          tags_habilitadas = chamada.tags;
          resolve(projeto.integracoes.nuvemshop.webhooks[webhook] = chamada);
        }));
      }
    });
    // REALIZAR CHAMADAS NA API DO NUVEMSHOP
    Promise.all(chamadas).then(async () => {
      if (Object.entries(projeto.integracoes.nuvemshop.webhooks).length === 0) delete projeto.integracoes.nuvemshop.webhooks;
      projeto.integracoes = JSON.stringify(projeto.integracoes);
      // ATUALIZAR PROJETO
      await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
        integracoes: projeto.integracoes
      });
      // CRIAR O LOG
      let descricao = '';
      if (integracoes_habilitadas.length > 0 ) descricao = descricao + 'Eventos habilitados: ' + integracoes_habilitadas.join(', ') + '.' + ' Tags habilitadas: ' + tags_habilitadas.join(', ') +  '. ';
      if (integracoes_desabilitadas.length > 0) descricao = descricao + 'Eventos desabilitados: ' + integracoes_desabilitadas.join(', ') + '.' + ' Tags desabilitadas: ' + tags_desabilitadas.join(', ') +  '. ';
      if (descricao) {
        await axios.post(process.env.API_URL + '/log_nuvemshop', {
          tipo: 'warning',
          descricao: descricao,
          origem: 'app',
          extra: JSON.stringify({
            user: {
              id: req.session.passport.user.id,
              displayName: req.session.passport.user.displayName,
              email: req.session.passport.user.emails.slice().shift().value
            }
          }),
          projeto_id: req.session.kampana.projeto.id
        });
      }
      // ENVIAR RESPOSTA POSITIVA
      res.status(200).send('Ok');
    });
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_nuvemshop', {
      tipo: 'danger',
      descricao: 'Erro na atualização dos eventos.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ATUALIZAR SCRIPTS DE UMA INTEGRAÇÃO
router.put('/scripts', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // CARREGAR APP - NUVEMSHOP
    let app = (await axios.get(process.env.API_URL + '/app_nuvemshop/' + projeto.app_nuvemshop_id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // COMPARAR REQUISIÇÃO COM DADOS ATUAIS
    projeto.integracoes.nuvemshop.scripts = projeto.integracoes.nuvemshop.scripts || {};
    projeto.integracoes.rdstation ? projeto.integracoes.rdstation.custom_fields = projeto.integracoes.rdstation.custom_fields || {}: '';
    let options = ['tracking_code_ac', 'tracking_code_rd', 'abandoned_cart'];
    let chamadas = [];
    //
    let integracoes_habilitadas = [];
    let integracoes_desabilitadas = [];
    let tags_habilitadas = [];
    let tags_desabilitadas = [];
    //
    options.forEach(script => {
      // EXISTE ESSA INTEGRAÇÃO CONFIGUADA HOJE?
      if (projeto.integracoes.nuvemshop.scripts[script]) {
        // ELA DEVE SER REMOVIDA?
        if (!req.body.scripts[script] && projeto.integracoes.nuvemshop.scripts[script].tags.length == 1) chamadas.push(new Promise(async (resolve, reject) => {
          // SUBIR NA FILA DE CHAMADAS À API
          let chamada = (await axios.delete(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/scripts/' + projeto.integracoes.nuvemshop.scripts[script].id, {
            headers: {
              'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
              'User-Agent': app.user_agent
            }
          })).data;
          integracoes_desabilitadas.push(script);
          tags_desabilitadas = [req.body.tag];
          resolve(delete projeto.integracoes.nuvemshop.scripts[script]);
        }));
        else if (!req.body.scripts[script]) {
          chamadas.push(new Promise(async (resolve, reject) => {
            // SETANDO O ATRIBUTOS DAS TAGS
            let tags = projeto.integracoes.nuvemshop.scripts[script].tags;
            // TAG - RD STATION
            if (req.body.tag == 'rdstation') tags.indexOf('rdstation') >= 0 ? tags.splice(tags.indexOf('rdstation'), 1) : '';
            // TAG - ACTIVECAMPAIGN
            if (req.body.tag == 'activecampaign') tags.indexOf('activecampaign') >= 0 ? tags.splice(tags.indexOf('activecampaign'), 1) : '';
            // TAG - WHATSAPP
            if (req.body.tag == 'whatsapp') tags.indexOf('whatsapp') >= 0 ? tags.splice(tags.indexOf('whatsapp'), 1) : '';
            // TAG - SMS
            if (req.body.tag == 'sms') tags.indexOf('sms') >= 0 ? tags.splice(tags.indexOf('sms'), 1) : '';
            // LOG
            integracoes_desabilitadas.push(script);
            tags_desabilitadas = tags;
            resolve(projeto.integracoes.nuvemshop.scripts[script].tags = tags);
          }));
        }  
        else {
          chamadas.push(new Promise(async (resolve, reject) => {
            // SETANDO O ATRIBUTOS DAS TAGS
            let tags = projeto.integracoes.nuvemshop.scripts[script].tags;
            // TAG - RD STATION
            if (req.body.scripts[script] && req.body.tag == 'rdstation') tags.indexOf('rdstation') === -1 ? tags.push('rdstation') : '';
            // TAG - ACTIVECAMPAIGN
            if (req.body.scripts[script] && req.body.tag == 'activecampaign') tags.indexOf('activecampaign') === -1 ? tags.push('activecampaign') : '';
            // TAG - WHATSAPP
            if (req.body.scripts[script] && req.body.tag == 'whatsapp') tags.indexOf('whatsapp') === -1 ? tags.push('whatsapp') : '';
            // TAG - SMS
            if (req.body.scripts[script] && req.body.tag == 'sms') tags.indexOf('sms') === -1 ? tags.push('sms') : '';
            // LOG
            integracoes_habilitadas.push(script);
            tags_habilitadas = tags;
            resolve(projeto.integracoes.nuvemshop.scripts[script].tags = tags);
          }));
        }
      }
      //
      else {
        // ELA DEVE SER CRIADA?
        if (req.body.scripts[script]) chamadas.push(new Promise(async (resolve, reject) => {
          let objeto = {};
          // DETERMINAR OBJETO
          if (script == 'tracking_code_rd') {
            objeto = {
              src: projeto.integracoes.rdstation.tracking_code,
              event: 'onload',
              where: 'store'
            }
          } else if (script == 'tracking_code_ac') {
            objeto = {
              src: process.env.HUB_API_URL + '/js/activecampaign/tracking-code.js?projeto_id=' + req.session.kampana.projeto.id,
              event: 'onload',
              where: 'store'
            }
          }
          else if (script === 'abandoned_cart') {
            objeto = {
              src: process.env.HUB_API_URL + '/js/nuvemshop/abandoned-cart.js',
              event: 'onload',
              where: 'store,checkout'
            }
          }
          // SUBIR NA FILA DE CHAMADAS À API
          let chamada = (await axios.post(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/scripts',
            // OBJETO
            objeto,
            // AUTENTICAÇÃO
            {
              headers: {
                'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
                'User-Agent': app.user_agent
              }
            }
          )).data;
          // SETANDO O ATRIBUTOS DAS TAGS
          chamada.tags = [];
          // TAG - RD STATION
          if (req.body.scripts[script] && req.body.tag == 'rdstation') chamada.tags.indexOf('rdstation') === -1 ? chamada.tags.push('rdstation') : '';
          // TAG - ACTIVECAMPAIGN
          if (req.body.scripts[script] && req.body.tag == 'activecampaign') chamada.tags.indexOf('activecampaign') === -1 ? chamada.tags.push('activecampaign') : '';
          // TAG - WHATSAPP
          if (req.body.scripts[script] && req.body.tag == 'whatsapp') chamada.tags.indexOf('whatsapp') === -1 ? chamada.tags.push('whatsapp') : '';
          // TAG - SMS
          if (req.body.scripts[script] && req.body.tag == 'sms') chamada.tags.indexOf('sms') === -1 ? chamada.tags.push('sms') : '';
          // LOG
          integracoes_habilitadas.push(script);
          tags_habilitadas = chamada.tags;
          resolve(projeto.integracoes.nuvemshop.scripts[script] = chamada);
        }));
      }
    });
    // REALIZAR CHAMADAS NA API DO NUVEMSHOP
    Promise.all(chamadas).then(async () => {
      if (Object.entries(projeto.integracoes.nuvemshop.scripts).length === 0) delete projeto.integracoes.nuvemshop.scripts;
      projeto.integracoes = JSON.stringify(projeto.integracoes);
      // ATUALIZAR PROJETO
      await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
        integracoes: projeto.integracoes
      });
       // CRIAR LOG
      let descricao = '';
      if (integracoes_habilitadas.length > 0 ) descricao = descricao + 'Scripts habilitados: ' + integracoes_habilitadas.join(', ') + '.' + ' Tags habilitadas: ' + tags_habilitadas.join(', ') +  '. ';
      if (integracoes_desabilitadas.length > 0) descricao = descricao + 'Scripts desabilitados: ' + integracoes_desabilitadas.join(', ') + '.' + ' Tags desabilitadas: ' + tags_desabilitadas.join(', ') +  '. ';
      if (descricao) {
        await axios.post(process.env.API_URL + '/log_nuvemshop', {
          tipo: 'warning',
          descricao: descricao,
          origem: 'app',
          extra: JSON.stringify({
            user: {
              id: req.session.passport.user.id,
              displayName: req.session.passport.user.displayName,
              email: req.session.passport.user.emails.slice().shift().value
            }
          }),
          projeto_id: req.session.kampana.projeto.id
        });
      }
      // ENVIAR RESPOSTA POSITIVA
      res.status(200).send('Ok');
    });
  } catch (e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_nuvemshop', {
      tipo: 'danger',
      descricao: 'Erro na atualização dos scripts.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/*/
 * AUTHENTICATION ROUTES
/*/

router.get('/login', ensureLoggedIn, async (req, res, next) => {
  // REDIRECIONAR PARA O AMBIENTE DE AUTORIZAÇÃO DO NUVEMSHOP
  res.redirect('https://www.nuvemshop.com.br/apps/' + process.env.NUVEMSHOP_CLIENT_ID + '/authorize');
});

router.get('/:id/callback', async (req, res, next) => {
  // USUÁRIO LOGADO - ASSOCIANDO DIRETAMENTE A UMA CONTA EXISTENTE
  if (req.session.passport) {
    try {
      let app = (await axios.get(process.env.API_URL + '/app_nuvemshop?_where=(client_id,eq,' + req.params.id + ')')).data.shift();
      // SOLICITAR TOKENS COM BASE NO CÓDIGO RETORNADO
      let tokens = (await axios.post(process.env.NUVEMSHOP_AUTH_URL, {
        'client_id': app.client_id,
        'client_secret': app.client_secret,
        'grant_type': 'authorization_code',
        'code': req.query.code
      })).data;
      // CARREGAR PROJETO
      let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
      // INCLUIR A INTEGRAÇÃO NO PROJETO
      projeto.integracoes = JSON.parse(projeto.integracoes) || {};
      projeto.integracoes.nuvemshop = tokens;
      projeto.integracoes.nuvemshop.last_timestamp = Math.floor(+new Date() / 1000);
      projeto.integracoes.nuvemshop.status = true;
      // INCLUIR TAMBÉM O NOME DA CONTA
      projeto.integracoes.nuvemshop.original_domain = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/store', {
        headers: {
          'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
          'User-Agent': app.user_agent
        }
      })).data.original_domain;
      //
      projeto.integracoes = JSON.stringify(projeto.integracoes);
      // ATUALIZAR PROJETO
      await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
        integracoes: projeto.integracoes
      });
      // CRIAR O LOG
      projeto.integracoes = JSON.parse(projeto.integracoes) || {};
      await axios.post(process.env.API_URL + '/log_nuvemshop', {
        tipo: 'info',
        descricao: 'Integração criada com sucesso (' + projeto.integracoes.nuvemshop.original_domain + ').',
        origem: 'app',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        projeto_id: req.session.kampana.projeto.id
      });
      // REDIRECIONAR O USUÁRIO PARA A TELA DE CADASTRO COM MENASGEM DE SUCESSO
      res.redirect('/nuvemshop?sucesso=true');
    }
    catch (e) {
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_nuvemshop', {
        tipo: 'danger',
        descricao: 'Erro na criação da integração.',
        origem: 'app',
        extra: JSON.stringify({
          user: {
            id: req.session.passport.user.id,
            displayName: req.session.passport.user.displayName,
            email: req.session.passport.user.emails.slice().shift().value
          }
        }),
        projeto_id: req.session.kampana.projeto.id
      });
      // REDIRECIONAR O USUÁRIO PARA A TELA DE CADASTRO COM MENASGEM DE ERRO
      res.redirect('/nuvemshop?sucesso=false');
    }
  }
  // USUÁRIO NÃO LOGADO - ARMAZENANDO EM CACHE PARA ASSOCIAR APÓS O CADASTRO
  else {
    try {
      let app = (await axios.get(process.env.API_URL + '/app_nuvemshop?_where=(client_id,eq,' + req.params.id + ')')).data.shift();
      // SOLICITAR TOKENS COM BASE NO CÓDIGO RETORNADO
      let tokens = (await axios.post(process.env.NUVEMSHOP_AUTH_URL, {
        'client_id': app.client_id,
        'client_secret': app.client_secret,
        'grant_type': 'authorization_code',
        'code': req.query.code
      })).data;
      // BUSCAR INFORMAÇÕES DA LOJA PARA PRE PROCESSAMENTO DO CADASTRO
      let store = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', tokens.user_id) + '/store', {
        headers: {
          'Authentication': tokens.token_type + ' ' + tokens.access_token,
          'User-Agent': app.user_agent
        }
      })).data;
      // SALVAR A INTEGRAÇÃO NO CACHE
      await axios.post(process.env.API_URL + '/cache_nuvemshop', {
        code: req.query.code,
        tokens: JSON.stringify(tokens),
        client_id: req.params.id,
        store: JSON.stringify(store),
        status: 0
      });
      // REDIRECIONAR O USUÁRIO PARA A TELA DE CADASTRO
      res.redirect('/cadastro?code=' + req.query.code);
    }
    catch (e) {
      // REDIRECIONAR O USUÁRIO PARA O HOTSITE
      res.redirect('/cadastro');
    }
  }
});

module.exports = router;