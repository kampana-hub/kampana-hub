const auth0 = require('auth0');
const axios = require('axios');
const AWS = require('aws-sdk');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const us2 = require('url-safe-string');
const express = require('express');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const router = express.Router();
const dotenv = require('dotenv');

//

dotenv.config();

let management = {};

router.use((req, res, next) => {
  let authentication = new auth0.AuthenticationClient({
    domain: process.env.AUTH0_DOMAIN,
    clientId: process.env.AUTH0_MANAGEMENT_CLIENT_ID,
    clientSecret: process.env.AUTH0_MANAGEMENT_CLIENT_SECRET
  }).clientCredentialsGrant({
    audience: 'https://' + process.env.AUTH0_DOMAIN + '/api/v2/',
    scope: 'read:users'
  }, (err, response) => {
    management = new auth0.ManagementClient({
      token: response.access_token,
      domain: process.env.AUTH0_DOMAIN
    });
    next();
  });
});

//

router.get('/', ensureLoggedIn, (req, res, next) => {
  management.getUsers((err, users) => {
    if (err) res.status(500).send('Erro ao consultar os usuários.');
    else res.json(users.filter(user => user.identities.filter(identity => identity.connection === process.env.AUTH0_APP_CONNECTION).length > 0));
  });
});

router.get('/logs/:id', ensureLoggedIn, (req, res, next) => {
  management.getUserLogs({
    id: req.params.id
  }, (err, logs) => {
    if (err) res.status(500).send('Erro ao consultar os logs do usuário.');
    else res.json(logs);
  });
});

router.get('/:id', ensureLoggedIn, async (req, res, next) => {
  management.getUser({
    id: req.params.id
  }, (err, user) => {
    if (err) res.status(500).send('Erro ao consultar o usuário.');
    else {
      user.cliente = req.session.kampana.cliente;
      res.json(user);
    }
  });
});

// IMAGEM DE PERFIL
const safe = new us2();

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/cache');
  },
  filename: (req, file, cb) => {
    let extension = path.extname(file.originalname);
    let filename = safe.generate(file.fieldname) + '-' + (+ new Date()) + extension;
    cb(null, filename);
  }
});

let fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') cb(null, true);
  else cb(null, false);
}

let upload = multer({
  storage: storage,
  fileFilter: fileFilter
});

router.post('/', ensureLoggedIn, upload.single('file'), (req, res, next) => {
  let user = {
    connection: 'Customers',
    name: req.body.nome,
    email: req.body.email,
    password: 'Kampana@2021',
    blocked: req.body.status ? false : true,
    user_metadata: {
      nascimento: req.body.nascimento,
      telefone: req.body.telefone
    },
    app_metadata: {
      projetos: req.body.projetos
    }
  };
  if (req.file) user.picture = process.env.API_PUBLIC_URL + '/cache/' + req.file.filename;
  management.createUser(user, (err, user) => {
    if (err) res.status(500).send('Erro ao criar o usuário.');
    else res.status(200).send('OK');
  });
});

router.put('/password/:id', ensureLoggedIn, (req, res, next) => {
  management.updateUser({
    id: req.params.id
  }, {
    password: req.body.password,
  }, (err, user) => {
    if (err) res.status(500).send('Erro ao alterar a senha do usuário.');
    else res.status(200).send('OK');
  });
});

router.put('/:id', ensureLoggedIn, upload.single('file'), async (req, res, next) => {
  let user = {
    name: req.body.nome,
    user_metadata: {
      nascimento: req.body.nascimento,
      telefone: req.body.telefone
    }
  };
  let bucket_name = '03-app-usuarios';
  if (req.file) {
    let s3upload = null;
    let s3Client = new AWS.S3({
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_KEY,
      region: process.env.AWS_REGION
    });
    let fileStream = fs.createReadStream(req.file.path);
    s3upload = await s3Client.upload({
      Bucket: process.env.AWS_BUCKET + '/' + bucket_name,
      Key: req.file.filename,
      Body: fileStream,
    }).promise();
    user.picture = process.env.AWS_PUBLIC_URL + '/' + bucket_name + '/' + req.file.filename;
  } else user.picture = process.env.AWS_PUBLIC_URL + '/' + bucket_name + '/' + 'blank.jpeg';
  management.updateUser({
    id: req.params.id
  }, user, (err, user) => {
    if (err) {
      console.log(err);
      res.status(500).send('Erro ao atualizar o usuário.');
    } 
    else res.send(user);
  });
});

router.delete('/:id', ensureLoggedIn, (req, res, next) => {
  management.deleteUser({
    id: req.params.id
  }, (err, user) => {
    if (err) res.status(500).json(err);
    else res.json(user);
  });
});

module.exports = router;