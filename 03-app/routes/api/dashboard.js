const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');
const stripe = require('stripe')(process.env.STP_SECRET_KEY);

const router = express.Router();

router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let integracoes = (await axios.get(process.env.API_URL + '/v_projeto_integracoes?_where=(projeto_id,eq,' + req.session.kampana.projeto.id + ')')).data.shift();
    //
    let dashboard = {};
    //
    if (integracoes.activecampaign) dashboard.activecampaign = (await axios.get(process.env.API_URL + '/v_activecampaign?_where=(projeto_id,eq,' + req.session.kampana.projeto.id + ')')).data.shift();
    if (integracoes.nuvemshop)      dashboard.nuvemshop      = (await axios.get(process.env.API_URL + '/v_nuvemshop?_where=(projeto_id,eq,'      + req.session.kampana.projeto.id + ')')).data.shift();
    if (integracoes.rdstation)      dashboard.rdstation      = (await axios.get(process.env.API_URL + '/v_rdstation?_where=(projeto_id,eq,'      + req.session.kampana.projeto.id + ')')).data.shift();
    if (integracoes.whatsapp)       dashboard.whatsapp       = (await axios.get(process.env.API_URL + '/v_whatsapp?_where=(projeto_id,eq,'       + req.session.kampana.projeto.id + ')')).data.shift();
    //
    if (req.session.kampana.cliente.id_stripe) dashboard.assinaturas = (await stripe.subscriptions.list({
      customer: req.session.kampana.cliente.id_stripe
    })).data;
    //
    res.json(dashboard);
  }
  //
  catch (e) {
    res.status(500).send('Erro.');
  }
});

module.exports = router;