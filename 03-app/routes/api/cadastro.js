const auth0 = require('auth0');
const axios = require('axios');
const express = require('express');
const stripe = require('stripe')(process.env.STP_SECRET_KEY);

const router = express.Router();

let management = {};

router.use((req, res, next) => {
  let authentication = new auth0.AuthenticationClient({
    domain: process.env.AUTH0_DOMAIN,
    clientId: process.env.AUTH0_MANAGEMENT_CLIENT_ID,
    clientSecret: process.env.AUTH0_MANAGEMENT_CLIENT_SECRET
  }).clientCredentialsGrant({
    audience: 'https://' + process.env.AUTH0_DOMAIN + '/api/v2/',
    scope: 'read:users'
  }, (err, response) => {
    management = new auth0.ManagementClient({
      token: response.access_token,
      domain: process.env.AUTH0_DOMAIN
    });
    next();
  });
});

router.post('/', async (req, res, next) => {
  /*
    {
      usuario_nome: "José Luís",
      cliente_id_mf: "020.661.551-52",
      cliente_nome: "",
      cliente_tipo: "PF",
      code: "4421d7359ea57ea95d682b3867ced9d626c60455",
      usuario_email: "joseluisvieiras@hotmail.com",
      usuario_nascimento: "12/05/1993",
      usuario_nome: "José Luís",
      usuario_senha: "Jl#2422230",
      usuario_senha_c: "Jl#2422230",
      usuario_telefone: "(62) 99503-0938"
    }
	*/
  try {
    // CRIAR CLIENTE
    let cliente = {
      tipo: req.body.cliente_tipo,
      id_mf: req.body.cliente_id_mf.replace(/\D/g, ''),
      nome: req.body.cliente_tipo == 'PF' ? req.body.usuario_nome : req.body.cliente_nome,
      descricao: 'Cliente criado automaticamente pelo checkout.',
      status: true
    }
    let cliente_response = (await axios.post(process.env.API_URL + '/cliente', cliente)).data;
    // CRIAR PROJETO
    let projeto = {
      cliente_id: cliente_response.insertId,
      nome: req.body.cliente_tipo == 'PF' ? req.body.usuario_nome : req.body.cliente_nome,
      descricao: 'Projeto criado automaticamente pelo checkout.',
      externo: true,
      status: true
    }
    // AVALIAR SE EXISTE UM CÓDIGO
    if (req.body.code) {
      projeto.integracoes = {};
      // CARREGAR OS TOKENS
      let cache = (await axios.get(process.env.API_URL + '/cache_nuvemshop/' + req.body.code)).data.shift();
      //
      projeto.integracoes.nuvemshop = JSON.parse(cache.tokens);
      projeto.integracoes.nuvemshop.last_timestamp = Math.floor(+new Date() / 1000);
      projeto.integracoes.nuvemshop.status = true;
      // INCLUIR TAMBÉM O NOME DA CONTA
      projeto.integracoes.nuvemshop.original_domain = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/store', {
        headers: {
          'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
          'User-Agent': process.env.NUVEMSHOP_USER_AGENT
        }
      })).data.original_domain;
      //
      projeto.integracoes = JSON.stringify(projeto.integracoes);
      //
      let app = (await axios.get(process.env.API_URL + '/app_nuvemshop?_where=(client_id,eq,' + cache.client_id + ')')).data.shift();
      projeto.app_nuvemshop_id = app.id;
    }
    let projeto_response = (await axios.post(process.env.API_URL + '/projeto', projeto)).data;
    /* */
    if (req.body.code) {
      // CRIAR LOG DA INTEGRAÇÃO
      projeto.integracoes = JSON.parse(projeto.integracoes);
      await axios.post(process.env.API_URL + '/log_nuvemshop', {
        tipo: 'info',
        descricao: 'Integração criada com sucesso (' + projeto.integracoes.nuvemshop.original_domain + ').',
        origem: 'app',
        extra: JSON.stringify({
          user: {
            displayName: req.body.usuario_nome,
            email: req.body.usuario_email
          }
        }),
        projeto_id: projeto_response.insertId
      });
    }
    /* */
    // CRIAR USUÁRIO
    let usuario = {
      connection: process.env.AUTH0_APP_CONNECTION,
      name: req.body.usuario_nome,
      email: req.body.usuario_email,
      password: req.body.usuario_senha,
      user_metadata: {
        nascimento: req.body.usuario_nascimento.substring(6, 10) + '-' + req.body.usuario_nascimento.substring(3, 5) + '-' + req.body.usuario_nascimento.substring(0, 2),
        telefone: '55' + req.body.usuario_telefone.replace(/\D/g, '')
      },
      app_metadata: {
        projetos: [projeto_response.insertId]
      }
    }
    management.createUser(usuario, async (e, user) => {
      if (e) {console.log(e); throw new Error(e);}
      else {
        // PRODUTO ESTÁTICO - ASSINATURA RD + NUVEMSHOP
        const session = await stripe.checkout.sessions.create({
          mode: 'subscription',
          payment_method_types: ['card'],
          customer_email: usuario.email,
          line_items: [{
            price: process.env.STP_PRICE_ID,
            quantity: 1
          }],
          subscription_data: {
            trial_period_days: 7
          },
          success_url: process.env.HUB_APP_URL + '/api/cadastro/callback?cliente_id=' + cliente_response.insertId + '&id={CHECKOUT_SESSION_ID}',
          cancel_url: process.env.HUB_URL
        });
        return res.status(200).send({
          sessionId: session.id
        });
      }
    });
  }
  //
  catch (e) {
    console.log(e);
    res.status(500).send(e);
  }
});

router.get('/callback', async (req, res, next) => {
  try {
    if (req.query) {
      const session_retrieve = await stripe.checkout.sessions.retrieve(req.query.id);
      const cliente_update = (await axios.patch(process.env.API_URL + '/cliente/' + req.query.cliente_id, {
        id_stripe: session_retrieve.customer,
      })).data;
      //
      return res.redirect(process.env.HUB_APP_URL);
    }
  } catch (e) {
    res.status(500).send('Erro');
  }
});

router.get('/consultar-usuario/:email', async (req, res, next) => {
  try {
    management.getUsersByEmail(req.params.email, {
      fields: 'identities',
      include_fields: true
    }, (e, users) => {
      if (e) throw new Error(e);
      else {
        if (users.length > 0 && users.find(user => user.identities.find(identity => identity.connection === process.env.AUTH0_APP_CONNECTION))) res.status(200).send(true);
        else res.status(200).send(false);
      }
    });
  } catch (e) {
    res.status(500).send('Erro');
  }
});

router.get('/consultar-cliente/:id_mf', async (req, res, next) => {
  try {
    let clientes = (await axios.get(process.env.API_URL + '/cliente?_where=(id_mf,eq,' + req.params.id_mf.replace(/\D/g, '') + ')')).data;
    if (clientes.length > 0) res.status(200).send(true);
    else res.status(200).send(false);
  } catch (e) {
    res.status(500).send('Erro');
  }
});

module.exports = router;