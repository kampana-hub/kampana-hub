const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');

const router = express.Router();

/*/
 * BASIC ROUTES
/*/

// LER AS INFORMAÇÕES DE UMA INTEGRAÇÃO
router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let whatsapp = (await axios.get(process.env.API_URL + '/v_whatsapp?_where=(projeto_id,eq,' + req.session.kampana.projeto.id + ')')).data.shift();
    // ENVIAR RESPOSTA POSITIVA
    res.json(whatsapp);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ALTERAR O STATUS DE UMA INTEGRAÇÃO
router.put('/', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // MUDAR  STATUS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.whatsapp.status = !projeto.integracoes.whatsapp.status;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    await axios.post(process.env.API_URL + '/log_whatsapp', {
      tipo: projeto.integracoes.whatsapp.status ? 'info' : 'warning',
      descricao: projeto.integracoes.whatsapp.status ? 'Integração habilitada com sucesso.' : 'Integração desabilitada com sucesso.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_whatsapp', {
      tipo: 'danger',
      descricao: 'Erro na alteração do status da integração.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// EXCLUIR UMA INTEGRAÇÃO
router.delete('/', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // REMOVER A INTEGRAÇÃO DO PROJETO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    delete projeto.integracoes.whatsapp;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_whatsapp', {
      tipo: 'warning',
      descricao: 'Integração excluída com sucesso.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_whatsapp', {
      tipo: 'danger',
      descricao: 'Erro na exclusão da integração.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/*/
 * CUSTOM ROUTES
/*/

// TESTAR UMA INTEGRAÇÃO
router.post('/debug', ensureLoggedIn, async (req, res, next) => {
  try {
    let whatsapp = (await axios.get(process.env.API_URL + '/v_whatsapp?_where=(projeto_id,eq,' + req.session.kampana.projeto.id + ')')).data.shift();
    let debug = (await axios.post(process.env.WPP_BASE_URL + '/' + whatsapp.sender_id + '/sendText/',
      // OBJETO
      req.body,
      // AUTENTICAÇÃO
      {
        headers: {
          'Authorization': process.env.WPP_API_PREFIX + ' ' + process.env.WPP_API_KEY,
          'Content-Type': 'application/json'
        }
      }
    )).data;
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_whatsapp', {
      tipo: 'info',
      descricao: 'Integração testada com sucesso.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    res.json(debug);
  } catch(e) {
    console.log(e);
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_whatsapp', {
      tipo: 'danger',
      descricao: 'Erro no teste da integração.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// LISTAR NÚMEROS DISPONÍVEIS
router.get('/senders', ensureLoggedIn, async (req, res, next) => {
  try {
    let senders = (await axios.get(process.env.WPP_BASE_URL + '/senders', {
      headers: {
        'Authorization': process.env.WPP_API_PREFIX + ' ' + process.env.WPP_API_KEY,
        'Content-Type': 'application/json'
      }
    })).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(senders);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ATUALIZAR UMA INTEGRAÇÃO
/*
router.get('/refresh', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // CARREGAR OS DADOS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    // REALIZAR CHAMADA DE ATUALIZAÇÃO NA API DO RD STATION    
    projeto.integracoes.rdstation.access_token = (await axios.post(process.env.RD_BASE_URL + '/auth/token', {
      client_id: process.env.RD_CLIENT_ID,
      client_secret: process.env.RD_CLIENT_SECRET,
      refresh_token: projeto.integracoes.rdstation.refresh_token
    })).data.access_token;
    projeto.integracoes.rdstation.last_timestamp = Math.floor(+new Date() / 1000);
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'info',
      descricao: 'Integração atualizada com sucesso.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_rdstation', {
      tipo: 'danger',
      descricao: 'Erro na atualização da integração.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});
*/

// LISTAR O LOG DE UMA INTEGRAÇÃO
router.get('/logs', ensureLoggedIn, async (req, res, next) => {
  try {
    let logs = (await axios.get(process.env.API_URL + '/v_whatsapp_log?_where=(projeto_id,eq,' + req.session.kampana.projeto.id + ')&_sort=-id')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(logs);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/*/
 * AUTHENTICATION ROUTES
/*/

router.post('/login', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // CRIAR INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.whatsapp = {
      sender_id: req.body.sender_id,
      last_timestamp: Math.floor(+new Date() / 1000),
      status: true
    }
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_whatsapp', {
      tipo: 'info',
      descricao: 'Integração criada com sucesso (' + projeto.nome + ').',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_whatsapp', {
      tipo: 'danger',
      descricao: 'Erro na criação da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    res.status(500).send('Not Ok');
  }
});

module.exports = router;