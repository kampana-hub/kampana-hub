const axios = require('axios');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const express = require('express');

const router = express.Router();

/*/
 * BASIC ROUTES
/*/

// LER AS INFORMAÇÕES DE UMA INTEGRAÇÃO
router.get('/', ensureLoggedIn, async (req, res, next) => {
  try {
    let sms = (await axios.get(process.env.API_URL + '/v_sms?_where=(projeto_id,eq,' + req.session.kampana.projeto.id + ')')).data.shift();
    // ENVIAR RESPOSTA POSITIVA
    res.json(sms);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// ALTERAR O STATUS DE UMA INTEGRAÇÃO
router.put('/', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // MUDAR  STATUS DA INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.sms.status = !projeto.integracoes.sms.status;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    await axios.post(process.env.API_URL + '/log_sms', {
      tipo: projeto.integracoes.sms.status ? 'info' : 'warning',
      descricao: projeto.integracoes.sms.status ? 'Integração habilitada com sucesso.' : 'Integração desabilitada com sucesso.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_sms', {
      tipo: 'danger',
      descricao: 'Erro na alteração do status da integração.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// EXCLUIR UMA INTEGRAÇÃO
router.delete('/', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // REMOVER A INTEGRAÇÃO DO PROJETO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    delete projeto.integracoes.sms;
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_sms', {
      tipo: 'warning',
      descricao: 'Integração excluída com sucesso.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_sms', {
      tipo: 'danger',
      descricao: 'Erro na exclusão da integração.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/*/
 * CUSTOM ROUTES
/*/

// TESTAR UMA INTEGRAÇÃO
router.post('/debug', ensureLoggedIn, async (req, res, next) => {
  try {
    let debug = (await axios.post(process.env.N_BASE_URL + '/sms?napikey=' + process.env.N_API_KEY,
      // OBJETO
      req.body,
      // AUTENTICAÇÃO
      {
        headers: {
          '-': 'Authorization: Bearer ' + process.env.N_API_TOKEN,
          'Content-Type': 'application/json'
        }
      }
    )).data;
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_sms', {
      tipo: 'info',
      descricao: 'Integração testada com sucesso.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    res.json(debug);
  } catch(e) {
    console.log(e);
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_sms', {
      tipo: 'danger',
      descricao: 'Erro no teste da integração.',
      origem: 'app',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

// LISTAR O LOG DE UMA INTEGRAÇÃO
router.get('/logs', ensureLoggedIn, async (req, res, next) => {
  try {
    let logs = (await axios.get(process.env.API_URL + '/v_sms_log?_where=(projeto_id,eq,' + req.session.kampana.projeto.id + ')&_sort=-id')).data;
    // ENVIAR RESPOSTA POSITIVA
    res.json(logs);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

/*/
 * AUTHENTICATION ROUTES
/*/

router.post('/login', ensureLoggedIn, async (req, res, next) => {
  try {
    // CARREGAR PROJETO
    let projeto = (await axios.get(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id)).data.shift();
    // CRIAR INTEGRAÇÃO
    projeto.integracoes = JSON.parse(projeto.integracoes) || {};
    projeto.integracoes.sms = {
      last_timestamp: Math.floor(+new Date() / 1000),
      status: true
    }
    projeto.integracoes = JSON.stringify(projeto.integracoes);
    // ATUALIZAR PROJETO
    await axios.patch(process.env.API_URL + '/projeto/' + req.session.kampana.projeto.id, {
      integracoes: projeto.integracoes
    });
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_sms', {
      tipo: 'info',
      descricao: 'Integração criada com sucesso (' + projeto.nome + ').',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    // ENVIAR RESPOSTA POSITIVA
    res.status(200).send('Ok');
  } catch(e) {
    // CRIAR O LOG
    await axios.post(process.env.API_URL + '/log_sms', {
      tipo: 'danger',
      descricao: 'Erro na criação da integração.',
      origem: 'hub',
      extra: JSON.stringify({
        user: {
          id: req.session.passport.user.id,
          displayName: req.session.passport.user.displayName,
          email: req.session.passport.user.emails.slice().shift().value
        }
      }),
      projeto_id: req.session.kampana.projeto.id
    });
    res.status(500).send('Not Ok');
  }
});

module.exports = router;