const axios = require('axios');
const express = require('express');
const router = express.Router();

/*/
 * BASIC ROUTES
/*/

// LER AS INFORMAÇÕES DA TABLE CACHE-NUVEMSHOP
router.get('/:code', async (req, res, next) => {
  try {
    let cache_nuvemshop = (await axios.get(process.env.API_URL + '/cache_nuvemshop?_where=(code,eq,' + req.params.code + ')')).data.shift();
    // ENVIAR RESPOSTA POSITIVA
    res.json(cache_nuvemshop);
  } catch (e) {
    // ENVIAR RESPOSTA NEGATIVA
    res.status(500).send(e);
  }
});

module.exports = router;