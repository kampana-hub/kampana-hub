/*
 * DEPENDENCIES
 */

const Auth0Strategy = require('passport-auth0');
const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const dotenv = require('dotenv');
const express = require('express');
const logger = require('morgan');
const passport = require('passport');
const path = require('path');
const session = require('express-session');

/*
 * INITIALIZATION
 */

dotenv.config({ path: __dirname + '/../.env' });

const app = express();

/*
 * AUTHENTICATION PARAMETERS
 */

const strategy = new Auth0Strategy({
      domain: process.env.AUTH0_DOMAIN,
      clientID: process.env.AUTH0_APP_CLIENT_ID,
      clientSecret: process.env.AUTH0_APP_CLIENT_SECRET,
      callbackURL: process.env.AUTH0_APP_CALLBACK_URL
   },
   (accessToken, refreshToken, extraParams, profile, done) => {
      return done(null, profile);
   }
);

passport.use(strategy);

passport.serializeUser((user, done) => {
   done(null, user);
});
passport.deserializeUser((user, done) => {
   done(null, user);
});

app.use(
   session({
      secret: 'shhhhhhhhh',
      resave: false,
      saveUninitialized: false
   })
);

app.use((req, res, next) => {
   res.locals.loggedIn = false;
   if (req.session.passport && typeof req.session.passport.user != 'undefined') {
      res.locals.loggedIn = true;
   }
   next();
});

app.use(passport.initialize());
app.use(passport.session());

/*
 * EXPRESS STUFF
 */

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
   extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/*
 * ROUTES
 */

app.use('/', require('./routes/index'));

app.use('/api/usuarios'  , require('./routes/api/usuarios'));

app.use('/api/dashboard'  , require('./routes/api/dashboard'));

app.use('/api/cadastro'   , require('./routes/api/cadastro'));

app.use('/api/nuvemshop'  , require('./routes/api/nuvemshop'));
app.use('/api/cache-nuvemshop'  , require('./routes/api/cache-nuvemshop'))
app.use('/api/rdstation'  , require('./routes/api/rdstation'));
app.use('/api/activecampaign'  , require('./routes/api/activecampaign'));
app.use('/api/whatsapp'  , require('./routes/api/whatsapp'));
app.use('/api/sms'  , require('./routes/api/sms'));

/*
 * ERROR HANDLING
 */

app.use((req, res, next) => {
   next(createError(404));
});

app.use((err, req, res, next) => {
   res.locals.message = err.message;
   res.locals.error = req.app.get('env') === 'development' ? err : {};
   res.status(err.status || 500);
   res.render('error', {
      message: err.message,
      error: res.locals.error,
      status: err.status || 500
   });
});

/*
 * SERVER START
 */

app.listen(4003, () => {
   console.log('[DEV] Kampana Hub - APP | Listening on port 4003.');
});

module.exports = app;