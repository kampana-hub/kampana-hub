const axios  = require('axios');
const cron   = require('node-cron');
const dotenv = require('dotenv');
const moment = require('moment');

// CARREGAR VARIÁVEIS DE AMBIENTE

dotenv.config({ path: __dirname + '/../../.env' });

cron.schedule('* * * * *', async () => {
  try {
    // CONSULTAR QUAIS INTEGRAÇÕES EXPIRAM DENTRO DE UMA HORA
    let integracoes = (await axios.get(process.env.API_URL + '/v_tray')).data.filter(integracao => {
      let gap = moment(integracao.date_expiration_access_token).diff(moment(), 'minutes');
      return gap <= 60 ? true : false;
    });
    // ATUALIZAR INTEGRAÇÕES PRÓXIMAS DE EXPIRAR
    integracoes.forEach(async integracao => {
      // CARREGAR PROJETO
      let projeto = (await axios.get(process.env.API_URL + '/projeto/' + integracao.projeto_id)).data.shift();
      // CARREGAR OS DADOS DA INTEGRAÇÃO
      projeto.integracoes = JSON.parse(projeto.integracoes) || {};
      // REALIZAR CHAMADA DE ATUALIZAÇÃO NA API DO TRAY
      let auth = projeto.integracoes.tray.auth;
      projeto.integracoes.tray = (await axios.get(projeto.integracoes.tray.auth.api_address + '/auth?refresh_token=' + projeto.integracoes.tray.refresh_token)).data
      projeto.integracoes.tray.auth = auth;
      projeto.integracoes.tray.last_timestamp = Math.floor(+new Date() / 1000);
      projeto.integracoes.tray.status = true;
      projeto.integracoes = JSON.stringify(projeto.integracoes);
      // ATUALIZAR PROJETO
      await axios.patch(process.env.API_URL + '/projeto/' + integracao.projeto_id, {
        integracoes: projeto.integracoes
      });
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_tray', {
        tipo: 'info',
        descricao: 'Integração atualizada com sucesso.',
        origem: 'api',
        projeto_id: integracao.projeto_id
      });
      // CONSOLE
      console.log('Integração atualizada com sucesso. ' + integracao.cliente_nome + ' (' + integracao.cliente_id + ')');
    });
    
  } catch(e) {
    //
    // ERRO
    //
    console.log(e);
  }
});