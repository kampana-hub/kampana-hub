const axios  = require('axios');
const cron   = require('node-cron');
const dotenv = require('dotenv');

// CARREGAR VARIÁVEIS DE AMBIENTE

dotenv.config({ path: __dirname + '/../../.env' });

cron.schedule('* * * * *', async () => {
  try {
    // CONSULTAR QUAIS INTEGRAÇÕES EXPIRAM DENTRO DE UMA HORA
    let integracoes = (await axios.get(process.env.API_URL + '/v_rdstation')).data.filter(integracao => {
      let expires_at = parseInt(integracao.last_timestamp) + parseInt(integracao.expires_in);
      let is_expired = parseInt(integracao.current_timestamp) <= expires_at - 3600 ? false : true;
      return is_expired;
    });
    // ATUALIZAR INTEGRAÇÕES PRÓXIMAS DE EXPIRAR
    integracoes.filter(integracao => integracao.status == 'true').forEach(async integracao => {
      // CARREGAR PROJETO
      let projeto = (await axios.get(process.env.API_URL + '/projeto/' + integracao.projeto_id)).data.shift();
      // CARREGAR OS DADOS DA INTEGRAÇÃO
      projeto.integracoes = JSON.parse(projeto.integracoes) || {};
      // REALIZAR CHAMADA DE ATUALIZAÇÃO NA API DO RD STATION    
      projeto.integracoes.rdstation.access_token = (await axios.post(process.env.RD_BASE_URL + '/auth/token', {
        client_id: process.env.RD_CLIENT_ID,
        client_secret: process.env.RD_CLIENT_SECRET,
        refresh_token: projeto.integracoes.rdstation.refresh_token
      })).data.access_token;
      projeto.integracoes.rdstation.last_timestamp = Math.floor(+new Date() / 1000);
      projeto.integracoes = JSON.stringify(projeto.integracoes);
      // ATUALIZAR PROJETO
      await axios.patch(process.env.API_URL + '/projeto/' + integracao.projeto_id, {
        integracoes: projeto.integracoes
      });
      // CRIAR O LOG
      await axios.post(process.env.API_URL + '/log_rdstation', {
        tipo: 'info',
        descricao: 'Integração atualizada com sucesso.',
        origem: 'api',
        projeto_id: integracao.projeto_id
      });
      // CONSOLE
      console.log('Integração atualizada com sucesso. ' + integracao.cliente_nome + ' (' + integracao.cliente_id + ')');
    });
  } catch(e) {
    //
    // ERRO
    //
    console.log(e);
  }
});