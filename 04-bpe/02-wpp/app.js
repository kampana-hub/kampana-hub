const axios  = require('axios');
const dotenv = require('dotenv');
const cron   = require('node-cron');
const venom  = require('venom-bot');

// CARREGAR VARIÁVEIS DE AMBIENTE

dotenv.config({ path: __dirname + '/../../.env' });

// CONFIGURAR MONGO DB

const { mdb_campanhas } = require('./mdb');
const campanhaSchema = require('./models/campanhas');

// CONFIGURAR WHATSAPP

const browserArgs = [
  '--disable-web-security', '--no-sandbox', '--disable-web-security',
  '--aggressive-cache-discard', '--disable-cache', '--disable-application-cache',
  '--disable-offline-load-stale-cache', '--disk-cache-size=0',
  '--disable-background-networking', '--disable-default-apps', '--disable-extensions',
  '--disable-sync', '--disable-translate', '--hide-scrollbars', '--metrics-recording-only',
  '--mute-audio', '--no-first-run', '--safebrowsing-disable-auto-update',
  '--ignore-certificate-errors', '--ignore-ssl-errors', '--ignore-certificate-errors-spki-list'
];

// INICIALIZAR VARIÁVEL DE CONTROLE DO WHATSAPP

let whatsapp = {};

// COLOCAR CAMPANHAS NA FILA
cron.schedule('* * * * *', async () => {
  try {
    let campanhas = (await axios.get(process.env.API_URL + '/v_campanha')).data;
    // 2 - AGENDADAS
    campanhas.filter(campanha => campanha.status_id === 2).forEach(async (campanha) => {
      let agora = new Date();
      let agendamento = new Date(campanha.agendamento);
      // TÁ NA HORA DE COMEÇAR O ENVIO?
      if (agora >= agendamento) {
        try {
          // INICIALIZAR O WHATSAPP
          venom
          .create(campanha.projeto_id.toString(), undefined, undefined, {
            browserArgs: browserArgs
          })
          .then(async (client) => {
            whatsapp[campanha.id] = client;
            // ATUALIZAR O STATUS DA CAMPANHA
            await axios.patch(process.env.API_URL + '/campanha/' + campanha.id, {
              status_id: 3
            });
            // CRIAR O LOG
            await axios.post(process.env.API_URL + '/log_campanha', {
              tipo: 'warning',
              descricao: 'Campanha inicializada com sucesso.',
              origem: 'api',
              campanha_id: campanha.id
            });
            console.log('#' + campanha.id + ' - CAMPANHA INICIALIZADA');
          })
          .catch((e) => {
            //
            // ERRO
            //
          });
        } catch(e) {
          //
          // ERRO
          //
        }
      }
    });
    // 3 - EM ANDAMENTO
    campanhas.filter(campanha => campanha.status_id === 3).forEach(campanha => {
      // WHATSAPP ESTÁ INICIALIZADO?
      if (!whatsapp[campanha.id]) {
        // INICIALIZAR O WHATSAPP
        venom
        .create(campanha.projeto_id.toString(), undefined, undefined, {
          browserArgs: browserArgs
        })
        .then(async (client) => {
          whatsapp[campanha.id] = client;
          // CRIAR O LOG
          await axios.post(process.env.API_URL + '/log_campanha', {
            tipo: 'warning',
            descricao: 'Campanha resumida com sucesso.',
            origem: 'api',
            campanha_id: campanha.id
          });
          console.log('#' + campanha.id + ' - CAMPANHA RESUMIDA');
        })
        .catch((e) => {
          //
          // ERRO
          //
        });
      }
    });
    // 4 - PAUSADAS, CONCLUÍDAS, COM ERRO E CANCELADAS
    campanhas.filter(campanha => [4, 5, 6, 7].includes(campanha.status_id)).forEach(campanha => {
      // WHATSAPP ESTÁ INICIALIZADO?
      if (whatsapp[campanha.id]) {
        whatsapp[campanha.id].close();
        delete whatsapp[campanha.id];
      }
    });
  } catch(e) {
    //
    // ERRO
    //
  }
});

// PROCESSAR CAMPANHAS DA FILA
cron.schedule('*/2 * * * *', async () => {
  if (Object.keys(whatsapp).length > 0) Object.keys(whatsapp).forEach(async (campanha) => {
    // CARREGAR OS LEADS PENDENTES DA CAMPANHA
    let collection = mdb_campanhas.model(campanha, campanhaSchema);
    let leads = await collection.find({
      status: 0
      /*
        $or: [{
          status: 0
        }, {
          status: 2
        }]
      */
    });
    if (leads.length > 0) {
      // SORTEAR UM LEAD PARA ENVIO
      let lead = leads[Math.floor(Math.random() * leads.length)];
      whatsapp[campanha]
      .sendText(lead.telefone + '@c.us', lead.mensagem).then(async (result) => {
        if (lead.anexo) {
          //
          whatsapp[campanha]
          .sendImage(
            lead.telefone + '@c.us',
            lead.anexo,
            '',
            ''
          )
          .then(async (result) => {
            console.log('✅ - ' + lead.telefone + ' - ' + lead.mensagem);
            lead.status = 1;
            // ATUALIZAR LEAD COMO ENVIADO
            await collection.findOneAndUpdate({ email: lead.email }, lead, {
              upsert: true
            });
          })
          .catch(async (erro) => {
            console.log('❌ - ' + lead.telefone + ' - ' + lead.mensagem);
            lead.status = 2;
            // ATUALIZAR LEAD COM ERRO NO ENVIO
            await collection.findOneAndUpdate({ email: lead.email }, lead, {
              upsert: true
            });
          });
          //
        }
        //
        else {
          console.log('✅ - ' + lead.telefone + ' - ' + lead.mensagem);
          lead.status = 1;
          // ATUALIZAR LEAD COMO ENVIADO
          await collection.findOneAndUpdate({ email: lead.email }, lead, {
            upsert: true
          });
        }
      })
      .catch(async (e) => {
        console.log('❌ - ' + lead.telefone + ' - ' + lead.mensagem);
        lead.status = 2;
        // ATUALIZAR LEAD COM ERRO NO ENVIO
        await collection.findOneAndUpdate({ email: lead.email }, lead, {
          upsert: true
        });
      });
    }
    else {
      // BUSCAR POR LEADS COM ERRO
      let leads = await collection.find({
        status: 2
      });
      if (leads.length == 0) {
        // ATUALIZAR O STATUS DA CAMPANHA - CONCLUÍDA COM SUCESSO
        await axios.patch(process.env.API_URL + '/campanha/' + campanha, {
          status_id: 6
        });
        // CRIAR O LOG
        await axios.post(process.env.API_URL + '/log_campanha', {
          tipo: 'success',
          descricao: 'Campanha concluída com sucesso.',
          origem: 'api',
          campanha_id: campanha
        });
        console.log('#' + campanha + ' - CAMPANHA CONCLUÍDA COM SUCESSO');
      }
      else {
        // ATUALIZAR O STATUS DA CAMPANHA - COM ERRO
        await axios.patch(process.env.API_URL + '/campanha/' + campanha, {
          status_id: 5
        });
        // CRIAR O LOG
        await axios.post(process.env.API_URL + '/log_campanha', {
          tipo: 'danger',
          descricao: 'Campanha concluída com erro.',
          origem: 'api',
          campanha_id: campanha
        });
        console.log('#' + campanha + ' - CAMPANHA CONCLUÍDA COM ERRO');
      }
    }
  });
});

process.on('SIGINT', function() {
  if (Object.keys(whatsapp).length > 0) Object.keys(whatsapp).forEach(async (campanha) => whatsapp[campanha].close());
  console.log('terminado corretamente');
});