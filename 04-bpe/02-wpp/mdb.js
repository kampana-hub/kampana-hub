const mongoose = require('mongoose');

const campanhas = mongoose.createConnection(
  'mongodb+srv://' + process.env.MDB_USER + ':' + process.env.MDB_PASS + '@' + process.env.MDB_CLUSTER + '/campanhas?retryWrites=true&w=majority',
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
  }
);

module.exports = {
  mdb_campanhas: campanhas
};