const axios  = require('axios');
const cron   = require('node-cron');
const dotenv = require('dotenv');
const moment = require('moment');

// CARREGAR VARIÁVEIS DE AMBIENTE

dotenv.config({ path: __dirname + '/../../.env' });

// CONFIGURAR MONGO DB

const { mdb_checkouts } = require('./mdb');
const checkoutSchema = require('./models/checkouts');

//

cron.schedule('* * * * *', async () => {
  let collection = mdb_checkouts.model('checkout', checkoutSchema);
  let checkouts = await collection.find({hub_status: 0}).lean();
  //
  if (checkouts.length > 0) {
    checkouts.forEach(async checkout => {
      let delta = moment().diff(moment(checkout.created_at), 'minutes');
      //
      if (delta >= 30) {
        // CARREGAR PROJETO
        let projeto = (await axios.get(process.env.API_URL + '/projeto/' + checkout.projeto_id)).data.shift();
        // CARREGAR APP - NUVEMSHOP
        let app = (await axios.get(process.env.API_URL + '/app_nuvemshop/' + projeto.app_nuvemshop_id)).data.shift();
        projeto.integracoes = JSON.parse(projeto.integracoes);
        // VERIFICAR SE PROJETO / INTEGRAÇÃO ESTÃO ATIVOS
        if (projeto.integracoes.nuvemshop && projeto.integracoes.nuvemshop.status == true) {
          // CONSULTAR PEDIDO NA API DO NUVEMSHOP
          let order = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/orders/' + checkout.id, {
            headers: {
              'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
              'User-Agent': app.user_agent
            }
          })).data;
          // PEDIDO REALIZADO - NÃO GERAR CARRINHO ABANDONADO
          if (order.gateway) {
            console.log(moment().format('DD/MM/YYYY - HH:mm:ss') + ' - ' + checkout.id + ' - ' + (checkout.customer.email || checkout.customer.id) + ' -  PEDIDO REALIZADO - NÃO GERAR CARRINHO ABANDONADO');
            checkout.updated_at = moment().format();
            checkout.hub_status = 2;
            // ATUALIZAR CACHE - PEDIDO REALIZADO
            await collection.findOneAndUpdate({id: checkout.id}, checkout, {upsert: true, setDefaultsOnInsert: true});
          }
          // PEDIDO NÃO REALIZADO - GERAR CARRINHO ABANDONADO
          else {
            console.log(moment().format('DD/MM/YYYY - HH:mm:ss') + ' - ' + checkout.id + ' - ' + (checkout.customer.email || checkout.customer.id) + ' -  PEDIDO NÃO REALIZADO - GERAR CARRINHO ABANDONADO');
            // MONTAR O OBJETO DO LEAD
            let lead = {
              // name: '',
              // email: '',
              // bio: '',
              // website: '',
              // job_title: '',
              // personal_phone: '',
              // mobile_phone: '',
              // city: '',
              // state: '',
              // country: '',
              // twitter: '',
              // facebook: '',
              // linkedin: '',
              // tags: [],
              // extra_emails: []
              //
              // cf_hub_abandoned_cart_url: '',
              // cf_hub_abandoned_cart_size: '',
              // cf_hub_total_spent: '',
              // cf_hub_order_count: '',
              //
              // cf_hub_product_01_name: '',
              // cf_hub_product_01_price: '',
              // cf_hub_product_01_image_url: '',
              //
              // cf_hub_product_02_name: '',
              // cf_hub_product_02_price: '',
              // cf_hub_product_02_image_url: ''
            };
            // VARIÁVEIS PARA ENVIO DAS INFORMAÇÕES / LOG
            let name  = '';
            let email = '';
            // USUÁRIO JÁ CADASTRADO
            if (checkout.customer.id) {
              let customer = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/customers/' + checkout.customer.id, {
                headers: {
                  'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
                  'User-Agent': app.user_agent
                }
              })).data;
              // LEAD START
              if (customer.name)             lead.name                       = customer.name;
              if (customer.phone)            lead.personal_phone             = customer.phone;
              if (customer.billing_city)     lead.city                       = customer.billing_city;
              if (customer.billing_province) lead.state                      = customer.billing_province;
              if (customer.billing_country)  lead.country                    = customer.billing_country
                                             lead.tags                       = ['nuvemshop', 'kampana-hub', 'cart-abandoned'];
              if (checkout.url)              lead.cf_hub_abandoned_cart_url  = checkout.url
                                             lead.cf_hub_abandoned_cart_size = checkout.items.length;
              if (customer.total_spent)      lead.cf_hub_total_spent         = (customer.total_spent / 100).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
              // LEAD END
              name  = customer.name;
              email = customer.email;
            }
            // USUÁRIO NÃO CADASTRADO
            else {
              // LEAD START
              if (checkout.customer.name)    lead.name                       = checkout.customer.name;
              if (checkout.customer.phone)   lead.personal_phone             = checkout.customer.phone;
              if (checkout.customer.city)    lead.city                       = checkout.customer.city
              if (checkout.customer.state)   lead.state                      = checkout.customer.state
              if (checkout.customer.country) lead.country                    = checkout.customer.country
                                             lead.tags                       = ['nuvemshop', 'kampana-hub', 'cart-abandoned'];
              if (checkout.url)              lead.cf_hub_abandoned_cart_url  = checkout.url
                                             lead.cf_hub_abandoned_cart_size = checkout.items.length;
              // LEAD END
              name  = checkout.customer.name;
              email = checkout.customer.email;
            }
            // MAPEAR DADOS DOS PRODUTOS
            let mensagem_produto = '';
            let imagem_produto = [];
            let imagem_url = [];
            let index = 0;
            for (let item of checkout.items) {
              try {
                if (index == 0) {
                  lead.cf_hub_product_01_name = item.name;
                  mensagem_produto = '_' + item.name + '_';
                  lead.cf_hub_product_01_price = (item.unit_price / 100).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
                  if (item.sku) {
                    let image = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/products/sku/' + item.sku, {
                      headers: {
                        'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
                        'User-Agent': app.user_agent
                      }
                    })).data.images.find(image => image.position == 1);
                    if (image) {
                      imagem_url.push(image.src);
                      lead.cf_hub_product_01_image_url = image.src;
                    }
                  } else if (item.product_id) {
                    let image = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/products/' + item.product_id, {
                      headers: {
                        'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
                        'User-Agent': app.user_agent
                      }
                    })).data.images.find(image => image.position == 1);
                    if (image) {
                      imagem_url.push(image.src);
                      lead.cf_hub_product_01_image_url = image.src;
                    }
                  }
                  imagem_produto.push(lead.cf_hub_product_01_image_url);
                }
                else if (index == 1) {
                  lead.cf_hub_product_02_name = item.name;
                  mensagem_produto += ' e _' + item.name + '_';
                  lead.cf_hub_product_02_price = (item.unit_price / 100).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
                  if (item.sku) {
                    let image  = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/products/sku/' + item.sku, {
                      headers: {
                        'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
                        'User-Agent': app.user_agent
                      }
                    })).data.images.find(image => image.position == 1);
                    if (image) lead.cf_hub_product_02_image_url = image.src;
                  } else if (item.product_id) {
                    let image  = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/products/' + item.product_id, {
                      headers: {
                        'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
                        'User-Agent': app.user_agent
                      }
                    })).data.images.find(image => image.position == 1);
                    if (image) lead.cf_hub_product_02_image_url = image.src;
                  }
                  imagem_produto.push(lead.cf_hub_product_02_image_url);
                }
                else {
                  if (item.sku) {
                    let image = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/products/sku/' + item.sku, {
                      headers: {
                        'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
                        'User-Agent': app.user_agent
                      }
                    })).data.images.find(image => image.position == 1);
                    if (image) {
                      imagem_url.push(image.src);
                    }
                  } else if (item.product_id) {
                    let image = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/products/' + item.product_id, {
                      headers: {
                        'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
                        'User-Agent': app.user_agent
                      }
                    })).data.images.find(image => image.position == 1);
                    if (image) {
                      imagem_url.push(image.src);
                    }
                  }
                }
              } catch(e) {
                // NÃO FOI POSSÍVEL ENCONTRAR A IMAGEM
              }
              index ++;
            }
            // PROMISE ARRAY
            let chamadas = [];
            let event_label = 'CART_ABANDONED';
            // EXISTE ESSA INTEGRAÇÃO CONFIGUADA HOJE?
            if (projeto.integracoes.nuvemshop.scripts.abandoned_cart) {
              // POSSUI A TAG "rdstation"?
              if (projeto.integracoes.nuvemshop.scripts.abandoned_cart.tags.includes('rdstation')) chamadas.push(new Promise(async (resolve, reject) => {
                // ATUALIZAR LEAD NO RD STATION
                try {
                  let chamada = (await axios.patch(process.env.RD_BASE_URL + '/platform/contacts/email:' + email,
                    // OBJETO
                    lead,
                    // AUTENTICAÇÃO
                    {
                      headers: {
                        'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token,
                        'Content-Type': 'application/json'
                      }
                    }
                  )).data;
                  // CRIAR O LOG
                  await axios.post(process.env.API_URL + '/log_nuvemshop', {
                    tipo: 'success',
                    descricao: 'Lead enviado para o RD Station com sucesso.',
                    origem: 'api',
                    extra: JSON.stringify({
                      nuvemshop: checkout,
                      rdstation: {
                        account: projeto.integracoes.rdstation.name,
                        lead_id: email
                      }
                    }),
                    projeto_id: checkout.projeto_id
                  });
                }
                // ERRO NA ATUALIZAÇÃO DO LEAD
                catch(e) {
                  // CRIAR O LOG
                  await axios.post(process.env.API_URL + '/log_nuvemshop', {
                    tipo: 'danger',
                    descricao: 'Erro ao enviar lead para o RD Station.',
                    origem: 'api',
                    extra: JSON.stringify({
                      nuvemshop: checkout,
                      rdstation: {
                        account: projeto.integracoes.rdstation.name,
                        lead_id: email
                      }
                    }),
                    projeto_id: checkout.projeto_id
                  });
                }
                // MONTAR O OBJETO DOS EVENTOS
                let events = [
                  {
                    event_family:'CDP',
                    event_type: 'OPPORTUNITY',
                    payload: {
                      email: email,
                      funnel_name: 'default'
                    }
                  },
                  {
                    event_type: 'CART_ABANDONED',
                    event_family:'CDP',
                    //
                    payload: {
                      name: name,
                      email: email,
                      cf_cart_id: checkout.id.toString(),
                      cf_cart_total_items: checkout.items.length,
                      cf_cart_status: checkout.status
                    }
                  }
                ];
                // ENVIAR EVENTOS
                try {
                  let chamada = (await axios.post(process.env.RD_BASE_URL + '/platform/events/batch',
                    // OBJETO
                    events,
                    // AUTENTICAÇÃO
                    {
                      headers: {
                        'Authorization': 'Bearer ' + projeto.integracoes.rdstation.access_token,
                        'Content-Type': 'application/json'
                      }
                    }
                  )).data;
                  // CRIAR O LOG
                  await axios.post(process.env.API_URL + '/log_nuvemshop', {
                    tipo: 'success',
                    descricao: 'Evento (Carrinho Abandonado) enviado para o RD Station com sucesso.',
                    origem: 'api',
                    extra: JSON.stringify({
                      nuvemshop: checkout,
                      rdstation: {
                        account: projeto.integracoes.rdstation.name,
                        lead_id: email,
                        event_type: 'CART_ABANDONED'
                      }
                    }),
                    projeto_id: checkout.projeto_id
                  });
                }
                // ERRO NO ENVIO DOS EVENTOS
                catch(e) {
                  // CRIAR O LOG
                  await axios.post(process.env.API_URL + '/log_nuvemshop', {
                    tipo: 'danger',
                    descricao: 'Erro ao enviar evento (Carrinho Abandonado) para o RD Station.',
                    origem: 'api',
                    extra: JSON.stringify({
                      nuvemshop: checkout,
                      rdstation: {
                        account: projeto.integracoes.rdstation.name,
                        lead_id: email,
                        event_type: 'CART_ABANDONED'
                      }
                    }),
                    projeto_id: checkout.projeto_id
                  });
                }
                resolve();
              }));
              // POSSUI A TAG "active"?
              if (projeto.integracoes.nuvemshop.scripts.abandoned_cart.tags.includes('activecampaign')) chamadas.push(new Promise(async (resolve, reject) => {
                // MONTAR O OBJETO DO CONTACT
                let contactdata = JSON.stringify({
                  contact: {
                    email: order.customer.email,
                    firstName: order.customer.name,
                    lastName: "",
                    phone: order.customer.phone,
                    fieldValues: [
                      {
                        field: parseInt(projeto.integracoes.activecampaign.custom_fields.cf_hub_total_spent),
                        value:order.customer.total_spent ? parseInt(order.customer.total_spent.replace('.', '')) : ''
                      },
                      {
                        field: parseInt(projeto.integracoes.activecampaign.custom_fields.cf_hub_product_01_name),
                        value: order.products.length > 0 ? order.products[0].name : ''
                      },
                      {
                        field: parseInt(projeto.integracoes.activecampaign.custom_fields.cf_hub_product_01_price),
                        value: order.products.length > 0 ? parseInt(order.products[0].price.replace('.', '')) : ''
                      },
                      {
                        field: parseInt(projeto.integracoes.activecampaign.custom_fields.cf_hub_product_02_name),
                        value: order.products.length > 1 ? order.products[1].name : ''
                      },
                      { 
                        field: parseInt(projeto.integracoes.activecampaign.custom_fields.cf_hub_product_02_price),
                        value: order.products.length > 1 ? parseInt(order.products[1].price.replace('.', '')) : ''
                      },
                      { 
                        field: projeto.integracoes.activecampaign.custom_fields.cf_hub_abandoned_cart_url,
                        value: checkout.url
                      },
                      { 
                        field: projeto.integracoes.activecampaign.custom_fields.cf_hub_abandoned_cart_size,
                        value: checkout.items.length
                      }
                    ]
                  }
                });
                // ATUALIZAR CONTACT NA ACTIVECAMPAIGN
                let contact_response;
                try {
                  contact_response = (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/contact/sync',
                    // OBJETO
                    contactdata,
                    // AUTENTICAÇÃO
                    {
                      headers: {
                        'Api-Token': projeto.integracoes.activecampaign.key
                      }
                    }
                  )).data.contact.id;
                  // CRIAR O LOG
                  await axios.post(process.env.API_URL + '/log_nuvemshop', {
                    tipo: 'success',
                    descricao: 'Contact enviado para a ActiveCampaign com sucesso.',
                    origem: 'api',
                    extra: JSON.stringify({
                      nuvemshop: checkout,
                      activecampaign: {
                        account: projeto.integracoes.activecampaign.name,
                        contact_id: order.customer.email
                      }
                    }),
                    projeto_id: checkout.projeto_id
                  });
                }                
                // ERRO NA ATUALIZAÇÃO DO LEAD
                catch(e) {
                  console.log(e);
                  // CRIAR O LOG
                  await axios.post(process.env.API_URL + '/log_nuvemshop', {
                    tipo: 'danger',
                    descricao: 'Erro ao enviar o contact para a ActiveCampaign.',
                    origem: 'api',
                    extra: JSON.stringify({
                      nuvemshop: checkout,
                      activecampaign: {
                        account: projeto.integracoes.activecampaign.name,
                        contact_id: email
                      }
                    }),
                    projeto_id: checkout.projeto_id
                  });
                }
                // MONTAR O OBJETO DOS EVENTOS
                let events = {};
                let custormer = {};
                let contacttag = {};
                // MONTAR VETOR DOS PRODUTOS
                let order_products = [];
                order_products = checkout.items.map((item, index) => {
                  return {
                    externalid: item.product_id ? item.product_id : item.id,
                    name: item.name,
                    price: parseInt(item.unit_price),
                    quantity: parseInt(item.quantity),
                    imageUrl: imagem_url[index]
                  }
                });
                console.log(order_products);
                // MONTAR OBJETO PARA ATUALIZAR TAG DO CONTACT
                contacttag = JSON.stringify({
                  contactTag: {
                    contact: contact_response,
                    tag: parseInt(projeto.integracoes.activecampaign.tags.carrinho_abandonado),
                  }
                });
                // CHAMADA PARA ATUALIZAR TAG DO CONTACT
                try {
                  (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/contactTags',
                    // OBJETO
                    contacttag,
                    // AUTENTICAÇÃO
                    {
                      headers: {
                        'Api-Token': projeto.integracoes.activecampaign.key
                      }
                    }
                  ));
                } catch(e) { console.log(e.response.data); }
                // CHAMADA PARA BUSCAR AS TAGS DO CONTACT
                let contact_tags = (await axios.get(projeto.integracoes.activecampaign.url + '/api/3/contacts/' + contact_response + '/contactTags',
                  // AUTENTICAÇÃO
                  {
                    headers: {
                      'Api-Token': projeto.integracoes.activecampaign.key
                    }
                  }
                )).data.contactTags;
                //
                if (contact_tags.length > 0) {
                  contact_tags.forEach(async item => {
                    if (item.tag == projeto.integracoes.activecampaign.tags.pedido_pago) {
                      // CHAMADA PARA REMOVER TAG DE 'pedido_criado' CASO EXISTA
                      try {
                        (await axios.delete(projeto.integracoes.activecampaign.url + '/api/3/contactTags/' + item.id,
                          // AUTENTICAÇÃO
                          {
                            headers: {
                              'Api-Token': projeto.integracoes.activecampaign.key
                            }
                          }
                        ));
                      } catch(e) { console.log(e.response.data); }
                    }
                  });
                }
                // MONTAR OBJETO DO CUSTOMER ASSOCIADO A ORDER
                custormer = JSON.stringify({
                  ecomCustomer: {
                    connectionid: parseInt(projeto.integracoes.activecampaign.connection.id),
                    externalid: parseInt(order.customer.id),
                    email: order.customer.email,
                    acceptsMarketing: "1",
                    totalRevenue: parseInt(order.customer.total_spent.replace('.', '')),
                  }
                });
                // CHAMADA PARA CRIAR CUSTOMER
                let customer_response;
                try {
                  // VERIFICAR SE JÁ EXISTE UM CUSTOMER ASSOCIADO A ESSE EMAIL
                  let customer_id = (await axios.get(projeto.integracoes.activecampaign.url + '/api/3/ecomCustomers?filters[email]=' + order.customer.email,
                    // AUTENTICAÇÃO
                    {
                      headers: {
                        'Api-Token': projeto.integracoes.activecampaign.key
                      }
                    }
                  )).data.ecomCustomers;
                  console.log(customer_id);
                  if (customer_id.length > 0) {
                    customer_id.forEach((item) => {
                      if (item.connection) customer_response = item.id;
                    }); 
                  }
                  else {
                    customer_response = (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/ecomCustomers',
                      // OBJETO
                      custormer,
                      // AUTENTICAÇÃO
                      {
                        headers: {
                          'Api-Token': projeto.integracoes.activecampaign.key
                        }
                      }
                    )).data.ecomCustomer.id;
                  }
                  console.log(customer_response);
                } catch(e) { console.log(e.response.data); }
                // MONTAR OBJETO DA ORDER
                events = JSON.stringify({
                  ecomOrder: {
                    externalcheckoutid: checkout.id,
                    abandonedDate: checkout.created_at,
                    source: "1",
                    email: order.customer.email,
                    orderProducts: order_products,
                    orderUrl: checkout.url,
                    externalCreatedDate: checkout.created_at,
                    externalUpdatedDate: order.updated_at,
                    totalPrice: parseInt(order.total.replace('.', '')),
                    currency: "BRL",//order.currency,
                    orderNumber: order.number,
                    connectionid: parseInt(projeto.integracoes.activecampaign.connection.id),
                    customerid: parseInt(customer_response)
                  }
                });
                // AVALIAR O ENVIO DE EVENTOS
                if (events != '{}') {
                  console.log(JSON.parse(events));
                  // ENVIAR EVENTOS
                  try {
                    let chamada = (await axios.post(projeto.integracoes.activecampaign.url + '/api/3/ecomOrders',
                      // OBJETO
                      events,
                      // AUTENTICAÇÃO
                      {
                        headers: {
                          'Api-Token': projeto.integracoes.activecampaign.key
                        }
                      }
                    )).data;
                    // CRIAR O LOG
                    await axios.post(process.env.API_URL + '/log_nuvemshop', {
                      tipo: 'success',
                      descricao: 'Evento (' + event_label + ') enviado para a ActiveCampaign com sucesso.',
                      origem: 'api',
                      extra: JSON.stringify({
                        nuvemshop: checkout,
                        activecampaign: {
                          account: projeto.integracoes.activecampaign.name,
                          contact_id: order.customer.email,
                        }
                      }),
                      projeto_id: checkout.projeto_id
                    });
                  }
                  // ERRO NO ENVIO DOS EVENTOS
                  catch(e) {
                    console.log(e.response.data);
                    // CRIAR O LOG
                    await axios.post(process.env.API_URL + '/log_nuvemshop', {
                      tipo: 'danger',
                      descricao: 'Erro ao enviar evento (' + event_label + ') para a ActiveCampaign.',
                      origem: 'api',
                      extra: JSON.stringify({
                        nuvemshop: checkout,
                        activecampaign: {
                          account: projeto.integracoes.activecampaign.name,
                          contact_id: order.customer.email,
                        }
                      }),
                      projeto_id: checkout.projeto_id
                    });
                  }
                }
                resolve();
              }));
              // POSSUI A TAG "whatsapp"?
              if (projeto.integracoes.nuvemshop.scripts.abandoned_cart.tags.includes('whatsapp')) chamadas.push(new Promise(async (resolve, reject) => {
                try {
                  // BUSCAR INFORMAÇÕES DA LOJA
                  let loja = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/store', {
                    headers: {
                      'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
                      'User-Agent': app.user_agent
                    }
                  })).data;
                  let event = {};
                  // MONTAR OBJETO - MANDAR MENSAGENS INICIALMEMTE PARA GRUPO "DEV - KAMPANA"

                  // event.telefone = lead.personal_phone;

                  // PARA MAIS DE UM PRODUTO ADICIONADO NO CARRINHO - ENVIAR IMAGEM DO 2° PRODUTO, CASO TENHA, PARA MELHOR VISUALIZAÇÃO NO WHATSAPP
                  if (imagem_produto.length > 1) {
                    event.mensagem = ' ';
                    event.imagem = imagem_produto[1];
                    // CHAMADA PARA PRODUÇÃO - ROTA /sendText | CHAMADA PARA TESTE - ROTA /sendTextDemo
                    let chamada = (await axios.post(process.env.WPP_BASE_URL + '/' + projeto.integracoes.whatsapp.sender_id + '/sendImageDemo',
                      // OBJETO
                      event,
                      // AUTENTICAÇÃO
                      {
                        headers: {
                          'Authorization': process.env.WPP_API_PREFIX + ' ' + process.env.WPP_API_KEY,
                          'Content-Type': 'application/json'
                        }
                      }
                    )).data;
                  }
                  // ENVIAR MENSAGEM COM A IMAGEM DO 1° PRODUTO + TEXTO
                  if (lead.name) {
                    event.mensagem = 'Finalize seu pedido na _' + projeto.nome + '_, *' + lead.name + '*! 🛒\n\n' +
                      'Vimos que você se interessou por ' + mensagem_produto + ', mas não finalizou sua compra. O que houve? 🤔\n\n' +
                      'Estes produtos são exclusivos e por isso temos uma quantidade bem pequena em estoque. Volte agora mesmo para o site e garanta suas peças!\n\n' + 
                      'Estamos te esperando!\n\n' + 
                      loja.url_with_protocol + '/checkout/v3/next/' + order.id + '/' + order.token + '\n\n';
                  } else {
                    event.mensagem = 'Finalize seu pedido na _' + projeto.nome + '_! 🛒\n\n' +
                      'Vimos que você se interessou por ' + mensagem_produto + ', mas não finalizou sua compra. O que houve? 🤔\n\n' +
                      'Estes produtos são exclusivos e por isso temos uma quantidade bem pequena em estoque. Volte agora mesmo para o site e garanta suas peças!\n\n' + 
                      'Estamos te esperando!\n\n' + 
                      loja.url_with_protocol + '/checkout/v3/next/' + order.id + '/' + order.token + '\n\n';
                  }
                  event.imagem = imagem_produto[0];
                  // CHAMADA PARA PRODUÇÃO - ROTA /sendText | CHAMADA PARA TESTE - ROTA /sendTextDemo
                  let chamada = (await axios.post(process.env.WPP_BASE_URL + '/' + projeto.integracoes.whatsapp.sender_id + '/sendImageDemo',
                    // OBJETO
                    event,
                    // AUTENTICAÇÃO
                    {
                      headers: {
                        'Authorization': process.env.WPP_API_PREFIX + ' ' + process.env.WPP_API_KEY,
                        'Content-Type': 'application/json'
                      }
                    }
                  )).data;
                  // CRIAR O LOG
                  await axios.post(process.env.API_URL + '/log_nuvemshop', {
                    tipo: 'success',
                    descricao: 'Evento (' + event_label + ') enviado para o Whatsapp com sucesso.',
                    origem: 'api',
                    extra: JSON.stringify({
                      nuvemshop: checkout,
                      whatsapp: {
                        sender_id: projeto.integracoes.whatsapp.sender_id,
                        whatsapp: lead.personal_phone,
                        event_type: event_label
                      }
                    }),
                    projeto_id: checkout.projeto_id
                  });
                }
                // ERRO NO ENVIO DA NOTIFICAÇÃO PAR O WHATSAPP
                catch(e) {
                  // CRIAR O LOG
                  await axios.post(process.env.API_URL + '/log_nuvemshop', {
                    tipo: 'danger',
                    descricao: 'Erro ao enviar evento (' + event_label + ') para o Whatsapp. O número ' + lead.personal_phone + ' não é Whatsapp.',
                    origem: 'api',
                    extra: JSON.stringify({
                      nuvemshop: checkout,
                      whatsapp: {
                        sender_id: projeto.integracoes.whatsapp.sender_id,
                        whatsapp: lead.personal_phone,
                        event_type: event_label
                      }
                    }),
                    projeto_id: checkout.projeto_id
                  });
                }
                resolve();
              }));
              // POSSUI A TAG "sms"?
              if (projeto.integracoes.nuvemshop.scripts.abandoned_cart.tags.includes('sms')) chamadas.push(new Promise(async (resolve, reject) => {
                try {
                  // BUSCAR INFORMAÇÕES DA LOJA
                  let loja = (await axios.get(process.env.NUVEMSHOP_BASE_URL.replace('{store_id}', projeto.integracoes.nuvemshop.user_id) + '/store', {
                    headers: {
                      'Authentication': projeto.integracoes.nuvemshop.token_type + ' ' + projeto.integracoes.nuvemshop.access_token,
                      'User-Agent': app.user_agent
                    }
                  })).data;
                  let event = {};
                  // ENVIAR MENSAGEM 
                  //event.numberPhone = order.customer.phone ? order.customer.phone : order.billing_phone;
                  event.numberPhone = '62981079144';
                  if (lead.name) {
                    event.message = 'Finalize seu pedido, ' + lead.name + '! Vimos que você se interessou por ' + mensagem_produto + ', mas não finalizou sua compra. O que houve?';
                      /*
                      'Estes produtos são exclusivos e por isso temos uma quantidade bem pequena em estoque. Volte agora mesmo para o site e garanta suas peças!\n\n' + 
                      'Estamos te esperando!\n\n' + 
                      loja.url_with_protocol + '/checkout/v3/next/' + order.id + '/' + order.token + '\n\n';
                      */
                  } else {
                    event.message = 'Finalize seu pedido! Vimos que você se interessou por ' + mensagem_produto + ', mas não finalizou sua compra. O que houve?';
                      /*
                      'Estes produtos são exclusivos e por isso temos uma quantidade bem pequena em estoque. Volte agora mesmo para o site e garanta suas peças!\n\n' + 
                      'Estamos te esperando!\n\n' + 
                      loja.url_with_protocol + '/checkout/v3/next/' + order.id + '/' + order.token + '\n\n';
                      */
                  }
                  event.flashSms = false;
                  // CHAMADA
                  let chamada = (await axios.post(process.env.N_BASE_URL + '/sms?napikey=' + process.env.N_API_KEY,
                    // OBJETO
                    event,
                    // AUTENTICAÇÃO
                    {
                      headers: {
                        '-': 'Authorization: Bearer ' + process.env.N_API_TOKEN,
                        'Content-Type': 'application/json'
                      }
                    }
                  )).data;
                  // CRIAR O LOG
                  await axios.post(process.env.API_URL + '/log_nuvemshop', {
                    tipo: 'success',
                    descricao: 'Evento (' + event_label + ') enviado por SMS com sucesso.',
                    origem: 'api',
                    extra: JSON.stringify({
                      nuvemshop: checkout,
                      sms: {
                        sms_number: lead.personal_phone,
                        event_type: event_label
                      }
                    }),
                    projeto_id: checkout.projeto_id
                  });
                }
                // ERRO NO ENVIO DA NOTIFICAÇÃO POR SMS
                catch(e) {
                  // CRIAR O LOG
                  await axios.post(process.env.API_URL + '/log_nuvemshop', {
                    tipo: 'danger',
                    descricao: 'Erro ao enviar evento (' + event_label + ') pelo Envio de SMS.',
                    origem: 'api',
                    extra: JSON.stringify({
                      nuvemshop: checkout,
                      sms: {
                        sms_number: lead.personal_phone,
                        event_type: event_label
                      }
                    }),
                    projeto_id: checkout.projeto_id
                  });
                }
                resolve();
              }));
            }
            // REALIZAR CHAMADAS
            Promise.all(chamadas).then(async () => {
              checkout.updated_at = moment().format();
              checkout.hub_status = 1;
              // ATUALIZAR CACHE - EVENTO ENVIADO
              await collection.findOneAndUpdate({id: checkout.id}, checkout, {upsert: true, setDefaultsOnInsert: true});
            });
          }
        }
      }
    });
  }
});