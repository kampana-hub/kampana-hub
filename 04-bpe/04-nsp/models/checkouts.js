const mongoose = require('mongoose');

const customerSchema = new mongoose.Schema({
  id: {
    type: Number
  },
  email: {
    type: String,
  },
  name: {
    type: String
  },
  phone: {
    type: String
  },
  city: {
    type: String
  },
  state: {
    type: String
  },
  country: {
    type: String
  },
});

const checkoutSchema = new mongoose.Schema({
  id: {
    type: Number,
    required: true,
    unique: true
  },
  status: {
    type: String,
    required: true
  },
  items: {
    type: Array,
    required: true
  },
  url: {
    type: String
  },
  customer: {
    type: customerSchema,
    required: true
  },
  //
  user_id: {
    type: Number,
    required: true
  },
  projeto_id: {
    type: Number,
    required: true
  },
  //
  hub_status: {
    type: Number,
    required: true,
    default: 0
  },
  //
  created_at: {
    type: Date,
    required: true,
    default: Date.now
  },
  updated_at: {
    type: Date
  }
});

module.exports = checkoutSchema;