const mongoose = require('mongoose');

//

const checkouts = mongoose.createConnection(
  'mongodb+srv://' + process.env.MDB_USER + ':' + process.env.MDB_PASS + '@' + process.env.MDB_CLUSTER + '/nuvemshop?retryWrites=true&w=majority',
  {
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true
  }
);

//

module.exports = {
  mdb_checkouts: checkouts
};